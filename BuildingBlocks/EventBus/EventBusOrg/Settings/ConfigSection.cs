﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PickupService.BuildingBlocks.EventBus.Settings
{
    public static class ConfigSection
    {
        public static string EventBusConfig = "EventBusConfig";

        public static string ConnectionStrings = "ConnectionStrings";

        public static string ServiceEndpointsConfig = "ServiceEndpointsConfig";

        public static string ServiceTokenExchange = "ServiceTokenExchange";

        public static string CacheConfig = "CacheConfig";

        public static string Authentication = "Authentication";
        public static string ApplicationConfig  = "ApplicationConfig";

    }
}
