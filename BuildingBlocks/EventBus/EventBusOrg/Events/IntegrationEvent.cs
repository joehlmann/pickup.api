﻿using System;
using Newtonsoft.Json;

namespace PickupService.BuildingBlocks.EventBus.Events
{
    public class IntegrationEvent
    {
        public IntegrationEvent()
        {
            Id = Guid.NewGuid();
            CreationDate = DateTime.UtcNow;
        }

        [JsonConstructor]
        public IntegrationEvent(Guid id, DateTime createDate)
        {
            Id = id;
            CreationDate = createDate;
        }


        [JsonProperty] 
        public Guid Id { get; private set; }

        [JsonProperty]
        public DateTime CreationDate { get; private set; }


        /// <summary>
        /// Correlation Info
        /// </summary>
        /// <param name="correlationId"></param>
        /// <param name="correlationName"></param>
        public void SetCorrelationDetails(string correlationId, string correlationName)
        {
            CorrelationId = correlationId;
            CorrelationName = correlationName;
        }

        public void SetAuthToken(string authToken)
        {
            AuthToken = authToken;
        }

        public string CorrelationId { get; private set; }
        public string CorrelationName { get; private set; }

        public string AuthToken { get; private set; }
    }
}

    
