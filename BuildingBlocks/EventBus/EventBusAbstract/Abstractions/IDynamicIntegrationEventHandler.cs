﻿using System.Threading.Tasks;

namespace EventBusAbstract.Abstractions
{
    public interface IDynamicIntegrationEventHandler
    {
        Task Handle(dynamic eventData);
    }
}
