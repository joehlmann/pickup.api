﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using Newtonsoft.Json;
using PickupService.BuildingBlocks.EventBus.Events;

namespace PickupService.BuildingBlocks.IntegrationEventLogEF
{
    public static class IntegrationEventLogContextSeed
    {

        public static async Task SeedAsync(IApplicationBuilder applicationBuilder)
        {
            var context = (IntegrationEventLogContext) applicationBuilder.ApplicationServices
                .GetService(typeof(IntegrationEventLogContext));


            using (context)
            {
                context.Database.Migrate();

                if (!context.IntegrationEventLogs.Any())
                {
                    await context.IntegrationEventLogs.AddRangeAsync(GetInitEventLogEntry());

                    await context.SaveChangesAsync();
                }
            }

        }

        private static IEnumerable<IntegrationEventLogEntry> GetInitEventLogEntry()
        {
            return new List<IntegrationEventLogEntry>()
            {
                new IntegrationEventLogEntry(new IntegrationEvent(),Guid.NewGuid() )

            };


        }

    }
}
