﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bex.Utilities.Helper
{
    public static class StringHelper
    {
        public static string RemoveSpecialCharacters(this string str)
        {
            StringBuilder sb = new StringBuilder();
            foreach (char c in str)
            {
                if ((c >= '0' && c <= '9') || (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') || c == '.' || c == '_')
                {
                    sb.Append(c);

                }
            }
            return sb.ToString();
        }

        public static string ToGuidStr(this string str)
        {
            Guid id;

            string result;
            if (!Guid.TryParse(str, out id))
            {
                int idInt;
                if (!int.TryParse(str, out idInt))
                {
                    return default;
                }

                return ((Guid.Empty).SeedId(idInt)).ToString();
            }

            return (Guid.Parse(str)).ToString();
        }

        public static T ToEnum<T>(this string value)
        {
            return (T)Enum.Parse(typeof(T), value, true);
        }
        public static bool IsInt(this string str)
        {
            int i = 0;
            bool result = int.TryParse(str, out i);
            return result;
        }

        public static int ToInt(this string str)
        {
            int id;

            string result;
            if (!int.TryParse(str, out id))
            {
                return default;

            }

            return id;
        }


        public static Guid ToGuid(this string str)
        {
            Guid id;

            string result;
            if (!Guid.TryParse(str, out id))
            {
                int idInt;
                if (!int.TryParse(str, out idInt))
                {
                    return default;
                }

                return (Guid.Empty).SeedId(idInt);
            }

            return Guid.Parse(str);
        }
        public static string IsGuid(this string str)
        {
            Guid id;

            string result;
            if (!Guid.TryParse(str, out id))
            {
                int idInt;
                if (!int.TryParse(str, out idInt))
                {
                    return str;
                }

                return (Guid.Empty).SeedId(idInt).ToString();
            }

            return str;
        }


        public static bool IsByteArray(this string str)
        {

            Span<byte> buffer = new Span<byte>(new byte[str.Length]);
            return Convert.TryFromBase64String(str, buffer, out int bytesParsed);

        }


        public static string NullIfEmpty(this string s)
        {
            return string.IsNullOrEmpty(s) ? null : s;
        }
        public static string NullIfSpace(this string s)
        {
            return string.IsNullOrWhiteSpace(s) ? null : s;
        }

        public static DateTime ToDate(this string probDate)
        {
            if (!string.IsNullOrWhiteSpace(probDate))
            {
                DateTime converted;
                if (DateTime.TryParse(probDate, out converted))
                {
                    return converted;
                }
            }
            return DateTime.MinValue;
        }

        public static bool IsDate(this string input)
        {
            DateTime dt;
            var result =  (DateTime.TryParse(input, out dt));

            return result;
        }
    }


}
