﻿using ConsignmentSvc.Infrastructure.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ConsignmentSvc.Infrastructure.EntityConfiguration.Configuration
{
    public class ApplicationConfigurationItemDbModelEntityTypeConfiguration : IEntityTypeConfiguration<ApplicationConfigurationItemDbModel>
    {
        public void Configure(EntityTypeBuilder<ApplicationConfigurationItemDbModel> builder)
        {
            builder.HasKey(e => e.Id)
                .ForSqlServerIsClustered(false);
            builder.ToTable("vwConfiguration");

          
            builder.Property(e => e.Id).HasColumnName("pkConfiguration").ValueGeneratedOnAdd();
            builder.Property(e => e.Title)
                .HasColumnName("fldOptionName")
                .HasMaxLength(200);

            builder.Property(e => e.Value)
                .HasColumnName("fldSetting")
                .HasColumnType("varchar(4000)")
                .HasMaxLength(4000);

        }
    }
}
