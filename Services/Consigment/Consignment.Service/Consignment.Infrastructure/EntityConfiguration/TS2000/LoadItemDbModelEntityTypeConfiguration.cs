﻿using ConsignmentSvc.Infrastructure.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ConsignmentSvc.Infrastructure.EntityConfiguration.TS2000
{
    public class LoadItemDbModelEntityTypeConfiguration : IEntityTypeConfiguration<LoadItemDbModel>
    {
        public void Configure(EntityTypeBuilder<LoadItemDbModel> builder)
        {
            builder.HasKey(e => e.Id)
                .ForSqlServerIsClustered(false);
            builder.ToTable("vwLoadItem");

            builder.HasOne(e => e.Consignment)
               .WithMany(e => e.Items)
               .HasForeignKey(c => c.ConsignmentForeignKey);

            builder.Property(e => e.Id).HasColumnName("pkInvoiceLoadItem").ValueGeneratedOnAdd();
            builder.Property(e => e.ItemReference)
                .HasColumnName("fldCustomerReference")
                .HasMaxLength(500);

            builder.Property(e => e.ConsignmentForeignKey)
                    .HasColumnName("fkInvoice");

           
        }
    }
}
