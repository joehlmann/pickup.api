﻿using ConsignmentSvc.Infrastructure.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ConsignmentSvc.Infrastructure.EntityConfiguration.TS2000
{
    public class LogisticUnitDbModelEntityTypeConfiguration : IEntityTypeConfiguration<LogisticUnitDbModel>
    {
        public void Configure(EntityTypeBuilder<LogisticUnitDbModel> builder)
        {
            builder.HasKey(e => e.Id)
                .ForSqlServerIsClustered(false);
            builder.ToTable("vwLogisticUnit");

            builder.HasOne(e => e.Consignment)
               .WithMany(e => e.LogisticUnits)
               .HasForeignKey(c => c.ConsignmentForeignKey);
            builder.HasOne(e => e.LoadItem)
                .WithMany(e => e.LogisticUnits)
                .HasForeignKey(c => c.LoadItemForeignKey);

            builder.Property(e => e.Id).HasColumnName("pkLogisticUnit").ValueGeneratedOnAdd();
            builder.Property(e => e.Barcode)
                .HasColumnName("fldBarcode")
                .HasMaxLength(50);

            builder.Property(e => e.ConsignmentForeignKey)
                    .HasColumnName("fkInvoice");
           
            builder.Property(e => e.LoadItemForeignKey)
                .HasColumnName("fkLoadItem");

            builder.Property(e => e.BarcodeType)
                .HasColumnName("fkBarcodeType");

        }
    }
}
