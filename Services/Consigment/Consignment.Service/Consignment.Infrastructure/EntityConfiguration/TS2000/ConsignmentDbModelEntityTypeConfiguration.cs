﻿using ConsignmentSvc.Infrastructure.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ConsignmentSvc.Infrastructure.EntityConfiguration.TS2000
{
    public class ConsignmentDbModelEntityTypeConfiguration : IEntityTypeConfiguration<ConsignmentDbModel>
    {
        public void Configure(EntityTypeBuilder<ConsignmentDbModel> builder)
        {
            builder.HasKey(e => e.Id)
                .ForSqlServerIsClustered(false);
            builder.ToTable("vwConsignment");

            builder.Property(e => e.Id).HasColumnName("pkInvoice").ValueGeneratedOnAdd();
            builder.Property(e => e.Number)
                .HasColumnName("fldInvoiceNumber")
                .HasMaxLength(50);

            builder.Property(e => e.ETADate)
                    .HasColumnName("fldETADate")
                    .HasColumnType("datetime");
            builder.Property(e => e.ReceiverName)
                .HasColumnName("fldReceiverName")
                .HasMaxLength(50);
            builder.Property(e => e.ReceiverAddress1)
                .HasColumnName("fldReceiverAddressLine1")
                .HasMaxLength(150);
            builder.Property(e => e.ReceiverAddress2)
                .HasColumnName("fldReceiverAddressLine2")
                .HasMaxLength(150);
            builder.Property(e => e.SenderName)
                .HasColumnName("fldSenderName")
                .HasMaxLength(50);
            builder.Property(e => e.HasDangerousGoods)
                .HasColumnName("fldHasDangerousGoods")
                .HasColumnType("int32")
                .HasConversion(
                    v => v ? 1 : 2,
                    v => v != 2);

            builder.Property(e => e.ReceiverSuburbId)
                .HasColumnName("fkReceiverSuburb")
                .HasColumnType("int32");

            builder.Property(e => e.SenderSuburbId)
                .HasColumnName("fkSenderSuburb")
                .HasColumnType("int32");
            builder.Property(e => e.TotalUnits)
                .HasColumnName("fldTotalUnits")
                .HasColumnType("float");
            builder.Ignore(e => e.DeliveryRunNumber);
        }
    }
}
