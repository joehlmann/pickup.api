﻿using AutoMapper;
using ConsignmentSvc.Domain.Enumerators;
using ConsignmentSvc.Domain.ValueObjects;
using ConsignmentSvc.Infrastructure.IntegrationModels.Branches;
using ConsignmentSvc.Infrastructure.IntegrationModels.Suburbs;

namespace ConsignmentSvc.Infrastructure.AutoMapper
{
    public class IntegrationModelsProfile : Profile
    {
        public IntegrationModelsProfile()
        {
            CreateMap<SuburbIntegrationModel, Suburb>()
                .ForCtorParam("id", opt => opt.MapFrom(src => src.Id))
                .ForCtorParam("name", opt => opt.MapFrom(src => src.Suburb))
                .ForCtorParam("state", opt => opt.MapFrom(src => src.State))
                .ForCtorParam("postcode", opt => opt.MapFrom(src => src.Postcode))
                .ForCtorParam("preferredBranches", opt => opt.MapFrom(src => src.PreferredBranches))
                .ForCtorParam("preferredSendingBranch", opt => opt.MapFrom(src => src.PreferredSendingBranch))
                .ForCtorParam("preferredReceivingBranch", opt => opt.MapFrom(src => src.PreferredReceivingBranch))
                .ForCtorParam("runs", opt => opt.MapFrom(src => src.Runs))
                .ForAllMembers(opt => opt.Ignore());

            CreateMap<SuburbPreferredBranchIntegrationModel, SuburbPreferredBranch>()
                .ForCtorParam("branchId", opt => opt.MapFrom(src => src.BranchId))
                .ForCtorParam("isOnforwardEnabled", opt => opt.MapFrom(src => src.IsOnforwardEnabled))
                .ForCtorParam("order", opt => opt.MapFrom(src => src.Order))
                .ForCtorParam("type", opt => opt.MapFrom(src => src.Type == 'S' ? PreferredBranchType.Receiver : src.Type == 'R' ? PreferredBranchType.Sender : PreferredBranchType.Unknown))
                .ForAllMembers(opt => opt.Ignore());

            CreateMap<SuburbRunIntegrationModel, SuburbRun>()
                .ForCtorParam("deliveryRunNumber", opt => opt.MapFrom(src => src.DeliveryRunNumber))
                .ForCtorParam("order", opt => opt.MapFrom(src => src.Order))
                .ForAllMembers(opt => opt.Ignore());

            CreateMap<BranchIntegrationModel, Branch>()
                .ForCtorParam("id", opt => opt.MapFrom(src => src.Id))
                .ForCtorParam("name", opt => opt.MapFrom(src => src.Branch))
                .ForCtorParam("abbreviation", opt => opt.MapFrom(src => src.Abbreviation))
                .ForAllMembers(opt => opt.Ignore());

        }
    }
}
