﻿namespace ConsignmentSvc.Infrastructure.IntegrationModels.Suburbs
{
    public class SuburbRunIntegrationModel
    {
        public int DeliveryRunNumber { get; set; }
        public int Order { get; set; }
    }
}
