﻿using System.Collections.Generic;

namespace ConsignmentSvc.Infrastructure.IntegrationModels.Suburbs
{
    public class SuburbsIntegrationModel
    {
        public List<SuburbIntegrationModel> Suburbs { get; set; }
    }
}
