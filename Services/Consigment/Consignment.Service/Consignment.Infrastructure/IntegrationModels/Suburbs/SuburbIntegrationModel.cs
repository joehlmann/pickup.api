﻿using System.Collections.Generic;

namespace ConsignmentSvc.Infrastructure.IntegrationModels.Suburbs
{
    public class SuburbIntegrationModel
    {
        public int Id { get; set; }
        public string Postcode { get; set; }
        public string Suburb { get; set; }
        public string State { get; set; }
        public List<SuburbPreferredBranchIntegrationModel> PreferredBranches { get; set; }
        public SuburbPreferredBranchIntegrationModel PreferredSendingBranch { get; set; }
        public SuburbPreferredBranchIntegrationModel PreferredReceivingBranch { get; set; }
        public List<SuburbRunIntegrationModel> Runs { get; set; }
    }
}
