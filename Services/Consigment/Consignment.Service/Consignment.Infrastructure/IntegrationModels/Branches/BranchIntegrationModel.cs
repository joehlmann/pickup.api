﻿namespace ConsignmentSvc.Infrastructure.IntegrationModels.Branches
{
    public class BranchIntegrationModel
    {
        public int Id { get; set; }
        public string Branch { get; set; }
        public string Abbreviation { get; set; }
    }

}
