﻿using System.Collections.Generic;

namespace ConsignmentSvc.Infrastructure.IntegrationModels.Branches
{
    public class BranchesIntegrationModel
    {
        public List<BranchIntegrationModel> Branches { get; set; }
    }
}
