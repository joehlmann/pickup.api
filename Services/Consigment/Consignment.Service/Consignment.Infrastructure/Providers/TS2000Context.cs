﻿using System.Threading;
using System.Threading.Tasks;
using ConsignmentSvc.Infrastructure.Models;
using ConsignmentSvc.Infrastructure.Providers.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace ConsignmentSvc.Infrastructure.Providers
{
    public class TS2000Context : DbContext, IUnitOfWork
    {
        public virtual DbSet<ConsignmentDbModel> Consignments { get; set; }
        public virtual DbSet<LoadItemDbModel> LoadItems { get; set; }
        public virtual DbSet<LogisticUnitDbModel> LogisticUnits { get; set; }

        public TS2000Context(DbContextOptions<TS2000Context> options)
            : base(options)
        {

        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("consignments");
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(TS2000Context).Assembly);
            //modelBuilder.ApplyConfiguration(new ConsignmentDbModelEntityTypeConfiguration());
            //modelBuilder.ApplyConfiguration(new LoadItemDbModelEntityTypeConfiguration());
            //modelBuilder.ApplyConfiguration(new LogisticUnitDbModelEntityTypeConfiguration());
        }

        public async Task<bool> SaveEntitiesAsync(CancellationToken cancellationToken = default)
        {
            // TODO: Check the errors collection and throw as we never want to save an invalid Entity to the database


            // Dispatch Domain Events collection. 
            // Choices:
            // A) Right BEFORE committing data (EF SaveChanges) into the DB will make a single transaction including  
            // side effects from the domain event handlers which are using the same DbContext with "InstancePerLifetimeScope" or "scoped" lifetime


            // After executing this line all the changes (from the Command Handler and Domain Event Handlers) 
            // performed through the DbContext will be committed
            var result = await base.SaveChangesAsync();

            // B) Right AFTER committing data (EF SaveChanges) into the DB will make multiple transactions. 
            // You will need to handle eventual consistency and compensatory actions in case of failures in any of the Handlers. 
            // await _mediator.DispatchDomainEventsAsync(this);

            return true;
        }
    }
}
