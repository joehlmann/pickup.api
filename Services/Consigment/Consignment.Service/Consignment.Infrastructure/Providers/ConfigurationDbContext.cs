﻿using ConsignmentSvc.Infrastructure.Models;
using Microsoft.EntityFrameworkCore;

namespace ConsignmentSvc.Infrastructure.Providers
{
    public class ConfigurationDbContext : DbContext
    {
        public ConfigurationDbContext(DbContextOptions<ConfigurationDbContext> options)
            : base(options)
        { }
        public virtual DbSet<ApplicationConfigurationItemDbModel> ApplicationConfigurationItem { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("consignments");
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(TS2000Context).Assembly);
            //modelBuilder.ApplyConfiguration(new ApplicationConfigurationItemDbModelEntityTypeConfiguration());
            
        }
    }
}
