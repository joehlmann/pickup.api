﻿namespace ConsignmentSvc.Infrastructure.Models
{
    public class ApplicationConfigurationItemDbModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Value { get; set; }
    }
}
