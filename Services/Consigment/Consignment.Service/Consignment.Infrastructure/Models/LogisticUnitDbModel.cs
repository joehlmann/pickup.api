﻿namespace ConsignmentSvc.Infrastructure.Models
{
    public class LogisticUnitDbModel
    {
        public int Id { get; set; }
        public string Barcode { get; set; }
        public int BarcodeType { get; set; }
        public int ConsignmentForeignKey { get; set; }
        public int? LoadItemForeignKey { get; set; }
        public ConsignmentDbModel Consignment { get; set; }
        public LoadItemDbModel LoadItem { get; set; }
    }
}
