﻿using System;
using System.Collections.Generic;

namespace ConsignmentSvc.Infrastructure.Models
{
    public class ConsignmentDbModel
    {
        public int Id { get; set; }
        public string Number { get; set; }
        public int SenderSuburbId { get; set; }
        public string SenderName { get; set; }
        public string ReceiverName { get; set; }
        public string ReceiverAddress1 { get; set; }
        public string ReceiverAddress2 { get; set; }
        public int ReceiverSuburbId { get; set; }
        public DateTime? ETADate { get; set; }
        public int DeliveryRunNumber { get; set; }
        public bool HasDangerousGoods { get; set; }
        public double TotalUnits { get; set; }
        public List<LoadItemDbModel> Items { get; set; }
        public List<LogisticUnitDbModel> LogisticUnits { get; set; }
    }
}
