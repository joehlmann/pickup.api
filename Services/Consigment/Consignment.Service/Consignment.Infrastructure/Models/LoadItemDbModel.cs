﻿using System.Collections.Generic;

namespace ConsignmentSvc.Infrastructure.Models
{
    public class LoadItemDbModel
    {
        public int Id { get; set; }
        public int ConsignmentForeignKey { get; set; }
        public string ItemReference { get; set; }
        public ConsignmentDbModel Consignment { get; set; }
        public List<LogisticUnitDbModel> LogisticUnits { get; set; }
    }
}
