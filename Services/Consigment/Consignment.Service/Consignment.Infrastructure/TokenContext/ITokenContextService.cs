﻿namespace ConsignmentSvc.Infrastructure.TokenContext
{
    public interface ITokenContextService
    {
        void SetAccessToken(string token);
        string GetAccessToken();
    }
}