﻿using System.Net.Http;
using ConsignmentSvc.Domain.Infrastructure;
using ConsignmentSvc.Infrastructure.CorrelationContext;
using ConsignmentSvc.Infrastructure.TokenContext;

namespace ConsignmentSvc.Infrastructure.HttpClients
{
    public class BranchHttpClient
    {
        public readonly HttpClient _client;
        private readonly ITokenContextService _tokenService;
        private readonly ICorrelationContextService _correlationContextService;

        public BranchHttpClient(HttpClient client, ITokenContextService tokenService, ICorrelationContextService correlationContextService)
        {
            _client = client;
            _tokenService = tokenService;
            _correlationContextService = correlationContextService;
        }
        public HttpRequestMessage CreateRequest(string uri, HttpMethod method)
        {
            var request = new HttpRequestMessage(method, uri);
            var accessToken = _tokenService.GetAccessToken();
            if (accessToken.IsPresent())
            {
                request.Properties.Add("access_token", accessToken);
            }

            var correlationId = _correlationContextService.GetCorrelationId();
            var correlationHeader = _correlationContextService.GetCorrelationHeader();

            if (correlationHeader.IsPresent())
            {
                request.Headers.Add(correlationHeader, correlationId);
            }

            return request;
        }
    }
}
