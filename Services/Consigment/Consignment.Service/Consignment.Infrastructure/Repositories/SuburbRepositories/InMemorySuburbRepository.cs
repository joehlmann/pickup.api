﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ConsignmentSvc.Domain.Enumerators;
using ConsignmentSvc.Domain.ValueObjects;
using ConsignmentSvc.Infrastructure.Repositories.Interfaces;

namespace ConsignmentSvc.Infrastructure.Repositories.SuburbRepositories
{
    public class InMemorySuburbRepository : ISuburbRepository
    {
        private List<Suburb> _suburbs;

        public InMemorySuburbRepository()
        {
            var pre = new List<SuburbPreferredBranch>();
            pre.Add(new SuburbPreferredBranch(1, true, 1, PreferredBranchType.Sender));
            pre.Add(new SuburbPreferredBranch(2, true, 1, PreferredBranchType.Receiver));
            var runs = new List<SuburbRun>()
            {
                new SuburbRun(22,1)
            };

            _suburbs = new List<Suburb>
            {
                new Suburb(2, "Albury", "NSW", "2640", pre,runs,pre.First(),pre.Last()),
                new Suburb(14352, "Wodonga", "VIC", "3690", pre,runs,pre.First(),pre.Last()),
                new Suburb(3, "Melbourne", "3000", "VIC", pre,runs,pre.First(),pre.Last()),
                new Suburb(8166, "Marrickville", "2204", "NSW", pre,runs,pre.First(),pre.Last()),
                new Suburb(32, "Wetherill Park", "2164", "NSW", pre,runs,pre.First(),pre.Last()),
                new Suburb(10709,"Port Kembla","NSW","2505",pre,runs,pre.First(),pre.Last()),
                new Suburb(201,"Alexandria","NSW","2015",pre,runs,pre.First(),pre.Last()),
                new Suburb(13117,"Truganina","VIC","3029",pre,runs,pre.First(),pre.Last()),
                new Suburb(19832,"Len Waters Estate","NSW","2171",pre,runs,pre.First(),pre.Last())
            };
        }

        public Task<Suburb> FindByIdAsync(int id)
        {
            return Task.FromResult(_suburbs.Find(x => x.Id == id));
        }

        public  Task<Suburb> FindByNameAndPostCodeAsync(string name, string postcode)
        {
            return Task.FromResult(_suburbs.Find(x =>
                x.Postcode.Equals(postcode, StringComparison.OrdinalIgnoreCase) &&
                x.Name.Equals(name, StringComparison.OrdinalIgnoreCase)));
        }
    }
}
