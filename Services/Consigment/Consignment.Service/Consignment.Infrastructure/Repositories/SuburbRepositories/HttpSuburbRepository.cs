﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using AutoMapper;
using ConsignmentSvc.Domain.ValueObjects;
using ConsignmentSvc.Infrastructure.HttpClients;
using ConsignmentSvc.Infrastructure.IntegrationModels.Suburbs;
using ConsignmentSvc.Infrastructure.Repositories.Interfaces;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Polly.CircuitBreaker;

namespace ConsignmentSvc.Infrastructure.Repositories.SuburbRepositories
{
    public class HttpSuburbRepository : ISuburbRepository
    {
        private readonly ILogger<HttpSuburbRepository> _logger;
        private readonly IMapper _mapper;
        private readonly SuburbHttpClient _http;
        public HttpSuburbRepository(ILogger<HttpSuburbRepository> logger, SuburbHttpClient http, IMapper mapper)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _http = http ?? throw new ArgumentNullException(nameof(http));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }
        public async Task<Suburb> FindByIdAsync(int id)
        {

            //TODO: Add caching support
            try
            {
                var request = _http.CreateRequest($"suburbs/{id.ToString()}", HttpMethod.Get);
                var result = await _http._client.SendAsync(request);
                if (result.IsSuccessStatusCode)
                {
                    var suburb = JsonConvert.DeserializeObject<SuburbIntegrationModel>(await result.Content.ReadAsStringAsync());

                    return _mapper.Map<Suburb>(suburb);
                }
                return new Suburb();
            }
            catch (BrokenCircuitException e)
            {
                HandleBrokenCircuitException(e);
                throw;
            }
        }

        public async Task<Suburb> FindByNameAndPostCodeAsync(string name, string postcode)
        {
            //TODO: Add caching support
            try
            {
                var s = GenerateSuburbsJsonQueryString(name, postcode);
                var request = _http.CreateRequest($"?filter={s}", HttpMethod.Get);
                var result = await _http._client.SendAsync(request);

                if (result.IsSuccessStatusCode)
                {
                    var suburb =
                        JsonConvert.DeserializeObject<SuburbsIntegrationModel>(
                            await result.Content.ReadAsStringAsync());

                    return _mapper.Map<Suburb>(suburb?.Suburbs?.FirstOrDefault());
                }

                return new Suburb();
            }
            catch (BrokenCircuitException e)
            {
                HandleBrokenCircuitException(e);
                throw;
            }
        }

        public string GenerateSuburbsJsonQueryString(string name, string postcode)
        {
            return GenerateSuburbsJsonQueryString(new List<string> { name }, new List<string> { postcode });
        }
        public string GenerateSuburbsJsonQueryString(List<string> suburbNames, List<string> suburbPostcode)
        {
            var filter = new JObject();

            if (suburbNames != null && suburbNames.Any())
            {
                filter.Add(new JProperty("suburbs", new JArray(suburbNames)));
            }


            if (suburbPostcode != null && suburbPostcode.Any())
            {
                filter.Add(new JProperty("postcodes", new JArray(suburbPostcode)));
            }

            return Uri.EscapeDataString(filter.ToString());
        }

        private void HandleBrokenCircuitException(BrokenCircuitException e)
        {
            _logger.LogError(e, $"{typeof(SuburbHttpClient)} Circuit breaker tripped");
        }
    }
}
