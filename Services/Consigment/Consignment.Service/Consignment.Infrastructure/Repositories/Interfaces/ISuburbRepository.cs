﻿using System.Threading.Tasks;
using ConsignmentSvc.Domain.ValueObjects;

namespace ConsignmentSvc.Infrastructure.Repositories.Interfaces
{
    public interface ISuburbRepository
    {
        Task<Suburb> FindByIdAsync(int id);
        Task<Suburb> FindByNameAndPostCodeAsync(string name, string postcode);
    }
}
