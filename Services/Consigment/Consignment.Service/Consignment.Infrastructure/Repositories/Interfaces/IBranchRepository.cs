﻿using System.Threading.Tasks;
using ConsignmentSvc.Domain.ValueObjects;

namespace ConsignmentSvc.Infrastructure.Repositories.Interfaces
{
    public interface IBranchRepository
    {
        Task<Branch> FindByIdAsync(int id);
        Task<Branch> FindByNameAsync(string name);
    }
}
