﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using AutoMapper;
using ConsignmentSvc.Domain.ValueObjects;
using ConsignmentSvc.Infrastructure.HttpClients;
using ConsignmentSvc.Infrastructure.IntegrationModels.Branches;
using ConsignmentSvc.Infrastructure.Repositories.Interfaces;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Polly.CircuitBreaker;

namespace ConsignmentSvc.Infrastructure.Repositories.BranchRepositories
{
    public class HttpBranchRepository : IBranchRepository
    {
        private readonly ILogger<HttpBranchRepository> _logger;
        private readonly IMapper _mapper;
        private readonly BranchHttpClient _http;

        public HttpBranchRepository(ILogger<HttpBranchRepository> logger, BranchHttpClient http, IMapper mapper)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _http = http ?? throw new ArgumentNullException(nameof(http));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }
        public async Task<Branch> FindByIdAsync(int id)
        {
            //TODO: Add caching support
            try
            {
                var request = _http.CreateRequest($"branches/{id.ToString()}", HttpMethod.Get);
                var result = await _http._client.SendAsync(request);
                if (result.IsSuccessStatusCode)
                {
                    var branch = JsonConvert.DeserializeObject<BranchIntegrationModel>(await result.Content.ReadAsStringAsync());

                    return _mapper.Map<Branch>(branch);
                }
                return new Branch();
            }
            catch (BrokenCircuitException e)
            {
                HandleBrokenCircuitException(e);
                throw;
            }
        }

        public async Task<Branch> FindByNameAsync(string name)
        {
            //TODO: Add caching support
            try
            {
                var s = GenerateBranchesJsonQueryString(name);
                var request = _http.CreateRequest($"?filter={s}", HttpMethod.Get);
                var result = await _http._client.SendAsync(request);

                if (result.IsSuccessStatusCode)
                {
                    var branch =
                        JsonConvert.DeserializeObject<BranchesIntegrationModel>(
                            await result.Content.ReadAsStringAsync());

                    return _mapper.Map<Branch>(branch?.Branches?.FirstOrDefault());
                }

                return new Branch();
            }
            catch (BrokenCircuitException e)
            {
                HandleBrokenCircuitException(e);
                throw;
            }
        }

        public string GenerateBranchesJsonQueryString(string name)
        {
            return GenerateBranchesJsonQueryString(new List<string> { name });
        }
        public string GenerateBranchesJsonQueryString(List<string> branchNames)
        {
            var filter = new JObject();

            if (branchNames != null && branchNames.Any())
            {
                filter.Add(new JProperty("branches", new JArray(branchNames)));
            }


            return Uri.EscapeDataString(filter.ToString());
        }

        private void HandleBrokenCircuitException(BrokenCircuitException e)
        {
            _logger.LogError(e, $"{typeof(BranchHttpClient)} Circuit breaker tripped");
        }
    }
}
