﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ConsignmentSvc.Domain.ValueObjects;
using ConsignmentSvc.Infrastructure.Repositories.Interfaces;

namespace ConsignmentSvc.Infrastructure.Repositories.BranchRepositories
{
    public class InMemoryBranchRepository : IBranchRepository
    {
        private List<Branch> _branches;

        public InMemoryBranchRepository()
        {
            _branches = new List<Branch>
            {
                new Branch(7, "Sydney", "SYD"),
                new Branch(19, "Wollongong", "WOL"),
                new Branch(2, "Melbourne", "MEL")
            };
        }
        public Task<Branch> FindByIdAsync(int id)
        {
            return Task.FromResult(_branches.Find(x => x.Id == id));
        }

        public Task<Branch> FindByNameAsync(string name)
        {
            return Task.FromResult(_branches.Find(x => x.Name.Equals(name, StringComparison.OrdinalIgnoreCase)));
        }
    }
}
