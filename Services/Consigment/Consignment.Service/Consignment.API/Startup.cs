﻿using System;
using System.Net.Http;
using System.Reflection;
using AutoMapper;
using ConsignmentSvc.API.Application.Factories;
using ConsignmentSvc.API.Application.Factories.Interfaces;
using ConsignmentSvc.API.Application.Requests.Labels;
using ConsignmentSvc.API.Application.Services.ShippingLabelPdfGenerator;
using ConsignmentSvc.API.Application.ViewModels;
using ConsignmentSvc.API.Infrastructure.Authentication;
using ConsignmentSvc.API.Infrastructure.Authentication.ServiceTokenExchange;
using ConsignmentSvc.API.Infrastructure.Authorization;
using ConsignmentSvc.API.Infrastructure.Correlation;
using ConsignmentSvc.API.Infrastructure.Logging;
using ConsignmentSvc.Infrastructure.CorrelationContext;
using ConsignmentSvc.Infrastructure.HttpClients;
using ConsignmentSvc.Infrastructure.Models;
using ConsignmentSvc.Infrastructure.Providers;
using ConsignmentSvc.Infrastructure.Repositories.BranchRepositories;
using ConsignmentSvc.Infrastructure.Repositories.Interfaces;
using ConsignmentSvc.Infrastructure.Repositories.SuburbRepositories;
using ConsignmentSvc.Infrastructure.TokenContext;
using CorrelationId;
using IdentityServer4.AccessTokenValidation;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Polly;
using Polly.Extensions.Http;
using Polly.Timeout;

namespace ConsignmentSvc.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IHostingEnvironment hostingEnvironment, IAssertions assertions)
        {
            _configuration = configuration;
            _hostingEnvironment = hostingEnvironment;
            _assertions = assertions;
        }

        public IConfiguration _configuration { get; }
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IAssertions _assertions;

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services
            .AddAutoMapper(typeof(ConsignmentLabelViewModel).GetTypeInfo().Assembly, typeof(ConsignmentDbModel).GetTypeInfo().Assembly)
            .AddCustomDbContext(_configuration, _hostingEnvironment)
            .AddHttpClients(_configuration, _hostingEnvironment)
            .AddMediatR(typeof(GenerateConsignmentLabelRequest).GetTypeInfo().Assembly)
            
            .AddCorrelationId();
            services.AddMemoryCache();
            services.AddAuthentication(IdentityServerAuthenticationDefaults.AuthenticationScheme)
                .AddIdentityServerAuthentication(
                    options =>
                    {
                        var config = _configuration.GetSection("Authentication");
                        var host = _hostingEnvironment;

                        options.Authority = config["Authority"];
                        options.ApiName = config["APIName"];
                        options.SupportedTokens = SupportedTokens.Jwt;
                        options.RoleClaimType = "role";
                        //if (host.EnvironmentName == "Debug")
                        //{
                        options.RequireHttpsMetadata = false;
                        //}

                    });
            //TODO: Add back in
            services.AddScoped<ISuburbRepository, HttpSuburbRepository>();
            services.AddScoped<IBranchRepository, HttpBranchRepository>();

            //services.AddScoped<ISuburbRepository, InMemorySuburbRepository>();
            //services.AddScoped<IBranchRepository, InMemoryBranchRepository>();
            
            services.AddSingleton<IBarcodeFactory, BarcodeFactory>();
            services.AddScoped<ITokenContextService, TokenContextService>();
            services.AddScoped<ICorrelationContextService, CorrelationContextService>();
            services.AddScoped<ILabelSizeDimensionFactory, LabelSizeDimensionFactory>();
            services.AddScoped<IShippingLabelPdfGenerator, SyncfusionShippingLabelPdfGenerator>();
            //TODO: Remove when token exchange service is implemented
            //services.AddTransient<HttpClientServiceTokenExchangeDelegatingHandler>();
            services.AddBExAuthorization(_assertions);
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            services.AddServiceTokenExchange();
            services.AddHttpContextAccessor();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            // Apply the sync fusion licence
            RegisterSyncfusion();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseAuthentication();
            app.UseCorrelationId("request-id");
            app.UseMiddleware<SerilogMiddleware>();
            app.UseMiddleware<AccessTokenContextMiddleware>();
            app.UseMiddleware<CorrelationContextMiddleware>();
            //app.UseHttpsRedirection();
            app.UseMvc();
        }

        private void RegisterSyncfusion()
        {
            var config = _configuration.GetSection("Application:Syncfusion");
            var key = config["LicenceKey"];

            if (!string.IsNullOrEmpty(key))
            {
                //Register Syncfusion license
                Syncfusion.Licensing.SyncfusionLicenseProvider.RegisterLicense(key);
            }
            

        }
    }
    static class CustomExtensionsMethods
    {
        /// <summary>
        /// Configure Entity frameworks db contexts
        /// </summary>
        /// <param name="services"></param>
        /// <param name="configuration"></param>
        /// <param name="hostingEnvironment"></param>
        /// <returns></returns>
        public static IServiceCollection AddCustomDbContext(this IServiceCollection services,
            IConfiguration configuration, IHostingEnvironment hostingEnvironment)
        {

            var connection = configuration.GetConnectionString("ConsignmentDb");

            services.AddDbContext<TS2000Context>(
                options => options.UseSqlServer(connection)
                    .ConfigureWarnings(x => x.Throw(RelationalEventId.QueryClientEvaluationWarning))
                    .EnableSensitiveDataLogging(hostingEnvironment.IsDevelopment()));

            //TODO: Investigate adding Sql Retry 
            //Configuring Connection Resiliency: https://docs.microsoft.com/en-us/ef/core/miscellaneous/connection-resiliency 
            //sqlOptions.EnableRetryOnFailure(maxRetryCount: 10, maxRetryDelay: TimeSpan.FromSeconds(30), errorNumbersToAdd: null);
            return services;
        }

        public static IServiceCollection AddHttpClients(this IServiceCollection services,
              IConfiguration configuration, IHostingEnvironment hostingEnvironment)
        {
            var userAgent = $"Consignment.API-{hostingEnvironment.EnvironmentName}";

            

            AddSuburbHttpService(services,configuration,userAgent);
            AddBranchHttpService(services, configuration, userAgent);

            return services;
        }

        private static void AddSuburbHttpService(IServiceCollection services, IConfiguration configuration, string userAgent)
        {
            var suburbServiceUri = configuration.GetConnectionString("SuburbHttpUri");

            if (!string.IsNullOrEmpty(suburbServiceUri))
            {
                var serviceTokenConfig = configuration.GetSection("ServiceTokenExchange");
                bool.TryParse(serviceTokenConfig["EnableCache"], out var enableTokenCache);


                // A specific SuburbHttpClient is required due to a limitation in the DI Scope, should be fixed in version 2.2
                // https://github.com/aspnet/HttpClientFactory/issues/134#issuecomment-412597323
                services.AddHttpClient<SuburbHttpClient, SuburbHttpClient>((client) =>
                    {


                        client.BaseAddress = new Uri(suburbServiceUri);
                        client.DefaultRequestHeaders.Add("Accept", "application/json");
                        client.DefaultRequestHeaders.Add("User-Agent", userAgent);
                        ;
                    })

                    .AddHttpMessageHandler<HttpClientServiceTokenExchangeDelegatingHandler>()
                    .AddPolicyHandler(GetRetryPolicy())
                    .AddPolicyHandler(GetCircuitBreakerPolicy())
                    .AddPolicyHandler(GetTimeoutPolicy());
            }
        }
        private static void AddBranchHttpService(IServiceCollection services, IConfiguration configuration, string userAgent)
        {
            var branchServiceUri = configuration.GetConnectionString("BranchHttpUri");

            if (!string.IsNullOrEmpty(branchServiceUri))
            {
                var serviceTokenConfig = configuration.GetSection("ServiceTokenExchange");
                bool.TryParse(serviceTokenConfig["EnableCache"], out var enableTokenCache);


                // A specific BranchHttpClient is required due to a limitation in the DI Scope, should be fixed in version 2.2
                // https://github.com/aspnet/HttpClientFactory/issues/134#issuecomment-412597323
                services.AddHttpClient<BranchHttpClient, BranchHttpClient>((client) =>
                    {


                        client.BaseAddress = new Uri(branchServiceUri);
                        client.DefaultRequestHeaders.Add("Accept", "application/json");
                        client.DefaultRequestHeaders.Add("User-Agent", userAgent);
                        ;
                    })

                    .AddHttpMessageHandler<HttpClientServiceTokenExchangeDelegatingHandler>()
                    .AddPolicyHandler(GetRetryPolicy())
                    .AddPolicyHandler(GetCircuitBreakerPolicy())
                    .AddPolicyHandler(GetTimeoutPolicy());
            }
        }
        /// <summary>
        /// Configure authorization for a border express service
        /// </summary>
        /// <param name="services"></param>
        /// <param name="assertions"></param>
        /// <returns></returns>
        public static IServiceCollection AddBExAuthorization(this IServiceCollection services,
            IAssertions assertions)
        {
            services.AddAuthorization(options =>
            {
                options.AddPolicy("ReadConsignments", builder => builder.RequireAssertion(context => assertions.CanReadConsignments(context)));
                options.AddPolicy("WriteConsignments", builder => builder.RequireAssertion(context => assertions.CanWriteConsignments(context)));
                options.AddPolicy("DeleteConsignments", builder => builder.RequireAssertion(context => assertions.CanDeleteConsignments(context)));
            });

            return services;
        }
        static IAsyncPolicy<HttpResponseMessage> GetRetryPolicy()
        {
            return HttpPolicyExtensions
                .HandleTransientHttpError()
                .Or<TimeoutRejectedException>() // TimeoutRejectedException from Polly's TimeoutPolicy
                .WaitAndRetryAsync(3, retryAttempt => TimeSpan.FromSeconds(Math.Pow(2, retryAttempt)));


        }
        static IAsyncPolicy<HttpResponseMessage> GetCircuitBreakerPolicy()
        {
            return HttpPolicyExtensions
                .HandleTransientHttpError()
                .CircuitBreakerAsync(5, TimeSpan.FromSeconds(30));
        }

        static IAsyncPolicy<HttpResponseMessage> GetTimeoutPolicy()
        {
            return Policy.TimeoutAsync<HttpResponseMessage>(2);
        }

    }
}
