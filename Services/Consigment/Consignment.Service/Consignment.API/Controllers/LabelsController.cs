﻿using System;
using System.Threading.Tasks;
using ConsignmentSvc.API.Application.Requests.Labels;
using ConsignmentSvc.Domain.Enumerators;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace ConsignmentSvc.API.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class LabelsController : ControllerBase
    {
        private readonly IMediator _mediator;

        public LabelsController(IMediator mediator)
        {
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
        }
        // GET labels/consignments/1234
        //[Authorize(Policy = "ReadConsignments")]
        [HttpGet("consignments/{consignmentNumber}")]
        public async Task<IActionResult> GetConsignmentLabel(string consignmentNumber, string size = "A5", int copies = 1)
        {
            LabelSize labelSize;
            if (Enum.IsDefined(typeof(LabelSize), size))
            {
                labelSize = Enum.Parse<LabelSize>(size);
            }
            else
            {
                // TODO: Make this a consistent error object
                return BadRequest("Invalid label size.");
            }
            var result = await _mediator.Send(new GenerateConsignmentLabelRequest
            {
                ConsignmentNumber = consignmentNumber,
                Size = labelSize,
                Copies = copies
            });

            //return Ok();
            if (result?.File?.Bytes != null && result?.File?.Bytes.Length > 0)
            {
                return File(result.File.Bytes, result.File.MimeType);
            }
            else
            {
                return NotFound();
            }

        }
        // GET labels/barcodes/5
        [HttpGet("barcodes/{barcode}")]
        public ActionResult<string> GetBarcodeLabel(string barcode, string size = "A5", int copies = 1)
        {
            return NotFound();
        }
    }
}
