﻿using System.IO;
using Microsoft.AspNetCore.StaticFiles;

namespace ConsignmentSvc.API.Application.ViewModels
{
    public class FileViewModel
    {
        private string fileName { get; set; }

        public string FileName {
            get {
                return fileName;
            }
            set {
                fileName = value;
                Extension = Path.GetExtension(value).Replace(".","");
                Name = Path.GetFileNameWithoutExtension(value);
                MimeType = DetermineMimeType(value);
            } }
        public byte[] Bytes { get; set; }
        public string Name { get; private set; }
        public string Extension { get; private set; }
        public string MimeType { get; private set; }

        private string DetermineMimeType(string fileName)
        {
            new FileExtensionContentTypeProvider().TryGetContentType(fileName, out var contentType);

            return contentType;
        }
    }
}
