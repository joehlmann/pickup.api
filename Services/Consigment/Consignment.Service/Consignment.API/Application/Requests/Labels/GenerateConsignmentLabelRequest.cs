﻿using ConsignmentSvc.API.Application.Responses.Labels;
using ConsignmentSvc.Domain.Enumerators;
using MediatR;

namespace ConsignmentSvc.API.Application.Requests.Labels
{
    public class GenerateConsignmentLabelRequest : IRequest<GenerateConsignmentLabelResponse>
    {
        public string ConsignmentNumber { get; set; }
        public LabelSize Size { get; set; }
        public int Copies { get; set; }
    }
}
