﻿using System;
using ConsignmentSvc.API.Application.Factories.Interfaces;
using ConsignmentSvc.API.Application.Models.LabelSize.LabelSizeDimensions;
using ConsignmentSvc.Domain.Enumerators;

namespace ConsignmentSvc.API.Application.Factories
{
    public class LabelSizeDimensionFactory : ILabelSizeDimensionFactory
    {
        public LabelSizeDimension Create(LabelSize labelSize)
        {
            switch (labelSize)
            {
                case LabelSize.A5: return new A5LabelSizeDimension();
                case LabelSize.A6: return new A6LabelSizeDimension();
                default: throw new ArgumentException("The label size specified has not been configured");
            }

            
        }
    }
}
