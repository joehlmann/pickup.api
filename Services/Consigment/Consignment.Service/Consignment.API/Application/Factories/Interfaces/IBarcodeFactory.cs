﻿using ConsignmentSvc.Domain.ValueObjects.Barcodes;

namespace ConsignmentSvc.API.Application.Factories.Interfaces
{
    public interface IBarcodeFactory
    {
        Barcode Create(int barcodeType, string barcodeNumber);
    }
}