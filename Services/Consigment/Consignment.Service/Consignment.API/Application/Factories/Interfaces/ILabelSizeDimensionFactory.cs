﻿using ConsignmentSvc.API.Application.Models.LabelSize.LabelSizeDimensions;
using ConsignmentSvc.Domain.Enumerators;

namespace ConsignmentSvc.API.Application.Factories.Interfaces
{
    public interface ILabelSizeDimensionFactory
    {
        LabelSizeDimension Create(LabelSize labelSize);
    }
}
