﻿using System;
using ConsignmentSvc.API.Application.Factories.Interfaces;
using ConsignmentSvc.Domain.Enumerators;
using ConsignmentSvc.Domain.ValueObjects.Barcodes;
using Microsoft.Extensions.Configuration;

namespace ConsignmentSvc.API.Application.Factories
{
    public class BarcodeFactory : IBarcodeFactory
    {
        public BarcodeFactory(IConfiguration configuration)
        {
            _gs1CompanyCode = configuration["GS1CompanyCode"];

            if (String.IsNullOrWhiteSpace(_gs1CompanyCode))
            {
                // Default border express code
                _gs1CompanyCode = "9347780";
            }
            
        }
        private readonly string _gs1CompanyCode;

        public Barcode Create(int barcodeType, string barcodeNumber)
        {
            var barcodeTypeEnum = (BarcodeType) barcodeType;

            switch (barcodeTypeEnum)
            {
                case BarcodeType.Customer:
                    return new CustomerBarcode(barcodeNumber,BarcodeFormat.Code128);
                case BarcodeType.BEXSSCC:
                    return new BExSSCCBarcode(barcodeNumber,BarcodeFormat.GS1);
                case BarcodeType.Consolidated:
                    return new ConsolidatedBarcode(barcodeNumber, BarcodeFormat.GS1);
                case BarcodeType.ExistingCN:
                    return new ExistingConsignmentBarcode(barcodeNumber,BarcodeFormat.GS1, _gs1CompanyCode);
                default:
                    return new OtherBarcode(barcodeNumber, BarcodeFormat.GS1);
            }
        }
    }
}
