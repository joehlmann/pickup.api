﻿using ConsignmentSvc.API.Application.ViewModels;

namespace ConsignmentSvc.API.Application.Responses.Labels
{
    public class GenerateConsignmentLabelResponse
    {
        public FileViewModel File { get; set; }
    }
}
