﻿using System;

namespace ConsignmentSvc.API.Application.Dtos
{
    public class ShipmentLabelDtoModel
    {
        public string BranchAbbreviation { get; set; }
        public int DeliveryNumber { get; set; }
        public string SenderName { get; set; }
        public string SenderSuburb { get; set; }
        public int SenderSuburbId { get; set; }
        public string SenderState { get; set; }
        public string SenderPostcode { get; set; }

        public string ReceiverName { get; set; }
        
        public string ReceiverAddress1 { get; set; }
        public string ReceiverAddress2 { get; set; }
        public int ReceiverSuburbId { get; set; }
        public string ReceiverSuburb { get; set; }
        public string ReceiverState { get; set; }
        public string ReceiverPostcode { get; set; }
        public string ItemReference { get; set; }
        public bool HasItemReference => !String.IsNullOrEmpty(ItemReference);
        public string ConsignmentNumber { get; set; }
        public string ConsignmentBarcode { get; set; }
        public string ConsignmentBarcodeText { get; set; }
        public int LogisticUnitId { get; set; }
        public int LogisticUnitBarcodeType { get; set; }
        public string LogisticUnitBarcode { get; set; }
        public string LogisticUnitBarcodeText { get; set; }
        public string LogisticUnitBarcodeFormat { get; set; }
        public int LabelCounter { get; set; }
        public int LabelTotal { get; set; }
        public DateTime? EtaDate { get; set; }
        public bool HasDangerousGoods { get; set; }
        public string ConsignmentBarcodeFormat { get; set; }

        public string Carrier { get; set; }
        public string DGText { get; set; }
    }
}
