﻿namespace ConsignmentSvc.API.Application.Models.LabelSize.LabelSizeDimensions
{
    public class A6LabelSizeDimension : LabelSizeDimension
    {
        public override string Name => "A6x1";
        public override LabelPdfMargins Margins => new LabelPdfMargins() { Bottom = 8, Left = 8, Right = 8, Top = 5 };

        public override float PageWidth => 277;
        public override float PageHeight => 405;
        public override float GraphicXOffset => 1;
        public override float GraphicYOffset => 1;
        public override float DetailsLeftPadding => 9;
        public override float BorderOffset => 1;
        public override float BranchAbbTextHeight => 24;
        public override float BranchAbbTextWidth => 148;
        public override float BranchAbbTextXOffset => 0;
        public override float BranchAbbTextYOffset => 0;

        public override float BranchAbbHeight => 24;
        public override float BranchAbbWidth => 172;
        public override float BranchAbbXOffset => 0;

        public override float BranchAbbNoDeliveryNumberTextWidth => 172;
        public override float BranchDeliveryNumberHeight => 24;
        public override float BranchDeliveryNumberWidth => 25;
        public override float BranchDeliveryNumberXOffset => (BranchAbbXOffset + BranchAbbNoDeliveryNumberTextWidth) - BranchDeliveryNumberWidth;
        public override float BranchDeliveryNumberYOffset => BranchAbbTextYOffset;

        public override float DGWidth => 74;
        public override float DGHeight => 18;
        public override float HorizontalLineBreakYOffset => 165;
        public override float HorizontalLineBreakHeight => 8;
        public override float CarrierYOffset => GraphicYOffset;
        public override float CarrierHeight => 23;
        public override float CarrierWidth => 102;
        public override float ReceiverToWidth => 72;
        public override float ReceiverToHeight => 10;
        public override float ReceiverToXOffset => GraphicXOffset + BorderOffset + DetailsLeftPadding;
        public override float ReceiverToYOffset => GraphicYOffset + 46;
        public override float ReceiverNameXOffset => GraphicXOffset + BorderOffset + DetailsLeftPadding;
        public override float ReceiverNameYOffset => ReceiverToYOffset + ReceiverToHeight;
        public override float ReceiverNameHeight => 15;
        public override float ReceiverNameWidth => 216;
        public override float ReceiverAddress1XOffset => GraphicXOffset + BorderOffset + DetailsLeftPadding;
        public override float ReceiverAddress1YOffset => ReceiverNameYOffset + ReceiverNameHeight;
        public override float ReceiverAddress1Height => 11;
        public override float ReceiverAddress1Width => 216;
        public override float ReceiverAddress2XOffset => GraphicXOffset + BorderOffset + DetailsLeftPadding;
        public override float ReceiverAddress2YOffset => ReceiverAddress1YOffset + ReceiverAddress1Height;
        public override float ReceiverAddress2Height => 11;
        public override float ReceiverAddress2Width => 216;
        public override float ReceiverSuburbXOffset => GraphicXOffset + BorderOffset + DetailsLeftPadding;
        public override float ReceiverSuburbYOffset => ReceiverAddress2YOffset + ReceiverAddress2Height;
        public override float ReceiverSuburbHeight => 20;
        public override float ReceiverSuburbWidth => 216;

        public override float ItemReferenceLabelHeight => 10;
        public override float ItemReferenceLabelWidth => 35;
        public override float ItemReferenceLabelXOffset => GraphicXOffset + BorderOffset + DetailsLeftPadding;
        public override float ItemReferenceLabelYOffset => 115;
        public override float ItemReferenceHeight => 10;
        public override float ItemReferenceWidth => 300;

        public override float ItemReferenceXOffset => ItemReferenceLabelXOffset + ItemReferenceLabelWidth + 2;
        public override float ItemReferenceYOffset => ItemReferenceLabelYOffset;

        public override float ConsignmentLabelXOffset => GraphicXOffset + BorderOffset + DetailsLeftPadding;
        public override float ConsignmentLabelYOffset => GraphicYOffset + 132;
        public override float ConsignmentLabelHeight => 11;
        public override float ConsignmentLabelWidth => 144;
        public override float ConsignmentNumberXOffset => GraphicXOffset + BorderOffset + DetailsLeftPadding;
        public override float ConsignmentNumberYOffset => ConsignmentLabelYOffset + ConsignmentLabelHeight;
        public override float ConsignmentNumberHeight => 20;
        public override float ConsignmentNumberWidth => 144;
        public override float LabelCountYOffset => GraphicYOffset + 174;
        public override float LabelCountHeight => 13;
        public override float LabelCountWidth => 93;

        public override float ConsignmentBarcodeYOffset => GraphicYOffset + 192;
        public override float ConsignmentBarcodeXOffset => (PageWidth / 2) - (ConsignmentBarcodeWidth / 2);
        public override float ConsignmentBarcodeHeight => 82;
        public override float ConsignmentBarcodeWidth => 246;
        public override float ConsignmentBarcodeLabelXOffset => GraphicXOffset;
        public override float ConsignmentBarcodeLabelYOffset => ConsignmentBarcodeYOffset + ConsignmentBarcodeHeight;
        public override float ConsignmentBarcodeLabelHeight => 20;

        public override float LogisticUnitBarcodeYOffset => GraphicYOffset + 305;
        public override float LogisticUnitBarcodeHeight => 82;
        public override float LogisticUnitBarcodeWidth => 246;
        public override float LogisticUnitBarcodeXOffset => (PageWidth / 2) - (LogisticUnitBarcodeWidth / 2);

        public override float LogisticUnitBarcodeLabelHeight => 15;
        public override float LogisticUnitBarcodeLabelXOffset => GraphicXOffset;
        public override float LogisticUnitBarcodeLabelYOffset => LogisticUnitBarcodeYOffset + LogisticUnitBarcodeHeight;
        public override float ConsignmentETAYOffset => GraphicYOffset + 132;
        public override float ConsignmentETAHeight => 13;
        public override float ConsignmentETAWidth => 72;
        public override float SenderNameXOffset => SenderNameLabelXOffset + SenderNameLabelWidth + 2;
        public override float SenderNameYOffset => GraphicYOffset + 25;
        public override float SenderNameHeight => 9;
        public override float SenderNameWidth => 122;
        public override float SenderNameLabelXOffset => GraphicXOffset + BorderOffset + DetailsLeftPadding;
        public override float SenderNameLabelYOffset => SenderNameYOffset;
        public override float SenderNameLabelHeight => 9;
        public override float SenderNameLabelWidth => 31;
        public override float SenderSuburbXOffset => GraphicXOffset + BorderOffset + DetailsLeftPadding;
        public override float SenderSuburbYOffset => SenderNameYOffset + SenderNameHeight + 1;
        public override float SenderSuburbHeight => 9;
        public override float SenderSuburbWidth => 122;

        public override float LabelHeaderFontSize => 8;
        public override float CarrierFontSize => 8;
        public override float ReceiverAddressFontSize => 8;
        public override float SenderHeaderFontSize => 7;
        public override float DGFontSize => 7;
        public override float SenderDetailFontSize => 7;
        public override float ConsignmentNumberFontSize => 15;
        public override float ReceiverSuburbFontSize => 15;
        public override float ReceiverNameFontSize => 12;
        public override float LabelCountFontSize => 11;
        public override float BranchAbbreviationFontSize => 19;
        public override float BarcodeTextFontSize => 10;

        public override float ItemReferenceFontSize => 8;
    }
}
