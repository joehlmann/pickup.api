﻿namespace ConsignmentSvc.API.Application.Models.LabelSize.LabelSizeDimensions
{
    public abstract class LabelSizeDimension
    {
        public abstract string Name { get; }
        
        public abstract LabelPdfMargins Margins { get; }
        public abstract float PageWidth { get; }
        public abstract float PageHeight { get; }

        public abstract float GraphicXOffset { get; }
        public abstract float GraphicYOffset { get; }
        public abstract float DetailsLeftPadding { get; }
        public abstract float BorderOffset { get; }

        // Branch Abbrv
        public abstract float BranchAbbHeight { get; }
        public abstract float BranchAbbWidth { get; }
        public abstract float BranchAbbXOffset { get; }
        public abstract float BranchAbbTextHeight { get; }
        public abstract float BranchAbbTextWidth { get; }
        public abstract float BranchAbbTextXOffset { get; }
        public abstract float BranchAbbTextYOffset { get; }
        public abstract float BranchAbbNoDeliveryNumberTextWidth { get; }
        

        // Delivery No
        public abstract float BranchDeliveryNumberHeight { get; }
        public abstract float BranchDeliveryNumberWidth { get; }
        public abstract float BranchDeliveryNumberXOffset { get; }
        public abstract float BranchDeliveryNumberYOffset { get; }

        // DG
        public abstract float DGWidth { get; }
        public abstract float DGHeight { get; }
        public abstract float HorizontalLineBreakYOffset { get; }
        public abstract float HorizontalLineBreakHeight { get; }

        // Carrier Information
        public abstract float CarrierYOffset { get; }
        public abstract float CarrierHeight { get; }
        public abstract float CarrierWidth { get; }

        // Receiver Details
        public abstract float ReceiverToWidth { get; }
        public abstract float ReceiverToHeight { get; }
        public abstract float ReceiverToXOffset { get; }
        public abstract float ReceiverToYOffset { get; }
        public abstract float ReceiverNameXOffset { get; }
        public abstract float ReceiverNameYOffset { get; }
        public abstract float ReceiverNameHeight { get; }
        public abstract float ReceiverNameWidth { get; }
        public abstract float ReceiverAddress1XOffset { get; }
        public abstract float ReceiverAddress1YOffset { get; }
        
        public abstract float ReceiverAddress1Height { get; }
        public abstract float ReceiverAddress1Width { get; }
        public abstract float ReceiverAddress2XOffset { get; }
        public abstract float ReceiverAddress2YOffset { get; }
        public abstract float ReceiverAddress2Height { get; }
        public abstract float ReceiverAddress2Width { get; }
        public abstract float ReceiverSuburbXOffset { get; }
        public abstract float ReceiverSuburbYOffset { get; }
        public abstract float ReceiverSuburbHeight { get; }
        public abstract float ReceiverSuburbWidth { get; }

        // Customer Reference
        public abstract float ItemReferenceLabelHeight { get; }
        public abstract float ItemReferenceLabelWidth { get; }
        public abstract float ItemReferenceLabelXOffset { get; }
        public abstract float ItemReferenceLabelYOffset { get; }
        public abstract float ItemReferenceHeight { get; }
        public abstract float ItemReferenceWidth { get; }
        public abstract float ItemReferenceXOffset { get; }
        public abstract float ItemReferenceYOffset { get; }

        // Consignment Label
        public abstract float ConsignmentLabelXOffset { get; }
        public abstract float ConsignmentLabelYOffset { get; }
        public abstract float ConsignmentLabelHeight { get; }
        public abstract float ConsignmentLabelWidth { get; }
        public abstract float ConsignmentNumberXOffset { get; }
        public abstract float ConsignmentNumberYOffset { get; }
        public abstract float ConsignmentNumberHeight { get; }
        public abstract float ConsignmentNumberWidth { get; }

        // Label Count 
        public abstract float LabelCountYOffset { get; }
        public abstract float LabelCountHeight { get; }
        public abstract float LabelCountWidth { get; }

        // Consignment Barcode Image
        public abstract float ConsignmentBarcodeXOffset { get; }
        public abstract float ConsignmentBarcodeYOffset { get; }
        public abstract float ConsignmentBarcodeHeight { get; }
        public abstract float ConsignmentBarcodeWidth { get; }

        // Consignment Barcode Text
        public abstract float ConsignmentBarcodeLabelXOffset { get; }
        public abstract float ConsignmentBarcodeLabelYOffset { get; }
        public abstract float ConsignmentBarcodeLabelHeight { get; }

        // Logistic Unit Barcode
        public abstract float LogisticUnitBarcodeXOffset { get; }
        public abstract float LogisticUnitBarcodeYOffset { get; }
        public abstract float LogisticUnitBarcodeHeight { get; }
        public abstract float LogisticUnitBarcodeWidth { get; }

        // Logistic Unit Barcode Text
        public abstract float LogisticUnitBarcodeLabelHeight { get; }
        public abstract float LogisticUnitBarcodeLabelXOffset { get; }
        public abstract float LogisticUnitBarcodeLabelYOffset { get; }

        // Consignment ETA Date
        public abstract float ConsignmentETAYOffset { get; }
        public abstract float ConsignmentETAHeight { get; }
        public abstract float ConsignmentETAWidth { get; }

        // Sender Details
        public abstract float SenderNameXOffset { get; }
        public abstract float SenderNameYOffset { get; }
        public abstract float SenderNameHeight { get; }
        public abstract float SenderNameWidth { get; }
        public abstract float SenderNameLabelXOffset { get; }
        public abstract float SenderNameLabelYOffset { get; }
        public abstract float SenderNameLabelHeight { get; }
        public abstract float SenderNameLabelWidth { get; }
        public abstract float SenderSuburbXOffset { get; }
        public abstract float SenderSuburbYOffset { get; }
        public abstract float SenderSuburbHeight { get; }
        public abstract float SenderSuburbWidth { get; }

        // Font sizes
        public abstract float LabelHeaderFontSize { get; }
        public abstract float CarrierFontSize { get; }
        public abstract float ReceiverAddressFontSize { get; }
        public abstract float SenderHeaderFontSize { get; }
        public abstract float DGFontSize { get; }
        public abstract float SenderDetailFontSize { get; }
        public abstract float ConsignmentNumberFontSize { get; }
        public abstract float ReceiverSuburbFontSize { get; }
        public abstract float ReceiverNameFontSize { get; }
        public abstract float LabelCountFontSize { get; }
        public abstract float BranchAbbreviationFontSize { get; }
        public abstract float BarcodeTextFontSize { get; }
        public abstract float ItemReferenceFontSize { get; }

    }
}
