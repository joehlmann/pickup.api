﻿
namespace ConsignmentSvc.API.Application.Models.LabelSize.LabelSizeDimensions
{
    public class A5LabelSizeDimension : LabelSizeDimension
    {
        public override string Name => "A5x1";
        public override LabelPdfMargins Margins => new LabelPdfMargins(){Bottom = 19,Left=16,Right = 16,Top = 16};
        
        public override float PageWidth => 386;
        public override float PageHeight => 550;
        public override float GraphicXOffset => 1;
        public override float GraphicYOffset => 1;
        public override float DetailsLeftPadding => 8;
        public override float BorderOffset => 1;
        public override float BranchAbbTextHeight => 24;
        public override float BranchAbbTextWidth => 150;
        public override float BranchAbbTextXOffset => 0;
        public override float BranchAbbTextYOffset => 0;

        public override float BranchAbbHeight => 24;
        public override float BranchAbbWidth => 170;
        public override float BranchAbbXOffset => 0;

        public override float BranchAbbNoDeliveryNumberTextWidth => 170;
        public override float BranchDeliveryNumberHeight => 24;
        public override float BranchDeliveryNumberWidth => 35;
        public override float BranchDeliveryNumberXOffset => (BranchAbbXOffset + BranchAbbNoDeliveryNumberTextWidth) - BranchDeliveryNumberWidth;
        public override float BranchDeliveryNumberYOffset => BranchAbbTextYOffset;

        public override float DGWidth => 75;
        public override float DGHeight => 23;
        public override float HorizontalLineBreakYOffset => 189;
        public override float HorizontalLineBreakHeight => 6;
        public override float CarrierYOffset => GraphicYOffset;
        public override float CarrierHeight => 24;
        public override float CarrierWidth => 99;
        public override float ReceiverToWidth => 100;
        public override float ReceiverToHeight => 10;
        public override float ReceiverToXOffset => GraphicXOffset + BorderOffset + DetailsLeftPadding;
        public override float ReceiverToYOffset => GraphicYOffset + 55;
        public override float ReceiverNameXOffset => GraphicXOffset + BorderOffset + DetailsLeftPadding;
        public override float ReceiverNameYOffset => ReceiverToYOffset + ReceiverToHeight;
        public override float ReceiverNameHeight => 18;
        public override float ReceiverNameWidth => 300;
        public override float ReceiverAddress1XOffset => GraphicXOffset + BorderOffset + DetailsLeftPadding;
        public override float ReceiverAddress1YOffset => ReceiverNameYOffset + ReceiverNameHeight;
        public override float ReceiverAddress1Height => 10;
        public override float ReceiverAddress1Width => 300;
        public override float ReceiverAddress2XOffset => GraphicXOffset + BorderOffset + DetailsLeftPadding;
        public override float ReceiverAddress2YOffset => ReceiverAddress1YOffset + ReceiverAddress1Height;
        public override float ReceiverAddress2Height => 10;
        public override float ReceiverAddress2Width => 300;
        public override float ReceiverSuburbXOffset => GraphicXOffset + BorderOffset + DetailsLeftPadding;
        public override float ReceiverSuburbYOffset => ReceiverAddress2YOffset + ReceiverAddress2Height;
        public override float ReceiverSuburbHeight => 20;
        public override float ReceiverSuburbWidth => 300;

        public override float ItemReferenceLabelHeight => 15;
        public override float ItemReferenceLabelWidth => 35;
        public override float ItemReferenceLabelXOffset => GraphicXOffset + BorderOffset + DetailsLeftPadding;
        public override float ItemReferenceLabelYOffset => 122;
        public override float ItemReferenceHeight => 15;
        public override float ItemReferenceWidth => 345;

        public override float ItemReferenceXOffset => ItemReferenceLabelXOffset + ItemReferenceLabelWidth + 2;
        public override float ItemReferenceYOffset => ItemReferenceLabelYOffset;

        public override float ConsignmentLabelXOffset => GraphicXOffset + BorderOffset + DetailsLeftPadding;
        public override float ConsignmentLabelYOffset => GraphicYOffset + 143;
        public override float ConsignmentLabelHeight => 12;
        public override float ConsignmentLabelWidth => 200;
        public override float ConsignmentNumberXOffset => GraphicXOffset + BorderOffset + DetailsLeftPadding;
        public override float ConsignmentNumberYOffset => ConsignmentLabelYOffset + ConsignmentLabelHeight;
        public override float ConsignmentNumberHeight => 20;
        public override float ConsignmentNumberWidth => 200;
        public override float LabelCountYOffset => GraphicYOffset + 200;
        public override float LabelCountHeight => 18;
        public override float LabelCountWidth => 130;
        
        public override float ConsignmentBarcodeYOffset => GraphicYOffset + 230;
        public override float ConsignmentBarcodeXOffset => (PageWidth / 2) - (ConsignmentBarcodeWidth / 2);
        public override float ConsignmentBarcodeHeight => 114;
        public override float ConsignmentBarcodeWidth => 324;
        public override float ConsignmentBarcodeLabelXOffset => GraphicXOffset;
        public override float ConsignmentBarcodeLabelYOffset => ConsignmentBarcodeYOffset + ConsignmentBarcodeHeight + 5;
        public override float ConsignmentBarcodeLabelHeight => 20;
        
        public override float LogisticUnitBarcodeYOffset => GraphicYOffset + 394;
        public override float LogisticUnitBarcodeHeight => 114;
        public override float LogisticUnitBarcodeWidth => 324;
        public override float LogisticUnitBarcodeXOffset => (PageWidth / 2) - (LogisticUnitBarcodeWidth / 2);

        public override float LogisticUnitBarcodeLabelHeight => 20;
        public override float LogisticUnitBarcodeLabelXOffset => GraphicXOffset;
        public override float LogisticUnitBarcodeLabelYOffset => LogisticUnitBarcodeYOffset + LogisticUnitBarcodeHeight + 5;
        public override float ConsignmentETAYOffset => GraphicYOffset + 143;
        public override float ConsignmentETAHeight => 18;
        public override float ConsignmentETAWidth => 75;
        public override float SenderNameXOffset => SenderNameLabelXOffset + SenderNameLabelWidth + 2;
        public override float SenderNameYOffset => GraphicYOffset + 24;
        public override float SenderNameHeight => 10;
        public override float SenderNameWidth => 170;
        public override float SenderNameLabelXOffset => GraphicXOffset + BorderOffset + DetailsLeftPadding;
        public override float SenderNameLabelYOffset => SenderNameYOffset;
        public override float SenderNameLabelHeight => 10;
        public override float SenderNameLabelWidth => 32;
        public override float SenderSuburbXOffset => GraphicXOffset + BorderOffset + DetailsLeftPadding;
        public override float SenderSuburbYOffset => SenderNameYOffset + SenderNameHeight;
        public override float SenderSuburbHeight => 10;
        public override float SenderSuburbWidth => 170;
        public override float LabelHeaderFontSize => 8;
        public override float CarrierFontSize => 8;
        public override float ReceiverAddressFontSize => 8;
        public override float SenderHeaderFontSize => 7;
        public override float DGFontSize => 7;
        public override float SenderDetailFontSize => 7;
        public override float ConsignmentNumberFontSize => 16;
        public override float ReceiverSuburbFontSize => 16;
        public override float ReceiverNameFontSize => 12;
        public override float LabelCountFontSize => 12;
        public override float BranchAbbreviationFontSize => 20;
        public override float BarcodeTextFontSize => 10;
        public override float ItemReferenceFontSize => 8;
    }
}
