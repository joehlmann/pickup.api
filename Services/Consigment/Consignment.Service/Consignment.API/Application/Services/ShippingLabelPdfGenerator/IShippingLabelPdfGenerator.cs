﻿using System.Collections.Generic;
using System.IO;
using ConsignmentSvc.API.Application.Dtos;
using ConsignmentSvc.Domain.Enumerators;

namespace ConsignmentSvc.API.Application.Services.ShippingLabelPdfGenerator
{
    public interface IShippingLabelPdfGenerator
    {
        MemoryStream GenerateLabels(List<ShipmentLabelDtoModel> labels, LabelSize size, int copies);
    }
}