﻿using System;
using System.Collections.Generic;
using System.IO;
using ConsignmentSvc.API.Application.Dtos;
using ConsignmentSvc.API.Application.Factories.Interfaces;
using ConsignmentSvc.API.Application.Models.LabelSize.LabelSizeDimensions;
using ConsignmentSvc.Domain.Enumerators;
using Microsoft.Extensions.Logging;
using Syncfusion.Drawing;
using Syncfusion.Pdf;
using Syncfusion.Pdf.Graphics;

namespace ConsignmentSvc.API.Application.Services.ShippingLabelPdfGenerator
{
    public class SyncfusionShippingLabelPdfGenerator : IShippingLabelPdfGenerator
    {
        private ILabelSizeDimensionFactory _labelSizeDimensionFactory;
        private LabelSizeDimension _labelSizeDimension;
        private readonly ILogger _logger;

        #region PdfObjects
        private readonly PdfBrush BlackBrush = new PdfSolidBrush(Color.Black);
        private readonly PdfBrush WhiteBrush = new PdfSolidBrush(Color.White);

    
        private PdfFont _labelHeaderFont;
        private PdfFont _carrierFont;
        private PdfFont _receiverAddressFont;
        private PdfFont _senderHeaderFont;
        private PdfFont _dgFont;
        private PdfFont _senderDetailFont;
        private PdfFont _consignmentNumberFont;
        private PdfFont _receiverSuburbFont;
        private PdfFont _receiverNameFont;
        private PdfFont _labelCountFont;
        private PdfFont _branchAbbreviationFont;
        private PdfFont _barcodeTextFont;
        private PdfFont _itemReferenceFont;
        

        private readonly PdfStringFormat VAlignLeftHAlignMiddleFormat = new PdfStringFormat()
            { Alignment = PdfTextAlignment.Left, LineAlignment = PdfVerticalAlignment.Middle };

        private readonly PdfStringFormat VAlignCenterHAlignMiddleFormat = new PdfStringFormat()
            { Alignment = PdfTextAlignment.Center, LineAlignment = PdfVerticalAlignment.Middle };

        private readonly PdfStringFormat VAlignRightHAlignMiddleFormat = new PdfStringFormat()
            { Alignment = PdfTextAlignment.Right, LineAlignment = PdfVerticalAlignment.Middle };

        private readonly PdfLayoutFormat FitToPageLayout = new PdfLayoutFormat(){ Layout = PdfLayoutType.OnePage, Break = PdfLayoutBreakType.FitPage };

        #endregion

        public SyncfusionShippingLabelPdfGenerator(ILabelSizeDimensionFactory labelSizeDimensionFactory, ILogger<SyncfusionShippingLabelPdfGenerator> logger)
        {
            _labelSizeDimensionFactory = labelSizeDimensionFactory ?? throw new ArgumentNullException(nameof(labelSizeDimensionFactory));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));

            
        }

        private void SetupFonts()
        {
            if (_labelSizeDimension is null)
            {
                throw new ArgumentNullException(nameof(_labelSizeDimension));
            }

            _labelHeaderFont = new PdfStandardFont(PdfFontFamily.Helvetica, _labelSizeDimension.LabelHeaderFontSize, PdfFontStyle.Bold);
            _carrierFont = new PdfStandardFont(PdfFontFamily.Helvetica, _labelSizeDimension.CarrierFontSize, PdfFontStyle.Bold);
            _receiverAddressFont = new PdfStandardFont(PdfFontFamily.Helvetica, _labelSizeDimension.ReceiverAddressFontSize, PdfFontStyle.Regular);
            _senderHeaderFont = new PdfStandardFont(PdfFontFamily.Helvetica, _labelSizeDimension.SenderHeaderFontSize, PdfFontStyle.Regular);
            _dgFont = new PdfStandardFont(PdfFontFamily.Helvetica, _labelSizeDimension.DGFontSize, PdfFontStyle.Bold);
            _senderDetailFont = new PdfStandardFont(PdfFontFamily.Helvetica, _labelSizeDimension.SenderDetailFontSize, PdfFontStyle.Bold);
            _consignmentNumberFont = new PdfStandardFont(PdfFontFamily.Helvetica, _labelSizeDimension.ConsignmentNumberFontSize, PdfFontStyle.Bold);
            _receiverSuburbFont = new PdfStandardFont(PdfFontFamily.Helvetica, _labelSizeDimension.ReceiverSuburbFontSize, PdfFontStyle.Bold);
            _receiverNameFont = new PdfStandardFont(PdfFontFamily.Helvetica, _labelSizeDimension.ReceiverNameFontSize, PdfFontStyle.Bold);
            _labelCountFont = new PdfStandardFont(PdfFontFamily.Helvetica, _labelSizeDimension.LabelCountFontSize, PdfFontStyle.Regular);
            _branchAbbreviationFont = new PdfStandardFont(PdfFontFamily.Helvetica, _labelSizeDimension.BranchAbbreviationFontSize, PdfFontStyle.Bold);
            _barcodeTextFont = new PdfStandardFont(PdfFontFamily.Helvetica, _labelSizeDimension.BarcodeTextFontSize, PdfFontStyle.Bold);
            _itemReferenceFont = new PdfStandardFont(PdfFontFamily.Helvetica, _labelSizeDimension.ItemReferenceFontSize);
        }
        public MemoryStream GenerateLabels(List<ShipmentLabelDtoModel> labels, LabelSize size, int copies)
        {
            var watch = System.Diagnostics.Stopwatch.StartNew();

            _labelSizeDimension = _labelSizeDimensionFactory.Create(size);
            
            // Configure the reusable fonts
            SetupFonts();

            //Creating new PDF document instance
            var document = new PdfDocument
            {
                PageSettings =
                {
                    // Set the page margins and size
                    Margins = new PdfMargins()
                    {
                        Bottom = _labelSizeDimension.Margins?.Bottom ?? 0,
                        Top = _labelSizeDimension.Margins?.Top ?? 0,
                        Left = _labelSizeDimension.Margins?.Left ?? 0,
                        Right = _labelSizeDimension.Margins?.Right ?? 0
                    },
                    Size = PageSize(size)
                }
            };



            _logger.LogDebug($"New document created in {watch.ElapsedMilliseconds}ms");
            watch.Restart();

            foreach (var consignmentLabel in labels)
            {
                
                // Add a copy of the labels
                for (int i = 0; i < copies; i++)
                {
                    // Insert another copy of the label (Would prefer if we could just insert the existing instance of "page" but it's not supported by syncfusion
                    InsertPdfPage(document, consignmentLabel);
                }
            }

            _logger.LogDebug($"PDF generation completed in {watch.ElapsedMilliseconds}ms");
            watch.Restart();

            //Saving the PDF to the MemoryStream
            MemoryStream ms = new MemoryStream();
            document.Save(ms);

            // Close the document
            document.Close(true);

            // Reset the stream to the start if possible
            if (ms.CanSeek)
            {
                ms.Position = 0;
            }

            return ms;

        }

        private void InsertPdfPage(PdfDocument document, ShipmentLabelDtoModel shipmentLabel)
        {
            // Add a new page to the document
            var page = document.Pages.Add();

            // Render a blank canvas ready to be drawn on, on the new page
            var pageCanvas = page.Graphics;

            // Render all the detail for the label
            RenderPdfPage(page, pageCanvas, shipmentLabel);
            
        }
        private void RenderPdfPage(PdfPage page, PdfGraphics canvas, ShipmentLabelDtoModel label)
        {

            // Draw the label border
            canvas.DrawRectangle(new PdfPen(Color.Black), new RectangleF(_labelSizeDimension.GraphicXOffset, _labelSizeDimension.GraphicYOffset, _labelSizeDimension.PageWidth, _labelSizeDimension.PageHeight));
            RenderBranchAbbr(canvas, page, label);
            RenderSenderDetails(page, label);
            RenderReceiverDetails(page, label);
            RenderConsignmentDetails(page, label);
            RenderCarrierDetails(page, label);

            RenderItemReferenceText(page, label);

            RenderLineBreak(page, canvas);
            RenderLabelCount(page, label);

            RenderConsignmentBarcode(canvas, label);
            RenderConsignmentBarcodeText(page, label);

            RenderLogisticUnitBarcode(canvas, label);
            RenderLogisticUnitBarcodeText(page, label);

            if (label.HasDangerousGoods)
            {
                RenderDGBlock(page, canvas, new RectangleF((_labelSizeDimension.PageWidth - _labelSizeDimension.DGWidth)+1, (_labelSizeDimension.HorizontalLineBreakYOffset - _labelSizeDimension.DGHeight) + 1, _labelSizeDimension.DGWidth, _labelSizeDimension.DGHeight), label);
            }
        }

        private void RenderLogisticUnitBarcodeText(PdfPage page, ShipmentLabelDtoModel label)
        {
            // Create a text element 
            PdfTextElement element = new PdfTextElement(label.LogisticUnitBarcodeText)
            {
                Brush = BlackBrush, StringFormat = VAlignCenterHAlignMiddleFormat, Font = _barcodeTextFont
            };


            // Draw the text element with the properties.
            element.Draw(page, new RectangleF(_labelSizeDimension.LogisticUnitBarcodeLabelXOffset, _labelSizeDimension.LogisticUnitBarcodeLabelYOffset,_labelSizeDimension.PageWidth,_labelSizeDimension.LogisticUnitBarcodeLabelHeight), FitToPageLayout);

        }

        private void RenderConsignmentBarcodeText(PdfPage page, ShipmentLabelDtoModel label)
        {
            // Create a text element 
            PdfTextElement element = new PdfTextElement(label.ConsignmentBarcodeText)
            {
                Brush = BlackBrush, StringFormat = VAlignCenterHAlignMiddleFormat, Font = _barcodeTextFont
            };

            // Draw the text element with the properties.
            element.Draw(page, new RectangleF(_labelSizeDimension.ConsignmentBarcodeLabelXOffset, _labelSizeDimension.ConsignmentBarcodeLabelYOffset, _labelSizeDimension.PageWidth, _labelSizeDimension.ConsignmentBarcodeLabelHeight), FitToPageLayout);

        }

        private void RenderItemReferenceText(PdfPage page, ShipmentLabelDtoModel label)
        {
            //if (label.HasItemReference)
            //{
                // Create the label for the customer reference
                PdfTextElement labelElement = new PdfTextElement("Item Ref:")
                {
                    Brush = BlackBrush,
                    StringFormat = VAlignLeftHAlignMiddleFormat,
                    Font = _labelHeaderFont
                };

                // Draw the text element with the properties.
                labelElement.Draw(page, new RectangleF(_labelSizeDimension.ItemReferenceLabelXOffset, _labelSizeDimension.ItemReferenceLabelYOffset, _labelSizeDimension.ItemReferenceLabelWidth, _labelSizeDimension.ItemReferenceLabelHeight), FitToPageLayout);

            // Create a text element 
            PdfTextElement element = new PdfTextElement(label.ItemReference)
            {
                Brush = BlackBrush,
                StringFormat = VAlignLeftHAlignMiddleFormat,
                Font = _itemReferenceFont
            };

            // Draw the text element with the properties.
            element.Draw(page, new RectangleF(_labelSizeDimension.ItemReferenceXOffset, _labelSizeDimension.ItemReferenceYOffset, _labelSizeDimension.ItemReferenceWidth, _labelSizeDimension.ItemReferenceHeight), FitToPageLayout);
            //}


        }

        private void RenderDGBlock(PdfPage page, PdfGraphics canvas, RectangleF location, ShipmentLabelDtoModel label)
        {
            canvas.DrawRectangle(BlackBrush, location);

            RenderDGText(page, location, label);
        }

        private void RenderDGText(PdfPage page, RectangleF block, ShipmentLabelDtoModel label)
        {
            // Protect against empty text
            if (string.IsNullOrWhiteSpace(label.DGText))
            {
                return;
            }

            // Create a text element 
            PdfTextElement element = new PdfTextElement(label.DGText)
            {
                Brush = WhiteBrush, StringFormat = VAlignCenterHAlignMiddleFormat, Font = _dgFont
            };

            // Draw the text element with the properties.
            element.Draw(page, block, FitToPageLayout);
        }

        private void RenderLogisticUnitBarcode(PdfGraphics canvas, ShipmentLabelDtoModel label)
        {
            RenderBarcodeImage(canvas, label.LogisticUnitBarcode, _labelSizeDimension.LogisticUnitBarcodeXOffset, _labelSizeDimension.LogisticUnitBarcodeYOffset, _labelSizeDimension.LogisticUnitBarcodeWidth, _labelSizeDimension.LogisticUnitBarcodeHeight, string.Equals(label.LogisticUnitBarcodeFormat, "gs1", StringComparison.OrdinalIgnoreCase));
        }

        private void RenderConsignmentBarcode(PdfGraphics canvas, ShipmentLabelDtoModel label)
        {
            RenderBarcodeImage(canvas, label.ConsignmentBarcode, _labelSizeDimension.ConsignmentBarcodeXOffset, _labelSizeDimension.ConsignmentBarcodeYOffset, _labelSizeDimension.ConsignmentBarcodeWidth, _labelSizeDimension.ConsignmentBarcodeHeight, string.Equals(label.ConsignmentBarcodeFormat, "gs1", StringComparison.OrdinalIgnoreCase));
        }

        private void RenderBarcodeImage(PdfGraphics canvas, string barcode, float barcodeXOffset, float barcodeYOffset, float barcodeWidth, float barcodeHeight, bool isGS1)
        {
            // TODO: Review method
            // get the Code128 codes to represent the message
            int[] codes = Code128Encode.Encode(barcode, isGS1);

        var quietZonePadding = 20;
        float barWeight;


            barWeight = barcodeWidth / ((codes.Length - 3) * 11 + 35);

            //TODO: implement the quite zone
            var AddQuietZone = false;
            //if (AddQuietZone)
            //{
            //    width += 2 * cQuietWidth * barWeight;  // on both sides
            //}


            canvas.DrawRectangle(WhiteBrush, new RectangleF(barcodeXOffset, barcodeYOffset, barcodeWidth, barcodeHeight));

            // get surface to draw on

            //    // skip quiet zone
            float cursor = AddQuietZone ? quietZonePadding * barWeight : barcodeXOffset;

            for (int codeidx = 0; codeidx < codes.Length; codeidx++)
            {
                int code = codes[codeidx];

                // take the bars two at a time: a black and a white
                for (int bar = 0; bar < 8; bar += 2)
                {
                    float barwidth = cPatterns[code, bar] * barWeight;
                    float spcwidth = cPatterns[code, bar + 1] * barWeight;

                    // if width is zero, don't try to draw it
                    if (barwidth > 0)
                    {
                        canvas.DrawRectangle(BlackBrush, new RectangleF(cursor, barcodeYOffset, barwidth, barcodeHeight));
                    }

                    // note that we never need to draw the space, since we 
                    // initialized the graphics to all white

                    // advance cursor beyond this pair
                    cursor += (barwidth + spcwidth);
                }
            }


        }
        
        private void RenderLabelCount(PdfPage page, ShipmentLabelDtoModel label)
        {
            // Create a text element 
            PdfTextElement element = new PdfTextElement($"Label {label.LabelCounter} of {label.LabelTotal}")
            {
                Brush = BlackBrush,
                StringFormat = VAlignRightHAlignMiddleFormat,
                Font = _labelCountFont
            };

            // Draw the text element with the properties.
            element.Draw(page,
                new RectangleF((_labelSizeDimension.PageWidth - _labelSizeDimension.LabelCountWidth) - 15, _labelSizeDimension.LabelCountYOffset, _labelSizeDimension.LabelCountWidth, _labelSizeDimension.LabelCountHeight), FitToPageLayout);

        }

        private void RenderLineBreak(PdfPage page, PdfGraphics canvas)
        {
            // Line break
            canvas.DrawRectangle(BlackBrush, new RectangleF(_labelSizeDimension.GraphicXOffset, _labelSizeDimension.HorizontalLineBreakYOffset + _labelSizeDimension.GraphicYOffset, _labelSizeDimension.PageWidth, _labelSizeDimension.HorizontalLineBreakHeight));
        }

        private void RenderCarrierDetails(PdfPage page, ShipmentLabelDtoModel label)
        {
            if (string.IsNullOrWhiteSpace(label.Carrier))
            {
                return;
            }

            // Create a text element 
            var element = new PdfTextElement($"Carrier: {label.Carrier}")
            {
                Brush = BlackBrush, StringFormat = VAlignCenterHAlignMiddleFormat, Font = _carrierFont
            };

            // Draw the text element with the properties.
            element.Draw(page,
                new RectangleF(_labelSizeDimension.PageWidth - _labelSizeDimension.CarrierWidth, _labelSizeDimension.CarrierYOffset, _labelSizeDimension.CarrierWidth, _labelSizeDimension.CarrierHeight), FitToPageLayout);
        }

        private void RenderConsignmentDetails(PdfPage page, ShipmentLabelDtoModel label)
        {
            RenderConsignmentLabel(page);
            RenderConsignmentNumber(page, label);
            RenderConsignmentETA(page, label);
        }

        private void RenderConsignmentETA(PdfPage page, ShipmentLabelDtoModel label)
        {
            if (label.EtaDate != null)
            {
                
                // Create a text element 
                PdfTextElement consignmentEta = new PdfTextElement($"ETA:  {label.EtaDate.Value:dd/MM/yyyy}")
                {
                    Brush = BlackBrush,
                    StringFormat = VAlignCenterHAlignMiddleFormat,
                    Font = _labelHeaderFont
                };

                // Draw the Consignment eta text element with the properties.
                consignmentEta.Draw(page, new RectangleF(_labelSizeDimension.PageWidth - _labelSizeDimension.ConsignmentETAWidth, _labelSizeDimension.ConsignmentETAYOffset, _labelSizeDimension.ConsignmentETAWidth, _labelSizeDimension.ConsignmentETAHeight), FitToPageLayout);

            }
        }

        private void RenderConsignmentNumber(PdfPage page, ShipmentLabelDtoModel label)
        {
            // Create a text element 
            PdfTextElement consignmentNumber = new PdfTextElement(label.ConsignmentNumber)
            {
                Brush = BlackBrush,
                StringFormat = VAlignLeftHAlignMiddleFormat,
                Font = _consignmentNumberFont
            };

            // Draw the Consignment number text element with the properties.
            consignmentNumber.Draw(page, new RectangleF(_labelSizeDimension.ConsignmentNumberXOffset, _labelSizeDimension.ConsignmentNumberYOffset, _labelSizeDimension.ConsignmentNumberWidth, _labelSizeDimension.ConsignmentNumberHeight), FitToPageLayout);
        }

        private void RenderConsignmentLabel(PdfPage page)
        {

            // Create a text element 
            PdfTextElement consignmentLabel = new PdfTextElement("CONSIGNMENT:")
            {
                Brush = BlackBrush,
                StringFormat = VAlignLeftHAlignMiddleFormat,
                Font = _labelHeaderFont
            };

            // Draw the Consignment label text element with the properties.
            consignmentLabel.Draw(page, new RectangleF(_labelSizeDimension.ConsignmentLabelXOffset, _labelSizeDimension.ConsignmentLabelYOffset, _labelSizeDimension.ConsignmentLabelWidth, _labelSizeDimension.ConsignmentLabelHeight), FitToPageLayout);
        }

        private void RenderReceiverDetails(PdfPage page, ShipmentLabelDtoModel label)
        {
            RenderReceiverTo(page);
            RenderReceiverName(page, label);
            RenderReceiverAddress(page, label);
            RenderReceiverSuburb(page, label);
        }

        private void RenderReceiverSuburb(PdfPage page, ShipmentLabelDtoModel label)
        {
            // Make sure all the detail has been provided
            if (string.IsNullOrWhiteSpace(label.ReceiverSuburb) && string.IsNullOrWhiteSpace(label.ReceiverState) &&
                string.IsNullOrWhiteSpace(label.ReceiverPostcode))
            {
                return;
            }

            var suburb =
                new PdfTextElement($"{label.ReceiverSuburb} {label.ReceiverState} {label.ReceiverPostcode}")
                {
                    Brush = BlackBrush,
                    StringFormat = VAlignLeftHAlignMiddleFormat,
                    Font = _receiverSuburbFont
                };


            suburb.Draw(page,
                new RectangleF(_labelSizeDimension.ReceiverSuburbXOffset, _labelSizeDimension.ReceiverSuburbYOffset, _labelSizeDimension.ReceiverSuburbWidth,
                    _labelSizeDimension.ReceiverSuburbHeight), FitToPageLayout);
        }

        private void RenderReceiverAddress(PdfPage page, ShipmentLabelDtoModel label)
        {
            // Check if the detail has been provided
            if (string.IsNullOrWhiteSpace(label.ReceiverAddress1) &&
                string.IsNullOrWhiteSpace(label.ReceiverAddress2))
            {
                return;
            }

            if (!string.IsNullOrWhiteSpace(label.ReceiverAddress1))
            {
                // Create a text element 
                var address1 = new PdfTextElement(label.ReceiverAddress1)
                {
                    Brush = BlackBrush,
                    StringFormat = VAlignLeftHAlignMiddleFormat,
                    Font = _receiverAddressFont
                };
                address1.Draw(page, new RectangleF(_labelSizeDimension.ReceiverAddress1XOffset, _labelSizeDimension.ReceiverAddress1YOffset, _labelSizeDimension.ReceiverAddress1Width, _labelSizeDimension.ReceiverAddress1Height), FitToPageLayout);
            }

            // Check if a second address line has been provided
            if (string.IsNullOrWhiteSpace(label.ReceiverAddress2))
            {
                return;
            }

            var address2 = new PdfTextElement(label.ReceiverAddress2)
            {
                Brush = BlackBrush,
                StringFormat = VAlignLeftHAlignMiddleFormat,
                Font = _receiverAddressFont
            };


            address2.Draw(page,
                new RectangleF(_labelSizeDimension.ReceiverAddress2XOffset, _labelSizeDimension.ReceiverAddress2YOffset, _labelSizeDimension.ReceiverAddress2Width,
                    _labelSizeDimension.ReceiverAddress2Height), FitToPageLayout);
        }

        private void RenderReceiverName(PdfPage page, ShipmentLabelDtoModel label)
        {
            if (string.IsNullOrWhiteSpace(label.ReceiverName))
            {
                return;
            }

            // Create a text element 
            var element = new PdfTextElement(label.ReceiverName)
            {
                Brush = BlackBrush,
                StringFormat = VAlignLeftHAlignMiddleFormat,
                Font = _receiverNameFont
            };

            // Draw the text element with the properties.
            element.Draw(page,
                new RectangleF(_labelSizeDimension.ReceiverNameXOffset, _labelSizeDimension.ReceiverNameYOffset, _labelSizeDimension.ReceiverNameWidth, _labelSizeDimension.ReceiverNameHeight), FitToPageLayout);
        }

        private void RenderReceiverTo(PdfPage page)
        {
            // Create a text element 
            var element = new PdfTextElement("To:")
            {
                Brush = BlackBrush, StringFormat = VAlignLeftHAlignMiddleFormat, Font = _labelHeaderFont
            };
            

            // Draw the text element with the properties.
            element.Draw(page, new RectangleF(_labelSizeDimension.ReceiverToXOffset, _labelSizeDimension.ReceiverToYOffset, _labelSizeDimension.ReceiverToWidth, _labelSizeDimension.ReceiverToHeight), FitToPageLayout);
        }

        private void RenderSenderDetails(PdfPage page, ShipmentLabelDtoModel label)
        {
            RenderSenderName(page, label);
            RenderSenderSuburb(page, label);
        }

        private void RenderSenderSuburb(PdfPage page, ShipmentLabelDtoModel label)
        {
            if (string.IsNullOrWhiteSpace(label.SenderSuburb) && string.IsNullOrWhiteSpace(label.SenderState) &&
                string.IsNullOrWhiteSpace(label.SenderPostcode))
            {
                return;
            }

            var suburbText = $"{label.SenderSuburb} {label.SenderState} {label.SenderPostcode}";
            var suburb =
                new PdfTextElement(suburbText.ToUpper())
                {
                    Brush = BlackBrush,
                    StringFormat = VAlignLeftHAlignMiddleFormat,
                    Font = _senderDetailFont
                };


            suburb.Draw(page,
                new RectangleF(_labelSizeDimension.SenderSuburbXOffset, _labelSizeDimension.SenderSuburbYOffset, _labelSizeDimension.SenderSuburbWidth, _labelSizeDimension.SenderSuburbHeight), FitToPageLayout);
        }

        private void RenderSenderName(PdfPage page, ShipmentLabelDtoModel label)
        {
            // Create a text element 
            var senderNameLabel = new PdfTextElement("Shipper:")
            {
                Brush = BlackBrush,
                StringFormat = VAlignLeftHAlignMiddleFormat,
                Font = _senderDetailFont
            };

            // Draw the sender name text element with the properties.
            senderNameLabel.Draw(page, new RectangleF(_labelSizeDimension.SenderNameLabelXOffset, _labelSizeDimension.SenderNameLabelYOffset, _labelSizeDimension.SenderNameLabelWidth, _labelSizeDimension.SenderNameLabelHeight), FitToPageLayout);

            if (string.IsNullOrEmpty(label.SenderName))
            {
                return;
            }

            // Create a text element 
            var senderName = new PdfTextElement(label.SenderName.ToUpper())
            {
                Brush = BlackBrush,
                StringFormat = VAlignLeftHAlignMiddleFormat,
                Font = _senderHeaderFont
            };

            // Draw the sender name text element with the properties.
            senderName.Draw(page, new RectangleF(_labelSizeDimension.SenderNameXOffset, _labelSizeDimension.SenderNameYOffset, _labelSizeDimension.SenderNameWidth, _labelSizeDimension.SenderNameHeight), FitToPageLayout);
        }

        private void RenderBranchAbbr(PdfGraphics canvas, PdfPage page, ShipmentLabelDtoModel label)
        {
         
            // Draw the branch abb background
            canvas.DrawRectangle(BlackBrush, new RectangleF(_labelSizeDimension.GraphicXOffset, _labelSizeDimension.GraphicYOffset, _labelSizeDimension.BranchAbbWidth, _labelSizeDimension.BranchAbbHeight));

            RenderBranchAbbrText(page, label);
            RenderRunNoText(page, label);

        }

        private void RenderBranchAbbrText(PdfPage page, ShipmentLabelDtoModel label)
        {
            // Create a text element 
            var branchAbb = new PdfTextElement($"{label.BranchAbbreviation}")
            {
                Brush = WhiteBrush,
                StringFormat = VAlignCenterHAlignMiddleFormat,
                Font = _branchAbbreviationFont
            };
            var width = label.DeliveryNumber > 0 ? _labelSizeDimension.BranchAbbTextWidth : _labelSizeDimension.BranchAbbNoDeliveryNumberTextWidth;

            // Draw the sender name text element with the properties.
            branchAbb.Draw(page, new RectangleF(_labelSizeDimension.BranchAbbTextXOffset, _labelSizeDimension.BranchAbbTextYOffset, width, _labelSizeDimension.BranchAbbTextHeight), FitToPageLayout);
        }
        private void RenderRunNoText(PdfPage page, ShipmentLabelDtoModel label)
        {
            if (label.DeliveryNumber == 0)
            {
                return;
            }

            // Create a text element 
            var runNo = new PdfTextElement($"{label.DeliveryNumber}")
            {
                Brush = WhiteBrush,
                StringFormat = VAlignCenterHAlignMiddleFormat,
                Font = _branchAbbreviationFont
            };
            
            // Draw the sender name text element with the properties.
            runNo.Draw(page, new RectangleF(_labelSizeDimension.BranchDeliveryNumberXOffset, _labelSizeDimension.BranchDeliveryNumberYOffset, _labelSizeDimension.BranchDeliveryNumberWidth, _labelSizeDimension.BranchDeliveryNumberHeight), FitToPageLayout);
        }
        private SizeF PageSize(LabelSize size)
        {
            switch (size)
            {
                case LabelSize.A5: return PdfPageSize.A5;
                case LabelSize.A6: return PdfPageSize.A6;
                default: throw new ArgumentException("The label size specified has not been configured");
            }
        }

        #region Code patterns

        // in principle these rows should each have 6 elements
        // however, the last one -- STOP -- has 7. The cost of the
        // extra integers is trivial, and this lets the code flow
        // much more elegantly
        private static readonly int[,] cPatterns =
                     {
                        {2,1,2,2,2,2,0,0},  // 0
                        {2,2,2,1,2,2,0,0},  // 1
                        {2,2,2,2,2,1,0,0},  // 2
                        {1,2,1,2,2,3,0,0},  // 3
                        {1,2,1,3,2,2,0,0},  // 4
                        {1,3,1,2,2,2,0,0},  // 5
                        {1,2,2,2,1,3,0,0},  // 6
                        {1,2,2,3,1,2,0,0},  // 7
                        {1,3,2,2,1,2,0,0},  // 8
                        {2,2,1,2,1,3,0,0},  // 9
                        {2,2,1,3,1,2,0,0},  // 10
                        {2,3,1,2,1,2,0,0},  // 11
                        {1,1,2,2,3,2,0,0},  // 12
                        {1,2,2,1,3,2,0,0},  // 13
                        {1,2,2,2,3,1,0,0},  // 14
                        {1,1,3,2,2,2,0,0},  // 15
                        {1,2,3,1,2,2,0,0},  // 16
                        {1,2,3,2,2,1,0,0},  // 17
                        {2,2,3,2,1,1,0,0},  // 18
                        {2,2,1,1,3,2,0,0},  // 19
                        {2,2,1,2,3,1,0,0},  // 20
                        {2,1,3,2,1,2,0,0},  // 21
                        {2,2,3,1,1,2,0,0},  // 22
                        {3,1,2,1,3,1,0,0},  // 23
                        {3,1,1,2,2,2,0,0},  // 24
                        {3,2,1,1,2,2,0,0},  // 25
                        {3,2,1,2,2,1,0,0},  // 26
                        {3,1,2,2,1,2,0,0},  // 27
                        {3,2,2,1,1,2,0,0},  // 28
                        {3,2,2,2,1,1,0,0},  // 29
                        {2,1,2,1,2,3,0,0},  // 30
                        {2,1,2,3,2,1,0,0},  // 31
                        {2,3,2,1,2,1,0,0},  // 32
                        {1,1,1,3,2,3,0,0},  // 33
                        {1,3,1,1,2,3,0,0},  // 34
                        {1,3,1,3,2,1,0,0},  // 35
                        {1,1,2,3,1,3,0,0},  // 36
                        {1,3,2,1,1,3,0,0},  // 37
                        {1,3,2,3,1,1,0,0},  // 38
                        {2,1,1,3,1,3,0,0},  // 39
                        {2,3,1,1,1,3,0,0},  // 40
                        {2,3,1,3,1,1,0,0},  // 41
                        {1,1,2,1,3,3,0,0},  // 42
                        {1,1,2,3,3,1,0,0},  // 43
                        {1,3,2,1,3,1,0,0},  // 44
                        {1,1,3,1,2,3,0,0},  // 45
                        {1,1,3,3,2,1,0,0},  // 46
                        {1,3,3,1,2,1,0,0},  // 47
                        {3,1,3,1,2,1,0,0},  // 48
                        {2,1,1,3,3,1,0,0},  // 49
                        {2,3,1,1,3,1,0,0},  // 50
                        {2,1,3,1,1,3,0,0},  // 51
                        {2,1,3,3,1,1,0,0},  // 52
                        {2,1,3,1,3,1,0,0},  // 53
                        {3,1,1,1,2,3,0,0},  // 54
                        {3,1,1,3,2,1,0,0},  // 55
                        {3,3,1,1,2,1,0,0},  // 56
                        {3,1,2,1,1,3,0,0},  // 57
                        {3,1,2,3,1,1,0,0},  // 58
                        {3,3,2,1,1,1,0,0},  // 59
                        {3,1,4,1,1,1,0,0},  // 60
                        {2,2,1,4,1,1,0,0},  // 61
                        {4,3,1,1,1,1,0,0},  // 62
                        {1,1,1,2,2,4,0,0},  // 63
                        {1,1,1,4,2,2,0,0},  // 64
                        {1,2,1,1,2,4,0,0},  // 65
                        {1,2,1,4,2,1,0,0},  // 66
                        {1,4,1,1,2,2,0,0},  // 67
                        {1,4,1,2,2,1,0,0},  // 68
                        {1,1,2,2,1,4,0,0},  // 69
                        {1,1,2,4,1,2,0,0},  // 70
                        {1,2,2,1,1,4,0,0},  // 71
                        {1,2,2,4,1,1,0,0},  // 72
                        {1,4,2,1,1,2,0,0},  // 73
                        {1,4,2,2,1,1,0,0},  // 74
                        {2,4,1,2,1,1,0,0},  // 75
                        {2,2,1,1,1,4,0,0},  // 76
                        {4,1,3,1,1,1,0,0},  // 77
                        {2,4,1,1,1,2,0,0},  // 78
                        {1,3,4,1,1,1,0,0},  // 79
                        {1,1,1,2,4,2,0,0},  // 80
                        {1,2,1,1,4,2,0,0},  // 81
                        {1,2,1,2,4,1,0,0},  // 82
                        {1,1,4,2,1,2,0,0},  // 83
                        {1,2,4,1,1,2,0,0},  // 84
                        {1,2,4,2,1,1,0,0},  // 85
                        {4,1,1,2,1,2,0,0},  // 86
                        {4,2,1,1,1,2,0,0},  // 87
                        {4,2,1,2,1,1,0,0},  // 88
                        {2,1,2,1,4,1,0,0},  // 89
                        {2,1,4,1,2,1,0,0},  // 90
                        {4,1,2,1,2,1,0,0},  // 91
                        {1,1,1,1,4,3,0,0},  // 92
                        {1,1,1,3,4,1,0,0},  // 93
                        {1,3,1,1,4,1,0,0},  // 94
                        {1,1,4,1,1,3,0,0},  // 95
                        {1,1,4,3,1,1,0,0},  // 96
                        {4,1,1,1,1,3,0,0},  // 97
                        {4,1,1,3,1,1,0,0},  // 98
                        {1,1,3,1,4,1,0,0},  // 99
                        {1,1,4,1,3,1,0,0},  // 100
                        {3,1,1,1,4,1,0,0},  // 101
                        {4,1,1,1,3,1,0,0},  // 102
                        {2,1,1,4,1,2,0,0},  // 103
                        {2,1,1,2,1,4,0,0},  // 104
                        {2,1,1,2,3,2,0,0},  // 105
                        {2,3,3,1,1,1,2,0}   // 106
                     };

        #endregion Code patterns
    }
    //TODO: Move to it's own class???
    public static class Code128Encode
    {
        #region Constants

        private const int cSHIFT = 98;
        private const int cCODEC = 99;
        private const int cCODEA = 101;
        private const int cCODEB = 100;

        private const int cFNC1 = 102;

        private const int cSTARTA = 103;
        private const int cSTARTB = 104;
        private const int cSTARTC = 105;
        private const int cSTOP = 106;

        #endregion

        public static int[] Encode(string value, bool isGS1)
        {
            int pos = 0;
            int ascii;
            int test;
            string sampleChunk = "";
            bool useTableB = false;
            System.Collections.ArrayList codes = new System.Collections.ArrayList();
            int length = 0;

            if (!String.IsNullOrEmpty(value))
            {
                length = value.Length;

                //Check for valid characters
                for (pos = 0; pos < length; pos++)
                {
                    if ((value[pos] < 32 && value[pos] != 29) || (value[pos] > 126 && value[pos] != 203))
                    {
                        throw new Exception("Invalid value");
                    }
                }

                //Calculation of the code string with optimized use of tables B and C 
                // NOTE: haven't implemented CodeA or the use of the SHIFT character
                useTableB = true;
                pos = 0;

                while (pos < length)
                {
                    if (useTableB)
                    {
                        // See if interesting to switch to table C
                        // yes for 4 digits at start or end, else if 6 digits


                        test = 0;
                        if (pos + 1 < length)
                        {     // if only one char left, can't use C
                            sampleChunk = value.Substring(pos, 2);
                            Int32.TryParse(sampleChunk, out test);
                        }

                        // sample was taken and next two characters are numeric
                        if (sampleChunk == "00" || test > 0)
                        {
                            useTableB = false;
                        }


                        if (useTableB)
                        {
                            // still using tableB
                            if (pos == 0)
                            {
                                codes.Add(cSTARTB);
                                if (isGS1) codes.Add(cFNC1); ;
                            }
                        }
                        else
                        {
                            // switched to tableC
                            if (pos == 0)
                            {
                                codes.Add(cSTARTC);
                                if (isGS1) codes.Add(cFNC1); ;
                            }
                            else
                            {
                                codes.Add(cCODEC);
                            }
                            useTableB = false;
                        }
                    }

                    if (!useTableB)
                    {
                        // We are on table C, try to process 2 digits
                        test = 0;
                        if (pos + 1 < length)
                        {     // if only one char left, can't use C
                            sampleChunk = value.Substring(pos, 2);
                            Int32.TryParse(sampleChunk, out test);
                        }
                        else
                        {
                            sampleChunk = value.Substring(pos, 1);
                        }

                        if (sampleChunk == "00" || test > 0)
                        {
                            codes.Add(test);
                            pos = pos + 2;
                        }
                        else
                        {
                            // We haven't 2 digits, switch to table B
                            useTableB = true;
                            codes.Add(cCODEB);
                        }
                    }

                    if (useTableB)
                    {
                        ascii =
                       // Process 1 digit with table B
                       codes.Add((int)(value.Substring(pos, 1))[0] - 32);
                        pos = pos + 1;
                    }

                }

                // calculate the check digit
                int checksum = (int)(codes[0]);
                for (int i = 1; i < codes.Count; i++)
                {
                    checksum += i * (int)(codes[i]);
                }
                codes.Add(checksum % 103);

                codes.Add(cSTOP);

            }

            return codes.ToArray(typeof(int)) as int[];
        }

    }
}
