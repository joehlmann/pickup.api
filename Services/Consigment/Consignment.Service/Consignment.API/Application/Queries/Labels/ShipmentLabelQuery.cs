﻿using System.Collections.Generic;
using ConsignmentSvc.API.Application.Dtos;
using MediatR;

namespace ConsignmentSvc.API.Application.Queries.Labels
{
    public class ShipmentLabelQuery : IRequest<List<ShipmentLabelDtoModel>>
    {
        public string ConsignmentNumber { get; set; }
    }
}
