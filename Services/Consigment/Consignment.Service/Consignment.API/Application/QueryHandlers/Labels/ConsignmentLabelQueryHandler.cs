﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ConsignmentSvc.API.Application.Dtos;
using ConsignmentSvc.API.Application.Factories.Interfaces;
using ConsignmentSvc.API.Application.Queries.Labels;
using ConsignmentSvc.Domain.Enumerators;
using ConsignmentSvc.Domain.ValueObjects;
using ConsignmentSvc.Infrastructure.Providers;
using ConsignmentSvc.Infrastructure.Repositories.Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace ConsignmentSvc.API.Application.QueryHandlers.Labels
{
    public class ConsignmentLabelQueryHandler : IRequestHandler<ShipmentLabelQuery, List<ShipmentLabelDtoModel>>
    {
        private readonly TS2000Context _context;
        private readonly ISuburbRepository _suburbRepository;
        private readonly IBarcodeFactory _barcodeFactory;
        private readonly IBranchRepository _branchRepository;
        public ConsignmentLabelQueryHandler(ISuburbRepository suburbRepository, TS2000Context context, IBarcodeFactory barcodeFactory, IBranchRepository branchRepository)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _suburbRepository = suburbRepository ?? throw new ArgumentNullException(nameof(suburbRepository));
            _barcodeFactory = barcodeFactory ?? throw new ArgumentNullException(nameof(barcodeFactory));
            _branchRepository = branchRepository ?? throw new ArgumentNullException(nameof(branchRepository));
        }
        public async Task<List<ShipmentLabelDtoModel>> Handle(ShipmentLabelQuery request, CancellationToken cancellationToken)
        {
            var consignments = await _context.LogisticUnits
               .Where(c => c.Consignment.Number == request.ConsignmentNumber)
               .Include(c => c.Consignment)
               .Select(c => new ShipmentLabelDtoModel
               {
                   LogisticUnitBarcode = c.Barcode,
                   LogisticUnitBarcodeType = c.BarcodeType,
                   ConsignmentNumber = c.Consignment.Number,
                   EtaDate = c.Consignment.ETADate,
                   ReceiverAddress1 = c.Consignment.ReceiverAddress1,
                   ReceiverAddress2 = c.Consignment.ReceiverAddress2,
                   ReceiverName = c.Consignment.ReceiverName,
                   ReceiverSuburbId = c.Consignment.ReceiverSuburbId,
                   ConsignmentBarcode = c.Consignment.Number,
                   SenderName = c.Consignment.SenderName,
                   SenderSuburbId = c.Consignment.SenderSuburbId,
                   //TODO: Prefered if this used an outer apply instead of an inline subquery to do the count
                   ItemReference = c.LoadItemForeignKey != null ? c.LoadItem.ItemReference : c.Consignment.Items.Count == 1 ? c.Consignment.Items.FirstOrDefault().ItemReference : "",
                   HasDangerousGoods = c.Consignment.HasDangerousGoods,
                   LogisticUnitId = c.Id,
                   LabelTotal = Convert.ToInt32(c.Consignment.TotalUnits),
                   DGText = "Please refer to DG Transport Document",
                   Carrier = "Border Express (BEX)"
               })
               .AsNoTracking()
               .ToListAsync(cancellationToken);

            var firstConsignment = consignments.FirstOrDefault();

            if (firstConsignment is null)
            {
                return new List<ShipmentLabelDtoModel>();
            }
            // Look up the suburb details
            var receiverSuburbTask = FindSuburbAsync(firstConsignment.ReceiverSuburbId);
            var senderSuburbTask = FindSuburbAsync(firstConsignment.SenderSuburbId);

            var receiverSuburb = await receiverSuburbTask;
            var prefReceivingBranch = await FindBranchAsync(receiverSuburb?.PreferredReceivingBranch?.BranchId ?? 0);
            
            var senderSuburb = await senderSuburbTask;
            var runNumber = 0;

            if (receiverSuburb?.Runs != null && receiverSuburb.Runs.Any())
            {
                //TODO: Investigate if the logic has been added to the suburbs service
                runNumber = receiverSuburb.Runs.OrderBy(r => r.Order).First().DeliveryNumber;
            }

            // Only runs for one con, so generate the barcode once
            var conBarcode = _barcodeFactory.Create((int)BarcodeType.ExistingCN, firstConsignment.ConsignmentNumber);

            // Update all the consignments in the list (needs ToList to prevent lazy evaluation)
            consignments.Select((c,i) =>
            {
            
                var luBarcode = _barcodeFactory.Create(c.LogisticUnitBarcodeType, c.LogisticUnitBarcode);

                c.ReceiverState = receiverSuburb?.State;
                c.ReceiverSuburb = receiverSuburb?.Name;
                c.ReceiverPostcode = receiverSuburb?.Postcode;
                c.SenderState = senderSuburb?.State;
                c.SenderSuburb = senderSuburb?.Name;
                c.SenderPostcode = senderSuburb?.Postcode;
                c.ConsignmentBarcodeText = conBarcode.DisplaySSCC;
                c.ConsignmentBarcode = conBarcode.Number;
                c.ConsignmentBarcodeFormat = conBarcode.Format.ToString();
                c.LogisticUnitBarcodeText = luBarcode.DisplaySSCC;
                c.LogisticUnitBarcode = luBarcode.Number;
                c.LogisticUnitBarcodeFormat = luBarcode.Format.ToString();
                c.BranchAbbreviation = prefReceivingBranch?.Abbreviation;
                c.LabelCounter = i+1;
                c.DeliveryNumber = runNumber;
                return c;
            }).OrderBy(o => o.LogisticUnitId).ToList();

            return consignments;
        }

        /// <summary>
        /// Find more details about the suburb
        /// </summary>
        /// <param name="suburbId">ID of the suburb</param>
        /// <returns></returns>
        private Task<Suburb> FindSuburbAsync(int suburbId)
        {
            return suburbId > 0
                ? _suburbRepository.FindByIdAsync(suburbId)
                : null;
        }

        private Task<Branch> FindBranchAsync(int branchId)
        {
            return branchId > 0
                ? _branchRepository.FindByIdAsync(branchId)
                : Task.FromResult<Branch>(null);
        }
    }
}
