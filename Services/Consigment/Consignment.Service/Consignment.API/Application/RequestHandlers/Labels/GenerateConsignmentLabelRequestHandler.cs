﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ConsignmentSvc.API.Application.Dtos;
using ConsignmentSvc.API.Application.Queries.Labels;
using ConsignmentSvc.API.Application.Requests.Labels;
using ConsignmentSvc.API.Application.Responses.Labels;
using ConsignmentSvc.API.Application.Services.ShippingLabelPdfGenerator;
using ConsignmentSvc.API.Application.ViewModels;
using MediatR;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace ConsignmentSvc.API.Application.RequestHandlers.Labels
{
    public class GenerateConsignmentLabelRequestHandler : IRequestHandler<GenerateConsignmentLabelRequest, GenerateConsignmentLabelResponse>
    {
        private readonly IMediator _mediator;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly ILogger _logger;
        private readonly IConfiguration _config;
        private IShippingLabelPdfGenerator _labelGenerator;
        


        public GenerateConsignmentLabelRequestHandler(IMediator mediator, IHostingEnvironment hostingEnvironment, ILogger<GenerateConsignmentLabelRequestHandler> logger, IConfiguration config, IShippingLabelPdfGenerator labelGenerator)
        {
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
            _hostingEnvironment = hostingEnvironment ?? throw new ArgumentNullException(nameof(hostingEnvironment));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _config = config ?? throw new ArgumentNullException(nameof(config));
            _labelGenerator = labelGenerator ?? throw new ArgumentNullException(nameof(labelGenerator));
        }
        /// <summary>
        /// Generates a ConsignmentLabelResponse, this is a collection of all the associated shipment labels
        /// </summary>
        /// <param name="request">Specific parameters that control how the labels are rendered</param>
        /// <param name="cancellationToken"></param>
        /// <returns>A representation of the shipment labels</returns>
        public async Task<GenerateConsignmentLabelResponse> Handle(GenerateConsignmentLabelRequest request, CancellationToken cancellationToken)
        {
            FileViewModel file = null;

            // Collect the data for the labels
            var consignmentLabels = await GetReadonlyShipmentLabelsAsync(request.ConsignmentNumber).ConfigureAwait(false);
            if (consignmentLabels.Any())
            {
                // Use Task.Run to force the rendering onto a different thread
                var fileTask = Task.Run( () =>
                {              
                    // store local instances of the parameters to avoid cross thread reference issues
                    var localRequest = request;
                    var localLabels = consignmentLabels;

                    return GenerateLabelFile(localRequest, localLabels);
                }, cancellationToken);

                file = await fileTask.ConfigureAwait(false);
            }
          
            // Generate the pdf
            return new GenerateConsignmentLabelResponse() { File = file };
        }

        /// <summary>
        /// Retrieve the labels data
        /// </summary>
        /// <param name="consignmentNumber">The consignment number to get the data for</param>
        /// <returns></returns>
        private Task<List<ShipmentLabelDtoModel>> GetReadonlyShipmentLabelsAsync(string consignmentNumber)
        {
            return _mediator.Send(new ShipmentLabelQuery
            {
                ConsignmentNumber = consignmentNumber
            });
        }



        /// <summary>
        /// Generate a pdf file for the labels
        /// </summary>
        /// <param name="request">Request parameters</param>
        /// <param name="labels">Details of the labels to be printed</param>
        /// <returns></returns>
        private FileViewModel GenerateLabelFile(GenerateConsignmentLabelRequest request, List<ShipmentLabelDtoModel> labels)
        {
            
            
            // Generate a HTML Template
            if (labels.Any())
            {

                var labelMs = _labelGenerator.GenerateLabels(labels, request.Size, request.Copies);

                var data = labelMs.ToArray();

                //TODO: Generate the correct name
                return new FileViewModel() { FileName = $"GS1_Labels.pdf", Bytes = data };
            }

            return null;
        }

       
    }
}
