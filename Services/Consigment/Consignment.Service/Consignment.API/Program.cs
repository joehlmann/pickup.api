﻿using System;
using ConsignmentSvc.API.Infrastructure.Authorization;
using ConsignmentSvc.API.Infrastructure.Configuration;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Serilog;
using Serilog.Core;
using Serilog.Events;

namespace ConsignmentSvc.API
{
    public class Program
    {
        public static void Main(string[] args)
        {
            try
            {
                BuildWebHost(args).Run();
                Log.Information("Starting Consignment.API");
            }
            //catch (Exception ex)
            //{
            //    Log.Fatal(ex, "Consignment.API terminated unexpectedly");
            //}
            finally
            {
                Log.CloseAndFlush();
            }
          
        }
        /// <summary>
        /// Setup the static Log instance
        /// </summary>
        /// <param name="config"></param>
        public static void ConfigureLogger(IConfiguration config)
        {
            var loggingConfig = config.GetSection("Logging");
            var levelSwitch = new LoggingLevelSwitch();
            var seqConnection = loggingConfig["SeqServerUrl"];
            var seqAPIKey = loggingConfig["SeqAPIKey"];
            var idOut = 0;
            Int32.TryParse(loggingConfig["SeqEventBodyLimitBytes"], out idOut);

            //Default event body size
            var eventBodyLimit = idOut > 0 ? idOut : 262144;

            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Verbose()
                .MinimumLevel.Override("Microsoft", LogEventLevel.Warning)
                .MinimumLevel.Override("System", LogEventLevel.Warning)
                .Enrich.FromLogContext()
                .WriteTo.Seq(seqConnection, apiKey: seqAPIKey, controlLevelSwitch: levelSwitch, eventBodyLimitBytes: eventBodyLimit)

                .CreateLogger();


        }
        private static void AddDbConfiguration(WebHostBuilderContext context, IConfigurationBuilder builder)
        {
            var configuration = builder.Build();
            var connectionString = configuration.GetConnectionString("ConfigurationDb");
            builder.AddSqlConfigurationDbProvider(options => SqlServerDbContextOptionsExtensions.UseSqlServer(options, connectionString));
        }
        public static IWebHost BuildWebHost(string[] args)
        {
            IWebHostBuilder builder = WebHost.CreateDefaultBuilder(args)
                .ConfigureLogging((ctx, logging) => { ConfigureLogger(ctx.Configuration); })
                .ConfigureServices(serviceCollection =>
                    serviceCollection.AddSingleton<IAssertions, BorderExpressAssertions>())
                .ConfigureAppConfiguration(AddDbConfiguration)
                .UseSerilog()
                .UseStartup<Startup>();
                //.UseUrls("http://0.0.0.0:5018");

            IWebHost host = builder.Build();

            return host;
        }
       
    }
}
