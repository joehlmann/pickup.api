﻿using Microsoft.AspNetCore.Authorization;

namespace ConsignmentSvc.API.Infrastructure.Authorization
{
    public interface IAssertions
    {
        bool CanReadConsignments(AuthorizationHandlerContext context);
        bool CanWriteConsignments(AuthorizationHandlerContext context);
        bool CanDeleteConsignments(AuthorizationHandlerContext context);

    }
}