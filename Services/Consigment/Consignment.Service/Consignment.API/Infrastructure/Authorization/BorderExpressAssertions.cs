﻿using System.Linq;
using Microsoft.AspNetCore.Authorization;

namespace ConsignmentSvc.API.Infrastructure.Authorization
{
    public class BorderExpressAssertions : IAssertions
    {
        public bool CanReadConsignments(AuthorizationHandlerContext context)
        {
            // Split the scopes string into an array
            var scopes = context.User.Claims.Where(c => c.Type == BorderExpressIdentity.Claims.Scope.Type && c.Issuer == BorderExpressIdentity.Claims.Scope.Issuer).ToList();

            // Succeed if the scope array contains the required scope
            var hasRequiredScopes = scopes.Any(s => (
                // Check if the access token has the read scope
                s.Value == BorderExpressIdentity.Scopes.ReadConsignments.Name && s.Issuer == BorderExpressIdentity.Scopes.ReadConsignments.Issuer
            ));

            return hasRequiredScopes;
        }

        public bool CanWriteConsignments(AuthorizationHandlerContext context)
        {
            // Split the scopes string into an array
            var scopes = context.User.Claims.Where(c => c.Type == BorderExpressIdentity.Claims.Scope.Type && c.Issuer == BorderExpressIdentity.Claims.Scope.Issuer).ToList();

            // Succeed if the scope array contains the required scope
            var hasRequiredScopes = scopes.Any(s => (
                // Check if the access token has the write only scope
                s.Value == BorderExpressIdentity.Scopes.WriteConsignments.Name && s.Issuer == BorderExpressIdentity.Scopes.WriteConsignments.Issuer
            ));

            return hasRequiredScopes;
        }

        public bool CanDeleteConsignments(AuthorizationHandlerContext context)
        {
            // Split the scopes string into an array
            var scopes = context.User.Claims.Where(c => c.Type == BorderExpressIdentity.Claims.Scope.Type && c.Issuer == BorderExpressIdentity.Claims.Scope.Issuer).ToList();

            // Succeed if the scope array contains the required scope
            var hasRequiredScopes = scopes.Any(s => (
                // Check if the access token has the delete only scope
                s.Value == BorderExpressIdentity.Scopes.DeleteConsignments.Name && s.Issuer == BorderExpressIdentity.Scopes.DeleteConsignments.Issuer
            ));

            return hasRequiredScopes;
        }
    }
}
