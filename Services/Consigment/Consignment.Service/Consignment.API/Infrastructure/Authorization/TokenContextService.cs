﻿using ConsignmentSvc.Infrastructure.TokenContext;

namespace ConsignmentSvc.API.Infrastructure.Authorization
{
    public class TokenContextService : ITokenContextService
    {

        private string AccessToken { get; set; }
        public void SetAccessToken(string token)
        {
            AccessToken = token;
        }

        public string GetAccessToken()
        {
            return AccessToken;
        }


    }
}
