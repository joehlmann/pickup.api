﻿using ConsignmentSvc.API.Infrastructure.Authorization.Models;

namespace ConsignmentSvc.API.Infrastructure.Authorization
{
    public class BorderExpressIdentity
    {
        //TODO: This should come from a config as it will need to change with environments
        private static string _issuer = "http://borderexpressidp";
        //private static string _issuer = "https://localhost:5005";
        public static class Scopes
        {
            public static Scope ReadConsignments = new Scope { Name = "consignment|read", Issuer = _issuer };
            public static Scope DeleteConsignments = new Scope { Name = "consignment|delete", Issuer = _issuer };
            public static Scope WriteConsignments = new Scope { Name = "consignment|write", Issuer = _issuer };
        }

        public static class Claims
        {
            public static Claim Scope = new Claim { Issuer = _issuer, Type = "scope", Value = "" };
        }
    }
}
