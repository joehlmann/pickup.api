﻿using System;
using System.Threading.Tasks;
using ConsignmentSvc.Infrastructure.CorrelationContext;
using CorrelationId;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;

namespace ConsignmentSvc.API.Infrastructure.Correlation
{
    public class CorrelationContextMiddleware
    {
        readonly RequestDelegate _next;
        private readonly ILogger<CorrelationContextMiddleware> _logger;


        public CorrelationContextMiddleware(RequestDelegate next, ILogger<CorrelationContextMiddleware> logger)
        {
            _next = next ?? throw new ArgumentNullException(nameof(next));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task Invoke(HttpContext httpContext, ICorrelationContextService correlationService, ICorrelationContextAccessor correlationContext)
        {
            if (httpContext == null) throw new ArgumentNullException(nameof(httpContext));
            if (correlationService == null) throw new ArgumentNullException(nameof(correlationService));

            _logger.LogTrace("Collecting correlation id");
            correlationService.SetCorrelationHeader(correlationContext.CorrelationContext.Header);
            correlationService.SetCorrelationId(correlationContext.CorrelationContext.CorrelationId);

            // Call the next middleware
            await _next(httpContext);
        }
    }
}
