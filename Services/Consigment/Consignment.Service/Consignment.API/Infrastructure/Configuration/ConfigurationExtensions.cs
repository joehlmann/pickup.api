﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace ConsignmentSvc.API.Infrastructure.Configuration
{
    public static class ConfigurationExtensions
    {
        public static IConfigurationBuilder AddSqlConfigurationDbProvider(
            this IConfigurationBuilder configuration, Action<DbContextOptionsBuilder> setup)
        {
            configuration.Add(new SqlDbConfigurationSource(setup));
            return configuration;
        }
    }
}
