﻿using System;
using System.Linq;
using ConsignmentSvc.Infrastructure.Providers;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace ConsignmentSvc.API.Infrastructure.Configuration
{
    public class SqlDbConfigurationProvider : ConfigurationProvider
    {
        private readonly Action<DbContextOptionsBuilder> _options;

        public SqlDbConfigurationProvider(Action<DbContextOptionsBuilder> options)
        {
            _options = options;

        }
        public override void Load()
        {
            var builder = new DbContextOptionsBuilder<ConfigurationDbContext>();
            _options(builder);

            using (var context = new ConfigurationDbContext(builder.Options))
            {
                var items = context.ApplicationConfigurationItem
                    .AsNoTracking()
                    .ToList();

                foreach (var item in items)
                {
                    Data.Add(item.Title, item.Value);
                }
            }
        }
    }
}
