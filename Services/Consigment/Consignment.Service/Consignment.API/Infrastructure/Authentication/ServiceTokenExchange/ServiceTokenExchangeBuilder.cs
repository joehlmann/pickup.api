﻿using System;
using ConsignmentSvc.API.Infrastructure.Authentication.ServiceTokenExchange.Interfaces;
using Microsoft.Extensions.DependencyInjection;

namespace ConsignmentSvc.API.Infrastructure.Authentication.ServiceTokenExchange
{
    public class ServiceTokenExchangeBuilder : IServiceTokenExchangeBuilder
    {
        public ServiceTokenExchangeBuilder(IServiceCollection services)
        {
            Services = services ?? throw new ArgumentNullException(nameof(services));
        }

        public IServiceCollection Services { get; }
    }
}
