﻿using ConsignmentSvc.API.Infrastructure.Authentication.ServiceTokenExchange.Interfaces;
using ConsignmentSvc.API.Infrastructure.Authentication.ServiceTokenExchange.Models;
using Microsoft.Extensions.Caching.Memory;

namespace ConsignmentSvc.API.Infrastructure.Authentication.ServiceTokenExchange.Caches
{
    public class TokenExchangeMemCache : ITokenExchangeCache
    {
        private readonly IMemoryCache _memoryCache;

        public TokenExchangeMemCache(IMemoryCache memoryCache)
        {
            _memoryCache = memoryCache;
        }
        public TokenExchangeResult FindToken(string referenceToken)
        {
            _memoryCache.TryGetValue(referenceToken, out TokenExchangeResult jwtToken);

            return jwtToken;
        }

        public void AddToken(TokenExchangeResult tokenResult)
        {
            _memoryCache.Set(tokenResult.ReferenceToken, tokenResult, tokenResult.AccessTokenExpiryTime);
        }

        public void RemoveToken(TokenExchangeResult tokenResult)
        {
            _memoryCache.Remove(tokenResult.ReferenceToken);
        }
    }
}
