﻿using ConsignmentSvc.API.Infrastructure.Authentication.ServiceTokenExchange.Caches;
using ConsignmentSvc.API.Infrastructure.Authentication.ServiceTokenExchange.Interfaces;
using ConsignmentSvc.API.Infrastructure.Authentication.ServiceTokenExchange.Models;
using ConsignmentSvc.API.Infrastructure.Authentication.ServiceTokenExchange.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace ConsignmentSvc.API.Infrastructure.Authentication.ServiceTokenExchange
{
    public static class ServiceTokenExchangeServiceExtensions
    {
        public static IServiceTokenExchangeBuilder AddServiceTokenExchange(this IServiceCollection services)
        {
            services.AddTransient<IServiceTokenExchangeServiceFactory, ServiceTokenExchangeServiceFactory>();
            services.AddTransient<ITokenExchangeService, ServiceTokenExchangeService>();
            services.AddTransient<ITokenExchangeCache, TokenExchangeMemCache>();
            services.AddTransient<ICachingTokenExchangeService, ServiceTokenExchangeServiceWithCache>();
            services.AddSingleton(serviceProvider =>
            {
                var config = (IConfiguration)serviceProvider.GetService(typeof(IConfiguration));

                var serviceTokenConfig = config.GetSection("ServiceTokenExchange");
                bool.TryParse(serviceTokenConfig["EnableCache"], out var enableTokenCache);

                return new ServiceTokenExchangeOptions
                {
                    IdentityServerRootUrl = serviceTokenConfig["Authority"],
                    UpdateAuthorizationHeader = true,
                    ClientId = serviceTokenConfig["ClientId"],
                    ClientSecret = serviceTokenConfig["ClientSecret"],
                    RequireHttpsEndpoints = false,
                    GrantType = "serviceTokenExchange",
                    EnableCaching = enableTokenCache
                };

            });

            services.AddTransient<HttpClientServiceTokenExchangeDelegatingHandler>();

            return new ServiceTokenExchangeBuilder(services);
        }
    }
}
