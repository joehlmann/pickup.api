﻿using Microsoft.Extensions.DependencyInjection;

namespace ConsignmentSvc.API.Infrastructure.Authentication.ServiceTokenExchange.Interfaces
{
    public interface IServiceTokenExchangeBuilder
    {
        IServiceCollection Services { get; }
    }
}