﻿using ConsignmentSvc.API.Infrastructure.Authentication.ServiceTokenExchange.Models;

namespace ConsignmentSvc.API.Infrastructure.Authentication.ServiceTokenExchange.Interfaces
{
    public interface ITokenExchangeCache
    {
        TokenExchangeResult FindToken(string referenceToken);

        void AddToken(TokenExchangeResult tokenResult);

        void RemoveToken(TokenExchangeResult tokenResult);
    }
}