﻿using System.Threading.Tasks;
using ConsignmentSvc.API.Infrastructure.Authentication.ServiceTokenExchange.Models;

namespace ConsignmentSvc.API.Infrastructure.Authentication.ServiceTokenExchange.Interfaces
{
    public interface ITokenExchangeService
    {
        Task<TokenExchangeResult> ExchangeTokenAsync(string accessToken, ServiceTokenExchangeOptions options);
    }
}