﻿using ConsignmentSvc.Domain.Enumerators;

namespace ConsignmentSvc.Domain.ValueObjects.Barcodes
{
    public class BExSSCCBarcode : Barcode
    {
        public BExSSCCBarcode(string number, BarcodeFormat format) : base(number, format)
        {
        }

        public override string DisplaySSCC => FormatNumberToSSCC(Number);

        private string FormatNumberToSSCC(string number)
        {
            if (number.Substring(0, 2) == "00")
            {
                return $"(00) {number.Substring(3,number.Length - 3)}";
            }

            return number;
        }
    }
}
