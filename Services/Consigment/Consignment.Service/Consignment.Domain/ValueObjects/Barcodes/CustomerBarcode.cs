﻿using ConsignmentSvc.Domain.Enumerators;

namespace ConsignmentSvc.Domain.ValueObjects.Barcodes
{
    public class CustomerBarcode : Barcode
    {
        public CustomerBarcode(string number, BarcodeFormat format) : base(number, format)
        {
        }

        public override string DisplaySSCC => Number;
    }
}
