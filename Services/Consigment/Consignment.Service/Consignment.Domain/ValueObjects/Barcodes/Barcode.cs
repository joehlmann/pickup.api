﻿using ConsignmentSvc.Domain.Enumerators;

namespace ConsignmentSvc.Domain.ValueObjects.Barcodes
{
    public abstract class Barcode
    {
        public abstract string DisplaySSCC { get;}
        public string Number { get; private set; }
        public BarcodeFormat Format { get; private set; }

        public Barcode(string number, BarcodeFormat format)
        {
            Number = number;
            Format = format;
        }
    }
}
