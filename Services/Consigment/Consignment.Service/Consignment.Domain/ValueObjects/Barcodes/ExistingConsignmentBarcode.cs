﻿using ConsignmentSvc.Domain.Enumerators;

namespace ConsignmentSvc.Domain.ValueObjects.Barcodes
{
    public class ExistingConsignmentBarcode : Barcode
    {
        private readonly string _number;
        private readonly string _companyCode;
        public ExistingConsignmentBarcode(string number, BarcodeFormat format, string companyCode) : base($"401{companyCode}{number}", format)
        {
            _number = number;
            _companyCode = companyCode;
        }

        public override string DisplaySSCC => $"(401) {_companyCode} {_number}";
        
    }
}
