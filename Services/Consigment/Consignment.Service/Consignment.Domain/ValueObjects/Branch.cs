﻿namespace ConsignmentSvc.Domain.ValueObjects
{
    public class Branch
    {
        public int Id { get; private set; }
        public string Name { get; private set; }
        public string Abbreviation { get; private set; }
       
        public Branch()
        {
            // Needed for AutoMapper
        }

        public Branch(int id, string name, string abbreviation)
        {
            Id = id;
            Name = name;
            Abbreviation = abbreviation;
            
        }
    }
}
