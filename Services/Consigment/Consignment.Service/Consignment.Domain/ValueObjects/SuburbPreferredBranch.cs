﻿using ConsignmentSvc.Domain.Enumerators;

namespace ConsignmentSvc.Domain.ValueObjects
{
    public class SuburbPreferredBranch
    {
        public int BranchId { get; private set; }
        public bool IsOnforwardEnabled { get; private set; }
        public int Order { get; private set; }
        public PreferredBranchType Type { get; private set; }

        public SuburbPreferredBranch(int branchId, bool isOnforwardEnabled, int order, PreferredBranchType type)
        {
            BranchId = branchId;
            IsOnforwardEnabled = isOnforwardEnabled;
            Order = order;
            Type = type;
        }
    }
}
