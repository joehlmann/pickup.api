﻿namespace ConsignmentSvc.Domain.ValueObjects
{
    public class SuburbRun
    {
        public int DeliveryNumber { get; private set; }
        public int Order { get; private set; }
        public SuburbRun(int deliveryRunNumber, int order)
        {
            DeliveryNumber = deliveryRunNumber;
            Order = order;
        }
    }
}
