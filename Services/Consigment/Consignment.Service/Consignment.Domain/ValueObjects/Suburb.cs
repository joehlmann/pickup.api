﻿using System.Collections.Generic;

namespace ConsignmentSvc.Domain.ValueObjects
{
    public class Suburb
    {
        public int Id { get; private set; }
        public string Name { get; private set; }
        public string Postcode { get; private set; }
        public string State { get; private set; }
        public SuburbPreferredBranch PreferredReceivingBranch { get; private set; }
        public SuburbPreferredBranch PreferredSendingBranch { get; private set; }
        public List<SuburbPreferredBranch> PreferredBranches { get; private set; }
        public List<SuburbRun> Runs { get; private set; }
        public Suburb()
        {
            // Needed for AutoMapper
        }
       
        public Suburb(int id, string name, string state, string postcode, List<SuburbPreferredBranch> preferredBranches, List<SuburbRun> runs, SuburbPreferredBranch preferredSendingBranch, SuburbPreferredBranch preferredReceivingBranch)
        {
            Id = id;
            Name = name;
            Postcode = postcode;
            PreferredBranches = preferredBranches;
            State = state;
            Runs = runs;
            PreferredSendingBranch = preferredSendingBranch;
            PreferredReceivingBranch = preferredReceivingBranch;
        }

        
    }
}
