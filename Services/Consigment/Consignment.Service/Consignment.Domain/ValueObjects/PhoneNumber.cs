﻿using PhoneNumbers;

namespace ConsignmentSvc.Domain.ValueObjects
{
    public class PhoneNumber
    {
        public string Display => FormatPhoneNumber(Number);
        public string Number { get; private set; }

        public PhoneNumber(string number)
        {
            Number = number;
        }

        private string FormatPhoneNumber(string number)
        {
            var phoneUtil = PhoneNumberUtil.GetInstance();
            var output = "";
            try
            {
                var phoneNumber = phoneUtil.Parse(number, "AU");
                if (phoneUtil.IsValidNumber(phoneNumber))
                {
                    output = phoneUtil.Format(phoneNumber, PhoneNumberFormat.NATIONAL);
                }
            }
            catch (NumberParseException)
            {

            }

            return output;


        }
    }
}
