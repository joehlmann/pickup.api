﻿namespace ConsignmentSvc.Domain.Enumerators
{
    public enum BarcodeType
    {
        Customer = 0,
        BEXSSCC = 1,
        Virtual = 2,
        Other = 3,
        PrePrint = 4,
        GS1GIN = 5,
        ExistingCN = 6,
        Consolidated = 7,
        ManifestLabel = 8
    }
}