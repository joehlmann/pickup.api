﻿namespace ConsignmentSvc.Domain.Enumerators
{
    public enum PreferredBranchType
    {
        Unknown = 'U',
        Sender = 'S',
        Receiver = 'R'
    }
}
