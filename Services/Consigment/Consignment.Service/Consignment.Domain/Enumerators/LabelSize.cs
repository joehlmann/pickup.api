﻿namespace ConsignmentSvc.Domain.Enumerators
{
    public enum LabelSize
    {
        A5 = 1,
        A6 = 2
    }
}