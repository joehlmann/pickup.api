﻿namespace ConsignmentSvc.Domain.Infrastructure
{
    public static class StringExtensionMethods
    {
        public static bool IsPresent(this string value)
        {
            return !string.IsNullOrWhiteSpace(value);
        }

        public static string Obfuscate(this string value)
        {
            var last4Chars = "****";
            if (value.IsPresent() && value.Length > 4)
            {
                last4Chars = value.Substring(value.Length - 4);
            }

            return "****" + last4Chars;
        }
    }
}
