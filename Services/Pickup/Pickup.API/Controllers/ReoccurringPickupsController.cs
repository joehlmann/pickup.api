﻿using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using PickupSvc.API.Application.Commands.Pickups;
using Polly.CircuitBreaker;

namespace PickupSvc.API.Controllers
{
    [ApiVersion("1")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    public class ReoccurringPickupsController : ControllerBase
    {
        private readonly IMediator _mediator;
        public ReoccurringPickupsController(IMediator mediator)
        {
            _mediator = mediator;
        }
        // POST: api/Pickup
        [HttpPost]
        [ApiExplorerSettings(IgnoreApi = true)]
        public async Task<IActionResult> PostCreatePickupRequest([FromBody] PickupCreateReoccurringRequest pickup)
        {
            //TODO: Remove [ApiExplorerSettings(IgnoreApi = true)] when implemented so it shows in swagger

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var results = await _mediator.Send(pickup);

                if (results.Errors.Any())
                {
                    return BadRequest(results.Errors);
                }

                return CreatedAtAction("Get", new { id = results.Id }, results);
            }
            catch (BrokenCircuitException e)
            {
                return StatusCode(408);
            }
        }

        // GET: api/Pickup/5
        [HttpGet("{id}")]
        [ApiExplorerSettings(IgnoreApi = true)]
        public async Task<IActionResult> Get([FromRoute] int id)
        {
            //TODO: Remove [ApiExplorerSettings(IgnoreApi = true)] when implemented so it shows in swagger
            if (!(id > 0))
            {
                return BadRequest("id must be greater than 0");
            }

            //var pickup = await _pickupRepository.GetAsync(id);

            //if (pickup == null)
            //{
            //    return NotFound();
            //}

            return BadRequest();
        }

    }
}