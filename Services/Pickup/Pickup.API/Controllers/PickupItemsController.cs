﻿using System;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PickupSvc.API.Application.Commands.PickupItems;
using PickupSvc.API.Application.CommandsQueries.PickupItems;
using PickupSvc.API.Application.DTOs;
using PickupSvc.Infrastructure.Repositories.Interfaces;
using Polly.CircuitBreaker;

namespace PickupSvc.API.Controllers
{
    
    [ApiVersion("1")]
    [Route("api/v{version:apiVersion}/[controller]/{pickupNumber}/items")]
    [ApiController]
    public class PickupItemsController : ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly IPickupRepository _pickupRepository;
        private readonly IMapper _mapper;
        private readonly ILogger<PickupItemsController> _logger;

        public PickupItemsController(IMediator mediator, IPickupRepository pickupRepository, IMapper mapper, ILogger<PickupItemsController> logger)
        {
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
            _pickupRepository = pickupRepository ?? throw new ArgumentNullException(nameof(pickupRepository));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));

        }
        // POST: api/PickupItems
        [HttpPost]
        public async Task<IActionResult> PostAddPickupItemRequest([FromRoute] int pickupNumber, [FromBody] AddPickupItemDTO pickupItem)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var pickupItemRequest = _mapper.Map<PickupItemAddCommand>(pickupItem);
            pickupItemRequest.PickupNumber = pickupNumber;

            try
            {
                var results = await _mediator.Send(pickupItemRequest);

                if (results.Errors.Any())
                {
                    return BadRequest(results.Errors);
                }

                return CreatedAtAction("Get", new { id = results.Id }, results);
            }
            catch (BrokenCircuitException e)
            {
                return StatusCode(408);
            }
        }

        [HttpPut("{id}")]
        [ApiExplorerSettings(IgnoreApi = true)]
        public async Task<IActionResult> UpdatePickupItemRequest([FromRoute] string pickupNumber, [FromRoute] int id, [FromBody] AddPickupItemDTO pickupItem)
        {
            //TODO: remove [ApiExplorerSettings(IgnoreApi = true)] when implemented so it shows in swagger
            return BadRequest();
        }
        
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeletePickupItemRequest([FromRoute] int id)
        {
            try
            {
                var result = await _mediator.Send(new PickupItemRemoveCommand(id, 0));
            

                if (result.Errors.Any())
                {
                    return BadRequest(result.Errors);
                }

                return Ok();
            }
            catch (BrokenCircuitException e)
            {
                return StatusCode(408);
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get([FromRoute] int id)
        {
            try
            {
                var result = await _mediator.Send(new PickupItemByIdQuery(id));
                

                if (result.Id !=0)
                {
                    return NotFound();
                }

                return Ok(result);
            }
            catch (BrokenCircuitException e)
            {
                return StatusCode(408);
            }
        }
        //[HttpGet]
        //public async Task<IActionResult> Get([FromRoute] int pickupNumber)
        //{
        //    try
        //    {
        //        var result = await _mediator.Send(new PickupItemsByNumberQuery
        //        {
        //            PickupNumber = pickupNumber
        //        });

        //        if (!result.Any())
        //        {
        //            return NotFound();
        //        }

        //        return Ok(result);
        //    }
        //    catch (BrokenCircuitException e)
        //    {
        //        return StatusCode(408);
        //    }
        //}
    }
}