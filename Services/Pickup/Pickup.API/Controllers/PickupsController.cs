﻿using System;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PickupSvc.API.Application.Commands.Pickups;
using PickupSvc.API.Application.CommandsQueries.Pickups;
using PickupSvc.API.Application.DTOs;
using PickupSvc.Infrastructure.Repositories.Interfaces;
using Polly.CircuitBreaker;

namespace PickupSvc.API.Controllers
{
    [ApiVersion("1")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    public class PickupsController : ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly IMapper _mapper;
        private readonly IPickupRepository _pickupRepository;
        private readonly ILogger<PickupsController> _logger;
        
        public PickupsController(IMediator mediator, IMapper mapper, IPickupRepository pickupRepository,ILogger<PickupsController> logger)
        {
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
            _pickupRepository = pickupRepository ?? throw new ArgumentNullException(nameof(pickupRepository));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }
        

        // GET: pickups/5
        [ProducesResponseType(typeof(PickupDTO),200)]
        [HttpGet("{number}")]
        public async Task<IActionResult> Get([FromRoute] int number)
        {
            try
            {
                var result = await _mediator.Send(new PickupByNumberQuery(number));
                

                if (result.Number == 0)
                {
                    return NotFound();
                }

                return Ok(result);
            }
            catch (BrokenCircuitException)
            {
                return StatusCode(408);
            }
        }

        // POST: api/Pickup
        [HttpPost]
        public async Task<IActionResult> PostCreatePickupRequest([FromBody] PickupCreateCommand pickup)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var results = await _mediator.Send(pickup);

                if (results.Errors.Any())
                {
                    return BadRequest(results.Errors);
                }

                return CreatedAtAction("Get", new { number = results.Number }, results);
            }
            catch (BrokenCircuitException)
            {
                return StatusCode(408);
            }
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        [ApiExplorerSettings(IgnoreApi = true)]
        public async Task<IActionResult> PutCreatePickupRequest([FromRoute] int id, [FromBody] PickupCreateCommand createPickupRequest)
        {
            
            
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            createPickupRequest.Id = id;
            //_context.Entry(createPickupRequest).State = EntityState.Modified;
            
            

            var pickup = await _pickupRepository.GetAsync(id);
            //var pickup = _mapper.Map<Pickup>(pickupDb);

           // pickup.SetPickupWindow("07:00","13:00");

            //_mapper.Map(pickup, pickupDb);
            //pickupDb.ReadyTime = DateTime.Now;
            _pickupRepository.Update(pickup);

            await _pickupRepository.UnitOfWork.SaveEntitiesAsync();
            //try
            //{
            //    await _context.SaveChangesAsync();
            //}
            //catch (DbUpdateConcurrencyException)
            //{
            //    if (!CreatePickupRequestExists(id))
            //    {
            //        return NotFound();
            //    }
            //    else
            //    {
            //        throw;
            //    }
            //}

            return NoContent();
        }
        

        
    }
}
