﻿using System.Collections.Generic;
using PickupSvc.API.Application.DTOs;
using PickupSvc.TheDomain.Entities;

namespace PickupSvc.API.Extensions
{
    public static class PickupItemExtensions
    {
        public static IEnumerable<PickupItemDTO> ToPickupItemDTOs(this IEnumerable<PickupItem> items)
        {
            foreach (var item in items)
            {
                yield return item.ToPickupItemDTO();
            }
        }


        public static PickupItemDTO ToPickupItemDTO(this PickupItem item)
        {
            return new PickupItemDTO();
        }

    }
}
