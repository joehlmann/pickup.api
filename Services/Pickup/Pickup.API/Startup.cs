﻿using System;
using System.Data.Common;
using System.IO;
using System.Net.Http;
using System.Reflection;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using CacheManager.Core;
using CorrelationId;
using EFSecondLevelCache.Core;
using EventBusAbstract;
using EventBusAbstract.Abstractions;
using EventBusAbstract.Settings;
using IdentityServer4.AccessTokenValidation;
using IntegrationEventLog.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.PlatformAbstractions;
using Newtonsoft.Json;
using PickupService.BuildingBlocks.EventBusRabbitMQ;
using PickupSvc.API.Application.DomainServices.Interfaces;
using PickupSvc.API.Application.IntegrationEvents;
using PickupSvc.API.Application.IntegrationEvents.EventHandlers;
using PickupSvc.API.Application.IntegrationEvents.EventHandlers.NewPickupBranchSet;
using PickupSvc.API.Application.IntegrationEvents.EventHandlers.PickupBooked;
using PickupSvc.API.Application.IntegrationEvents.EventHandlers.ReceiverAddressOnPickupChanged;
using PickupSvc.API.Application.IntegrationEvents.Events;
using PickupSvc.API.Application.IntegrationEvents.Events.ReceiverAddressOnPickupChanged;
using PickupSvc.API.Filters;
using PickupSvc.API.Infrastructure.Authentication.ServiceTokenExchange;
using PickupSvc.API.Infrastructure.Authorization;
using PickupSvc.API.Infrastructure.DelegateHandlers;
using PickupSvc.API.Infrastructure.Middleware.Authentication.AccessTokenContext;
using PickupSvc.API.Infrastructure.Middleware.Logging;
using PickupSvc.API.Infrastructure.Modules;
using PickupSvc.API.Settings;
using PickupSvc.API.Swagger;
using PickupSvc.Infrastructure;
using PickupSvc.Infrastructure.HttpClients;
using PickupSvc.Infrastructure.Repositories;
using PickupSvc.Infrastructure.Repositories.InMemory;
using PickupSvc.Infrastructure.Repositories.Interfaces;
using PickupSvc.Infrastructure.TokenContext;
using Polly;
using Polly.Extensions.Http;
using Polly.Timeout;
using RabbitMQ.Client;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace PickupSvc.API
{
    public class Startup
    {
        private readonly IConfiguration _configuration;
        private readonly IWebHostEnvironment _hostingEnvironment;
        

        public Startup(IConfiguration configuration, IWebHostEnvironment hostingEnvironment)
        {
            _configuration = configuration;
            _hostingEnvironment = hostingEnvironment;
        
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services
                .AddCustomMVC(_configuration)
                .AddSwaggerVer()
                .AddCustomOptions(_configuration)
                .AddCustomDbContext(_configuration, _hostingEnvironment)
                .AddHttpClients(_configuration, _hostingEnvironment)
                //.AddMediatR(typeof(CreatePickupCommand).GetTypeInfo().Assembly,typeof(BookedPickupEvent).GetTypeInfo().Assembly)
                .AddIntegrationServices(_configuration)
                .AddEventBus(_configuration)
                .AddCorrelationId();

            

            services.AddRepositories(_configuration);

            services.AddCustomServices(_configuration);

            services.AddCustomAuthentication(_configuration);


            
            //cache
            services.AddEFSecondLevelCache();
            services.AddRedisCacheServiceProvider();


            services.AddIntegrationEventHandlers();
            
            
           
          
            // AutoFac
            var container = new ContainerBuilder();
            container.Populate(services);


            container.RegisterModule(new MediatorModule());
            container.RegisterModule(new ApplicationModule(container,_configuration.GetConnectionString("PickupDb")));

            return new AutofacServiceProvider(container.Build());

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, IApiVersionDescriptionProvider provider)
        {
            app.UseCorrelationId("request-id");
            app.UseMiddleware<SerilogMiddleware>();
            

            if (env.IsDevelopment())
            {
                HibernatingRhinos.Profiler.Appender.EntityFramework.EntityFrameworkProfiler.Initialize();
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }



            // Populate Seed Repo
            var context = (PickupContext)app
                .ApplicationServices.GetService(typeof(PickupContext));

            PickupContextSeed.SeedAsync(context)
                .Wait();


            app.UseAuthentication();
            app.UseMiddleware<AccessTokenContextMiddleware>();
            //app.UseHttpsRedirection();
            app.UseMvc();


            app.UseSwagger();
            app.UseSwaggerUI(
                options =>
                {
                    // build a swagger endpoint for each discovered API version
                    foreach (var description in provider.ApiVersionDescriptions)
                    {
                        options.SwaggerEndpoint($"/swagger/{description.GroupName}/swagger.json", description.GroupName.ToUpperInvariant());
                    }
                });

            ConfigureEventBus(app);
        }

        
        private void ConfigureEventBus(IApplicationBuilder app)
        {
            var eventBus = app.ApplicationServices.GetRequiredService<IEventBus>();

            eventBus.Subscribe<PickupBookedIntegrationEvent, IIntegrationEventHandler<PickupBookedIntegrationEvent>>();
            eventBus.Subscribe<NewPickupBranchSetIntegrationEvent, NewPickupBranchSetSetReceiverDetailsIntegrationEventHandler>();
            eventBus.Subscribe<PickupBranchChangedIntegrationEvent, PickupBranchChangedIntegrationEventHandler>();
            eventBus.Subscribe<ReceiverAddressOnPickupChangedIntegrationEvent, ReceiverAddressOnPickupChangedSetReceivingRegionIntegrationEventHandler>();
        }

        
    }
    static class CustomExtensionsMethods
    {

        public static IServiceCollection AddSwaggerVer(this IServiceCollection services)
        {
            services.AddApiVersioning(
                options =>
                {
                    // reporting api versions will return the headers "api-supported-versions" and "api-deprecated-versions"
                    options.ReportApiVersions = true;
                });
            services.AddVersionedApiExplorer(
                options =>
                {
                    // add the versioned api explorer, which also adds IApiVersionDescriptionProvider service
                    // note: the specified format code will format the version as "'v'major[.minor][-status]"
                    options.GroupNameFormat = "'v'VVV";

                    // note: this option is only necessary when versioning by url segment. the SubstitutionFormat
                    // can also be used to control the format of the API version in route templates
                    options.SubstituteApiVersionInUrl = true;
                });
            services.AddTransient<IConfigureOptions<SwaggerGenOptions>, ConfigureSwaggerOptions>();
            services.AddSwaggerGen(
                options =>
                {
                    // add a custom operation filter which sets default values
                    options.OperationFilter<SwaggerDefaultValues>();

                    // integrate xml comments
                    options.IncludeXmlComments(XmlCommentsFilePath);
                });

            return services;
        }


        private static string XmlCommentsFilePath
        {
            get
            {
                var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                var fileName = typeof(Startup).GetTypeInfo().Assembly.GetName().Name + ".xml";
                return Path.Combine(basePath, fileName);
            }
        }

        

        public static IServiceCollection AddRedisCacheServiceProvider(this IServiceCollection services)
        {
            var settings = services.BuildServiceProvider().GetRequiredService<IOptions<CacheConfig>>().Value;

            var jss = new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore,
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            };


            string redisConfigurationKey = settings.RedisConfigurationKey;
            services.AddSingleton(typeof(ICacheManagerConfiguration),
                new CacheManager.Core.ConfigurationBuilder()
                    .WithJsonSerializer(serializationSettings: jss, deserializationSettings: jss)
                    .WithUpdateMode(CacheUpdateMode.Up)
                    .WithRedisConfiguration(redisConfigurationKey, config =>
                    {
                        config.WithAllowAdmin()
                            .WithDatabase(0)
                            .WithEndpoint(settings.RedisConnection, int.Parse(settings.RedisPort));
                    })
                    .WithMaxRetries(100)
                    .WithRetryTimeout(50)
                    .WithRedisCacheHandle(redisConfigurationKey)
                    .WithExpiration(ExpirationMode.Absolute, TimeSpan.FromMinutes(10))
                    .Build());
            services.AddSingleton(typeof(ICacheManager<>), typeof(BaseCacheManager<>));

            return services;
        }

        public static IServiceCollection AddCustomAuthentication(this IServiceCollection services, IConfiguration configuration)
        {

            var assertions = services.BuildServiceProvider().GetRequiredService<IAssertions>();

            services.AddAuthentication(IdentityServerAuthenticationDefaults.AuthenticationScheme)
                .AddIdentityServerAuthentication(
                    options =>
                    {
                        var config = configuration.GetSection("Authentication");

                        options.Authority = config["Authority"];
                        options.ApiName = config["APIName"];
                        options.SupportedTokens = SupportedTokens.Jwt;
                        options.RoleClaimType = "role";

                    });
            services.AddBExAuthorization(assertions);
            services.AddServiceTokenExchange();

            return services;
        }


        public static IServiceCollection AddCustomServices(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddScoped<ITokenContextService, TokenContextService>();

            services.AddScoped<IPickupNumberService, Application.DomainServices.PickupNumberService>();
            services.AddTransient<HttpClientCorrelationIdDelegatingHandler>();

            services.AddSingleton<IAssertions, BorderExpressAssertions>();

            // Singleton services
            //TODO: Change this to a threadsafe cache
            //services.AddSingleton<IPickupNumbersCache, InMemoryPickupNumbersCache>();
            return services;
        }

        public static IServiceCollection AddRepositories(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddScoped<ICustomerRepository, InMemoryCustomerRepository>();

            //TODO: Implement HttpUserRepository
            //TODO: Implement HttpDangerousGoodRepository
            // TODO: Implement HttpCustomerRepository
            services.AddScoped<IUserRepository, InMemoryUserRepository>();
            services.AddScoped<ISuburbRepository, HttpSuburbRepository>();
            services.AddScoped<IPickupRepository, PickupRepository>();
            services.AddScoped<IDangerousGoodRepository, InMemoryDangerousGoodRepository>();


            return services;
        }


        /// <summary>
        /// Configure Entity frameworks db contexts
        /// </summary>
        /// <param name="services"></param>
        /// <param name="configuration"></param>
        /// <param name="hostingEnvironment"></param>
        /// <returns></returns>
        public static IServiceCollection AddCustomDbContext(this IServiceCollection services,
            IConfiguration configuration, IWebHostEnvironment hostingEnvironment)
        {

            var settings = services.BuildServiceProvider().GetRequiredService<IOptions<ConnectionStringConfig>>().Value;

            PickupSvc.Infrastructure.Startup.ConfigureServices(
                services,
                settings.PickupDb);

            return services;
        }

        public static IServiceCollection AddCustomMVC(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddMvc(options =>
                {
                    options.Filters.Add(typeof(HttpGlobalExceptionFilter));
                    options.EnableEndpointRouting = false;
                })
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_2)
                .AddControllersAsServices();

            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                    builder => builder
                        .SetIsOriginAllowed((host) => true)
                        .AllowAnyMethod()
                        .AllowAnyHeader()
                        .AllowCredentials());
            });

            return services;
        }


        public static IServiceCollection AddCustomOptions(this IServiceCollection services,
            IConfiguration configuration)
        {
            services.Configure<EventBusConfig>(configuration.GetSection(ConfigSection.EventBusConfig));
            services.Configure<ConnectionStringConfig>(configuration.GetSection(ConfigSection.ConnectionStrings));
            services.Configure<CacheConfig>(configuration.GetSection(ConfigSection.CacheConfig));
            services.Configure<ApplicationConfig>(configuration.GetSection(ConfigSection.ApplicationConfig));

            var settings = services.BuildServiceProvider().GetRequiredService<IOptions<ApplicationConfig>>().Value;

            services.AddTransient<IApplicationConfig>(a=> new ApplicationConfig
            {
                MaximumNumberOfPickupRenumberAttempts = settings.MaximumNumberOfPickupRenumberAttempts,
                PickupNumberGenerationBatchSize = settings.PickupNumberGenerationBatchSize
            });

            return services;
        }

        public static IServiceCollection AddIntegrationServices(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddTransient<IPickupIntegrationEventService, PickupIntegrationEventService>();

            services.AddTransient<Func<DbConnection, IIntegrationEventLogService>>(
                sp => (DbConnection c) => new IntegrationEventLogService(c));

            services.AddSingleton<IRabbitMQPersistentConnection>(sp =>
            {
                var settings = sp.GetRequiredService<IOptions<EventBusConfig>>().Value;
                var logger = sp.GetRequiredService<ILogger<DefaultRabbitMQPersistentConnection>>();

                var factory = new ConnectionFactory()
                {
                    HostName = settings.EventBusConnection,
                    DispatchConsumersAsync = true
                };

                if (!string.IsNullOrEmpty(settings.EventBusUserName))
                {
                    factory.UserName = settings.EventBusUserName;
                }

                if (!string.IsNullOrEmpty(settings.EventBusPassword))
                {
                    factory.Password = settings.EventBusPassword;
                }

                var retryCount = 5;
                if (!string.IsNullOrEmpty(settings.EventBusRetryCount))
                {
                    retryCount = int.Parse(settings.EventBusRetryCount);
                }

                string amqpPort = "5672";
                if (!string.IsNullOrEmpty(settings.EventBusEnvVarNodePort))
                {
                    amqpPort = settings.EventBusEnvVarNodePort;
                    factory.Port = int.Parse(settings.EventBusEnvVarNodePort);
                }


                if (!string.IsNullOrEmpty(settings.EventBusConnection))
                {
                    factory.HostName = settings.EventBusConnection;
                }

                string vhost = @"";



                var rabbitUri = $"amqp://{factory.UserName}:{factory.Password}@{factory.HostName}:{amqpPort}/{vhost}";

                factory.Uri = new Uri(rabbitUri);


                return new DefaultRabbitMQPersistentConnection(factory, logger, retryCount);
            });

            return services;
        }

        public static IServiceCollection AddIntegrationEventHandlers(this IServiceCollection services)
        {
            
            services.AddTransient<PickupBookedUpdatePickupBranchIntegrationEventHandler>();
            services.AddTransient<NewPickupBranchSetSetReceiverDetailsIntegrationEventHandler>();
            services.AddTransient<PickupBranchChangedIntegrationEventHandler>();
            services.AddTransient<ReceiverAddressOnPickupChangedSetReceivingRegionIntegrationEventHandler>();
            return services;
        }

        public static IServiceCollection AddHttpClients(this IServiceCollection services,
            IConfiguration configuration, IWebHostEnvironment hostingEnvironment)
        {
            var userAgent = $"Pickup.API-{hostingEnvironment.EnvironmentName}";
            
            var suburbServiceUri = configuration.GetConnectionString("SuburbHttpUri");
            
            if (!string.IsNullOrEmpty(suburbServiceUri))
            {
                var serviceTokenConfig = configuration.GetSection("ServiceTokenExchange");
                bool.TryParse(serviceTokenConfig["EnableCache"], out var enableTokenCache);

                //services.AddTransient<SuburbHttpClient>((s) =>
                //{
                //    var factory = s.GetRequiredService<IHttpMessageHandlerFactory>();
                //    var handler = factory.CreateHandler(nameof(SuburbHttpClient));

                //    var otherHandler = s.GetRequiredService<HttpClientServiceTokenExchangeDelegatingHandler>();
                //    otherHandler.InnerHandler = handler;
                //    return new SuburbHttpClient(new HttpClient(otherHandler, disposeHandler: false));
                //});

                // A specific SuburbHttpClient is required due to a limitation in the DI Scope, should be fixed in version 2.2
                // https://github.com/aspnet/HttpClientFactory/issues/134#issuecomment-412597323
                services.AddHttpClient<SuburbHttpClient, SuburbHttpClient>((client) =>
                    {


                        client.BaseAddress = new Uri(suburbServiceUri);
                        client.DefaultRequestHeaders.Add("Accept", "application/json");
                        client.DefaultRequestHeaders.Add("User-Agent", userAgent);

                    })
                    .AddHttpMessageHandler<HttpClientCorrelationIdDelegatingHandler>()
                    .AddHttpMessageHandler<HttpClientServiceTokenExchangeDelegatingHandler>()
                    .AddPolicyHandler(GetRetryPolicy())
                    .AddPolicyHandler(GetCircuitBreakerPolicy())
                    .AddPolicyHandler(GetTimeoutPolicy());
            }
           

            //TODO: Investigate adding Sql Retry 
            //Configuring Connection Resiliency: https://docs.microsoft.com/en-us/ef/core/miscellaneous/connection-resiliency 
            //sqlOptions.EnableRetryOnFailure(maxRetryCount: 10, maxRetryDelay: TimeSpan.FromSeconds(30), errorNumbersToAdd: null);
            return services;
        }

        public static IServiceCollection AddEventBus(this IServiceCollection services, IConfiguration configuration)
        {
            var settings = services.BuildServiceProvider().GetRequiredService<IOptions<EventBusConfig>>().Value;
            var subscriptionClientName = settings.SubscriptionClientName;
            if (subscriptionClientName == null)
            {
                throw new ArgumentNullException(nameof(subscriptionClientName));
            }
            
                services.AddSingleton<IEventBus, EventBusRabbitMQ>(serviceProvider =>
                {
                    var rabbitMQPersistentConnection = serviceProvider.GetRequiredService<IRabbitMQPersistentConnection>();
                    var iLifetimeScope = serviceProvider.GetRequiredService<ILifetimeScope>();
                    var logger = serviceProvider.GetRequiredService<ILogger<EventBusRabbitMQ>>();
                    var eventBusSubscriptionsManager = serviceProvider.GetRequiredService<IEventBusSubscriptionsManager>();

                    var retryCount = 5;
                    if (!string.IsNullOrEmpty(settings.EventBusRetryCount))
                    {
                        retryCount = int.Parse(settings.EventBusRetryCount);
                    }

                    return new EventBusRabbitMQ(rabbitMQPersistentConnection, logger, iLifetimeScope, eventBusSubscriptionsManager, subscriptionClientName, retryCount);
                });
            

            services.AddSingleton<IEventBusSubscriptionsManager, InMemoryEventBusSubscriptionsManager>();

            return services;
        }

        /// <summary>
        /// Configure authorization for a border express service
        /// </summary>
        /// <param name="services"></param>
        /// <param name="assertions"></param>
        /// <returns></returns>
        public static IServiceCollection AddBExAuthorization(this IServiceCollection services,
            IAssertions assertions)
        {
            services.AddAuthorization(options =>
            {
                options.AddPolicy("ReadPickups",builder => builder.RequireAssertion(context => assertions.CanReadPickups(context)));
                options.AddPolicy("WritePickups",builder => builder.RequireAssertion(context => assertions.CanWritePickups(context)));
                options.AddPolicy("DeletePickups",builder => builder.RequireAssertion(context => assertions.CanDeletePickups(context)));
            });

            return services;
        }
        static IAsyncPolicy<HttpResponseMessage> GetRetryPolicy()
        {
            return HttpPolicyExtensions
                .HandleTransientHttpError()
                .Or<TimeoutRejectedException>() // TimeoutRejectedException from Polly's TimeoutPolicy
                .WaitAndRetryAsync(3, retryAttempt => TimeSpan.FromSeconds(Math.Pow(2, retryAttempt)));
                

        }
        static IAsyncPolicy<HttpResponseMessage> GetCircuitBreakerPolicy()
        {
            return HttpPolicyExtensions
                .HandleTransientHttpError()
                .CircuitBreakerAsync(5, TimeSpan.FromSeconds(30));
        }

        static IAsyncPolicy<HttpResponseMessage> GetTimeoutPolicy()
        {
            return Policy.TimeoutAsync<HttpResponseMessage>(2);
        }
    }
}
