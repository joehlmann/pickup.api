﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Serilog;
using Serilog.Events;

namespace PickupSvc.API.Infrastructure.Middleware.Logging
{
    public class SerilogMiddleware
    {
        const string MessageTemplate = "HTTP {RequestMethod} {RequestPath} responded {StatusCode} in {ElapsedMilliseconds:0.0000} ms";

        static readonly ILogger Log = Serilog.Log.ForContext<SerilogMiddleware>();

        static readonly HashSet<string> HeaderWhitelist = new HashSet<string> { "Content-Type", "Content-Length", "User-Agent" };

        readonly RequestDelegate _next;
        private readonly IConfiguration _config;

        public SerilogMiddleware(RequestDelegate next, IConfiguration config)
        {
            _next = next ?? throw new ArgumentNullException(nameof(next));
            _config = config;
        }

        // ReSharper disable once UnusedMember.Global
        public async Task Invoke(HttpContext httpContext)
        {
            if (httpContext == null) throw new ArgumentNullException(nameof(httpContext));
            Stream originalResponseBody = httpContext.Response.Body;
            Stream originalRequestBody = httpContext.Request.Body;

            var start = Stopwatch.GetTimestamp();
            var startTimestamp = DateTimeOffset.Now;
            var responseBody = "";
            var requestBody = "";
            bool.TryParse(_config["Logging:IncludeBody"], out var verboseBodyLogging);

            try
            {
                using (var memStreamRequest = new MemoryStream())
                using (var memStreamResponse = new MemoryStream())
                using (var streamReaderRequest = new StreamReader(memStreamRequest))
                using (var streamReaderResponse = new StreamReader(memStreamResponse))
                {

                    if (verboseBodyLogging)
                    {
                        // Hold the original request stream so we can work on it locally
                        await httpContext.Request.Body.CopyToAsync(memStreamRequest);
                        memStreamRequest.Seek(0, SeekOrigin.Begin);

                        // Read the placeholder stream to get the data
                        requestBody = ParseRequestBodyText(await streamReaderRequest.ReadToEndAsync());

                        memStreamRequest.Seek(0, SeekOrigin.Begin);

                        // Swap in the temporary streams
                        httpContext.Request.Body = memStreamRequest;
                        httpContext.Response.Body = memStreamResponse;
                    }


                    // Call the next middleware
                    await _next(httpContext);

                    if (verboseBodyLogging)
                    {
                        // Swap back to the original request stream
                        httpContext.Request.Body = originalRequestBody;

                        // Start working on the temporary response stream
                        memStreamResponse.Position = 0;
                        responseBody = ParseResponseBodyText(await streamReaderResponse.ReadToEndAsync());

                        memStreamResponse.Position = 0;

                        // Checking for size prevents exceptions on cached static file requests...
                        if (!string.IsNullOrEmpty(responseBody))
                        {
                            await memStreamResponse.CopyToAsync(originalResponseBody);
                        }
                    }
                    else
                    {
                        responseBody = "Logging body disabled";
                        requestBody = "Logging body disabled";
                    }

                    var stopTimestamp = DateTimeOffset.Now;
                    var elapsedMs = GetElapsedMilliseconds(start, Stopwatch.GetTimestamp());

                    var statusCode = httpContext.Response?.StatusCode;
                    var level = statusCode > 499 ? LogEventLevel.Error : LogEventLevel.Information;

                    var log = level == LogEventLevel.Error ? LogForErrorContext(httpContext) : LogContext(httpContext, startTimestamp, stopTimestamp, requestBody, responseBody);

                    log.Write(level, MessageTemplate, httpContext.Request.Method, GetPath(httpContext), statusCode, elapsedMs);
                }
            }
            // Never caught, because `LogException()` returns false.
            catch (Exception ex) when (LogException(httpContext, GetElapsedMilliseconds(start, Stopwatch.GetTimestamp()), ex)) { }
            finally
            {
                if (verboseBodyLogging)
                {
                    httpContext.Response.Body = originalResponseBody;
                }
            }
        }

        static bool LogException(HttpContext httpContext, double elapsedMs, Exception ex)
        {
            LogForErrorContext(httpContext)
                .Error(ex, MessageTemplate, httpContext.Request.Method, GetPath(httpContext), 500, elapsedMs);

            return false;
        }

        static ILogger LogForErrorContext(HttpContext httpContext)
        {
            var request = httpContext.Request;

            var loggedHeaders = request.Headers
                .Where(h => HeaderWhitelist.Contains(h.Key))
                .ToDictionary(h => h.Key, h => h.Value.ToString());

            var result = Log
                .ForContext("RequestHeaders", loggedHeaders, destructureObjects: true)
                .ForContext("RequestHost", request.Host)
                .ForContext("RequestProtocol", request.Protocol);

            return result;
        }
        static ILogger LogContext(HttpContext httpContext, DateTimeOffset? requestTimeStamp, DateTimeOffset responseTimeStamp, string requestBody, string responseBody)
        {
            var request = httpContext.Request;
            var response = httpContext.Response;

            // Extract the header detail
            var requestHeaders = SerializeHeaders(request.Headers);
            var responseHeaders = SerializeHeaders(response.Headers);

            //If the request come through a load balancer, check if there is an original IP to store
            var sourceIp = request.Headers["X-Forwarded-For"];

            var result = Log
                .ForContext("RequestHeaders", requestHeaders, destructureObjects: true)
                .ForContext("RequestTimestamp", requestTimeStamp)
                .ForContext("RequestProtocol", request.Protocol)
                .ForContext("RequestHost", request.Host)
                .ForContext("RequestContentType", request.ContentType)
                .ForContext("RequestBody", requestBody)
                .ForContext("RequestUrl", request.GetDisplayUrl())
                .ForContext("ResponseTimestamp", responseTimeStamp)
                .ForContext("ResponseHeaders", responseHeaders, destructureObjects: true)
                .ForContext("ResponseContentType", response.ContentType)
                .ForContext("ResponseBody", responseBody)
                .ForContext("Machine", Environment.MachineName)
                .ForContext("IPAddress",
                    string.IsNullOrWhiteSpace(sourceIp.ToString()) ? request.HttpContext.Features.Get<IHttpConnectionFeature>()?.RemoteIpAddress
                        ?.ToString() : sourceIp.ToString());


            return result;
        }
        static double GetElapsedMilliseconds(long start, long stop)
        {
            return (stop - start) * 1000 / (double)Stopwatch.Frequency;
        }

        static string GetPath(HttpContext httpContext)
        {
            return httpContext.Features.Get<IHttpRequestFeature>()?.RawTarget ?? httpContext.Request.Path.ToString();
        }
        static string SerializeHeaders(IHeaderDictionary headers)
        {
            var dict = new Dictionary<string, string>();
            var arrKeys = headers.Keys;

            foreach (var key in arrKeys)
            {
                var header = string.Empty;
                var values = headers[key];
                foreach (var value in values)
                {
                    header += value + " ";
                }
                // Trim the trailing space and add item to the dictionary
                header = header.TrimEnd(" ".ToCharArray());
                dict.Add(key, header);
            }

            return JsonConvert.SerializeObject(dict, Formatting.Indented);
        }

        static string ParseRequestBodyText(string response)
        {

            //Events for seri log are batches until they are written they get stored in memory, if this takes time the memory can fill up, for now
            //i'm being safe and removing the body if it's too large
            if (response.Length > 100000)
            {
                return "**TOO LARGE**";
            }
            if (response.Contains("password"))
            {
                //Be safe, if the request has a password in it, don't show it
                response = "**CENSORED**";
            }

            return response;
        }

        static string ParseResponseBodyText(string response)
        {
            //Events for seri log are batches until they are written they get stored in memory, if this takes time the memory can fill up, for now
            //i'm being safe and removing the body if it's too large
            if (response.Length > 100000)
            {
                return "**TOO LARGE**";
            }
            if (response.Contains("password"))
            {
                //Be safe, if the request has a password in it, don't show it
                response = "**CENSORED**";
            }

            return response;
        }
    }
}
