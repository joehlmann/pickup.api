﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using PickupSvc.Infrastructure.TokenContext;

namespace PickupSvc.API.Infrastructure.Middleware.Authentication.AccessTokenContext
{
    public class AccessTokenContextMiddleware
    {
        readonly RequestDelegate _next;
        private readonly ILogger<AccessTokenContextMiddleware> _logger;
        

        public AccessTokenContextMiddleware(RequestDelegate next, ILogger<AccessTokenContextMiddleware> logger)
        {
            _next = next ?? throw new ArgumentNullException(nameof(next));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task Invoke(HttpContext httpContext, ITokenContextService tokenService)
        {
            if (httpContext == null) throw new ArgumentNullException(nameof(httpContext));
            if (tokenService == null) throw new ArgumentNullException(nameof(tokenService));

            _logger.LogTrace("Collecting access token from request");
            tokenService.SetAccessToken(await httpContext.GetTokenAsync("access_token"));

            // Call the next middleware
            await _next(httpContext);
        }
    }
}
