﻿namespace PickupSvc.API.Infrastructure.Cache.Interfaces
{
    public interface IPickupNumbersCache
    {
        int[] GetCachedNumbers();
        void PopulateCachedNumbers(int[] numbersToCache);
        int LastRetrievedPickupNumberPosition();
        void SetLastRetrievedPickupNumberPosition(int position);
    }
}