﻿using PickupSvc.API.Infrastructure.Cache.Interfaces;

namespace PickupSvc.API.Infrastructure.Cache
{
    /// <summary>
    /// Used as a static cache of pickup numbers, should be used with a lock to prevent concurrency issues
    /// </summary>
    internal class InMemoryPickupNumbersCache : IPickupNumbersCache
    {
        private int[] _availableNumbers;
        private int _pickupNumberPosition = 0;

        public int[] GetCachedNumbers()
        {
            return _availableNumbers;
        }

        public void PopulateCachedNumbers(int[] numbersToCache)
        {
            _availableNumbers = numbersToCache;
        }

        public int LastRetrievedPickupNumberPosition()
        {
            return _pickupNumberPosition;
        }

        public void SetLastRetrievedPickupNumberPosition(int position)
        {
            _pickupNumberPosition = position;

        }
    }
}
