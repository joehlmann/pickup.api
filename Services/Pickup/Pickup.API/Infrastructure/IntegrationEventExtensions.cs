﻿using System;
using System.Collections.Generic;
using CorrelationId;
using EventBusAbstract.Events;

namespace PickupSvc.API.Infrastructure
{
    public static class IntegrationEventExtensions
    {
        public static Dictionary<string, object> GenerateLoggingMetaData(this IntegrationEvent eventMsg)
        {
            var correlationId = eventMsg.CorrelationId;
            var correlationName = eventMsg.CorrelationName;

            if (string.IsNullOrEmpty(correlationId))
            {
                correlationId = Guid.NewGuid().ToString();
            }

            correlationName = string.IsNullOrEmpty(correlationName) ? "request-id" : correlationName;

            var loggingMetaData = new Dictionary<string, object>()
            {
                {correlationName, correlationId}
            };

            // Set the correlation data
            new CorrelationContextFactory().Create(correlationId, correlationName);
            eventMsg.SetCorrelationDetails(correlationId, correlationName);
            return loggingMetaData;
        }
    }
}
