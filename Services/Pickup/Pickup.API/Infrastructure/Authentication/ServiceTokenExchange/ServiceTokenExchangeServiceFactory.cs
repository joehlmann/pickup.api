﻿using System;
using Microsoft.Extensions.Logging;
using PickupSvc.API.Infrastructure.Authentication.ServiceTokenExchange.Interfaces;

namespace PickupSvc.API.Infrastructure.Authentication.ServiceTokenExchange
{
    public class ServiceTokenExchangeServiceFactory : IServiceTokenExchangeServiceFactory
    {
        private readonly ILogger<ServiceTokenExchangeServiceFactory> _logger;
        private readonly ICachingTokenExchangeService _cachedTokenExchangeProvider;
        private readonly ITokenExchangeService _tokenExchangeProvider;

        public ServiceTokenExchangeServiceFactory(ICachingTokenExchangeService cachedTokenExchangeProvider, ITokenExchangeService tokenExchangeProvider, ILogger<ServiceTokenExchangeServiceFactory> logger)
        {
            _cachedTokenExchangeProvider = cachedTokenExchangeProvider;
            _tokenExchangeProvider = tokenExchangeProvider ?? throw new ArgumentNullException(nameof(tokenExchangeProvider));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public ITokenExchangeService GetInstance(bool enableCaching = false)
        {
            if (enableCaching)
            {
                if (_cachedTokenExchangeProvider == null)
                {
                    _logger.LogWarning(
                        "Caching is enabled, but no cache provider has been registered. Register a cache provider using " +
                        "UseInMemoryCache or one of the available overloads. " +
                        "Continuing without caching.");

                    return _tokenExchangeProvider;
                }

                return _cachedTokenExchangeProvider;
            }

            return _tokenExchangeProvider;
        }
        
    }
}
