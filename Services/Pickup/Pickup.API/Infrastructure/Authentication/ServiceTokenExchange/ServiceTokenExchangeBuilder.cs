﻿using System;
using Microsoft.Extensions.DependencyInjection;
using PickupSvc.API.Infrastructure.Authentication.ServiceTokenExchange.Interfaces;

namespace PickupSvc.API.Infrastructure.Authentication.ServiceTokenExchange
{
    public class ServiceTokenExchangeBuilder : IServiceTokenExchangeBuilder
    {
        public ServiceTokenExchangeBuilder(IServiceCollection services)
        {
            Services = services ?? throw new ArgumentNullException(nameof(services));
        }
        
        public IServiceCollection Services { get; }
    }
}