﻿// Credit to the original Github project that provided the base implementation of the reference exchange https://github.com/clawrenceks/ReferenceTokenExchange

using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using PickupSvc.API.Infrastructure.Authentication.ServiceTokenExchange.Caches;
using PickupSvc.API.Infrastructure.Authentication.ServiceTokenExchange.Interfaces;
using PickupSvc.API.Infrastructure.Authentication.ServiceTokenExchange.Models;
using PickupSvc.API.Infrastructure.Authentication.ServiceTokenExchange.Services;

namespace PickupSvc.API.Infrastructure.Authentication.ServiceTokenExchange
{
    public static class ServiceTokenExchangeServiceExtensions
    {
        public static IServiceTokenExchangeBuilder AddServiceTokenExchange(this IServiceCollection services)
        {
            services.AddTransient<IServiceTokenExchangeServiceFactory, ServiceTokenExchangeServiceFactory>();
            services.AddTransient<ITokenExchangeService, ServiceTokenExchangeService>();
            services.AddTransient<ITokenExchangeCache, TokenExchangeMemCache>();
            services.AddTransient<ICachingTokenExchangeService, ServiceTokenExchangeServiceWithCache>();
            services.AddSingleton(serviceProvider =>
            {
                var config = (IConfiguration)serviceProvider.GetService(typeof(IConfiguration));

                var serviceTokenConfig = config.GetSection("ServiceTokenExchange");
                bool.TryParse(serviceTokenConfig["EnableCache"], out var enableTokenCache);

                return new ServiceTokenExchangeOptions
                {
                    IdentityServerRootUrl = serviceTokenConfig["Authority"],
                    UpdateAuthorizationHeader = true,
                    ClientId = serviceTokenConfig["ClientId"],
                    ClientSecret = serviceTokenConfig["ClientSecret"],
                    RequireHttpsEndpoints = false,
                    GrantType = "serviceTokenExchange",
                    EnableCaching = enableTokenCache
                };
                
            });

            services.AddTransient<HttpClientServiceTokenExchangeDelegatingHandler>();

            return new ServiceTokenExchangeBuilder(services);
        }
    }
}
