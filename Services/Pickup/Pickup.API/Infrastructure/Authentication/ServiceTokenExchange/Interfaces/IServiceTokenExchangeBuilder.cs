﻿using Microsoft.Extensions.DependencyInjection;

namespace PickupSvc.API.Infrastructure.Authentication.ServiceTokenExchange.Interfaces
{
    public interface IServiceTokenExchangeBuilder
    {
        IServiceCollection Services { get; }
    }
}