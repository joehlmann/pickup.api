﻿using System.Threading.Tasks;
using PickupSvc.API.Infrastructure.Authentication.ServiceTokenExchange.Models;

namespace PickupSvc.API.Infrastructure.Authentication.ServiceTokenExchange.Interfaces
{
    public interface ITokenExchangeService
    {
        Task<TokenExchangeResult> ExchangeTokenAsync(string accessToken, ServiceTokenExchangeOptions options);
    }
}