﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using IdentityModel.Client;
using Microsoft.Extensions.Logging;
using PickupSvc.API.Infrastructure.Authentication.ServiceTokenExchange.Interfaces;
using PickupSvc.API.Infrastructure.Authentication.ServiceTokenExchange.Models;

namespace PickupSvc.API.Infrastructure.Authentication.ServiceTokenExchange.Services
{
    public class ServiceTokenExchangeService : ITokenExchangeService
    {
        private readonly ILogger<ServiceTokenExchangeService> _logger;

        public ServiceTokenExchangeService(ILogger<ServiceTokenExchangeService> logger)
        {
            _logger = logger;
        }

        public async Task<TokenExchangeResult> ExchangeTokenAsync(string accessToken, ServiceTokenExchangeOptions options)
        {
            if (accessToken == null)
            {
                accessToken = string.Empty;
            }

            var payload = new
            {
                token = accessToken
            };

            var identityServer = await DiscoverIdentityServerAsync(options.IdentityServerRootUrl, options.RequireHttpsEndpoints, options.AdditionalEndpointBaseAddresses);
            var client = new TokenClient(identityServer.TokenEndpoint, options.ClientId, options.ClientSecret, options.HttpHandler);

            _logger.LogInformation($"Requesting token exchange from Identity Server: {identityServer.TokenEndpoint}");
            var tokenResponse = await client.RequestCustomGrantAsync(options.GrantType, options.Scope, payload);

            if (tokenResponse.IsError)
            {
                _logger.LogWarning($"An error occured during reference token exchange: {tokenResponse.Error}");
            }

            var result = new TokenExchangeResult
            {
                ReferenceToken = accessToken,
                AccessToken = tokenResponse.AccessToken,
                AccessTokenExpiryTime = DateTimeOffset.UtcNow.AddSeconds(tokenResponse.ExpiresIn)
            };

            _logger.LogInformation("Token exchange with Identity Server completed successfully");
            return result;
        }

        private async Task<DiscoveryResponse> DiscoverIdentityServerAsync(string identityServerRootUrl, bool requireHttps, ICollection<string> additionalEndpointBaseAddresses)
        {
            var client = new DiscoveryClient(identityServerRootUrl);
            client.Policy.RequireHttps = requireHttps;
            client.Policy.AdditionalEndpointBaseAddresses = additionalEndpointBaseAddresses;

            _logger.LogDebug($"Discovering Identity Server metadata from {client.Url}");
            var response = await client.GetAsync();

            if (response.IsError)
            {
                _logger.LogError($"An error occured during metadata discovery: {response.Exception}");
                throw new InvalidOperationException(response.Error, response.Exception);
            }

            return response;
        }
    }
}
