﻿using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using PickupSvc.API.Infrastructure.Authentication.ServiceTokenExchange.Interfaces;
using PickupSvc.API.Infrastructure.Authentication.ServiceTokenExchange.Models;
using PickupSvc.TheDomain.Infrastructure;

namespace PickupSvc.API.Infrastructure.Authentication.ServiceTokenExchange
{
    public class HttpClientServiceTokenExchangeDelegatingHandler : DelegatingHandler
    {
        
        private readonly ILogger<HttpClientServiceTokenExchangeDelegatingHandler> _logger;
        private readonly IServiceTokenExchangeServiceFactory _tokenExchangeServiceFactory;
        private readonly ServiceTokenExchangeOptions _options;

        public HttpClientServiceTokenExchangeDelegatingHandler(
            ILogger<HttpClientServiceTokenExchangeDelegatingHandler> logger,
            IServiceTokenExchangeServiceFactory tokenExchangeServiceFactory,
            ServiceTokenExchangeOptions options)
        {
            
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _tokenExchangeServiceFactory = tokenExchangeServiceFactory ??
                                           throw new ArgumentNullException(nameof(tokenExchangeServiceFactory));
            _options = options ?? throw new ArgumentException(nameof(options));
        }

        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request,
            CancellationToken cancellationToken)
        {
            request.Properties.TryGetValue("access_token",out var currentTokenObject);
            var currentToken = currentTokenObject?.ToString();
            var tokenExchangeService = _tokenExchangeServiceFactory.GetInstance(_options.EnableCaching);

            if (!currentToken.IsPresent())
            {
                _logger.LogInformation("Unable to locate a service access token");
            }
            else
            {
                var tokenResponse = await tokenExchangeService.ExchangeTokenAsync(currentToken, _options);
                if (_options.DelegationTokenHeaderName.IsPresent())
                {
                    request.Headers.Add(_options.DelegationTokenHeaderName, tokenResponse.AccessToken);
                }

                // If we need to replace the existing bearer token, do it
                if (_options.UpdateAuthorizationHeader)
                {
                    request.Headers.Remove("Authorization");

                    var authorizationHeader = "Bearer " + tokenResponse.AccessToken;
                    request.Headers.Add("Authorization", authorizationHeader);
                }

                if (_options.DelegationTokenHeaderName.IsPresent() &&
                    !_options.UpdateAuthorizationHeader)
                {
                    _logger.LogWarning(
                        "A DelegationTokenHeaderName as not been provided and the default token has not been set to override, the new token has not been added to the forwarded request");
                }
            }


            //Continue on with the pipeline
            return await base.SendAsync(request, cancellationToken);
        }
    }
}