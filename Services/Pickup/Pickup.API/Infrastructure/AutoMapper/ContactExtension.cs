﻿using System.Collections.Generic;
using PickupSvc.API.Application.DTOs;
using PickupSvc.TheDomain.ValueObjects;

namespace PickupSvc.API.Infrastructure.AutoMapper
{
    public static class ContactExtension
    {

        public static IEnumerable<ContactDTO> ToDtos(this IEnumerable<Contact> items)
        {
            foreach (var item in items)
            {
                yield return item.ToDto();
            }
        }


        public static ContactDTO ToDto(this Contact item)
        {
            return new ContactDTO()
            {
                ContactName = item.ContactName,
                Email = item.Email,
                Phone = item.Phone
            };
        }

    }
}
