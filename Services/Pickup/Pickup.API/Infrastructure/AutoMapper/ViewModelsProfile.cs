﻿using AutoMapper;
using PickupSvc.API.Application.Commands.PickupItems;
using PickupSvc.API.Application.DTOs;

namespace PickupSvc.API.Infrastructure.AutoMapper
{
    public class ViewModelsProfile : Profile
    {
        /// <summary>
        /// Public ctor that defines the available AutoMapper profiles
        /// </summary>
        public ViewModelsProfile()
        {
            // Don't let AutoMapper inject values to constructors at this level as they are required to go through the business entity (enforces business rules)
            DisableConstructorMapping();
            CreateMap<PickupItemDTO, PickupItemAddCommand>().IgnoreAllPropertiesWithAnInaccessibleSetter();
            //CreateMap<CreateDangerousGoodItemViewModel, PickupItemModel>().IgnoreAllPropertiesWithAnInaccessibleSetter();
        }
    }
}
