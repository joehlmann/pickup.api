﻿namespace PickupSvc.API.Infrastructure.AutoMapper
{
    public interface IToDto<out T>
    {
        T ToDto();
    }
}