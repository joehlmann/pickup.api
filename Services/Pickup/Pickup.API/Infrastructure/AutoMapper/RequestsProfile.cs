﻿using AutoMapper;
using PickupSvc.API.Application.Commands.Pickups;

namespace PickupSvc.API.Infrastructure.AutoMapper
{
    public class RequestsProfile : Profile
    {
        /// <summary>
        /// Public ctor that defines the available AutoMapper profiles
        /// </summary>
        public RequestsProfile()
        {
            // Don't let AutoMapper inject values to constructors at this level as they are required to go through the business entity (enforces business rules)
            DisableConstructorMapping();
            CreateMap<PickupCreateCommand, TheDomain.Entities.Pickup>().IgnoreAllPropertiesWithAnInaccessibleSetter();
            
        }
    }
}

