﻿using System.Collections.Generic;
using PickupSvc.API.Application.DTOs;
using PickupSvc.TheDomain.ValueObjects;

namespace PickupSvc.API.Infrastructure.AutoMapper
{
    public static class AddressExtension 
    {

        public static IEnumerable<AddressDTO> ToDtos(this IEnumerable<Address> items)
        {
            foreach (var item in items)
            {
                yield return item.ToDto();
            }
        }

        public static AddressDTO ToDto(this Address item)
        {
            return new AddressDTO()
            {
                Address1 = item.Address1,
                Address2 = item.Address2,
                PostCode = item.PostCode,
                State = item.State,
                Suburb = item.Suburb
            };
        }

    }
}
