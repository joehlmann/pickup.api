﻿using Autofac;
using EventBusAbstract;
using IntegrationEventLog.Services;
using Microsoft.Data.SqlClient;
using PickupSvc.API.Application.DomainServices;
using PickupSvc.API.Application.DomainServices.Interfaces;
using PickupSvc.API.Application.IntegrationEvents;
using PickupSvc.API.Infrastructure.Authorization;
using PickupSvc.API.Infrastructure.DelegateHandlers;
using PickupSvc.Infrastructure.Idempotency;
using PickupSvc.Infrastructure.TokenContext;

namespace PickupSvc.API.Infrastructure.Modules
{
    public class ServicesModule : Module
    {

        //protected string _connectionStringEventLog;
        protected string _connectionString;


        public ServicesModule(string connectionString)
        {
            _connectionString = connectionString;


        }


        protected override void Load(ContainerBuilder builder)
        {


            // Authentication 
            builder.RegisterType<TokenContextService>()
                .As<ITokenContextService>()
                .InstancePerLifetimeScope();


            builder.RegisterType<BorderExpressAssertions>()
                .As<IAssertions>()
                .InstancePerLifetimeScope();

            

            //Domain Services

            builder.RegisterType<ChargeAccountValidService>()
                .As<IChargeAccountValidService>()
                .SingleInstance();

            builder.RegisterType<EntityIdValidService>()
                .As<IEntityIdValidService>()
                .InstancePerLifetimeScope();

            builder.RegisterType<AddressValidService>()
                .As<IAddressValidService>()
                .InstancePerLifetimeScope();

            builder.RegisterType<BranchValidService>()
                .As<IBranchValidService>()
                .InstancePerLifetimeScope();

            builder.RegisterType<ContactValidService>()
                .As<IContactValidService>()
                .InstancePerLifetimeScope();

            builder.RegisterType<DGoodsValidService>()
                .As<IDGoodsValidService>()
                .InstancePerLifetimeScope();

            builder.RegisterType<PalletValidService>()
                .As<IPalletValidService>()
                .InstancePerLifetimeScope();

            builder.RegisterType<PickupItemValidService>()
                .As<IPickupItemValidService>()
                .InstancePerLifetimeScope();

            builder.RegisterType<TimeInfoValidService>()
                .As<ITimeInfoValidService>()
                .InstancePerLifetimeScope();

            builder.RegisterType<UserValidService>()
                .As<IUserValidService>()
                .InstancePerLifetimeScope();



            // Infra Services


            builder.RegisterType<PickupIntegrationEventSvc>()
                .As<IPickupIntegrationEventSvc>()
                .InstancePerLifetimeScope();

            builder.RegisterType<PickupNumberService>()
                .WithParameter("connectionString", _connectionString)
                .As<IPickupNumberService>()
                .InstancePerLifetimeScope();


            builder.RegisterType<RequestManager>()
                .As<IRequestManager>()
                .InstancePerLifetimeScope();


            builder.RegisterType<IntegrationEventLogService>()
                .WithParameter("dbConnection", new SqlConnection(_connectionString))
                .As<IIntegrationEventLogService>()
                .InstancePerLifetimeScope();



            builder.RegisterType<HttpClientCorrelationIdDelegatingHandler>()
                .As<HttpClientCorrelationIdDelegatingHandler>()
                .InstancePerLifetimeScope();





            //Singleton
            builder.RegisterType<InMemoryEventBusSubscriptionsManager>()
                .As<IEventBusSubscriptionsManager>()
                .SingleInstance();

            builder.RegisterType<RequestManager>()
                .As<IRequestManager>()
                .SingleInstance();


        }
    }
}
