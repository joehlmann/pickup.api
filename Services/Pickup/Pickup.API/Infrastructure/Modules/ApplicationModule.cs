﻿using Autofac;
using PickupSvc.API.Application.CommandsQueries.CQRSQueries;
using PickupSvc.Infrastructure.Idempotency;
using PickupSvc.Infrastructure.Repositories;
using PickupSvc.Infrastructure.Repositories.Interfaces;

namespace PickupSvc.API.Infrastructure.Modules
{

    public class ApplicationModule
        :Module
    {

        public string QueriesConnectionString { get; }

        private ContainerBuilder _container;

        public ApplicationModule(ContainerBuilder container,string queriesConnectionString)
        {
            QueriesConnectionString = queriesConnectionString;
            _container = container;
        }

        protected override void Load(ContainerBuilder builder)
        {
            

            builder.Register(c => new PickupQueries(QueriesConnectionString))
                .As<IPickupQueries>()
                .InstancePerLifetimeScope();


            builder.RegisterType<BranchRepository>()
                .As<IBranchRepository>()
                .InstancePerLifetimeScope();

            builder.RegisterType<CustomerRepository>()
                .As<ICustomerRepository>()
                .InstancePerLifetimeScope();

            builder.RegisterType<DangerousGoodRepository>()
                .As<IDangerousGoodRepository>()
                .InstancePerLifetimeScope();

            builder.RegisterType<PickupRepository>()
                .As<IPickupRepository>()
                .InstancePerLifetimeScope();

            builder.RegisterType<RegionRepository>()
                .As<IRegionRepository>()
                .InstancePerLifetimeScope();

            builder.RegisterType<SuburbRepository>()
                .As<ISuburbRepository>()
                .InstancePerLifetimeScope();

            builder.RegisterType<UserRepository>()
                .As<IUserRepository>()
                .InstancePerLifetimeScope();

            builder.RegisterType<RequestManager>()
               .As<IRequestManager>()
               .InstancePerLifetimeScope();

           

        }
    }
}
