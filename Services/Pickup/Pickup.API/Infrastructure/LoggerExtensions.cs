﻿using System;
using System.Linq;
using Microsoft.Extensions.Logging;

namespace PickupSvc.API.Infrastructure
{
    public static class LoggerExtensions
    {
        public static IDisposable BeginScope(this ILogger logger, params (string key, object value)[] keys)
        {
            return logger.BeginScope(keys.ToDictionary(x => x.key, x => x.value));
        }
    }
}
