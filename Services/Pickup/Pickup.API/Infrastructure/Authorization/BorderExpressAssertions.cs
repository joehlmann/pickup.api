﻿using System.Linq;
using Microsoft.AspNetCore.Authorization;

namespace PickupSvc.API.Infrastructure.Authorization
{
    public class BorderExpressAssertions : IAssertions
    {
        public bool CanDeletePickups(AuthorizationHandlerContext context)
        {
            // Split the scopes string into an array
            var scopes = context.User.Claims.Where(c => c.Type == BorderExpressIdentity.Claims.Scope.Type && c.Issuer == BorderExpressIdentity.Claims.Scope.Issuer).ToList();

            // Succeed if the scope array contains the required scope
            var hasRequiredScopes = scopes.Any(s => (
                // Check if the access token has the read only scope
                s.Value == BorderExpressIdentity.Scopes.DeletePickups.Name && s.Issuer == BorderExpressIdentity.Scopes.DeletePickups.Issuer
            ));

            return hasRequiredScopes;
        }

        public bool CanReadPickups(AuthorizationHandlerContext context)
        {
            // Split the scopes string into an array
            var scopes = context.User.Claims.Where(c => c.Type == BorderExpressIdentity.Claims.Scope.Type && c.Issuer == BorderExpressIdentity.Claims.Scope.Issuer).ToList();

            // Succeed if the scope array contains the required scope
            var hasRequiredScopes = scopes.Any(s => (
                // Check if the access token has the read only scope
                s.Value == BorderExpressIdentity.Scopes.ReadPickups.Name && s.Issuer == BorderExpressIdentity.Scopes.ReadPickups.Issuer
            ));

            return hasRequiredScopes;
        }

        public bool CanWritePickups(AuthorizationHandlerContext context)
        {
            // Split the scopes string into an array
            var scopes = context.User.Claims.Where(c => c.Type == BorderExpressIdentity.Claims.Scope.Type && c.Issuer == BorderExpressIdentity.Claims.Scope.Issuer).ToList();

            // Succeed if the scope array contains the required scope
            var hasRequiredScopes = scopes.Any(s => (
                // Check if the access token has the read only scope
                s.Value == BorderExpressIdentity.Scopes.WritePickups.Name && s.Issuer == BorderExpressIdentity.Scopes.WritePickups.Issuer
            ));

            return hasRequiredScopes;
        }
    }
}
