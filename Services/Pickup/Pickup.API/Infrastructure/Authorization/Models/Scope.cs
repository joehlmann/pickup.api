﻿namespace PickupSvc.API.Infrastructure.Authorization.Models
{
    public class Scope
    {
        public string Name { get;set; }
        public string Issuer { get;set; }
    }
}
