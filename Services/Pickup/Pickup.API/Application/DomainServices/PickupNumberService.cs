﻿using System.Threading.Tasks;
using Dapper;
using Microsoft.Data.SqlClient;
using PickupSvc.API.Application.DomainServices.Interfaces; //using System.Data.SqlClient;

namespace PickupSvc.API.Application.DomainServices
{
    public class PickupNumberService : IPickupNumberService
    {
        private readonly string _connectionString;


        //private readonly int _pickupNumberBatchSize;

        public PickupNumberService(string connectionString)
        {
            _connectionString = connectionString;
        }

        /// <summary>
        /// Return a randomly generated pickup number
        /// </summary>
        /// <returns></returns>
        public async Task<int> GenerateNextAvailablePickupNumberAsync()
        {


            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                var result = await connection.QueryFirstAsync<int>(
                    @"select NEXT VALUE FOR dbo.PickupHiLo"
                    
                );

                

                return result;
            }


        }
        
       
    }
}
