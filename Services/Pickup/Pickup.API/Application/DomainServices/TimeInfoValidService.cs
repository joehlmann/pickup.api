﻿using System;
using System.Threading.Tasks;
using Bex.Utilities.Helper;
using CSharpFunctionalExtensions;
using PickupSvc.API.Application.DomainServices.Interfaces;
using PickupSvc.API.Application.DTOs;
using PickupSvc.TheDomain.Infrastructure;

namespace PickupSvc.API.Application.DomainServices
{
    public class TimeInfoValidService : ITimeInfoValidService
    {
       private Result CheckPickupDate(string dtoPickupDate)
       {

           var result = Result.FailureIf(string.IsNullOrWhiteSpace(dtoPickupDate),
                   ValidationMessages.PickupDateIsMissing.Message)
               .Bind(() =>
                   Result.SuccessIf(dtoPickupDate.IsDate, ValidationMessages.PickupDateIsNotDate.Message)
               .Bind(() => Result.FailureIf(dtoPickupDate.ToDate() == DateTime.MinValue,
                   ValidationMessages.PickupCloseTimeIsMissing.Message)));

            return result;

        }


        private Result CheckReadyTime(string dtoReadyTime)
        {

            if (dtoReadyTime != null)
            {
                var result = Result.SuccessIf(dtoReadyTime.IsDate, ValidationMessages.PickupReadyTimeIsInvalid.Message)
                    .Bind(() => Result.FailureIf(dtoReadyTime.ToDate() == DateTime.MinValue,
                        ValidationMessages.PickupReadyTimeIsMissing.Message));

                return result;
            }

            return Result.Success();
        }

        private Result CheckCloseTime(string dtoCloseTime)
        {

            if (dtoCloseTime != null)
            {
                var result = Result.SuccessIf(dtoCloseTime.IsDate, ValidationMessages.PickupCloseTimeIsMissing.Message)
                    .Bind(() => Result.FailureIf(dtoCloseTime.ToDate() == DateTime.MinValue,
                        ValidationMessages.PickupCloseTimeIsMissing.Message));
                return result;
            }

            return Result.Success();
        }


        private Result CheckBookedDate(string dtoBooked)
        {

            var result = Result.FailureIf(string.IsNullOrWhiteSpace(dtoBooked),
                    ValidationMessages.PickupBookingDateIsMissing.Message)
                .Bind(() => Result.SuccessIf(dtoBooked.IsDate, ValidationMessages.PickupBookingDateIsMissing.Message)
                .Bind(() => Result.FailureIf(dtoBooked.ToDate() == DateTime.MinValue,
                    ValidationMessages.PickupCloseTimeIsMissing.Message)));

            return result;

        }

        public async Task<Result<bool>> ValidTimeInfoAsync(TimeInfoDTO dto)
        {
           // var resPick = CheckPickupDate(dto.PickupDate);

            var resultReady = CheckReadyTime(dto.ReadyTime);

            var resultClose = CheckCloseTime(dto.CloseTime);

            var resultBooked = CheckBookedDate(dto.BookedDateTime);



            var result = Result.Combine( resultReady, resultClose, resultBooked);

            if (result.IsSuccess)
                return Result.Success(true);

            return await Task.FromResult(Result.Failure<bool>(result.Error));
        }

        public async Task<Result<bool>> ValidFullTimeInfoAsync(TimeInfoDTO dto)
        {
             var resPick = CheckPickupDate(dto.PickupDate);

            var resultReady = CheckReadyTime(dto.ReadyTime);

            var resultClose = CheckCloseTime(dto.CloseTime);

            var resultBooked = CheckBookedDate(dto.BookedDateTime);



            var result = Result.Combine(resultReady, resultClose, resultBooked);

            if (result.IsSuccess)
                return Result.Success(true);

            return await Task.FromResult(Result.Failure<bool>(result.Error));
        }


    }
}