﻿using System.Threading.Tasks;
using Bex.Utilities.Helper;
using CSharpFunctionalExtensions;
using PickupSvc.API.Application.DomainServices.Interfaces;
using PickupSvc.Infrastructure.Repositories.Interfaces;
using PickupSvc.TheDomain.Entities;

namespace PickupSvc.API.Application.DomainServices
{
    public class BranchValidService : IBranchValidService
    {
        private IBranchRepository _repository;


        public BranchValidService(IBranchRepository repository)
        {
            _repository = repository;
        }


       

        public async Task<Result<bool>> ValidBranchAsync(int branchId)
        {
            var result = await _repository.FindByIdAsync(branchId);

            if (result is null)
                return Result.Failure<bool>($"Branch With Id:{branchId} does not Exist");

            return Result.Success(true);

            
        }

        
        public async Task<Result<bool>> ValidBranchForPickupAsync(int branchId, string pickupNumber)
        {
            var result = await _repository.FindByIdAndPickupNumberAsync(branchId,pickupNumber.ToInt());

            if (result.Equals(Branch.Empty()))
                return Result.Failure<bool>($"Branch With Id:{branchId} is not valid for PickupNumber:{pickupNumber}");

            return Result.Success(true);
        }
    }
}
