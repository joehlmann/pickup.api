﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CSharpFunctionalExtensions;
using PickupSvc.API.Application.DomainServices.Interfaces;
using PickupSvc.API.Application.DTOs;
using PickupSvc.Infrastructure.Repositories.Interfaces;
using PickupSvc.TheDomain.Infrastructure;
using PickupSvc.TheDomain.ValueObjects;

namespace PickupSvc.API.Application.DomainServices
{
    public class DGoodsValidService : IDGoodsValidService
    {
        private readonly IDangerousGoodRepository _repository;
        private List<string> _errors = new List<string>();


        public DGoodsValidService(IDangerousGoodRepository repository)
        {
            _repository = repository;
        }

        public async Task<Result<bool>> ValidDGoodsAsync(IEnumerable<DangerousGoodItemDTO> dtos)
        {
            bool IsValid = true;

            foreach (var item in dtos)
            {
                var result = await _repository.GetByUNNumberAsync(item.UNNo);

                if (result.Equals(DangerousGood.Empty()))
                {
                    _errors.Add(string.Format("{0}-{1}",item.UNNo, ValidationMessages.PickupItemDGUnknown.Message));
                    IsValid = false;
                }
            }


            return  IsValid ? Result.Success(true) : Result.Failure<bool>(string.Join(':',_errors));
        }

        public async Task<Result<bool>> ValidDGoodAsync(DangerousGoodItemDTO dto)
        {
            bool IsValid = true;


            var result = await _repository.GetByUNNumberAsync(dto.UNNo);

            if (result.Equals(DangerousGood.Empty()))
            {
                _errors.Add(string.Format("{0}-{1}", dto.UNNo, ValidationMessages.PickupItemDGUnknown.Message));
                IsValid = false;
            }

            return IsValid ? Result.Success(true) : Result.Failure<bool>(string.Join(':', _errors));
        }
    }
}