﻿using System.Threading.Tasks;
using CSharpFunctionalExtensions;
using PickupSvc.API.Application.DomainServices.Interfaces;

namespace PickupSvc.API.Application.DomainServices
{
    public class ChargeAccountValidService : IChargeAccountValidService
    {
        public Task<Result<bool>> ValidChargeAccount(string accountNum)
        {
            //Todo 
            return Task.FromResult(Result.Success(true));
        }

        public Task<Result<bool>> ValidUserForChargeAccount(string accountNum, int user)
        {
            //Todo 
            return Task.FromResult(Result.Success(true));
        }
    }
}
