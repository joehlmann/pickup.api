﻿using System.Threading.Tasks;
using Bex.Utilities.Helper;
using CSharpFunctionalExtensions;
using Microsoft.EntityFrameworkCore;
using PickupSvc.API.Application.DomainServices.Interfaces;
using PickupSvc.Infrastructure;

namespace PickupSvc.API.Application.DomainServices
{
    public class EntityIdValidService : IEntityIdValidService
    {
        private readonly PickupContext _context;

        public EntityIdValidService(PickupContext context)
        {
            _context = context;
        }


        public async Task<Result<bool>> ValidUserIdAsync(string id)
        {
            var result = await _context.CustomerAccounts
                .AnyAsync(u => u.Id == id.ToInt())
                .ConfigureAwait(false);

            if (result )
                return Result.Success(true);

            return Result.Failure<bool>($"User With Id:{id} does not Exist");

            
        }

        

        public async Task<Result<bool>> ValidBranchIdAsync(string id)
        {

            var result = await _context.Branches
                .AnyAsync(u => u.Id == id.ToInt())
                .ConfigureAwait(false);

            if (result)
                return Result.Success(true);

            return Result.Failure<bool>($"Branch With Id:{id} does not Exist");
        }

        public async  Task<Result<bool>> ValidPickupIdAsync(string id)
        {
            var result = await _context.Pickups
                .AnyAsync(u => u.Id == id.ToInt())
                .ConfigureAwait(false);

            if (result)
                return Result.Success(true);

            return Result.Failure<bool>($"Pickup With Id:{id} does not Exist");
        }
        public async Task<Result<bool>> ValidPickupIdAsync(int id)
        {
            var result = await _context.Pickups
                .AnyAsync(u => u.Id == id)
                .ConfigureAwait(false);

            if (result)
                return Result.Success(true);

            return Result.Failure<bool>($"Pickup With Id:{id} does not Exist");
        }

        public async Task<Result<bool>> ValidPickupItemIdAsync(string id)
        {
            var result = await _context.PickupItems
                .AnyAsync(u => u.Id == id.ToInt())
                .ConfigureAwait(false);

            if (result)
                return Result.Success(true);

            return Result.Failure<bool>($"PickupItem With Id:{id} does not Exist");
        }

        
        
        

        public async Task<Result<bool>> ValidCustomerIdAsync(string id)
        {
            var result = await _context.CustomerAccounts
                .AnyAsync(u => u.Id == id.ToInt())
                .ConfigureAwait(false);

            if (result)
                return Result.Success(true);

            return Result.Failure<bool>($"Customer With Id:{id} does not Exist");
        }
    }
}