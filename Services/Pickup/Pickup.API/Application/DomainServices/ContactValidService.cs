﻿using System.Threading.Tasks;
using CSharpFunctionalExtensions;
using PickupSvc.API.Application.DomainServices.Interfaces;
using PickupSvc.API.Application.DTOs;
using PickupSvc.API.Application.Validations;

namespace PickupSvc.API.Application.DomainServices
{
   public class ContactValidService : IContactValidService
    {
        private Result CheckName(string name)
        {

            var result = Result.FailureIf(string.IsNullOrWhiteSpace(name), ErrorMsg.ContactNameEmpty)
                .Bind(() => Result.FailureIf(name.Length > 50, ErrorMsg.ContactNameToLong));


            return result;

        }

        private Result CheckEmail(string email)
        {


            //if (!Regex.IsMatch(email, @"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$"))
            //    return Result.Failure("E-mail is invalid");

            var result = Result.FailureIf(string.IsNullOrWhiteSpace(email), ErrorMsg.ContactEmailEmpty)
                .Bind(() => Result.FailureIf(email.Length > 100, ErrorMsg.ContactEmailToLong));

            //var resNameLen = Result.FailureIf(email.Length > 100, ErrorMsg.ContactEmailToLong);



            return result;
        }

        private Result CheckPhone(string phone)
        {


            var result = Result.FailureIf(string.IsNullOrWhiteSpace(phone), ErrorMsg.ContactPhoneEmpty)
                .Bind(() =>
                    Result.FailureIf(phone.Length > 9 || phone.Length < 7, ErrorMsg.ContactPhoneInvalidLength));

            return result;
        }
        public async Task<Result<bool>> ValidContactAsync(ContactDTO contactDTO)
        {

            var resName = CheckName(contactDTO.ContactName);

            var resultEmail = CheckEmail(contactDTO.Email);

            var resultPhone = CheckPhone(contactDTO.Phone);

            var result = Result.Combine(resName, resultEmail, resultPhone);

            if (result.IsSuccess)
                return Result.Success(true);

            return await Task.FromResult(Result.Failure<bool>(result.Error));
        }
    }
}

   
       
    
