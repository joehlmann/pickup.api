﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Bex.Utilities.Helper;
using CSharpFunctionalExtensions;
using Microsoft.EntityFrameworkCore;
using PickupSvc.API.Application.DomainServices.Interfaces;
using PickupSvc.API.Application.DTOs;
using PickupSvc.Infrastructure;
using PickupSvc.TheDomain.Infrastructure;

namespace PickupSvc.API.Application.DomainServices
{
    public class PickupItemValidService : IPickupItemValidService
    {
        private readonly IDGoodsValidService _dGoodsValidService;
        private List<string> _errors = new List<string>();
        private PickupContext _context;

        public PickupItemValidService(IDGoodsValidService dGoodsValidService, PickupContext context)
        {
            _dGoodsValidService = dGoodsValidService;
            _context = context;
        }

        public async Task<Result<bool>> ValidPickupItemIdAsync(string id)
        {
            var result = await _context.PickupItems
                .AnyAsync(u => u.Id == id.ToInt())
                .ConfigureAwait(false);

            if (result)
                return Result.Success(true);

            return Result.Failure<bool>($"PickupItem With Id:{id} does not Exist");
        }

        public async Task<Result<bool>> ValidPickupItemIdAsync(int id)
        {
            var result = await _context.PickupItems
                .AnyAsync(u => u.Id == id)
                .ConfigureAwait(false);

            if (result)
                return Result.Success(true);

            return Result.Failure<bool>($"PickupItem With Id:{id} does not Exist");
        }

        public async Task<Result<bool>> ValidPickupItemAsync(PickupItemDTO dto)
        {
            var resultDescription =
                Result.FailureIf(string.IsNullOrEmpty(dto.Description), ValidationMessages.PickupItemDescriptionMissing.Message);


            var resultWeightKg = Result.FailureIf(dto.WeightKg <= 0, ValidationMessages.PickupItemWeightMissing.Message);

            var resultQuantity =
                Result.FailureIf(dto.Quantity <= 0, ValidationMessages.PickupItemQuantityMissing.Message);



            var resultDGoods = Result.SuccessIf(await ValidDGoodsAsync(dto.DangerousGoods), string.Join(':', _errors));
                


            var result = Result.Combine(resultDescription, resultWeightKg, resultQuantity, resultDGoods);

            if (result.IsSuccess)
                return Result.Success(true);

            return await Task.FromResult(Result.Failure<bool>(result.Error));
        }


        private async Task<bool> ValidDGoodsAsync(IEnumerable<DangerousGoodItemDTO> dtos)
        {
            bool IsValid = true;
            var result = await _dGoodsValidService.ValidDGoodsAsync(dtos).ConfigureAwait(true);

            if (result.IsFailure)
            {
                _errors.Add(result.Error);
                IsValid = false;
            }
            

            return IsValid;

        }
    }
}