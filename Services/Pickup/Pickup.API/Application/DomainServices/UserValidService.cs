﻿using System.Threading.Tasks;
using Bex.Utilities.Helper;
using CSharpFunctionalExtensions;
using PickupSvc.API.Application.DomainServices.Interfaces;
using PickupSvc.Infrastructure.Repositories.Interfaces;

namespace PickupSvc.API.Application.DomainServices
{
    public  class UserValidService : IUserValidService
    {
        private readonly IUserRepository _repository;



        public UserValidService(IUserRepository repository)
        {
            _repository = repository;
        }

        
        public async Task<Result<bool>> ValidUserIdAsync(string id)
        {

            var resultQuery = await _repository.FindByIdAsync(id.ToInt());

            if (resultQuery is null)
                return Result.Failure<bool>($"User With Id:{id} does not Exist");



            return Result.Success(true);
        }

        public async Task<Result<bool>> ValidUserIdAsync(int id)
        {
            var resultQuery = await _repository.FindByIdAsync(id);

            if (resultQuery is null)
                return Result.Failure<bool>($"User With Id:{id} does not Exist");



            return Result.Success(true);
        }
    }
}