﻿using System.Threading.Tasks;
using CSharpFunctionalExtensions;
using PickupSvc.API.Application.DomainServices.Interfaces;

namespace PickupSvc.API.Application.DomainServices
{
    public class PostCodeSuburbValidService : IPostCodeSuburbValidService
    {
        //todo fix

        public async Task<Result<bool>> ValidPostCodeAsync(int postcode)
        {
            return Result.Success(true);
        }

        public async Task<Result<bool>> ValidSuburbAsync(string suburb)
        {
            return Result.Success(true);
        }
    }
}