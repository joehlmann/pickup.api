﻿using System.Threading.Tasks;
using CSharpFunctionalExtensions;
using PickupSvc.API.Application.DTOs;

namespace PickupSvc.API.Application.DomainServices.Interfaces
{
    public interface IContactValidService
    {
        Task<Result<bool>> ValidContactAsync(ContactDTO contactDTO);

    }
}