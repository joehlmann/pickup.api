﻿using System.Threading.Tasks;
using CSharpFunctionalExtensions;

namespace PickupSvc.API.Application.DomainServices.Interfaces
{
    public interface IPostCodeSuburbValidService
    {
        Task<Result<bool>> ValidPostCodeAsync(int postcode);

        Task<Result<bool>> ValidSuburbAsync(string suburb);
    }
}
