﻿using System.Threading.Tasks;
using CSharpFunctionalExtensions;

namespace PickupSvc.API.Application.DomainServices.Interfaces
{
    public interface IBranchValidService
    {
        Task<Result<bool>> ValidBranchAsync(int branchId);

        Task<Result<bool>> ValidBranchForPickupAsync(int branchId, string pickupNumber);
    }
}