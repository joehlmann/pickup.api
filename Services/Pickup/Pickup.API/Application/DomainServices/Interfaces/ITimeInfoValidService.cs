﻿using System.Threading.Tasks;
using CSharpFunctionalExtensions;
using PickupSvc.API.Application.DTOs;

namespace PickupSvc.API.Application.DomainServices.Interfaces
{
    public interface ITimeInfoValidService
    {
        Task<Result<bool>> ValidTimeInfoAsync(TimeInfoDTO dto);


        Task<Result<bool>> ValidFullTimeInfoAsync(TimeInfoDTO dto);
    }
}
