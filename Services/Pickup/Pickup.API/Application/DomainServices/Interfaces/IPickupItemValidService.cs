﻿using System.Threading.Tasks;
using CSharpFunctionalExtensions;
using PickupSvc.API.Application.DTOs;

namespace PickupSvc.API.Application.DomainServices.Interfaces
{

    public interface IPickupItemValidService
        {
            Task<Result<bool>> ValidPickupItemAsync(PickupItemDTO dto);

            Task<Result<bool>> ValidPickupItemIdAsync(string id);

            Task<Result<bool>> ValidPickupItemIdAsync(int id);

    }
}

