﻿using System.Threading.Tasks;
using CSharpFunctionalExtensions;
using PickupSvc.API.Application.DTOs;

namespace PickupSvc.API.Application.DomainServices.Interfaces
{
    public interface IPalletValidService
    {
        Task<Result<bool>> ValidPalletAsync(PalletDto dto);
    }
}