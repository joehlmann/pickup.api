﻿using System.Threading.Tasks;
using CSharpFunctionalExtensions;

namespace PickupSvc.API.Application.DomainServices.Interfaces
{
    public interface IChargeAccountValidService
    {
        Task<Result<bool>> ValidChargeAccount(string accountNum);

        Task<Result<bool>> ValidUserForChargeAccount(string accountNum,int id);
    }

    
}