﻿using System.Threading.Tasks;
using CSharpFunctionalExtensions;
using PickupSvc.API.Application.DTOs;

namespace PickupSvc.API.Application.DomainServices.Interfaces
{
    public interface IAddressValidService
    {
        Task<Result<bool>> ValidAddressAsync(AddressDTO addressDTO);
    }
}