﻿using System.Threading.Tasks;
using CSharpFunctionalExtensions;

namespace PickupSvc.API.Application.DomainServices.Interfaces
{
    public interface IPickupValidService
    {
        Task<Result<bool>> ValidPickupIdAsync(int id);

        Task<Result<bool>> ValidPickupNumberAsync(string id);

        Task<Result<bool>> ValidPickupNumberAsync(int id);

   
    }
}
