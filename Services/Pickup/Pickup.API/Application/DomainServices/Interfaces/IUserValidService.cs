﻿using System.Threading.Tasks;
using CSharpFunctionalExtensions;

namespace PickupSvc.API.Application.DomainServices.Interfaces
{
    public interface IUserValidService
    {
        Task<Result<bool>> ValidUserIdAsync(string id);

        Task<Result<bool>> ValidUserIdAsync(int id);
    }
}
