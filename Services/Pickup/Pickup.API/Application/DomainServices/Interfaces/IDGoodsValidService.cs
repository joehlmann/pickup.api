﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CSharpFunctionalExtensions;
using PickupSvc.API.Application.DTOs;

namespace PickupSvc.API.Application.DomainServices.Interfaces
{
    public interface IDGoodsValidService
    {
        Task<Result<bool>> ValidDGoodsAsync(IEnumerable<DangerousGoodItemDTO> dtos );

        Task<Result<bool>> ValidDGoodAsync(DangerousGoodItemDTO dto);
    }
}
