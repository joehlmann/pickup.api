﻿using System.Threading.Tasks;
using CSharpFunctionalExtensions;

namespace PickupSvc.API.Application.DomainServices.Interfaces
{
    public interface IEntityIdValidService
    {
       Task<Result<bool>> ValidUserIdAsync(string id);

       Task<Result<bool>> ValidCustomerIdAsync(string id);

       Task<Result<bool>> ValidBranchIdAsync(string id);

       Task<Result<bool>> ValidPickupIdAsync(string id);
       Task<Result<bool>> ValidPickupIdAsync(int id);

        Task<Result<bool>> ValidPickupItemIdAsync(string id);
    }
}