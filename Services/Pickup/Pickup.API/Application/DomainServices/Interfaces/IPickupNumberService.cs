﻿using System.Threading.Tasks;

namespace PickupSvc.API.Application.DomainServices.Interfaces
{
    public interface IPickupNumberService
    {
        Task<int> GenerateNextAvailablePickupNumberAsync();
    }
}