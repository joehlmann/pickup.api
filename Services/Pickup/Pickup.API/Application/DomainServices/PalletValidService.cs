﻿using System.Threading.Tasks;
using CSharpFunctionalExtensions;
using PickupSvc.API.Application.DomainServices.Interfaces;
using PickupSvc.API.Application.DTOs;

namespace PickupSvc.API.Application.DomainServices
{
    public class PalletValidService : IPalletValidService
    {
        //todo fix

        public async Task<Result<bool>> ValidPalletAsync(PalletDto dto)
        {
            return await Task.FromResult(Result.Success(true));
        }
    }
}