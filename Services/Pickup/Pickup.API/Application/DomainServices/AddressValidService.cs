﻿using System.Threading.Tasks;
using Bex.Utilities.Helper;
using CSharpFunctionalExtensions;
using PickupSvc.API.Application.DomainServices.Interfaces;
using PickupSvc.API.Application.DTOs;
using PickupSvc.API.Application.Validations;
using PickupSvc.TheDomain.Enumerators;

namespace PickupSvc.API.Application.DomainServices
{
    public class AddressValidService: IAddressValidService
    {


        public async Task<Result<bool>> ValidAddressAsync(AddressDTO addressDTO)
        {

            var resultAdd1 = Result.FailureIf(string.IsNullOrEmpty(addressDTO.Address1), ErrorMsg.AddressLine1Invalid);


            var resultPost = Result.FailureIf(string.IsNullOrEmpty(addressDTO.PostCode), ErrorMsg.AddressPostCodeNull)
                .Bind(() =>  Result.SuccessIf(CheckPostCodeIsValid(addressDTO.PostCode), ErrorMsg.AddressPostCode));
                




            var resultState = Result.FailureIf(string.IsNullOrEmpty(addressDTO.State), ErrorMsg.AddressStateInvalidNull)
                .Bind(() =>
                    Result.FailureIf(CheckStateIsValid(addressDTO.State), ErrorMsg.AddressStateInvalid));


            var resultSub = Result.FailureIf(string.IsNullOrEmpty(addressDTO.Suburb), ErrorMsg.AddressSubNull)
                .Bind(() => Result.SuccessIf(CheckSubIsValid(addressDTO.Suburb), ErrorMsg.AddressSub));


            var result = Result.Combine(resultAdd1, resultPost, resultState, resultSub);

            if (result.IsSuccess)
                return Result.Success(true);

            return await Task.FromResult(Result.Failure<bool>(result.Error));


        }


        private bool CheckStateIsValid(string postCode)
        {
            StatesEnum res;

            if (EnumHelper<StatesEnum>.TryParse(postCode, out res))
            {
                return false;
            }

            return true;
        }

        private bool CheckSubIsValid(string sub)
        {
            //ToDo Fix
            return sub.Length >= 3;

        }

        private bool CheckPostCodeIsValid(string postCode)
        {
            if (postCode.IsInt())
            {
                return  postCode.Length == 4;
            }

            return false;
        }

    }


}

