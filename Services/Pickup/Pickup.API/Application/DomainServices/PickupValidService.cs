﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Bex.Utilities.Helper;
using CSharpFunctionalExtensions;
using Microsoft.EntityFrameworkCore;
using PickupSvc.API.Application.DomainServices.Interfaces;
using PickupSvc.Infrastructure;

namespace PickupSvc.API.Application.DomainServices
{
    public class PickupValidService : IPickupValidService
    {
        
        private List<string> _errors = new List<string>();
        private PickupContext _context;

        public PickupValidService(PickupContext context)
        {
            
            _context = context;
        }

        public async Task<Result<bool>> ValidPickupIdAsync(int id)
        {
            var result = await _context.Pickups
                .AnyAsync(u => u.Id == id)
                .ConfigureAwait(false);

            if (result)
                return Result.Success(true);

            return Result.Failure<bool>($"Pickup With Id:{id} does not Exist");
        }

        public async Task<Result<bool>> ValidPickupNumberAsync(string id)
        {
            var result = await _context.Pickups
                .AnyAsync(u => u.Id == id.ToInt())
                .ConfigureAwait(false);

            if (result)
                return Result.Success(true);

            return Result.Failure<bool>($"Pickup With Id:{id} does not Exist");
        }

        public async Task<Result<bool>> ValidPickupNumberAsync(int id)
        {
            var result = await _context.Pickups
                .AnyAsync(u => u.Id == id)
                .ConfigureAwait(false);

            if (result)
                return Result.Success(true);

            return Result.Failure<bool>($"Pickup With Id:{id} does not Exist");
        }

        


        

    }
}