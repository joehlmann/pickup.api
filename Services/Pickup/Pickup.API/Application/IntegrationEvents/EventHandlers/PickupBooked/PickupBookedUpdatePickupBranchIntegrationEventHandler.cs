﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CorrelationId;
using EventBusAbstract.Abstractions;
using MediatR;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using PickupSvc.API.Application.Commands.Pickups;
using PickupSvc.API.Application.IntegrationEvents.Events;
using PickupSvc.API.Infrastructure;
using PickupSvc.Infrastructure.TokenContext;

namespace PickupSvc.API.Application.IntegrationEvents.EventHandlers.PickupBooked
{
    public class PickupBookedUpdatePickupBranchIntegrationEventHandler : IIntegrationEventHandler<PickupBookedIntegrationEvent>
    {
        private readonly IMediator _mediator;
        private readonly ILogger _logger;
        private readonly ICorrelationContextAccessor _correlationContext;
        private readonly ITokenContextService _tokenContext;
        
        public PickupBookedUpdatePickupBranchIntegrationEventHandler(
            ILogger<PickupBookedUpdatePickupBranchIntegrationEventHandler> logger,
            ICorrelationContextAccessor correlationContext,
            ITokenContextService tokenContextService,
            IMediator mediator)
        {

            _logger = logger ?? throw new ArgumentNullException(nameof(logger)); 
            _correlationContext = correlationContext ?? throw new ArgumentNullException(nameof(correlationContext));
            _tokenContext = tokenContextService ?? throw new ArgumentNullException(nameof(tokenContextService));
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
        }
        public async Task Handle(PickupBookedIntegrationEvent eventMsg)
        {
            
               
                _tokenContext.SetAccessToken(eventMsg.AuthToken);
            
                using (_logger.BeginScope(eventMsg.GenerateLoggingMetaData()))
                {
                    var pickupBranchRequest = GenerateUpdatePickupBranchRequest(eventMsg);

                    await ProcessUpdatePickupBranchRequest(pickupBranchRequest);

                    _logger.LogInformation($"PickupBooked integration event has been handled ",eventMsg);
                }
            
            
        }

        public Task<PickupBranchUpdateResponse> GenerateUpdatePickupBranchRequest(PickupBookedIntegrationEvent eventMsg)
        {
            return _mediator.Send(new PickupBranchUpdateCommand(eventMsg.PickupId,eventMsg.PickupNumber,eventMsg.PickupSuburb, eventMsg.PickupPostcode));
        }

        public async Task ProcessUpdatePickupBranchRequest(Task<PickupBranchUpdateResponse> pickupBranchResponseTask)
        {
            var pickupBranchResponse = await pickupBranchResponseTask;
            if (pickupBranchResponse.Errors.Any())
            {
                using (_logger.BeginScope(new Dictionary<string, object> {{"Errors", JsonConvert.SerializeObject(pickupBranchResponse.Errors)}}))
                {
                    // TODO: Dispatch Update pickup branch failure event
                    _logger.LogWarning($"Pickup branch failed to be set for pickup number {pickupBranchResponse.PickupNumber}");
                }
                
            }
            else
            {
                _logger.LogInformation($"Pickup branch successfully set for pickup number {pickupBranchResponse.PickupNumber}");
            }
        }

        

        //public Dictionary<string, object> GenerateLoggingMetaData(IntegrationEvent eventMsg)
        //{
        //    var correlationId = eventMsg.CorrelationId;
        //    var correlationName = eventMsg.CorrelationName;

        //    if (string.IsNullOrEmpty(correlationId))
        //    {
        //        correlationId = Guid.NewGuid().ToString();
        //    }

        //    correlationName = string.IsNullOrEmpty(correlationName) ? "RequestId" : correlationName;

        //    var loggingMetaData = new Dictionary<string, object>()
        //    {
        //        {correlationName, correlationId}
        //    };

        //    // Set the correlation data
        //    new CorrelationContextFactory().Create(correlationId, correlationName);
        //    eventMsg.SetCorrelationDetails(correlationId, correlationName);
        //    return loggingMetaData;
        //}

    }
}
