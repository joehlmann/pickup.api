﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CorrelationId;
using EventBusAbstract.Abstractions;
using EventBusAbstract.Events;
using MediatR;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using PickupSvc.API.Application.Commands.Pickups;
using PickupSvc.API.Application.IntegrationEvents.Events;
using PickupSvc.Infrastructure.TokenContext;

namespace PickupSvc.API.Application.IntegrationEvents.EventHandlers.NewPickupBranchSet
{
    public class NewPickupBranchSetSetReceiverDetailsIntegrationEventHandler : IIntegrationEventHandler<NewPickupBranchSetIntegrationEvent>
    {
        private readonly IMediator _mediator;
        private readonly ILogger _logger;
        private readonly ITokenContextService _tokenContext;
        private ICorrelationContextAccessor _correlationContext;


        public NewPickupBranchSetSetReceiverDetailsIntegrationEventHandler(
            ILogger<NewPickupBranchSetSetReceiverDetailsIntegrationEventHandler> logger, ICorrelationContextAccessor correlationContext,
            ITokenContextService tokenContextService,
            IMediator mediator)
        {

            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _correlationContext = correlationContext ?? throw new ArgumentNullException(nameof(correlationContext));
            _tokenContext = tokenContextService ?? throw new ArgumentNullException(nameof(tokenContextService));
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
        }

        public Task<DeterminePickupReceiverDetailsResponse> GenerateDeterminePickupReceiverDetailsRequest(NewPickupBranchSetIntegrationEvent eventMsg)
        {
            
            return _mediator.Send(new DeterminePickupReceiverDetailsCommand(eventMsg.PickupId, eventMsg.BranchId));
        }

        public async Task ProcessDeterminePickupReceiverDetailsRequest(Task<DeterminePickupReceiverDetailsResponse> pickupReceiverDetailsResponseTask)
        {
            var pickupReceiverDetailsResponse = await pickupReceiverDetailsResponseTask;
            if (pickupReceiverDetailsResponse.Errors.Any())
            {
                using (_logger.BeginScope(new Dictionary<string, object> {{"Errors", JsonConvert.SerializeObject(pickupReceiverDetailsResponse.Errors)}}))
                {
                    // TODO: Dispatch Pickup Receiver Details failure event
                    _logger.LogWarning($"Pickup receiver details failed to be set for pickup number {pickupReceiverDetailsResponse.PickupNumber}");
                }
            }
            else
            {
                _logger.LogInformation($"Pickup receiver details successfully set for pickup number {pickupReceiverDetailsResponse.PickupNumber}");
            }
        }

        public async Task Handle(NewPickupBranchSetIntegrationEvent eventMsg)
        {
            _tokenContext.SetAccessToken(eventMsg.AuthToken);
            using (_logger.BeginScope(GenerateLoggingMetaData(eventMsg)))
            {
                var result = GenerateDeterminePickupReceiverDetailsRequest(eventMsg);
                await ProcessDeterminePickupReceiverDetailsRequest(result);

                _logger.LogInformation($"Pickup branch set integration event has been handled ", eventMsg);
            }
        }

        public Dictionary<string, object> GenerateLoggingMetaData(IntegrationEvent eventMsg)
        {
            var correlationId = eventMsg.CorrelationId;
            var correlationName = eventMsg.CorrelationName;

            if (string.IsNullOrEmpty(correlationId))
            {
                correlationId = Guid.NewGuid().ToString();
            }

            correlationName = string.IsNullOrEmpty(correlationName) ? "request-id" : correlationName;

            var loggingMetaData = new Dictionary<string, object>()
            {
                {correlationName, correlationId}
            };

            // Set the correlation data
            new CorrelationContextFactory().Create(correlationId, correlationName);
            eventMsg.SetCorrelationDetails(correlationId, correlationName);
            return loggingMetaData;
        }

        
    }
}
