﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CorrelationId;
using EventBusAbstract.Abstractions;
using MediatR;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using PickupSvc.API.Application.Commands.Pickups;
using PickupSvc.API.Application.IntegrationEvents.Events.ReceiverAddressOnPickupChanged;
using PickupSvc.API.Infrastructure;
using PickupSvc.Infrastructure.TokenContext;

namespace PickupSvc.API.Application.IntegrationEvents.EventHandlers.ReceiverAddressOnPickupChanged
{
    public class ReceiverAddressOnPickupChangedSetReceivingRegionIntegrationEventHandler : IIntegrationEventHandler<ReceiverAddressOnPickupChangedIntegrationEvent>
    {
        private readonly IMediator _mediator;
        private readonly ILogger _logger;
        private readonly ICorrelationContextAccessor _correlationContext;
        private readonly ITokenContextService _tokenContextService;
        
        public ReceiverAddressOnPickupChangedSetReceivingRegionIntegrationEventHandler(ILogger<ReceiverAddressOnPickupChangedSetReceivingRegionIntegrationEventHandler> logger,
            ICorrelationContextAccessor correlationContext,
            ITokenContextService tokenContextService,
            IMediator mediator)
        {

            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _correlationContext = correlationContext ?? throw new ArgumentNullException(nameof(correlationContext));
            _tokenContextService = tokenContextService ?? throw new ArgumentNullException(nameof(tokenContextService));
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
        }
        public async Task Handle(ReceiverAddressOnPickupChangedIntegrationEvent @event)
        {
            _tokenContextService.SetAccessToken(@event.AuthToken);

            using (_logger.BeginScope(@event.GenerateLoggingMetaData()))
            {
                var receivingRegionRequest = GenerateUpdatePickupItemsReceivingRegionRequest(@event);

                await ProcessUpdatePickupItemsReceivingRegionRequest(receivingRegionRequest);

                _logger.LogInformation($"ReceiverAddressOnPickupChanged integration event has been handled ",@event);
            }
        }
        public Task<PickupItemsReceivingRegionUpdateResponse> GenerateUpdatePickupItemsReceivingRegionRequest(ReceiverAddressOnPickupChangedIntegrationEvent eventMsg)
        {
            return _mediator.Send(new PickupItemsReceivingRegionUpdateCommand(eventMsg.PickupId, eventMsg.PickupNumber, eventMsg.ReceiverSuburb,eventMsg.ReceiverPostcode));
        }

        public async Task ProcessUpdatePickupItemsReceivingRegionRequest(Task<PickupItemsReceivingRegionUpdateResponse> receivingRegionResponseTask)
        {
            var receivingRegionResponse = await receivingRegionResponseTask;
            if (receivingRegionResponse.Errors.Any())
            {
                using (_logger.BeginScope(new Dictionary<string, object> {{"Errors", JsonConvert.SerializeObject(receivingRegionResponse.Errors)}}))
                {
                    // TODO: Dispatch Update pickup items receiving region failure event
                    _logger.LogWarning($"Pickup items receiving region failed to be set for pickup number {receivingRegionResponse.PickupNumber}");
                }
                
            }
            else
            {
                _logger.LogInformation($"Pickup items receiving region successfully set for pickup number {receivingRegionResponse.PickupNumber}");
            }
        }

       
    }
}
