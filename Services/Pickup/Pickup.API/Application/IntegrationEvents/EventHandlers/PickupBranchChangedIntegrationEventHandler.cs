﻿using System;
using System.Threading.Tasks;
using CorrelationId;
using EventBusAbstract.Abstractions;
using MediatR;
using Microsoft.Extensions.Logging;
using PickupSvc.API.Application.IntegrationEvents.Events;
using PickupSvc.API.Infrastructure;
using PickupSvc.Infrastructure.TokenContext;

namespace PickupSvc.API.Application.IntegrationEvents.EventHandlers
{
    public class PickupBranchChangedIntegrationEventHandler : IIntegrationEventHandler<PickupBranchChangedIntegrationEvent>
    {
        private readonly IMediator _mediator;
        private readonly ILogger _logger;
        private readonly ICorrelationContextAccessor _correlationContext;
        private readonly ITokenContextService _tokenContextService;
        

        public PickupBranchChangedIntegrationEventHandler(
            ILogger<PickupBranchChangedIntegrationEventHandler> logger, ICorrelationContextAccessor correlationContext,
            ITokenContextService tokenContextService,
            IMediator mediator)
        {

            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _correlationContext = correlationContext ?? throw new ArgumentNullException(nameof(correlationContext));
            _tokenContextService = tokenContextService ?? throw new ArgumentNullException(nameof(tokenContextService));
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
        }

        

        public async Task Handle(PickupBranchChangedIntegrationEvent eventMsg)
        {
            using (_logger.BeginScope(eventMsg.GenerateLoggingMetaData()))
            {
                _logger.LogInformation($"Pickup branch changed integration event has been handled ", eventMsg);
            }

        }
    }
}
