﻿using System;
using System.Threading.Tasks;
using EventBusAbstract.Events;

namespace PickupSvc.API.Application.IntegrationEvents
{
    public interface IPickupIntegrationEventSvc
    {
        Task PublishEventsThroughEventBusAsync(Guid transactionId);
        Task AddAndSaveEventAsync(IntegrationEvent evt);
    }
}