﻿using System.Threading.Tasks;
using EventBusAbstract.Events;

namespace PickupSvc.API.Application.IntegrationEvents
{
    public interface IPickupIntegrationEventService
    {
        Task PublishThroughEventBusAsync(IntegrationEvent evt);



    }
}