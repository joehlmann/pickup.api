﻿using System;
using System.Threading.Tasks;
using CorrelationId;
using EventBusAbstract.Abstractions;
using EventBusAbstract.Events;
using PickupSvc.Infrastructure.TokenContext;

namespace PickupSvc.API.Application.IntegrationEvents
{
    public class PickupIntegrationEventService : IPickupIntegrationEventService
    {
        private readonly IEventBus _eventBus;
        private readonly ICorrelationContextAccessor _correlationContext;
        private readonly ITokenContextService _tokenContext;
        public PickupIntegrationEventService(IEventBus eventBus, ICorrelationContextAccessor correlationContext, ITokenContextService tokenContext)
        {
            _eventBus = eventBus ?? throw new ArgumentNullException(nameof(eventBus));
            _correlationContext = correlationContext;
            _tokenContext = tokenContext ?? throw new ArgumentNullException(nameof(tokenContext));
        }
        public Task PublishThroughEventBusAsync(IntegrationEvent evt)
        {
            SetCorrelationDetails(evt);
            evt.SetAuthToken(_tokenContext.GetAccessToken());

            //TODO: Handle failure to add to queue
            _eventBus.Publish(evt);
            return Task.CompletedTask;
        }

        public void SetCorrelationDetails(IntegrationEvent evt)
        {
            //BUG: Correlation Context times out before the next event gets here
            var correlationId = _correlationContext?.CorrelationContext?.CorrelationId;
            var correlationHeader = _correlationContext?.CorrelationContext?.Header;


            if (string.IsNullOrEmpty(correlationId))
            {
                correlationId = Guid.NewGuid().ToString();
            }

            correlationHeader = string.IsNullOrEmpty(correlationHeader) ? "request-id" : correlationHeader;
            evt.SetCorrelationDetails(correlationId,correlationHeader);
        }
    }
}
