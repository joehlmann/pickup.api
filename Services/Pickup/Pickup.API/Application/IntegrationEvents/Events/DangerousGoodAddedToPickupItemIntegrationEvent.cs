﻿using EventBusAbstract.Events;

namespace PickupSvc.API.Application.IntegrationEvents.Events
{
    public class DangerousGoodAddedToPickupItemIntegrationEvent : IntegrationEvent
    {
        public int ItemId { get; }
        public int UNNumber { get; }
        public double Class { get; }

        public DangerousGoodAddedToPickupItemIntegrationEvent(int itemId, int uNNumber, double classType)
        {
            UNNumber = uNNumber;
            Class = classType;
            ItemId = itemId;
        }
    }
}
