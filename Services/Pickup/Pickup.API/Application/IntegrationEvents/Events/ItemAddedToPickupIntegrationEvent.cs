﻿using EventBusAbstract.Events;

namespace PickupSvc.API.Application.IntegrationEvents.Events
{
    public class ItemAddedToPickupIntegrationEvent : IntegrationEvent
    {
        public int PickupId { get; }
        public int PickupNumber { get; }
        public int ItemId { get; }

        public ItemAddedToPickupIntegrationEvent(int pickupId, int pickupNumber, int itemId)
        {
            PickupId = pickupId;
            PickupNumber = pickupNumber;
            ItemId = itemId;
        }
    }
}
