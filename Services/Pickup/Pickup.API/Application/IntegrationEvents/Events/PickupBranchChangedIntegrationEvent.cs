﻿using EventBusAbstract.Events;
using MediatR;
using Newtonsoft.Json;

namespace PickupSvc.API.Application.IntegrationEvents.Events
{
    public class PickupBranchChangedIntegrationEvent : IntegrationEvent , INotification
    {
        public int PickupId { get; }
        public int PickupNumber { get; }
        public int BranchId { get; }
        public string BranchName { get; }

        public PickupBranchChangedIntegrationEvent(int pickupId, int pickupNumber, int branchId, string branchName)
        {
            PickupId = pickupId;
            PickupNumber = pickupNumber;
            BranchId = branchId;
            BranchName = branchName;
        }

        [JsonConstructor]
        public PickupBranchChangedIntegrationEvent(int pickupId, int pickupNumber, int branchId, string branchName,string correlationId, string correlationName,string authToken)
        {
            PickupId = pickupId;
            PickupNumber = pickupNumber;
            BranchId = branchId;
            BranchName = branchName;
            SetCorrelationDetails(correlationId,correlationName);
            SetAuthToken(authToken);
        }
    }
}
