﻿using System;
using EventBusAbstract.Events;
using Newtonsoft.Json;

namespace PickupSvc.API.Application.IntegrationEvents.Events
{
    public class PickupBookedIntegrationEvent : IntegrationEvent
    {
        public int PickupId { get; }
        public int PickupNumber { get; }
        public string PickupStatus { get; }
        public string PickupSuburb { get; }
        public string PickupPostcode { get; }
        public DateTimeOffset? PickupBookedDateTime { get; }
        public DateTime PickupCloseTime { get; }

        public PickupBookedIntegrationEvent(int pickupId, int pickupNumber, string pickupStatus, string pickupSuburb, string pickupPostcode, DateTimeOffset? pickupBookedDateTime, DateTime pickupCloseTime)
        {
            PickupId = pickupId;
            PickupStatus = pickupStatus;
            PickupNumber = pickupNumber;
            PickupSuburb = pickupSuburb;
            PickupPostcode = pickupPostcode;
            PickupBookedDateTime = pickupBookedDateTime;
            PickupCloseTime = pickupCloseTime;
        }

        [JsonConstructor]
        public PickupBookedIntegrationEvent(int pickupId, int pickupNumber, string pickupStatus, string pickupSuburb, string pickupPostcode, DateTimeOffset? pickupBookedDateTime, DateTime pickupCloseTime, string correlationId, string correlationName,string authToken)
        {
            PickupId = pickupId;
            PickupStatus = pickupStatus;
            PickupNumber = pickupNumber;
            PickupSuburb = pickupSuburb;
            PickupPostcode = pickupPostcode;
            PickupBookedDateTime = pickupBookedDateTime;
            PickupCloseTime = pickupCloseTime;
            SetCorrelationDetails(correlationId,correlationName);
            SetAuthToken(authToken);
        }

    }
}
