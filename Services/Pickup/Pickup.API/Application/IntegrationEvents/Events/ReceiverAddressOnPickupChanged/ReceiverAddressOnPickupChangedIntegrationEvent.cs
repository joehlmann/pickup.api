﻿using EventBusAbstract.Events;

namespace PickupSvc.API.Application.IntegrationEvents.Events.ReceiverAddressOnPickupChanged
{
    public class ReceiverAddressOnPickupChangedIntegrationEvent : IntegrationEvent
    {
        public int PickupId { get;set; }
        public int PickupNumber { get; }
        public string ReceiverPostcode { get; }
        public int ReceiverSuburbId { get; }
        public string ReceiverSuburb { get; }
        public string ReceiverState { get; }

        public ReceiverAddressOnPickupChangedIntegrationEvent(int pickupId,
            int pickupNumber,
            int receiverSuburbId,
            string receiverSuburb,
            string receiverState,
            string receiverPostcode)
        {
            PickupId = pickupId;
            PickupNumber = pickupNumber;
            ReceiverPostcode = receiverPostcode;
            ReceiverSuburb = receiverSuburb;
            ReceiverState = receiverState;
            ReceiverSuburbId = receiverSuburbId;
        }
    }
}
