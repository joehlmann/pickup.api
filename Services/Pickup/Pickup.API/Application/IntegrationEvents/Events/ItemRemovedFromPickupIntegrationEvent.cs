﻿using EventBusAbstract.Events;

namespace PickupSvc.API.Application.IntegrationEvents.Events
{
    public class ItemRemovedFromPickupIntegrationEvent : IntegrationEvent
    {
        public int PickupId { get; }
        public int PickupNumber { get; }
        public int ItemId { get; }

        public ItemRemovedFromPickupIntegrationEvent(int pickupId, int pickupNumber, int pickupItemId)
        {
            PickupId = pickupId;
            PickupNumber = pickupNumber;
            ItemId = pickupItemId;
        }
    }
}
