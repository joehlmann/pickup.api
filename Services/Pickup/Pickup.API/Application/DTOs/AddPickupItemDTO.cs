﻿using System.Collections.Generic;

namespace PickupSvc.API.Application.DTOs
{
    public class AddPickupItemDTO
    {
        public int Quantity { get; set; }
        public string Description { get; set; }
        public int WeightKg { get; set; }
        public bool FoodStuff { get; set; }
        public List<DangerousGoodItemDTO> DangerousGoods { get;set; }
    }
}
