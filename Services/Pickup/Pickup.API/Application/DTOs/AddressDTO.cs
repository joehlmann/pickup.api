﻿namespace PickupSvc.API.Application.DTOs
{
    public class AddressDTO
    {
        public string Address1 { get;  set; }
        public string Address2 { get;  set; }
        public string PostCode { get;  set; }
        public string State { get;  set; }
        public string Suburb { get;  set; }


        public static AddressDTO Empty()
        {
            return new AddressDTO()
            {
                Address1 = "",
                Address2 = "",
                PostCode = "",
                State = "",
                Suburb = ""
            };
        }

    }
}
