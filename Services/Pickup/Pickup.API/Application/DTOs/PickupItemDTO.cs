using System.Collections.Generic;

namespace PickupSvc.API.Application.DTOs
{
    public class PickupItemDTO
    {
        // Added DgGoods new branch
        public int Id { get;set; }
        public string Description { get;set; }
        public int WeightKg { get;set; }
        public int Quantity {get; set; }

        public bool FoodStuff { get; set; }
        public List<DangerousGoodItemDTO> DangerousGoods { get; set; }

        public string PickupItemType { get; set; }



        public static PickupItemDTO Empty()
        {
            return new PickupItemDTO()
            {
                PickupItemType = DtoState.Empty()
            };
        }
    }
}
