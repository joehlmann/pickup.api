﻿using System.Collections.Generic;

namespace PickupSvc.API.Application.DTOs
{
    public class PickupItemsDTO
    {
        public List<PickupItemDTO> Items { get;set; }
        
    }
}
