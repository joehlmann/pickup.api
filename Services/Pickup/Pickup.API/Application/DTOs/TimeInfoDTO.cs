﻿namespace PickupSvc.API.Application.DTOs
{
    public class TimeInfoDTO
    {
        public string PickupDate { get;  set; }
        public string ReadyTime { get;  set; }
        public string CloseTime { get; set; }

        public string BookedDateTime { get;  set; }



        public static TimeInfoDTO Empty()
        {
            return new TimeInfoDTO()
            {
                PickupDate = "",
                ReadyTime = "",
                CloseTime = "",
                BookedDateTime = ""
            };
        }

    }
}
