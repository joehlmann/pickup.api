﻿using PickupSvc.TheDomain.Enumerators;

namespace PickupSvc.API.Application.DTOs
{
    public class PalletDto
    {
        public bool HasPallets { get;  set; }
        public PalletType PalletType { get;  set; }
        public string ChepPalletTransferDocketNumber { get;  set; }


        public int ExchangeChepPalletsIn { get;  set; }
        public int ExchangeChepPalletsOut { get;  set; }

        public int TransferChepPalletsToBex { get;  set; }
        public int TransferChepPalletsToReceiver { get;  set; }

        public string LoscamPalletTransferDocketNumber { get;  set; }


        public int ExchangeLoscamPalletsIn { get;  set; }
        public int ExchangeLoscamPalletsOut { get;  set; }

        public int TransferLoscamPalletsToBex { get;  set; }
        public int TransferLoscamPalletsToReceiver { get;  set; }



        public static PalletDto Empty()
        {
            return new PalletDto()
            {
                PalletType = PalletType.Empty,
                ChepPalletTransferDocketNumber = "",
                LoscamPalletTransferDocketNumber = ""
            };
        }

    }
}
