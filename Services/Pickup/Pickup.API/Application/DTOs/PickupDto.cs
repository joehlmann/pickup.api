﻿using System;

namespace PickupSvc.API.Application.DTOs
{
    public class PickupDTO
    {
        public int Number { get; set; }
        public DateTime PickupDate { get; set; }
        public string ReadyTime { get; set; }
        public string CloseTime { get; set; }

        public string PickupType { get; set; }

        public static PickupDTO Empty()
        {
            return new PickupDTO()
            {
                PickupType = DtoState.Empty()
            };
        }
    }
}
