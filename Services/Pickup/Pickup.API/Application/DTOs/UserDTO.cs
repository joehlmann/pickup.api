﻿namespace PickupSvc.API.Application.DTOs
{
    public class UserDTO
    {

        public string UserId { get; set; }

        public string AliasName { get;  set; }

        public string Phone { get;  set; }

        public string UserStatus { get;  set; }
        

        

        public string UserType { get; set; }


        public static UserDTO Empty()
        {
            return new UserDTO()
            {
                UserType = DtoState.Empty()
            };
        }
    }
}
