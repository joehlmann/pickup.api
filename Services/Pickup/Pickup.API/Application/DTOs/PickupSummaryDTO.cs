﻿using System;
using System.Collections.Generic;

namespace PickupSvc.API.Application.DTOs
{
    public class PickupSummaryDTO
    {

        public int Id { get; set; }
        public string Number { get; set; }

        public DateTime PickupDate { get; set; }
        public DateTime ReadyTime { get; set; }
        public DateTime CloseTime { get; set; }

        public string SpecialInstructions { get; set; }
        public string SenderName { get; set; }
        public string SenderCode { get; set; }
        public string SenderId { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public int SuburbId { get; set; }
        public string State { get; set; }
        public string Postcode { get; set; }
        public string PickupBranch { get; set; }
        public string ContactName { get; set; }
        public string ContactPhone { get; set; }

        public bool FreightPayableByThirdParty { get; set; }
        public string ChargeAccountCode { get; set; }
        public string ChargeAccountForeignKey { get; set; }


        public int ReceiverSuburbId { get; set; }
        public string ReceiverState { get; set; }
        public string ReceiverPostcode { get; set; }

        // Pallets
        public bool PalletsNotApplicable { get; set; }
        public int PalletType { get; set; }

        public string ChepPalletTransferDocketNumber { get; set; }

        public int ExchangeChepPalletsOut { get; set; }
        public int TransferChepPalletsToBex { get; set; }
        public int TransferChepPalletsToReceiver { get; set; }

        public string LoscamPalletTransferDocketNumber { get; set; }

        public int ExchangeLoscamPalletsOut { get; set; }
        public int TransferLoscamPalletsToBex { get; set; }
        public int TransferLoscamPalletsToReceiver { get; set; }

        public bool HasChep { get; set; }
        public bool HasLoscam { get; set; }

        public string CreatedUser { get; set; }
        public DateTime BookedTime { get; set; }
        public int HasDangerousGoods { get; set; }
        public int HasDangerousGoodsInsurance { get; set; }
        public string DangerousGoodClasses { get; set; }
        public string DangerousGoodUNNumbers { get; set; }

        // Items
        public List<PickupItemDTO> Items { get; set; }

    }
}
