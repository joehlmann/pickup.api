﻿using MediatR;

namespace PickupSvc.API.Application.Commands.PickupItems
{
    public class PickupItemRemoveCommand : IRequest<PickupItemRemoveResponse>
    {
        public int PickupItemId { get; private set; }
        public int UserId { get; private set; }


        public PickupItemRemoveCommand(int pickupItemId, int userId)
        {
            PickupItemId = pickupItemId;
            UserId = userId;
        }
    }
}
