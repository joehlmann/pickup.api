﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.Extensions.Logging;
using PickupSvc.API.Application.DTOs;
using PickupSvc.Infrastructure.Repositories.Interfaces;
using PickupSvc.TheDomain.Entities;
using PickupSvc.TheDomain.ValueObjects;

namespace PickupSvc.API.Application.Commands.PickupItems
{
    public class PickupItemAddRequestHandler : IRequestHandler<PickupItemAddCommand, PickupItemAddResponse>
    {
        private readonly IPickupRepository _pickupRepository;
        private readonly IDangerousGoodRepository _dangerousGoodRepository;
        private readonly ILogger<PickupItemAddRequestHandler> _logger;

        public PickupItemAddRequestHandler(IPickupRepository pickupRepository, ILogger<PickupItemAddRequestHandler> logger, IDangerousGoodRepository dangerousGoodRepository)
        {
            _pickupRepository = pickupRepository ?? throw new ArgumentNullException(nameof(pickupRepository));
            _dangerousGoodRepository = dangerousGoodRepository ?? throw new ArgumentNullException(nameof(dangerousGoodRepository));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }
        public async Task<PickupItemAddResponse> Handle(PickupItemAddCommand request, CancellationToken cancellationToken)
        {
            var result = new PickupItemAddResponse();

            // Get the pickup
            var pickupTask = _pickupRepository.GetAsync(request.PickupNumber, cancellationToken);
            
            var dgList = new List<DangerousGood>();

            if (request.DangerousGoods.Any())
            {
                dgList = await LoadDGDetail(request.DangerousGoods);
            }


            // TODO Fix this crap
            var newItem = new PickupItem(
                request.Quantity,
                request.Description,
                request.WeightKg,
                request.FoodStuff,
                dgList
                );
            
            var pickup = await pickupTask;

            // Add the pickup item
            pickup.AddItem(newItem);

            // Check errors
            if (pickup.HasErrors)
            {
                result.Errors.AddRange(pickup.Errors);
            }

            // Any item errors?
            if (pickup.Items.Where(x => x.HasErrors).Select(x => x.HasErrors).Any())
            {
                result.Errors.AddRange(pickup.Items.Where(x => x.HasErrors).SelectMany(x => x.Errors));
            }
            
            // Return now as it's failed
            if (result.Errors.Any())
            {
                return result;
            }
            // Push the data to the database context
            _pickupRepository.Update(pickup);

            // Commit the changes
            await _pickupRepository.UnitOfWork.SaveEntitiesAsync(cancellationToken);

            // Refresh the data from storage cache (required to get the database ids)
            RestorePickupDbId(ref pickup);
   
            result.Id = GetPickupItemId(pickup,newItem);
            return result;
        }

        public void RestorePickupDbId(ref Pickup pickup)
        {
            try
            {
                // Refresh the data from storage cache (required to get the database ids)
               // pickup = _pickupRepository.GetCacheOnly(pickup.Id);
            }
            catch (Exception e)
            {
                _logger.LogError("Pickup {pickupNumber} not found in local repository cache.",pickup.PickupNumber);
                throw;
            }
            
        }

        public int GetPickupItemId(Pickup pickup, PickupItem item)
        {
            try
            {
                return pickup.Items.ToList().First(x => x.Id == item.Id).Id ;
            }
            catch (Exception e)
            {
                _logger.LogError("Pickup item on pickup {pickupNumber} not found in local repository cache.",pickup.PickupNumber);
                throw;
            }
            
        }

        public async Task<List<DangerousGood>> LoadDGDetail(List<DangerousGoodItemDTO> dangerousGoods)
        {
            // Lookup DG info
            var dgTasks = dangerousGoods.Select(x => x.UNNo).Distinct().Select(dgUN => _dangerousGoodRepository.GetByUNNumberAsync(dgUN)).ToArray();

            await Task.WhenAll(dgTasks);

            var dgDetails = new Dictionary<int, DangerousGood>();
            foreach (var dgTask in dgTasks)
            {
                if (dgTask.Result.UNNo > 0)
                {
                    dgDetails[dgTask.Result.UNNo] = dgTask.Result;
                }
            }

            return dangerousGoods.Select(x => dgDetails[x.UNNo] ?? new DangerousGood(x.UNNo)).ToList();
        }
    }


    
}
