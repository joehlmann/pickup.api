﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.Extensions.Logging;
using PickupSvc.Infrastructure.Repositories.Interfaces;
using PickupSvc.TheDomain.Infrastructure;

namespace PickupSvc.API.Application.Commands.PickupItems
{
    public class PickupItemRemoveCommandHandler : IRequestHandler<PickupItemRemoveCommand,PickupItemRemoveResponse>
    {
        private readonly IPickupRepository _pickupRepository;
        private readonly ILogger<PickupItemRemoveCommandHandler> _logger;

        public PickupItemRemoveCommandHandler(IPickupRepository pickupRepository, ILogger<PickupItemRemoveCommandHandler> logger)
        {
            _pickupRepository = pickupRepository ?? throw new ArgumentNullException(nameof(pickupRepository));
            
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task<PickupItemRemoveResponse> Handle(PickupItemRemoveCommand request, CancellationToken cancellationToken)
        {
            var result = new PickupItemRemoveResponse();
            var pickup = await _pickupRepository.GetByItemIdAsync(request.PickupItemId, cancellationToken);
            if (pickup != null)
            {
                var pickupItem = pickup.Items.FirstOrDefault(i => i.Id == request.PickupItemId);

                if (pickupItem != null)
                {
                    pickup.RemoveItem(pickupItem);
                }
            
                // Check errors
                if (pickup.HasErrors)
                {
                    result.Errors.AddRange(pickup.Errors);
                }

                // Any item errors?
                if (pickup.Items.Where(x => x.HasErrors).Select(x => x.HasErrors).Any())
                {
                    result.Errors.AddRange(pickup.Items.Where(x => x.HasErrors).SelectMany(x => x.Errors));
                }
            }
            else
            {
                result.Errors.Add(ValidationMessages.PickupItemUnknown);
            }
            
            
            // Return now as it's failed
            if (result.Errors.Any())
            {
                return result;
            }

            // Push the data to the database context
            _pickupRepository.Update(pickup);

            // Commit the changes
            await _pickupRepository.UnitOfWork.SaveEntitiesAsync(cancellationToken);

            return result;
        }
    }
}
