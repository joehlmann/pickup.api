﻿using System.Collections.Generic;
using MediatR;
using PickupSvc.API.Application.DTOs;

namespace PickupSvc.API.Application.Commands.PickupItems
{
    public class PickupItemAddCommand : IRequest<PickupItemAddResponse>
    {
        public int PickupNumber { get; set; }
        public int Quantity { get; private set; }
        public string Description { get; private set; }
        public int WeightKg { get; private set; }
        public bool FoodStuff { get; private set; }
        public List<DangerousGoodItemDTO> DangerousGoods { get; private set; }


        public PickupItemAddCommand(int pickupNumber, int quantity, string description, int weightKg, bool foodStuff, List<DangerousGoodItemDTO> dangerousGoods)
        {
            PickupNumber = pickupNumber;
            Quantity = quantity;
            Description = description;
            WeightKg = weightKg;
            FoodStuff = foodStuff;
            DangerousGoods = dangerousGoods;
        }
    }
}
