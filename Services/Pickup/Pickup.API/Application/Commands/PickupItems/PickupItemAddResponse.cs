﻿namespace PickupSvc.API.Application.Commands.PickupItems
{
    public class PickupItemAddResponse : BaseResponse
    {
        public int Id { get;set; }
    }
}
