﻿using System;
using System.Collections.Generic;
using MediatR;

namespace PickupSvc.API.Application.Commands.PickupItems
{
    public class PickupItemUpdateCommand : IRequest
    {
        public int Id { get; private set; }
        public int Quantity { get; private set; }
        public string Description { get; private set; }
        public int WeightKg { get; private set; }
        public bool FoodStuff { get; private set; }
        public bool DangerousGoods { get; private set; }
        public List<Decimal> DangerousGoodClasses { get; private set; }


        public PickupItemUpdateCommand(int id, int quantity, string description, int weightKg, bool foodStuff, bool dangerousGoods, List<decimal> dangerousGoodClasses)
        {
            Id = id;
            Quantity = quantity;
            Description = description;
            WeightKg = weightKg;
            FoodStuff = foodStuff;
            DangerousGoods = dangerousGoods;
            DangerousGoodClasses = dangerousGoodClasses;
        }
    }
}
