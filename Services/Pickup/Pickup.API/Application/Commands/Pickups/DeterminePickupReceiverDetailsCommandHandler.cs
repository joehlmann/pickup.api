﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using PickupSvc.Infrastructure.Repositories.Interfaces;
using PickupSvc.TheDomain.Infrastructure;

namespace PickupSvc.API.Application.Commands.Pickups
{
    public class DeterminePickupReceiverDetailsCommandHandler : IRequestHandler<DeterminePickupReceiverDetailsCommand, DeterminePickupReceiverDetailsResponse>
    {
        
        private readonly IPickupRepository _pickupRepository;
        private readonly ISuburbRepository _suburbRepository;
        private readonly IBranchRepository _branchRepository;

        public DeterminePickupReceiverDetailsCommandHandler(ISuburbRepository suburbRepository, IPickupRepository pickupRepository)
        {
            _pickupRepository = pickupRepository;
            _suburbRepository = suburbRepository;
        }
        public async Task<DeterminePickupReceiverDetailsResponse> Handle(DeterminePickupReceiverDetailsCommand request, CancellationToken cancellationToken)
        {
            var result = new DeterminePickupReceiverDetailsResponse();


            //Todo Clean Up this :S


            // Get the branch details
            if (request.PickupBranchId > 0)
            {
                var pickupTask = _pickupRepository.GetAsync(request.PickupId, cancellationToken);
                var branchDetails = await _branchRepository.FindByIdAsync(request.PickupBranchId);

                if (branchDetails.Id > 0)
                {
                    var pickup = await pickupTask;
                    var suburbTask = _suburbRepository.FindByNameAndPostCodeAsync(branchDetails.Address.Suburb,
                        branchDetails.Address.PostCode);

                    if (pickup.Id !=null)
                    {
                        result.PickupNumber = pickup.PickupNumber;

                        // Find the suburb
                        var suburb = await suburbTask;

                        if (suburb.Id > 0)
                        {
                            pickup.SetReceiverAddress(suburb);
                            _pickupRepository.Update(pickup);

                            if (!pickup.HasErrors)
                            {
                                // Save the record
                                await _pickupRepository.UnitOfWork.SaveEntitiesAsync(cancellationToken);
                            }
                            else
                            {
                                result.Errors.AddRange(pickup.Errors);
                            }
                            

                        }
                        else
                        {
                            // Branch suburb not found
                            result.Errors.Add(ValidationMessages.BranchPickupBranchSuburbIsUnknown);
                        }
                        
                    }
                    else
                    {
                        // Pickup not found
                        result.Errors.Add(ValidationMessages.PickupUnknownPickup);
                    }
                }
                else
                {
                    // Branch is unknown
                    result.Errors.Add(ValidationMessages.PickupBranchIsUnknown);
                }
            }
            else
            {
                // Branch must be provided
                result.Errors.Add(ValidationMessages.PickupBranchIsMissing);
            }
            

            
            return result;
        }
    }
}
