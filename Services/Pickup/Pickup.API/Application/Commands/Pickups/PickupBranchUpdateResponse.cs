﻿namespace PickupSvc.API.Application.Commands.Pickups
{
    public class PickupBranchUpdateResponse : BaseResponse
    {
        public int PickupId { get;set; }
        public int PickupNumber { get; set; }
    }
}
