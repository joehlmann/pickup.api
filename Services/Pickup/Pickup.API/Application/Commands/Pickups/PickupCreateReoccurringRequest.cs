﻿using MediatR;

namespace PickupSvc.API.Application.Commands.Pickups
{
    public class PickupCreateReoccurringRequest : IRequest<PickupCreateReoccurringResponse>
    {
    }
}
