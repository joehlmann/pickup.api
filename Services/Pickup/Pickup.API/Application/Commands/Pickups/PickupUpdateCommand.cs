﻿using System.Collections.Generic;
using MediatR;
using PickupSvc.API.Application.Commands.PickupItems;
using PickupSvc.API.Application.DTOs;

namespace PickupSvc.API.Application.Commands.Pickups
{
    public class PickupUpdateCommand : IRequest<PickupUpdateResponse>
    {
        public int Id { get; private set; }
        public string Number { get; private set; }

        public string SenderName { get; private set; }

        public string ChargeAccountCode { get; private set; }
        public string SpecialInstructions { get; private set; }

        public bool HirePallets { get; private set; }
        
   
        public TimeInfoDTO TimeInfo { get; private set; }

        public AddressDTO Address { get; private set; }

        public ContactDTO Contact { get; private set; }

        public List<PickupItemUpdateCommand> Items { get; private set; }

        public PickupUpdateCommand(int id, string number, string senderName, string chargeAccountCode, string specialInstructions, bool hirePallets, TimeInfoDTO timeInfo, AddressDTO address, ContactDTO contact, List<PickupItemUpdateCommand> items)
        {
            Id = id;
            Number = number;
            SenderName = senderName;
            ChargeAccountCode = chargeAccountCode;
            SpecialInstructions = specialInstructions;
            HirePallets = hirePallets;
            TimeInfo = timeInfo;
            Address = address;
            Contact = contact;
            Items = items;
        }
    }
}
