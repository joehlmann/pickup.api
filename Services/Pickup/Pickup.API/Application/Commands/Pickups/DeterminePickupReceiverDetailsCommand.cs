﻿using MediatR;

namespace PickupSvc.API.Application.Commands.Pickups
{
    public class DeterminePickupReceiverDetailsCommand : IRequest<DeterminePickupReceiverDetailsResponse>
    {
        public int PickupId { get; private set; }
        public int PickupBranchId { get; private set; }


        public DeterminePickupReceiverDetailsCommand(int pickupId, int pickupBranchId)
        {
            PickupId = pickupId;
            PickupBranchId = pickupBranchId;
        }
    }
}
