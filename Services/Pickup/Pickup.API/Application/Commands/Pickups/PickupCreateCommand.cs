﻿using System.Collections.Generic;
using MediatR;
using PickupSvc.API.Application.DTOs;

namespace PickupSvc.API.Application.Commands.Pickups
{
    public class PickupCreateCommand: IRequest<PickupCreateResponse>
    {
        public int Id { get;set; }
        public string Number { get; private set; }

        public int BookedByUserId { get; private set; }

        public int UserId { get; private set; }

        public string ChargeAccountCode { get; private set; }
        public string SenderCode { get; private set; }
        public string SenderName { get; private set; }

        public string SpecialInstructions { get; private  set; }

        public TimeInfoDTO TimeInfo { get; private set; }

        public AddressDTO Address { get; private  set; }

        public ContactDTO Contact { get; private set; }

        public PalletDto Pallets { get; private set; }

        

        public List<PickupItemDTO> Items { get; private set; }

        


        public PickupCreateCommand(string number, int bookedByUserId, int userId,
              string chargeAccountCode, string senderCode,
            string senderName, string specialInstructions, TimeInfoDTO timeInfo, AddressDTO address, 
            ContactDTO contact, PalletDto pallets, List<PickupItemDTO> items=null)
        {
            Number = number;
            BookedByUserId = bookedByUserId;
            UserId = userId;
            
            ChargeAccountCode = chargeAccountCode;
            SpecialInstructions = specialInstructions;
            SenderCode = senderCode;
            SenderName = senderName;

            TimeInfo = timeInfo;
            Address = address;
            Contact = contact;
            Pallets = pallets;
            Items = items ?? new List<PickupItemDTO>();
            
        }

        public PickupCreateCommand(string number, int bookedByUserId, int userId,
            string chargeAccountCode, string senderCode,
            string senderName, string specialInstructions)
        {
            Number = number;
            BookedByUserId = bookedByUserId;
            UserId = userId;

            ChargeAccountCode = chargeAccountCode;
            SpecialInstructions = specialInstructions;
            SenderCode = senderCode;
            SenderName = senderName;

            TimeInfo = TimeInfoDTO.Empty();
            Address = AddressDTO.Empty();
            Contact = ContactDTO.Empty();
            Pallets = PalletDto.Empty();
            Items = new List<PickupItemDTO>();

        }

    }
}
