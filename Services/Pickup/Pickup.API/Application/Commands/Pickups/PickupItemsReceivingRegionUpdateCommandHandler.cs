﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.Extensions.Logging;
using PickupSvc.Infrastructure.Repositories.Interfaces;
using PickupSvc.TheDomain.Entities;
using PickupSvc.TheDomain.Enumerators;
using PickupSvc.TheDomain.Infrastructure;

namespace PickupSvc.API.Application.Commands.Pickups
{
    public class PickupItemsReceivingRegionUpdateCommandHandler : IRequestHandler<PickupItemsReceivingRegionUpdateCommand,PickupItemsReceivingRegionUpdateResponse>
    {
        private readonly ILogger<PickupItemsReceivingRegionUpdateCommandHandler> _logger;
        private readonly IPickupRepository _pickupRepository;
        private readonly ISuburbRepository _suburbRepository;
        private readonly IRegionRepository _regionRepository;
        private readonly IBranchRepository _branchRepository;


        public PickupItemsReceivingRegionUpdateCommandHandler(IPickupRepository pickupRepository, ILogger<PickupItemsReceivingRegionUpdateCommandHandler> logger,ISuburbRepository suburbRepository, IRegionRepository regionRepository, IBranchRepository branchRepository)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _pickupRepository = pickupRepository ?? throw new ArgumentNullException(nameof(pickupRepository));
            _suburbRepository = suburbRepository ?? throw new ArgumentNullException(nameof(suburbRepository));
            _regionRepository = regionRepository ?? throw new ArgumentNullException(nameof(regionRepository));
            _branchRepository = branchRepository ?? throw new ArgumentNullException(nameof(branchRepository));
        }

        public async Task<PickupItemsReceivingRegionUpdateResponse> Handle(PickupItemsReceivingRegionUpdateCommand request, CancellationToken cancellationToken)
        {
            var result = new PickupItemsReceivingRegionUpdateResponse();

            // Get the pickup
            var pickupTask = _pickupRepository.GetAsync(request.Number, cancellationToken);

            // Get the receiving suburb details
            var suburb = await _suburbRepository.FindByNameAndPostCodeAsync(request.Suburb, request.Postcode);
            
           
            // Determine the preferred Receiving branch
            var branch = await GetPreferredReceivingBranchAsync(suburb, cancellationToken);

            if (branch != null && branch.Id > 0)
            {
                // TODO: Change the branch and region data to be returned together to avoid 2 DB calls, need work out how to implement expand properties
                // Get the PUD Default Region for the branch
                var receivingRegion = await _regionRepository.FindByIdAsync(branch.Id);

                // Make sure we found a region
                if (receivingRegion != null && receivingRegion.Id > 0)
                {
                    // Read the pickup details
                    var pickup = await pickupTask;

                    // Make sure the pickup is known
                    if (pickup != null )
                    {
                        // Set the region on all items
                        pickup.SetReceivingRegionOnItems(receivingRegion);

                        // Check errors
                        if (pickup.HasErrors)
                        {
                            result.Errors.AddRange(pickup.Errors);
                        }

                        // Any item errors?
                        if (pickup.Items.Where(x => x.HasErrors).Select(x => x.HasErrors).Any())
                        {
                            result.Errors.AddRange(pickup.Items.Where(x => x.HasErrors).SelectMany(x => x.Errors));
                        }
            
                        // Return now as it's failed
                        if (result.Errors.Any())
                        {
                            return result;
                        }
                        // Push the data to the database context
                        _pickupRepository.Update(pickup);

                        // Commit the changes
                        await _pickupRepository.UnitOfWork.SaveEntitiesAsync(cancellationToken);
                    }
                    else
                    {
                        // Unknown pickup
                        result.Errors.Add(ValidationMessages.PickupUnknownPickup);
                    }
                    
                }
                else
                {
                    // Unknown region
                    result.Errors.Add(ValidationMessages.RegionReceivingRegionIsUnknown);
                }
                
            }
            else
            {
                // Unknown preferred receiving branch
                result.Errors.Add(ValidationMessages.BranchPreferredReceivingBranchIsUnknown);
            }
            
            return result;
        }

        public async Task<Branch> GetPreferredReceivingBranchAsync(Suburb suburb, CancellationToken cancellationToken)
        {
            // Get the first preferred receiving suburb
            var prefBranches = suburb.PreferredBranches.FindAll(x => x.PreferredBranchType == PreferredBranchType.Receiver);

            if (prefBranches.Any())
            {
                var prefBranch = prefBranches.OrderBy(x => x.Order).First();

                return await _branchRepository.FindByIdAsync(prefBranch.BranchId);

            }

            return null;
        }
    }
}
