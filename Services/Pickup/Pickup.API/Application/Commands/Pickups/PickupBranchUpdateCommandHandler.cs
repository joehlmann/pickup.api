﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using PickupSvc.Infrastructure.Repositories.Interfaces;
using PickupSvc.TheDomain.Enumerators;
using PickupSvc.TheDomain.Infrastructure;

namespace PickupSvc.API.Application.Commands.Pickups
{
    public class PickupBranchUpdateCommandHandler : IRequestHandler<PickupBranchUpdateCommand, PickupBranchUpdateResponse>
    {
        private readonly ISuburbRepository _suburbRepository;
        private readonly IPickupRepository _pickupRepository;
        private readonly IBranchRepository _branchRepository;

        public PickupBranchUpdateCommandHandler(ISuburbRepository suburbRepository, IPickupRepository pickupRepository, IBranchRepository branchRepository)
        {
            _suburbRepository = suburbRepository;
            _pickupRepository = pickupRepository;
            _branchRepository = branchRepository;
        }

        public async Task<PickupBranchUpdateResponse> Handle(PickupBranchUpdateCommand request, CancellationToken cancellationToken)
        {
            var updateResult = new PickupBranchUpdateResponse { PickupId = request.Id, PickupNumber = request.Number};
            
            // Get the suburbs preferred sending branch
            var suburb = await _suburbRepository.FindByNameAndPostCodeAsync(request.Suburb, request.Postcode);
            
            // Get the pickup details
            var pickup = await _pickupRepository.GetAsync(request.Id, cancellationToken);

            // Get the first preferred sending suburb
            var prefBranch = suburb?.PreferredBranches?.FindAll(x => x.PreferredBranchType == PreferredBranchType.Sender);

            if (prefBranch != null && prefBranch.Any())
            {
                var branch = prefBranch.OrderBy(x => x.Order).First();

                // Get the branch details
                var branchDetails = await _branchRepository.FindByIdAsync(branch.BranchId);

                if (branchDetails != null && branchDetails.Id > 0)
                {

                    // Set the preferred branch
                    
                    if (pickup.PickupNumber > 0)
                    {
                        pickup.SetPickupBranch(branchDetails);
                    
                        _pickupRepository.Update(pickup);

                        if (!pickup.HasErrors)
                        {
                            // Save the record
                            await _pickupRepository.UnitOfWork.SaveEntitiesAsync(cancellationToken);
                        }
                        else
                        {
                            updateResult.Errors.AddRange(pickup.Errors);
                        }
                        
                    }
                    else
                    {
                        updateResult.Errors.Add(ValidationMessages.PickupUnknownPickup);
                    }
                    
                }
                else
                {
                    // Error, unknown branch
                    updateResult.Errors.Add(ValidationMessages.BranchPreferredSendingBranchIsUnknown);
                }
            }
            else
            {
                // Error, no preferred sending branch
                updateResult.Errors.Add(ValidationMessages.BranchPreferredSendingBranchIsMissing);
            }

            
            return updateResult;
        }
    }
}
