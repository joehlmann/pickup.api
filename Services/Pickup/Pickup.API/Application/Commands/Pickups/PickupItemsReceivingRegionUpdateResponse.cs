﻿namespace PickupSvc.API.Application.Commands.Pickups
{
    public class PickupItemsReceivingRegionUpdateResponse : BaseResponse
    {
        public string PickupNumber { get; set; }
    }
}
