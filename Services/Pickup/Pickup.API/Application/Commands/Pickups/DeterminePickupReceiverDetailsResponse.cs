﻿namespace PickupSvc.API.Application.Commands.Pickups
{
    public class DeterminePickupReceiverDetailsResponse : BaseResponse
    {
        public int PickupId { get;set; }
        public int PickupNumber {get; set; }
    }
}
