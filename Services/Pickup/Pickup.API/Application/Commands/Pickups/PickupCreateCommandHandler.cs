﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Bex.Utilities.Helper;
using MediatR;
using Microsoft.Extensions.Logging;
using PickupSvc.API.Application.DomainServices.Interfaces;
using PickupSvc.API.Application.DTOs;
using PickupSvc.Infrastructure.Repositories.Interfaces;
using PickupSvc.TheDomain.Entities;
using PickupSvc.TheDomain.Infrastructure;
using PickupSvc.TheDomain.ValueObjects;

namespace PickupSvc.API.Application.Commands.Pickups
{
    public class PickupCreateCommandHandler : IRequestHandler<PickupCreateCommand, PickupCreateResponse>
    {
        private readonly IMapper _mapper;
        private readonly IPickupRepository _pickupRepository;
        private readonly ISuburbRepository _suburbRepository;
        private readonly ICustomerRepository _customerRepository;
        private readonly IPickupNumberService _pickupService;
        private readonly ILogger<PickupCreateCommandHandler> _logger;
        private readonly IUserRepository _userRepository;
        private readonly IDangerousGoodRepository _dangerousGoodRepository;
        

        public PickupCreateCommandHandler(IPickupRepository pickupRepository,
            IMapper mapper,
            ISuburbRepository suburbRepository,
            ICustomerRepository customerRepository,
            IPickupNumberService pickupService,
            IUserRepository userRepository,
            ILogger<PickupCreateCommandHandler> logger,
            IDangerousGoodRepository dangerousGoodRepository
            )
        {
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _pickupRepository = pickupRepository ?? throw new ArgumentNullException(nameof(pickupRepository));
            _suburbRepository = suburbRepository ?? throw new ArgumentNullException(nameof(suburbRepository));
            _customerRepository = customerRepository ?? throw new ArgumentNullException(nameof(customerRepository));
            _pickupService = pickupService ?? throw new ArgumentNullException(nameof(pickupService));
            _userRepository = userRepository ?? throw new ArgumentNullException(nameof(userRepository));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _dangerousGoodRepository = dangerousGoodRepository ?? throw new ArgumentNullException(nameof(dangerousGoodRepository));

            
        }

        public async Task<PickupCreateResponse> Handle(
            PickupCreateCommand request,
            CancellationToken cancellationToken)
        {
            // Convert the request into a domain object
            var pickup = Pickup.Create();
            var pickupItems = ConstructPickupItems(request.Items);
            
            var errors = new List<Error>();
            var autoGeneratePickupNumber = false;
            var senderAccountTask = Task.FromResult(new CustomerAccount(request.SenderCode,request.SenderName));
            var bookedByUserTask = Task.FromResult<User>(null);

            // Populate the required data for validations and business logic
            var chargeAccountTask = FindCustomerAccountAsync(request.ChargeAccountCode);
            
            if (!string.Equals(request.ChargeAccountCode,request.SenderCode,StringComparison.OrdinalIgnoreCase))
            {
                if (!string.IsNullOrEmpty(request.SenderCode))
                {
                    senderAccountTask = FindCustomerAccountAsync(request.SenderCode);
                }                
            }
            else
            {
                senderAccountTask = chargeAccountTask;
            }

           
            var suburbTask = FindSuburbAsync(request.Address.Suburb, request.Address.PostCode);

            // Allow the number generation to run async
            var numberTask = Task.FromResult(request.Number);

            if (string.IsNullOrEmpty(request.Number))
            {
               var  anumberTask = _pickupService.GenerateNextAvailablePickupNumberAsync();
                // Let the handler know that it's ok to regenerate a new number if a number conflict occurs
                autoGeneratePickupNumber = true;
            }

            // Check the booked by user id
            if (request.BookedByUserId != null && request.BookedByUserId  > 0)
            {
                bookedByUserTask = FindUser(request.BookedByUserId);
            }

            // Wait for any of the lookups to complete
            await Task.WhenAll(chargeAccountTask, suburbTask, numberTask, senderAccountTask, bookedByUserTask);

            if (suburbTask.Result.Id == 0 &&
                (!string.IsNullOrEmpty(request.Address.Suburb) && !string.IsNullOrEmpty(request.Address.PostCode)))
            {
                errors.Add(ValidationMessages.PickupInvalidSuburbDetails);
            }

            if (bookedByUserTask?.Result != null && bookedByUserTask.Result.Id == 0 && request.BookedByUserId != null && request.BookedByUserId > 0)
            {
                errors.Add(ValidationMessages.PickupBookingUserIsInvalid);
            }

            var sender = senderAccountTask.Result;

            // Check the sender account
            // TODO: Add validation here
            if(sender != null)
            {
                pickup.SetSender(new CustomerAccount(sender.Id, sender.CustomerCode, sender.CustomerAccountName));
            }
            
            pickup.SetInstructions(request.SpecialInstructions);

            // Perform the business logic
            pickup.SetNumber(int.Parse(numberTask.Result));
            pickup.SetAddress(request.Address.Address1, request.Address.Address2,suburbTask.Result);
            pickup.SetPayingAccount(chargeAccountTask.Result);
            pickup.SetPickupDate(request.TimeInfo.PickupDate.ToDate());
            pickup.SetPickupWindow(request.TimeInfo.ReadyTime.ToDate(), request.TimeInfo.CloseTime.ToDate());
            pickup.SetContact(request.Contact.ContactName, request.Contact.Phone,request.Contact.Email);
            pickup.SetBookerDetails(bookedByUserTask.Result,request.TimeInfo.BookedDateTime.ToDate());
            


            //TODO: Store the context of token user calling the endpoint (application/user), booking application?
            pickup.AddItems(await pickupItems);
            
            //ToDO 
            //pickup.AddPalletDetails(new PickupPallets(request.ExchangeChepPalletsOut,request.TransferChepPalletsToBex,request.TransferChepPalletsToReceiver,request.ChepPalletTransferDockerNumber,request.ExchangeLoscamPalletsOut,request.TransferLoscamPalletsToBex,request.TransferLoscamPalletsToReceiver,request.LoscamPalletTransferDockerNumber));

            // Book the pickup
            pickup.Book();

            // Any item errors?
            var itemErrors = pickup.Items.Where(x => x.HasErrors).Select(x => x.HasErrors).Any();

            if (errors.Any() || pickup.HasErrors || itemErrors)
            {
                if (pickup.Errors != null)
                {
                    errors.AddRange(pickup.Errors);
                }

                if (itemErrors)
                {
                    errors.AddRange(pickup.Items.Where(x => x.HasErrors).SelectMany(x => x.Errors));
                }
                return new PickupCreateResponse {Errors = errors};
            }
            
            // Push the data to the database context
            _pickupRepository.Add(pickup);

          

            if (errors.Any())
            {
                return new PickupCreateResponse {Errors = errors};
            }

            
            return new PickupCreateResponse {Id = pickup.Id, Number = pickup.PickupNumber};
        }

        /// <summary>
        /// Find more details about the customer
        /// </summary>
        /// <param name="code">Customer account code</param>
        /// <returns></returns>
        private Task<CustomerAccount> FindCustomerAccountAsync(string code)
        {
            return !string.IsNullOrEmpty(code)
                ? _customerRepository.FindByCodeAsync(code)
                : Task.FromResult<CustomerAccount>(null);
        }

        /// <summary>
        /// Find more details about the suburb
        /// </summary>
        /// <param name="name">Name of the suburb</param>
        /// <param name="postcode">Postcode of the suburb</param>
        /// <returns></returns>
        private  Task<Suburb> FindSuburbAsync(string name, string postcode)
        {
            return !string.IsNullOrEmpty(name) && !string.IsNullOrEmpty(postcode)
                ? _suburbRepository.FindByNameAndPostCodeAsync(name, postcode)
                : null;
        }

        /// <summary>
        /// Find more details about the user
        /// </summary>
        /// <param name="userId">The id of the user</param>
        /// <returns></returns>
        private  Task<User> FindUser(int userId)
        {
            return  userId > 0 ? _userRepository.FindByIdAsync(userId) : null;
        }

        
        private async Task<List<PickupItem>> ConstructPickupItems(List<PickupItemDTO> items)
        {
            var pickupItems = new List<PickupItem>();
            var itemsDg = items.Where(d => d.DangerousGoods != null).SelectMany(i => i.DangerousGoods).ToList();
            var dgDict = new Dictionary<int, DangerousGood>();

            if (itemsDg.Any())
            {
                dgDict = await LoadDGDetail(itemsDg);
            }

            foreach (var item in items)
            {
                var dgList = item.DangerousGoods?.Select(x => dgDict[x.UNNo] ?? new DangerousGood(x.UNNo)).ToList();
                pickupItems.Add(new PickupItem(item.Quantity, item.Description, item.WeightKg, item.FoodStuff, dgList));
            }

            return pickupItems;
        }

        public async Task<Dictionary<int, DangerousGood>> LoadDGDetail(List<DangerousGoodItemDTO> dangerousGoods)
        {
            // Lookup DG info
            var dgTasks = dangerousGoods.Select(x => x.UNNo).Distinct().Select(dgUN => _dangerousGoodRepository.GetByUNNumberAsync(dgUN)).ToArray();

            await Task.WhenAll(dgTasks);

            var dgDetails = new Dictionary<int, DangerousGood>();
            foreach (var dgTask in dgTasks)
            {
                if (dgTask.Result.UNNo > 0)
                {
                    dgDetails[dgTask.Result.UNNo] = dgTask.Result;
                }
            }

            return dgDetails;
        }
    }
}