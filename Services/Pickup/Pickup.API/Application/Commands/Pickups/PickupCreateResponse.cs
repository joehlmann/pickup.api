﻿namespace PickupSvc.API.Application.Commands.Pickups
{
    public class PickupCreateResponse : BaseResponse
    {
       
        public int Id { get; set; }
        public int Number { get; set; }
    }
}
