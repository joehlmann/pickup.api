﻿using System.Collections.Generic;
using PickupSvc.TheDomain.Infrastructure;

namespace PickupSvc.API.Application.Commands.Pickups
{
    public class PickupUpdateResponse
    {
        public int Id { get; set; }
        public List<Error> Errors { get;set; }
    }
}
