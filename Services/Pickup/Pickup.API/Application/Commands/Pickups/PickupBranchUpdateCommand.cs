﻿using MediatR;

namespace PickupSvc.API.Application.Commands.Pickups
{
    public class PickupBranchUpdateCommand : IRequest<PickupBranchUpdateResponse>
    {
        public int Id { get; private set; }
        public int Number { get; private set; }
        public string Suburb { get; private set; }
        public string Postcode { get; private set; }


        public PickupBranchUpdateCommand(int id, int number, string suburb, string postcode)
        {
            Id = id;
            Number = number;
            Suburb = suburb;
            Postcode = postcode;
        }
    }
}
