﻿using System.Collections.Generic;
using PickupSvc.TheDomain.Infrastructure;

namespace PickupSvc.API.Application.Commands
{
    public class BaseResponse
    {
        public List<Error> Errors { get;set; }

        public BaseResponse()
        {
            Errors = new List<Error>();
        }
    }
}
