﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.Extensions.Logging;
using PickupSvc.API.Application.IntegrationEvents;
using PickupSvc.API.Application.IntegrationEvents.Events;
using PickupSvc.TheDomain.Events;

namespace PickupSvc.API.Application.DomainEventHandlers.DangerousGoodAddedToItem
{
    public class DangerousGoodAddedToItemDomainEventHandler : INotificationHandler<DangerousGoodAddedToPickupItemEvent>
    {
        private readonly ILogger _logger;
        private readonly IPickupIntegrationEventSvc _pickupIntegrationEventService;

        public DangerousGoodAddedToItemDomainEventHandler(IPickupIntegrationEventSvc pickupIntegrationEventService, ILogger<DangerousGoodAddedToItemDomainEventHandler> logger)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _pickupIntegrationEventService = pickupIntegrationEventService ?? throw new ArgumentNullException(nameof(pickupIntegrationEventService));
        }
        public async Task Handle(DangerousGoodAddedToPickupItemEvent notification, CancellationToken cancellationToken)
        {

            // Generate Integration Event
            var @event = new DangerousGoodAddedToPickupItemIntegrationEvent(notification.Item.Id,
                notification.DangerousGood.UNNo, notification.DangerousGood.Class);

            // Add Integration event to EventBus
            
            await _pickupIntegrationEventService.AddAndSaveEventAsync(@event).ConfigureAwait(false);
            _logger.LogInformation("-----New Dangerous Good: {@event}", @event.ToString());


            // Add trace logging
            _logger.LogTrace("New Dangerous good UN:{unNo} added to item {pickupItemId}.",notification.DangerousGood.UNNo,notification.Item.Id);
            
        }
    }
}
