﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.Extensions.Logging;
using PickupSvc.API.Application.IntegrationEvents;
using PickupSvc.API.Application.IntegrationEvents.Events;
using PickupSvc.Infrastructure.Repositories.Interfaces;
using PickupSvc.TheDomain.Events;

namespace PickupSvc.API.Application.DomainEventHandlers.PickupBooked
{
    public class PickupBookedDomainEventHandler : INotificationHandler<BookedPickupEvent>
    {
        private readonly ILogger _logger;
        private readonly IPickupIntegrationEventSvc _pickupIntegrationEventService;
        private readonly IPickupRepository _pickupRepository;
        
        public PickupBookedDomainEventHandler(ILogger<PickupBookedDomainEventHandler> logger, IPickupIntegrationEventSvc pickupIntegrationEventService, IPickupRepository pickupRepository)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _pickupIntegrationEventService = pickupIntegrationEventService ?? throw new ArgumentNullException(nameof(pickupIntegrationEventService));
            _pickupRepository = pickupRepository ?? throw new ArgumentNullException(nameof(pickupRepository));
        }
        public async Task Handle(BookedPickupEvent pickupBooked, CancellationToken cancellationToken)
        {
            // Make sure the Pickup has it's Id
            // TODO: Review how this could be done better so that the id is set as soon as the pickup is saved
            //pickupBooked.Pickup.SetId(_pickupRepository.GetCacheOnly(pickupBooked.Pickup.GetInternalGuid()).Id);

            // Generate Integration Event
            var @event = new PickupBookedIntegrationEvent(pickupBooked.Pickup.Id, pickupBooked.Pickup.PickupNumber, "Booked",pickupBooked.Pickup.PickupSuburb.Name, pickupBooked.Pickup.PickupSuburb.Postcode, pickupBooked.Pickup.TimeInfo.BookedDateTime,pickupBooked.Pickup.TimeInfo.CloseTime ?? DateTime.Now);

            // Add the message to the queue
            await _pickupIntegrationEventService.AddAndSaveEventAsync(@event).ConfigureAwait(false);
            _logger.LogInformation("-----Pickup Booked: {@event}", @event.ToString());

            // Add trace logging
            _logger.LogTrace($"Pickup Id: {pickupBooked.Pickup.Id} has been booked.");
        }
    }
}
