﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.Extensions.Logging;
using PickupSvc.API.Application.IntegrationEvents;
using PickupSvc.API.Application.IntegrationEvents.Events;
using PickupSvc.Infrastructure.Repositories.Interfaces;
using PickupSvc.TheDomain.Events;

namespace PickupSvc.API.Application.DomainEventHandlers.ItemRemovedFromPickup
{
    public class ItemRemovedFromPickupDomainEventHandler : INotificationHandler<ItemRemovedFromPickupEvent>
    {
        private readonly ILogger _logger;
        private readonly IPickupIntegrationEventSvc _pickupIntegrationEventService;
        private readonly IPickupRepository _pickupRepository;

        public ItemRemovedFromPickupDomainEventHandler(ILogger<ItemRemovedFromPickupDomainEventHandler> logger, IPickupIntegrationEventSvc pickupIntegrationEventService, IPickupRepository pickupRepository)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _pickupIntegrationEventService = pickupIntegrationEventService ?? throw new ArgumentNullException(nameof(pickupIntegrationEventService));
            _pickupRepository = pickupRepository ?? throw new ArgumentNullException(nameof(pickupRepository));
        }

        public async Task Handle(ItemRemovedFromPickupEvent notification, CancellationToken cancellationToken)
        {
            // Make sure the Pickup has it's Id
           // var pickup = _pickupRepository.GetCacheOnly(notification.Pickup.Id);

            // TODO: Review how this could be done better so that the id is set as soon as the pickup is saved
            //notification.Pickup.SetId(pickup.Id);
            
            
            var @event = new ItemRemovedFromPickupIntegrationEvent(notification.Pickup.Id,
                notification.Pickup.PickupNumber, notification.Item.Id);

            // Add the message to the queue
            await _pickupIntegrationEventService.AddAndSaveEventAsync(@event).ConfigureAwait(false);
            _logger.LogInformation("-----Item has been removed from pickup: {@event}", @event.ToString());


            // Add trace logging
            _logger.LogTrace("Item {pickupId} has been removed from pickup number: {pickupNumber}.",notification.Item.Id,notification.Pickup.PickupNumber);
            
        }
    }
}
