﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.Extensions.Logging;
using PickupSvc.API.Application.IntegrationEvents;
using PickupSvc.API.Application.IntegrationEvents.Events;
using PickupSvc.Infrastructure.Repositories.Interfaces;
using PickupSvc.TheDomain.Events;

namespace PickupSvc.API.Application.DomainEventHandlers.NewPickupBranchSet
{
    public class NewPickupBranchSetDomainEventHandler : INotificationHandler<NewPickupBranchSetEvent>
    {
        private readonly ILogger _logger;
        private readonly IPickupIntegrationEventSvc _pickupIntegrationEventService;
        private readonly IPickupRepository _pickupRepository;

        public NewPickupBranchSetDomainEventHandler(ILogger<NewPickupBranchSetDomainEventHandler> logger, IPickupIntegrationEventSvc pickupIntegrationEventService, IPickupRepository pickupRepository)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _pickupIntegrationEventService = pickupIntegrationEventService ?? throw new ArgumentNullException(nameof(pickupIntegrationEventService));
            _pickupRepository = pickupRepository ?? throw new ArgumentNullException(nameof(pickupRepository));
        }
        public async Task Handle(NewPickupBranchSetEvent notification, CancellationToken cancellationToken)
        {
            

            // Generate Integration Event
            var @event = new NewPickupBranchSetIntegrationEvent(notification.Pickup.Id, notification.Pickup.PickupNumber, notification.Pickup.PickupBranch.Id, notification.Pickup.PickupBranch.Name);

            // Add Integration event to EventBus
            
            await _pickupIntegrationEventService.AddAndSaveEventAsync(@event).ConfigureAwait(false);
            _logger.LogInformation("-----New Pickup Branch: {@event}", @event.ToString());


            // Add trace logging
            _logger.LogTrace($"New pickup branch has been set for Pickup Id: {notification.Pickup.Id}, number: {notification.Pickup.PickupNumber}.");

            
        }
    }
}
