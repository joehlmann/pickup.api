﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.Extensions.Logging;
using PickupSvc.API.Application.IntegrationEvents;
using PickupSvc.API.Application.IntegrationEvents.Events;
using PickupSvc.Infrastructure.Repositories.Interfaces;
using PickupSvc.TheDomain.Entities;
using PickupSvc.TheDomain.Events;

namespace PickupSvc.API.Application.DomainEventHandlers.ItemAddedToPickup
{
    public class ItemAddedToPickupDomainEventHandler : INotificationHandler<ItemAddedToPickupEvent>
    {
        private readonly ILogger _logger;
        private readonly IPickupIntegrationEventSvc _pickupIntegrationEventService;
        private readonly IPickupRepository _pickupRepository;

        public ItemAddedToPickupDomainEventHandler(ILogger<ItemAddedToPickupDomainEventHandler> logger, IPickupIntegrationEventSvc pickupIntegrationEventService, IPickupRepository pickupRepository)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _pickupIntegrationEventService = pickupIntegrationEventService ?? throw new ArgumentNullException(nameof(pickupIntegrationEventService));
            _pickupRepository = pickupRepository ?? throw new ArgumentNullException(nameof(pickupRepository));
        }
        public async Task Handle(ItemAddedToPickupEvent notification, CancellationToken cancellationToken)
        {
            // Make sure the Pickup has it's Id
            //var pickup = _pickupRepository.GetCacheOnly(notification.Pickup.Id);

            // TODO: Review how this could be done better so that the id is set as soon as the pickup is saved
            //notification.Pickup = pickup;
            //notification.Item.SetId(GetPickupItemId(pickup,notification.Item));

            // Generate Integration Event
            var @event = new ItemAddedToPickupIntegrationEvent(notification.Pickup.Id,
                notification.Pickup.PickupNumber, notification.Item.Id);

            // Add the message to the queue
            await _pickupIntegrationEventService.AddAndSaveEventAsync(@event).ConfigureAwait(false);
            _logger.LogInformation("-----New Item Added Tp Pickup: {@event}", @event.ToString());


            // Add trace logging
            _logger.LogTrace("New item {pickupId} has been added to pickup number: {pickupNumber}.",notification.Item.Id,notification.Pickup.PickupNumber);
        }

        public int GetPickupItemId(Pickup pickup, PickupItem item)
        {
            try
            {
                return pickup.Items.ToList().First(x => x.Id == item.Id).Id ;
            }
            catch (Exception e)
            {
                _logger.LogError("Pickup item on pickup {pickupNumber} not found in local repository cache.",pickup.PickupNumber);
                throw;
            }
            
        }
    }
}
