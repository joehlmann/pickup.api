﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.Extensions.Logging;
using PickupSvc.API.Application.IntegrationEvents;
using PickupSvc.API.Application.IntegrationEvents.Events.ReceiverAddressOnPickupChanged;
using PickupSvc.TheDomain.Events;

namespace PickupSvc.API.Application.DomainEventHandlers.ReceiverAddressOnPickupChanged
{
    public class ReceiverAddressOnPickupChangedDomainEventHandler : INotificationHandler<ReceiverAddressOnPickupChangedEvent>
    {
        private readonly ILogger _logger;
        private readonly IPickupIntegrationEventService _pickupIntegrationEventService;

        public ReceiverAddressOnPickupChangedDomainEventHandler(ILogger<ReceiverAddressOnPickupChangedDomainEventHandler> logger, IPickupIntegrationEventService pickupIntegrationEventService)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _pickupIntegrationEventService = pickupIntegrationEventService ?? throw new ArgumentNullException(nameof(pickupIntegrationEventService));
        }
        public async Task Handle(ReceiverAddressOnPickupChangedEvent notification, CancellationToken cancellationToken)
        {
            // Generate Integration Event
            var pickupReceiverAddressChangedIntegrationEvent = new ReceiverAddressOnPickupChangedIntegrationEvent(notification.Pickup.Id, notification.Pickup.PickupNumber, notification.Pickup.ReceiverSuburb.Id, notification.Pickup.ReceiverSuburb.Name,notification.Pickup.ReceiverSuburb.State,notification.Pickup.ReceiverSuburb.Postcode);
            
            // Add the message to the queue
            await _pickupIntegrationEventService.PublishThroughEventBusAsync(pickupReceiverAddressChangedIntegrationEvent);
            
            // Add trace logging
            _logger.LogTrace($"Pickup receiver address has been changed for Pickup Id: {notification.Pickup.Id}, number: {notification.Pickup.PickupNumber}.");
        }
    }
}
