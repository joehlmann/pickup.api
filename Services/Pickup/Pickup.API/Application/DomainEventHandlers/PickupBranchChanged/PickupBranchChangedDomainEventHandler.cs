﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.Extensions.Logging;
using PickupSvc.API.Application.IntegrationEvents;
using PickupSvc.API.Application.IntegrationEvents.Events;
using PickupSvc.Infrastructure.Repositories.Interfaces;
using PickupSvc.TheDomain.Events;

namespace PickupSvc.API.Application.DomainEventHandlers.PickupBranchChanged
{
    public class PickupBranchChangedDomainEventHandler : INotificationHandler<BranchChangedPickupEvent>
    {
        private readonly ILogger _logger;
        private readonly IPickupIntegrationEventSvc _pickupIntegrationEventService;
        private readonly IPickupRepository _pickupRepository;

        public PickupBranchChangedDomainEventHandler(ILogger<PickupBranchChangedDomainEventHandler> logger, IPickupIntegrationEventSvc pickupIntegrationEventService, IPickupRepository pickupRepository)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _pickupIntegrationEventService = pickupIntegrationEventService ?? throw new ArgumentNullException(nameof(pickupIntegrationEventService));
            _pickupRepository = pickupRepository ?? throw new ArgumentNullException(nameof(pickupRepository));
        }

        public async Task Handle(BranchChangedPickupEvent notification, CancellationToken cancellationToken)
        {
            //notification.Pickup.SetId(_pickupRepository.GetCacheOnly(notification.Pickup.Id).Id);

            // Generate Integration Event
            var @event = new PickupBranchChangedIntegrationEvent(notification.Pickup.Id, notification.Pickup.PickupNumber, notification.Pickup.PickupBranch.Id, notification.Pickup.PickupBranch.Name);


            // Add Integration event to EventBus
            await _pickupIntegrationEventService.AddAndSaveEventAsync(@event).ConfigureAwait(false);
            _logger.LogInformation("-----Pickup Branch Changed: {@event}", @event.ToString());


            // Add trace logging
            _logger.LogTrace($"Pickup branch has been changed for Pickup Id: {notification.Pickup.Id}, number: {notification.Pickup.PickupNumber}.");

            
        }
    }
}
