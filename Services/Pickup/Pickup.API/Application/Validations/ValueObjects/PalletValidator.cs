﻿using System.Threading.Tasks;
using FluentValidation;
using PickupSvc.API.Application.DomainServices.Interfaces;
using PickupSvc.API.Application.DTOs;

namespace PickupSvc.API.Application.Validations.ValueObjects
{
    public class PalletValidator : AbstractValidator<PalletDto>
    {
        private IPalletValidService _palletValidService; 
        private string _error= ErrorMsg.NoErrors;
        private object _cmd;
        public PalletValidator( IPalletValidService palletValidService)
        {
            _palletValidService = palletValidService;


            RuleFor(addressDto => addressDto).NotEmpty().MustAsync((a, cancel) => BeValidPallet(a)).WithMessage(c => string.Format("{0}", _error));
        }

        private bool GetCmd(object cmd)
        {
            _cmd = cmd;

            return true;
        }

        private async Task<bool> BeValidPallet(PalletDto dto)
        {
            var result = await _palletValidService.ValidPalletAsync(dto);

            if (result.IsFailure)
            {
                _error = result.Error;
                
            }

            return result.IsSuccess;
        }


    }
}
