﻿using System.Threading.Tasks;
using FluentValidation;
using PickupSvc.API.Application.DomainServices.Interfaces;
using PickupSvc.API.Application.DTOs;

namespace PickupSvc.API.Application.Validations.ValueObjects
{
    public class AddressValidator : AbstractValidator<AddressDTO>
    {
        private IAddressValidService _addressValidService;
        private string _error = ErrorMsg.NoErrors;
        private object _cmd;
        public AddressValidator(IAddressValidService addressValidService)
        {
            _addressValidService = addressValidService;
            RuleFor(c => c).Must(GetCmd);
            RuleFor(addressDto => addressDto).NotEmpty().MustAsync((a, cancel) => BeValidAddress(a)).WithMessage(c => string.Format("{0}", _error));

        }
        private bool GetCmd(object cmd)
        {
            _cmd = cmd;

            return true;
        }


        private async Task<bool> BeValidAddress(AddressDTO address)
        {
            var result = await _addressValidService.ValidAddressAsync(address);

            if (result.IsFailure)
            {
                _error = result.Error;
                return false;
            }

            return result.IsSuccess;
        }


    }
}
