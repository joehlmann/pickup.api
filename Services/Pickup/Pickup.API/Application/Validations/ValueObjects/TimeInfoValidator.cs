﻿using System.Threading.Tasks;
using FluentValidation;
using PickupSvc.API.Application.DomainServices.Interfaces;
using PickupSvc.API.Application.DTOs;

namespace PickupSvc.API.Application.Validations.ValueObjects
{
    public class TimeInfoValidator : AbstractValidator<TimeInfoDTO>
    {
        private readonly ITimeInfoValidService _timeInfoValidService;
        private string _error = ErrorMsg.NoErrors;
        private object _cmd;
        public TimeInfoValidator(ITimeInfoValidService timeInfoValidService)
        {
            _timeInfoValidService = timeInfoValidService;
            

            RuleFor(timeInfoDTO => timeInfoDTO).NotEmpty().MustAsync((a, cancel) => BeValidTimeInfoAsync(a)).WithMessage(c => string.Format("{0}", _error));

        }
        private bool GetCmd(object cmd)
        {
            _cmd = cmd;

            return true;
        }


        private async Task<bool> BeValidTimeInfoAsync(TimeInfoDTO dto)
        {
            var result = await _timeInfoValidService.ValidTimeInfoAsync(dto);

            if (result.IsFailure)
            {
                _error = result.Error;
                
            }

            return result.IsSuccess;
        }


    }
}
