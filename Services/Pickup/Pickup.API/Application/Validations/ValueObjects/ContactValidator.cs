﻿using System.Threading.Tasks;
using FluentValidation;
using PickupSvc.API.Application.DomainServices.Interfaces;
using PickupSvc.API.Application.DTOs;

namespace PickupSvc.API.Application.Validations.ValueObjects
{
    public class ContactValidator : AbstractValidator<ContactDTO>
    {
        private readonly IContactValidService _contactValidator;

        private string _error = ErrorMsg.NoErrors;
        private object _cmd;

        public ContactValidator(IContactValidService contactValidator)
        {
            _contactValidator = contactValidator;


            RuleFor(command => command).NotEmpty().MustAsync((c, cancel) => BeValidContact(c)).WithMessage(c => string.Format("{0}", _error));

        }


        private bool GetCmd(object cmd)
        {
            _cmd = cmd;

            return true;
        }

        private async Task<bool> BeValidContact(ContactDTO contact)
        {
            var result = await _contactValidator.ValidContactAsync(contact);

            if (result.IsFailure)
            {
                _error = result.Error;
                
            }

            return result.IsSuccess;
        }
    }
}
