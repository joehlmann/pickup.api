﻿using System.Threading.Tasks;
using Bex.Utilities.Helper;
using FluentValidation;
using Microsoft.Extensions.Logging;
using PickupSvc.API.Application.Commands.Pickups;
using PickupSvc.API.Application.DomainServices.Interfaces;

namespace PickupSvc.API.Application.Validations
{
    public class PickupBranchUpdateCmdValidator : AbstractValidator<PickupBranchUpdateCommand>
    {
        private readonly IUserValidService _userValidService;
        private readonly IPickupValidService _pickupValidService;
        private readonly IPostCodeSuburbValidService _postCodeSuburbValidService;
        private string _error = ErrorMsg.NoErrors;
        private PickupBranchUpdateCommand _cmd;


        public PickupBranchUpdateCmdValidator(ILogger<PickupBranchUpdateCmdValidator> logger, IUserValidService userValidService, IPickupValidService pickupValidService, IPostCodeSuburbValidService postCodeSuburbValidService)
        {

            _userValidService = userValidService;
            _pickupValidService = pickupValidService;
            _postCodeSuburbValidService = postCodeSuburbValidService;

            RuleFor(c => c).Must(GetCmd);
            RuleFor(command => command.Id).NotEmpty().GreaterThan(0).MustAsync((n, x) => ValidPickupId(n)).WithMessage(string.Format("{0}", _error));
            RuleFor(command => command.Number).NotEmpty().GreaterThan(0).MustAsync((n, x) => ValidPickupNumber(n)).WithMessage(string.Format("{0}", _error));
            RuleFor(command => command.Postcode).NotEmpty().MinimumLength(4).MustAsync((n, x) => ValidPostCode(n)).WithMessage(string.Format("{0}", _error));
            RuleFor(command => command.Postcode).NotEmpty().MinimumLength(3).MustAsync((n, x) => ValidSuburb(n)).WithMessage(string.Format("{0}", _error));

            logger.LogTrace("----- INSTANCE CREATED - {ClassName}", GetType().Name);
        }

        private bool GetCmd(PickupBranchUpdateCommand cmd)
        {
            _cmd = cmd;

            return true;
        }

        private async Task<bool> ValidPickupId(int id)
        {
            var result = await _pickupValidService.ValidPickupIdAsync(id).ConfigureAwait(true);

            if (result.IsFailure)
            {
                _error = result.Error;
                
            }

            return result.IsSuccess;
        }

        private async Task<bool> ValidPickupNumber(int id)
        {


            var result = await _pickupValidService.ValidPickupNumberAsync(id).ConfigureAwait(true);

            if (result.IsFailure)
            {
                _error = result.Error;
                
            }

            return result.IsSuccess;
        }

        private async Task<bool> ValidPostCode(string id)
        {


            var result = await _postCodeSuburbValidService.ValidPostCodeAsync(id.ToInt()).ConfigureAwait(true);

            if (result.IsFailure)
            {
                _error = result.Error;
                
            }

            return result.IsSuccess;
        }
        private async Task<bool> ValidSuburb(string id)
        {


            var result = await _postCodeSuburbValidService.ValidSuburbAsync(id).ConfigureAwait(true);

            if (result.IsFailure)
            {
                _error = result.Error;
                
            }

            return result.IsSuccess;
        }




    }
}
