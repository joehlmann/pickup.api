﻿using System.Collections.Generic;
using System.Threading.Tasks;
using FluentValidation;
using PickupSvc.API.Application.DomainServices.Interfaces;
using PickupSvc.API.Application.DTOs;

namespace PickupSvc.API.Application.Validations.Entities
{
    public class PickupItemValidator : AbstractValidator<PickupItemDTO>
    {
        private readonly IPickupItemValidService _itemValidService;
        private List<string> _errors= new List<string>();


        public PickupItemValidator(IPickupItemValidService itemValidService)
        {
            _itemValidService = itemValidService;

            RuleFor(items => items).NotEmpty().MustAsync((a, cancel) => ValidPickupItems(a)).WithMessage(c => string.Format("{0}", ProcessItemErrors()));
        }

        private async Task<bool> ValidPickupItems(PickupItemDTO dto)
        {
            bool IsValid = true;

         
                var result = await _itemValidService.ValidPickupItemAsync(dto);

                if (result.IsFailure)
                {
                    _errors.Add(result.Error);
                    IsValid = false;
                }
         

            return IsValid;
        }

        private string ProcessItemErrors() => string.Join(':', _errors);
        

    }
}
