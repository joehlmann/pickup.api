﻿using System;
using System.Threading.Tasks;
using FluentValidation;
using Microsoft.Extensions.Logging;
using PickupSvc.API.Application.Commands.Pickups;
using PickupSvc.API.Application.DomainServices.Interfaces;

namespace PickupSvc.API.Application.Validations
{
    public class DeterminePickupReceiverDetailsCmdValidator : AbstractValidator<DeterminePickupReceiverDetailsCommand>
    {
        private readonly IBranchValidService _branchValidator;
        private readonly IEntityIdValidService _entityIdValidService;

        private string _branchError = ErrorMsg.NoErrors;
        private string _pickupError = ErrorMsg.NoErrors;
        private object _cmd;
        public DeterminePickupReceiverDetailsCmdValidator(ILogger<DeterminePickupReceiverDetailsCmdValidator> logger, IEntityIdValidService entityIdValidService, IBranchValidService branchValidator)
        {

            _entityIdValidService = entityIdValidService;
            _branchValidator = branchValidator;


            RuleFor(command => command.PickupBranchId).NotEmpty().MustAsync((b,cancel)=>BeValidBranch(b)).WithMessage(_branchError); ;
            RuleFor(command => command.PickupId).NotEmpty().MustAsync((x,can) =>  BeValidPickup(x)).WithMessage(_pickupError); ;
            

            logger.LogTrace("----- INSTANCE CREATED - {ClassName}", GetType().Name);
        }

        private bool BeValidDate(DateTime dateTime)
        {
            return dateTime >= DateTime.UtcNow;
        }

        private bool GetCmd(object cmd)
        {
            _cmd = cmd;

            return true;
        }

        private async Task<bool> BeValidBranch(int id)
        {
            var result = await _branchValidator.ValidBranchAsync(id);

            if (result.IsFailure)
            {
                _branchError = result.Error;
                
            }

            return result.IsSuccess;

            
        }

        private async Task<bool> BeValidPickup(int id)
        {

            var result = await _entityIdValidService.ValidPickupIdAsync(id);

            if (result.IsFailure)
            {
                _pickupError = result.Error;
                
            }

            return result.IsSuccess;
        }

    }

}
