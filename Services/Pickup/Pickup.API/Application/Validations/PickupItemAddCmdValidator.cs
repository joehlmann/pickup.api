﻿using System.Threading.Tasks;
using FluentValidation;
using Microsoft.Extensions.Logging;
using PickupSvc.API.Application.Commands.PickupItems;
using PickupSvc.API.Application.DomainServices.Interfaces;
using PickupSvc.API.Application.DTOs;
using PickupSvc.TheDomain.Infrastructure;

namespace PickupSvc.API.Application.Validations
{
    public class PickupItemAddCmdValidator : AbstractValidator<PickupItemAddCommand>
    {
        private readonly IDGoodsValidService _dGoodsValidService;
        private readonly IEntityIdValidService _entityIdValidService;
        private string _error = ErrorMsg.NoErrors;
        private object _cmd;



        public PickupItemAddCmdValidator(ILogger<PickupItemAddCmdValidator> logger, IDGoodsValidService dGoodsValidService,IEntityIdValidService entityIdValidService)
        {
            _dGoodsValidService = dGoodsValidService;
            _entityIdValidService = entityIdValidService;
            RuleFor(c => c).Must(GetCmd);
            RuleFor(command => command.PickupNumber).NotEmpty().GreaterThan(0).MustAsync((n,x)=> ValidPickupNumber(n)).WithMessage(string.Format("{0}",_error));
            RuleFor(command => command.Quantity).NotEmpty().GreaterThan(0).WithMessage(ValidationMessages.PickupItemQuantityMissing.Message);
            RuleFor(command => command.Description).NotEmpty().WithMessage(ValidationMessages.PickupItemDescriptionMissing.Message);
            RuleFor(command => command.WeightKg).NotEmpty().GreaterThan(0).WithMessage(ValidationMessages.PickupItemWeightMissing.Message);
            RuleForEach(command => command.DangerousGoods).NotNull().MustAsync((d,x)=>ContainValidDgCodes(d)).WithMessage(ValidationMessages.PickupItemDGUnknown.Message);

            logger.LogTrace("----- INSTANCE CREATED - {ClassName}", GetType().Name);
        }
        private bool GetCmd(object cmd)
        {
            _cmd = cmd;

            return true;
        }
        private async Task<bool> ContainValidDgCodes(DangerousGoodItemDTO dto)
        {
            var result = await _dGoodsValidService.ValidDGoodAsync(dto).ConfigureAwait(true);

            if (result.IsFailure)
            {
                _error = result.Error;
                
            }

            return result.IsSuccess;
        }

        private async Task<bool> ValidPickupNumber( int number)
        {
            

            var result = await _entityIdValidService.ValidPickupIdAsync(number).ConfigureAwait(true);

            if (result.IsFailure)
            {
                _error = result.Error;
                
            }

            return result.IsSuccess;
        }




    }
}
