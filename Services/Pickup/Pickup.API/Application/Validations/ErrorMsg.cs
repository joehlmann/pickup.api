﻿namespace PickupSvc.API.Application.Validations
{
    public static class ErrorMsg
    {

        public static string NoErrors = "NoErrors";


        // Value Objects
        public static string AddressLine1Invalid = "Address:Invalid Address Line1";
        public static string AddressStateInvalid = "Address:Invalid State";
        public static string AddressStateInvalidNull = "Address:Invalid State cant be null";
        public static string AddressPostCode = "Address:Invalid PostCode";
        public static string AddressPostCodeNull = "Address:PostCode cant be null";
        public static string AddressSub = "Address:Invalid Suburb Name Length";
        public static string AddressSubNull = "Address:Invalid Suburb Name Null";
        



        //Customer
        public static string CustomerCreateFailed = "Create Customer Failed";
        public static string CustomerMustSupplyId = "Must Supply Id";
        public static string CustomerDoesNotExist = "Customer doesn't Exist";
        public static string CustomerCompanyNameNotLongEnough = "CompanyName Not Long Enough";
        public static string CustomerInvalidNumberFormat = "Invalid Number Format";
        public static string CustomerNumberInvalid = "Customer Number Invalid";




        // Contact
        public static string ContactInvalid = "Contact:Invalid Contact";
        public static string ContactNameEmpty = "Contact: Name Cant Be Empty";
        public static string ContactNameToLong = "Contact: Name To Long";
        public static string ContactPhoneEmpty = "Contact: Phone Empty";
        public static string ContactPhoneInvalidLength = "Contact: Phone Invalid length";
        public static string ContactEmailEmpty { get; } = "Contact:E-mail can't be empty";
        public static string ContactEmailToLong { get; } = "Contact:E-mail is too long";
    }
}
