﻿using FluentValidation;
using Microsoft.Extensions.Logging;
using PickupSvc.API.Application.Commands;
using PickupSvc.API.Application.Commands.PickupItems;

namespace PickupSvc.API.Application.Validations
{
    public class IdentifiedCommandValidator : AbstractValidator<IdentifiedCommand<PickupItemAddCommand, PickupItemAddResponse>>
    {
        private object _cmd;

        public IdentifiedCommandValidator(ILogger<IdentifiedCommandValidator> logger)
        {
            RuleFor(c => c).Must(GetCmd);
            RuleFor(command => command.Id).NotEmpty();

            logger.LogTrace("----- INSTANCE CREATED - {ClassName}", GetType().Name);
        }

        private bool GetCmd(object cmd)
        {
            _cmd = cmd;

            return true;
        }
    }
}
