﻿using System.Threading.Tasks;
using FluentValidation;
using Microsoft.Extensions.Logging;
using PickupSvc.API.Application.CommandsQueries.PickupItems;
using PickupSvc.API.Application.DomainServices.Interfaces;

namespace PickupSvc.API.Application.Validations.Queries
{
    public class PickupItemsByNumberQueryValidator : AbstractValidator<PickupItemsByNumberQuery>
    {
        private readonly IUserValidService _userValidService;
        private readonly IPickupValidService _pickupValidService;
        private string _error = ErrorMsg.NoErrors;
        private object _cmd;

        //Todo Clean

        public PickupItemsByNumberQueryValidator(ILogger<PickupItemRemoveCmdValidator> logger, IUserValidService userValidService, IPickupValidService pickupValidService)
        {

            _userValidService = userValidService;
            _pickupValidService = pickupValidService;
            RuleFor(c => c).Must(GetCmd);
            RuleFor(command => command.PickupNumber).NotEmpty().GreaterThan(0).MustAsync((n, x) => ValidPickupNumber(n)).WithMessage(string.Format("{0}", _error));
            

            logger.LogTrace("----- INSTANCE CREATED - {ClassName}", GetType().Name);
        }
        private bool GetCmd(object cmd)
        {
            _cmd = cmd;

            return true;
        }
        private async Task<bool> ValidPickupNumber(int id)
        {
            var result = await _pickupValidService.ValidPickupNumberAsync(id).ConfigureAwait(true);

            if (result.IsFailure)
            {
                _error = result.Error;
                
            }

            return result.IsSuccess;
        }

        private async Task<bool> ValidUser(int id)
        {


            var result = await _userValidService.ValidUserIdAsync(id.ToString()).ConfigureAwait(true);

            if (result.IsFailure)
            {
                _error = result.Error;
                
            }

            return result.IsSuccess;
        }
    }
}
