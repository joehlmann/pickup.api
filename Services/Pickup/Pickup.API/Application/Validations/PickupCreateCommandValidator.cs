﻿using System.Threading.Tasks;
using FluentValidation;
using Microsoft.Extensions.Logging;
using PickupSvc.API.Application.Commands.Pickups;
using PickupSvc.API.Application.DomainServices.Interfaces;
using PickupSvc.API.Application.Validations.Entities;
using PickupSvc.API.Application.Validations.ValueObjects;

namespace PickupSvc.API.Application.Validations
{
    public class PickupCreateCommandValidator : AbstractValidator<PickupCreateCommand>
    {
        private readonly IChargeAccountValidService _chargeAccountService;
        private string _error= ErrorMsg.NoErrors;
        //private string _user=@"N\A";
        private readonly IAddressValidService _addressValidator;
        private readonly IContactValidService _contactValidator;
        private readonly ITimeInfoValidService _timeInfoValidService;
        private readonly IPickupItemValidService _pickupItemValidService;

        private string _chargeAccountCode = "";
        private string _userChargeAccountCodeError = "";
        private PickupCreateCommand _cmd;
        //private string _sendCode;


        public PickupCreateCommandValidator(ILogger<PickupCreateCommandValidator> logger,
            IChargeAccountValidService chargeAccountService,IAddressValidService addressValidator,IContactValidService contactValidator, ITimeInfoValidService timeInfoValidService, IPickupItemValidService pickupItemValidService)
        {
            _chargeAccountService = chargeAccountService;
            _addressValidator = addressValidator;
            _contactValidator = contactValidator;
            _timeInfoValidService = timeInfoValidService;
            _pickupItemValidService = pickupItemValidService;

            RuleFor(c => c).Must(GetCmd);
            
            RuleFor(command => command.Number).NotEmpty().WithMessage("Must Have PickupNumber");
            RuleFor(command => command.UserId).NotEmpty().WithMessage("UserId Not Empty").GreaterThan(0).WithMessage("UserId Must Be Length > 0").MustAsync((u, cancel) => BeValidUserChargeAccountCode(u)).WithMessage(c => string.Format("{0}", _userChargeAccountCodeError));

            RuleFor(command => command.BookedByUserId).NotEmpty().WithMessage("Must Have Booked UserId");
            RuleFor(command => command.ChargeAccountCode).NotEmpty().MustAsync((c,cancel)=>BeValidChargeAccountCode(c)).WithMessage(string.Format("Sending Code:{0} Error:{1}",_chargeAccountCode, _error));
            RuleFor(command => command.SenderCode).NotEmpty().MustAsync((c, cancel) => BeValidChargeAccountCode(c)).WithMessage(string.Format("Sending Code:{0} is Not Valid {1}", _chargeAccountCode,  _error));
            RuleFor(command => command.SenderName).NotEmpty().WithMessage("SenderName Not Empty").Length(3).WithMessage("SenderName Must Be Length >= 3");
            
            RuleFor(command => command.Address).SetValidator(new AddressValidator(_addressValidator));
            RuleFor(command => command.Contact).SetValidator(new ContactValidator(_contactValidator));
            RuleFor(command => command.TimeInfo).SetValidator(new TimeInfoValidator(_timeInfoValidService));

            RuleForEach(command => command.Items).SetValidator(new PickupItemValidator(_pickupItemValidService));

       

            logger.LogTrace("----- INSTANCE CREATED - {ClassName}", GetType().Name);
        }

        private bool GetCmd(PickupCreateCommand cmd)
        {
            _cmd = cmd;

            return true;
        }


        



        private async Task<bool> BeValidChargeAccountCode(string accountCode)
        {
            _chargeAccountCode = accountCode;

            var result = await _chargeAccountService.ValidChargeAccount(accountCode).ConfigureAwait(true);

            if (result.IsFailure)
            {
                _error = result.Error;
                
            }

            return result.IsSuccess;
        }

        private async Task<bool> BeValidUserChargeAccountCode(int id)
        {
            var result = await _chargeAccountService.ValidUserForChargeAccount(_chargeAccountCode, id).ConfigureAwait(true);

            if (result.IsFailure)
            {
                _userChargeAccountCodeError = result.Error;
                
            }

            return result.IsSuccess;
        }

        

    }
}
