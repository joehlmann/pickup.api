﻿using System.Threading.Tasks;
using FluentValidation;
using Microsoft.Extensions.Logging;
using PickupSvc.API.Application.Commands.PickupItems;
using PickupSvc.API.Application.DomainServices.Interfaces;

namespace PickupSvc.API.Application.Validations
{
    public class PickupItemRemoveCmdValidator : AbstractValidator<PickupItemRemoveCommand>
    {
        private readonly IUserValidService _userValidService;
        private readonly IPickupItemValidService _pickupItemValidService;
        private string _error = ErrorMsg.NoErrors;

        private object _cmd;


        public PickupItemRemoveCmdValidator(ILogger<PickupItemRemoveCmdValidator> logger,   IUserValidService userValidService, IPickupItemValidService pickupItemValidService)
        {            
            
            _userValidService = userValidService;
            _pickupItemValidService = pickupItemValidService;
            RuleFor(c => c).Must(GetCmd);
            RuleFor(command => command.PickupItemId).NotEmpty().GreaterThan(0).MustAsync((n, x) => ValidPickupItemId(n)).WithMessage(string.Format("{0}", _error));
            RuleFor(command => command.UserId).NotEmpty().GreaterThan(0).MustAsync((n, x) => ValidUser(n)).WithMessage(string.Format("{0}", _error));


            logger.LogTrace("----- INSTANCE CREATED - {ClassName}", GetType().Name);
        }
        private bool GetCmd(object cmd)
        {
            _cmd = cmd;

            return true;
        }
        private async Task<bool> ValidPickupItemId(int id)
        {
            var result = await _pickupItemValidService.ValidPickupItemIdAsync(id).ConfigureAwait(true);

            if (result.IsFailure)
            {
                _error = result.Error;
                
            }

            return result.IsSuccess;
        }

        private async Task<bool> ValidUser(int id)
        {


            var result = await _userValidService.ValidUserIdAsync(id.ToString()).ConfigureAwait(true);

            if (result.IsFailure)
            {
                _error = result.Error;
                
            }

            return result.IsSuccess;
        }




    }
}
