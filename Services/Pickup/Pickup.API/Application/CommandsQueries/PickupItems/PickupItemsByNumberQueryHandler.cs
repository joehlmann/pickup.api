﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.EntityFrameworkCore;
using PickupSvc.API.Application.DTOs;
using PickupSvc.Infrastructure;

namespace PickupSvc.API.Application.CommandsQueries.PickupItems
{
    public class PickupItemsByNumberQueryHandler : IRequestHandler<PickupItemsByNumberQuery, IEnumerable<PickupItemDTO>>
    {
        private readonly PickupContext _context;

        public PickupItemsByNumberQueryHandler(PickupContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }


        // TODO Use Dapper for CQRS
        public async Task<IEnumerable<PickupItemDTO>> Handle(PickupItemsByNumberQuery request, CancellationToken cancellationToken)
        {
            var pickupItems = await _context.Pickups
                .Where(p => p.PickupNumber == request.PickupNumber)
                .Include(p => p.Items)
                .Select(p => new PickupItemsDTO()
                {
                   Items = p.Items.Select(i => new PickupItemDTO
                    {
                        Id = i.Id,
                        Quantity = Convert.ToInt32(i.Quantity),
                        WeightKg = Convert.ToInt32(i.WeightKg),
                        Description = i.Description
                    }).ToList()
                })
                .AsNoTracking()
                .FirstOrDefaultAsync(cancellationToken);

            return pickupItems.Items;
        }
    }
}
