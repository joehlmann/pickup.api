﻿using MediatR;
using PickupSvc.API.Application.DTOs;

namespace PickupSvc.API.Application.CommandsQueries.PickupItems
{
    public class PickupItemByIdQuery : IRequest<PickupItemDTO>
    {
        public int Id { get; private set; }

        public PickupItemByIdQuery(int num)
        {
            Id = num;
        }
    }
}
