﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Bex.Utilities.Helper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using PickupSvc.API.Application.DTOs;
using PickupSvc.Infrastructure;

namespace PickupSvc.API.Application.CommandsQueries.PickupItems
{
    public class PickupItemByIdQueryHandler : IRequestHandler<PickupItemByIdQuery,PickupItemDTO>
    {
        private readonly PickupContext _context;

        public PickupItemByIdQueryHandler(PickupContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public async Task<PickupItemDTO> Handle(PickupItemByIdQuery request, CancellationToken cancellationToken)
        {
            var pickup = await _context.PickupItems
                .Where(p => p.Id == request.Id)
                .Select(p => new PickupItemDTO
                {
                    Id = p.Id,
                    Quantity = Convert.ToInt32(p.Quantity),
                    WeightKg = Convert.ToInt32(p.WeightKg),
                    Description = p.Description,
                    PickupItemType = p.PickupItemType.GetEnumName(),
                    FoodStuff = p.HasFoodStuff

                })
                .AsNoTracking()
                .FirstOrDefaultAsync(cancellationToken);

            return pickup;
        }
    }
}
