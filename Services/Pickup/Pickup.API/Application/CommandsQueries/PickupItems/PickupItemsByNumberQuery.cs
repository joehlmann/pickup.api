﻿using System.Collections.Generic;
using MediatR;
using PickupSvc.API.Application.DTOs;

namespace PickupSvc.API.Application.CommandsQueries.PickupItems
{
    public class PickupItemsByNumberQuery : IRequest<IEnumerable<PickupItemDTO>>
    {
        public int PickupNumber {get; private set; }

        public PickupItemsByNumberQuery(int pickupNumber)
        {
            PickupNumber = pickupNumber;
        }
    }
}
