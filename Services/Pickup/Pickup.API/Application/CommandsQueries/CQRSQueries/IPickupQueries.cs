﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using PickupSvc.API.Application.DTOs;

namespace PickupSvc.API.Application.CommandsQueries.CQRSQueries
{
    public interface IPickupQueries
    {
        Task<PickupSummaryDTO> GetPickupAsync(Guid id);

        Task<IEnumerable<PickupSummaryDTO>> GetPickupsForUserAsync(Guid userId);

        Task<IEnumerable<PickupItemDTO>> GetPickupItemsAsync(int pickupNumber);
    }
}