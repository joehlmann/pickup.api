﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Dapper;
using Microsoft.Data.SqlClient;
using PickupSvc.API.Application.DTOs;

//using System.Data.SqlClient;

namespace PickupSvc.API.Application.CommandsQueries.CQRSQueries
{
    public class PickupQueries : IPickupQueries
    {

        private string _connectionString = string.Empty;



        public PickupQueries(string connectionString)
        {
            _connectionString = connectionString;
        }

        public async Task<PickupSummaryDTO> GetPickupAsync(Guid id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                var result = await connection.QueryAsync<dynamic>(
                    @"SELECT o.[Id] as pickupnumber,o.[PickupDate] as [date],os.[Name] as [status]
                     FROM [Pickups] o
                     LEFT JOIN [pickupitems] oi ON  o.Id = oi.pickupid                    
                     LEFT JOIN Customers c on c.CustomerId = o.CustomerId
                     LEFT JOIN Users os ON os.UserId = c.UserId
                     WHERE os.UserId = @userId
                     GROUP BY o.[Id], o.[PickupDate], c.[CustomerName] 
                     ORDER BY o.[Id]"
                    , new { id }
                );

                if (result.AsList().Count == 0)
                    throw new KeyNotFoundException();

                return MapPickupItems(result);
            }
        }

        public async Task<IEnumerable<PickupSummaryDTO>> GetPickupsForUserAsync(Guid userId)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                return await connection.QueryAsync<PickupSummaryDTO>(@"SELECT o.[Id] as pickupnumber,o.[PickupDate] as [date],os.[Name] as [status]
                     FROM [Pickups] o
                     LEFT JOIN [pickupitems] oi ON  o.Id = oi.pickupid                    
                     LEFT JOIN Customers c on c.CustomerId = o.CustomerId
                     LEFT JOIN Users os ON os.UserId = c.UserId
                     WHERE os.UserId = @userId
                     GROUP BY o.[Id], o.[PickupDate], c.[CustomerName] 
                     ORDER BY o.[Id]", new { userId });
            }
        }

        public async Task<IEnumerable<PickupItemDTO>> GetPickupItemsAsync(int pickupNumber)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                return await connection.QueryAsync<PickupItemDTO>(@"select  pit.PickupId [id] ,
		                    pit.DescriptionOfGoods [description],
		                    pit.WeightOfGoods [WeightKg],
		                    pit.NumberOfUnits [quantity],
		                    pit.HasFoodstuffs [FoodStuff],
		                    dg.UNNo
                    from dbo.Pickups p 
                    inner join dbo.PickupItems pit on pit.PickupId = p.PickupId
                    left join dbo.PickupItemDGoods dg on dg.PickupItemId = pit.PickupItemId
                    where p.PickupNumber = @pickupNumber
                    ", new { pickupNumber });
            }
        }



        private PickupSummaryDTO MapPickupItems(dynamic result)
        {
            var pickup = new PickupSummaryDTO()
            {
              Number = result[0].pickupnumber,
              PickupDate = result[0].pickupdate,
              CloseTime = result[0].closingtime,
              ReadyTime = result[0].readytime,
              Items = new List<PickupItemDTO>()
            };

            foreach (dynamic item in result)
            {
                var pickupitem = new PickupItemDTO()
                {
                    Description = item.Description,
                    Id = item.Id,
                    WeightKg = item.WeightKg,
                    Quantity = item.Quantity
                };
                pickup.Items.Add(pickupitem);
                
            }

            return pickup;
        }

    }
}
