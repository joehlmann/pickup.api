﻿using System.Collections.Generic;
using MediatR;
using PickupSvc.API.Application.DTOs;

namespace PickupSvc.API.Application.CommandsQueries.Pickups
{
    public class PickupsForUserQuery : IRequest<IEnumerable<PickupDTO>> 
    {
        public int UserId { get; private set; }


        public PickupsForUserQuery(int id)
        {
            UserId=id;
        }

    }
}
