﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Bex.Utilities.Helper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using PickupSvc.API.Application.DTOs;
using PickupSvc.Infrastructure;

namespace PickupSvc.API.Application.CommandsQueries.Pickups
{
    public class PickupByNumberQueryHandler : IRequestHandler<PickupByNumberQuery,PickupDTO>
    {
        private readonly PickupContext _context;

        public PickupByNumberQueryHandler(PickupContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }


        // TODO use Dapper CQRS
        public async Task<PickupDTO> Handle(PickupByNumberQuery request, CancellationToken cancellationToken)
        {
            var pickup = await _context.Pickups
                .Where(p => p.PickupNumber == request.Number)
                .Select(p => new PickupDTO
                {
                    Number = p.PickupNumber,
                    ReadyTime = p.TimeInfo.ReadyTime.ToString(),
                    CloseTime = p.TimeInfo.CloseTime.ToString(),
                    PickupDate = p.TimeInfo.PickupDate,
                    PickupType = p.PickupType.GetEnumName()
                })
                .AsNoTracking()
                .FirstOrDefaultAsync(cancellationToken);

            return pickup;
        }
    }
}
