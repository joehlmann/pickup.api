﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.EntityFrameworkCore;
using PickupSvc.API.Application.DTOs;
using PickupSvc.Infrastructure;

namespace PickupSvc.API.Application.CommandsQueries.Pickups
{
    public class PickupsForUserQueryHandler : IRequestHandler<PickupsForUserQuery, IEnumerable<PickupDTO>>
    {
        private readonly PickupContext _context;

        public PickupsForUserQueryHandler(PickupContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }


        // TODO use Dapper CQRS
        public async Task<IEnumerable< PickupDTO>> Handle(PickupsForUserQuery request, CancellationToken cancellationToken)
        {
            var pickup = await _context.Pickups
                .Where(p => p.BookedByUser.Id == request.UserId)
                .Select(p => new PickupDTO
                {
                    Number = p.PickupNumber,
                    ReadyTime = p.TimeInfo.ReadyTime.ToString(),
                    CloseTime = p.TimeInfo.CloseTime.ToString(),
                    PickupDate = p.TimeInfo.PickupDate
                }).ToListAsync(cancellationToken: cancellationToken);

            return pickup ?? new List<PickupDTO>() ;
        }
    }
}
