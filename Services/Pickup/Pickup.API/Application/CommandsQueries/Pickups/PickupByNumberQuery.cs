﻿using MediatR;
using PickupSvc.API.Application.DTOs;

namespace PickupSvc.API.Application.CommandsQueries.Pickups
{
    public class PickupByNumberQuery : IRequest<PickupDTO>
    {
        public int Number { get; private set; }


        public PickupByNumberQuery(int number)
        {
            Number = number;
        }

    }

    
}
