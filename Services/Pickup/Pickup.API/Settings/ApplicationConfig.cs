﻿using PickupSvc.Infrastructure.Repositories.Interfaces;

namespace PickupSvc.API.Settings
{
    

    public class ApplicationConfig : IApplicationConfig
    {
        public string PickupNumberGenerationBatchSize { get; set; }
        public string MaximumNumberOfPickupRenumberAttempts { get; set; }

    }
}
