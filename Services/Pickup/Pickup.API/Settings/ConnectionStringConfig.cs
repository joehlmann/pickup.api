﻿namespace PickupSvc.API.Settings
{
    public class ConnectionStringConfig
    {

        public string PickupDb { get; set; }

        public string ConnectionStringEvents { get; set; }

    }
}
