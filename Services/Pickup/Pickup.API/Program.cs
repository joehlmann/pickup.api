﻿using System;
using System.IO;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Serilog;
using Serilog.Core;
using Serilog.Events;

namespace PickupSvc.API
{
    public class Program
    {

        public static readonly string Namespace = typeof(Program).Namespace;
        public static readonly string AppName = Namespace.Substring(Namespace.LastIndexOf('.', Namespace.LastIndexOf('.') - 1) + 1);



        public static int Main(string[] args)
        {
            AppDomain.CurrentDomain.UnhandledException += AppUnhandledException;

            // CreateWebHostBuilder(args).Build().Run();
            var configuration = GetConfiguration();

            Log.Logger = CreateSerilogLogger(configuration);


            try
            {
                Log.Information("Configuring web host ({ApplicationContext})...", AppName);
                var host = BuildWebHost(configuration, args);

                //host.MigrateDbContext<CatalogContext>(context =>
                //{
                //    new CatalogContextSeed()
                //        .SeedAsync(context)
                //        .Wait();
                //});

                Log.Information("Applying migrations ({ApplicationContext})...", AppName);

                host.Run();
                return 0;

            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "Program terminated unexpectedly ({ApplicationContext})!", AppName);
                return 1;
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }


        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>();



        private static IWebHost BuildWebHost(IConfiguration configuration, string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .CaptureStartupErrors(false)
                .UseStartup<Startup>()
                .UseContentRoot(Directory.GetCurrentDirectory())
                .UseConfiguration(configuration)
                .UseSerilog()
                .Build();


        /// <summary>
        /// Setup the static Log instance
        /// </summary>
        /// <param name="config"></param>
        public static void ConfigureLogger(IConfiguration config)
        {
            var loggingConfig = config.GetSection("Logging");
            var levelSwitch = new LoggingLevelSwitch();
            var seqConnection = loggingConfig["SeqServerUrl"];
            var seqAPIKey = loggingConfig["SeqAPIKey"];
            var idOut = 0;
            Int32.TryParse(loggingConfig["SeqEventBodyLimitBytes"], out idOut);

            //Default event body size
            var eventBodyLimit = idOut > 0 ? idOut : 262144;
        
            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Verbose()
                .MinimumLevel.Override("Microsoft", LogEventLevel.Warning)
                .MinimumLevel.Override("System", LogEventLevel.Warning)
                .MinimumLevel.Override("Ocelot", LogEventLevel.Warning)
                .Enrich.FromLogContext()
                .WriteTo.Seq(seqConnection, apiKey: seqAPIKey, controlLevelSwitch: levelSwitch, eventBodyLimitBytes: eventBodyLimit)
                .CreateLogger();

        }


        private static ILogger CreateSerilogLogger(IConfiguration configuration)
        {

            var loggingConfig = configuration.GetSection("Logging");
            var levelSwitch = new LoggingLevelSwitch();
            var seqConnection = loggingConfig["SeqServerUrl"];
            var seqAPIKey = loggingConfig["SeqAPIKey"];
            var idOut = 0;
            Int32.TryParse(loggingConfig["SeqEventBodyLimitBytes"], out idOut);

            //Default event body size
            var eventBodyLimit = idOut > 0 ? idOut : 262144;

            var seqServerUrl = configuration["Logging:SeqServerUrl"];
            //var logstashUrl = configuration["Serilog:LogstashgUrl"];
            return new LoggerConfiguration()
                .MinimumLevel.Verbose()
                .MinimumLevel.Override("Microsoft", LogEventLevel.Information)
                .MinimumLevel.Override("System", LogEventLevel.Information)
                .MinimumLevel.Override("Ocelot", LogEventLevel.Information)
                .Enrich.WithThreadId()
                .Enrich.WithEnvironmentUserName()
                .Enrich.WithProperty("ApplicationContext", AppName)
                .Enrich.FromLogContext()
                .WriteTo.Console()
                .WriteTo.Seq(string.IsNullOrWhiteSpace(seqConnection) ? "http://seq:5341" : seqServerUrl, apiKey: seqAPIKey, controlLevelSwitch: levelSwitch, eventBodyLimitBytes: eventBodyLimit)
                //.WriteTo.Http(string.IsNullOrWhiteSpace(logstashUrl) ? "http://logstash:8080" : logstashUrl)
                .ReadFrom.Configuration(configuration)
                .CreateLogger();
        }


        private static void AppUnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            if (Log.Logger != null && e.ExceptionObject is Exception exception)
            {
                UnhandledExceptions(exception);

                // It's not necessary to flush if the application isn't terminating.
                if (e.IsTerminating)
                {
                    Log.CloseAndFlush();
                }
            }
        }

        private static void UnhandledExceptions(Exception e)
        {
            Log.Logger?.Error(e, "Console application crashed");
        }

        private static IConfiguration GetConfiguration()
        {

            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddEnvironmentVariables();

            return builder.Build();


        }

        //public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
        //    WebHost.CreateDefaultBuilder(args)
        //        .ConfigureLogging((ctx, logging) => { ConfigureLogger(ctx.Configuration); })
        //        .ConfigureServices(serviceCollection =>
        //            serviceCollection.AddSingleton<IAssertions, BorderExpressAssertions>())
        //        .UseStartup<Startup>()
        //        .UseSerilog();

        //.UseUrls("http://0.0.0.0:5013");
    }
}
