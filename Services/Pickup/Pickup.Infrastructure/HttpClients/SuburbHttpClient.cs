﻿using System.Net.Http;
using PickupSvc.Infrastructure.TokenContext;
using PickupSvc.TheDomain.Infrastructure;

namespace PickupSvc.Infrastructure.HttpClients
{
    public class SuburbHttpClient
    {
        public readonly HttpClient Client;
        private readonly ITokenContextService _tokenService;
        

        public SuburbHttpClient(HttpClient client, ITokenContextService tokenService)
        {
            Client = client;
            _tokenService = tokenService;
        }

        public HttpRequestMessage CreateRequest(string uri, HttpMethod method)
        {
            var request = new HttpRequestMessage(method,uri);
            var accessToken = _tokenService.GetAccessToken();
            if(accessToken.IsPresent())
            {
                request.Properties.Add("access_token",accessToken);
            }

            return request;
        }

        
    }
    
}
