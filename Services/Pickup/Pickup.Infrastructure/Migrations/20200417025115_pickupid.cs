﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace PickupSvc.Infrastructure.Migrations
{
    public partial class pickupid : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateSequence<int>(
                name: "BranchHiLo",
                startValue: 3000L,
                incrementBy: 5);

            migrationBuilder.CreateSequence<int>(
                name: "CustomerHiLo",
                startValue: 2000L,
                incrementBy: 5);

            migrationBuilder.CreateSequence(
                name: "PickupAddressHiLo",
                incrementBy: 10);

            migrationBuilder.CreateSequence<int>(
                name: "PickupHiLo",
                startValue: 5005L,
                incrementBy: 5);

            migrationBuilder.CreateSequence<int>(
                name: "PickupItemHiLo",
                startValue: 4000L,
                incrementBy: 5);

            migrationBuilder.CreateSequence<int>(
                name: "PickupTimeInfoHiLo",
                startValue: 7000L,
                incrementBy: 5);

            migrationBuilder.CreateSequence<int>(
                name: "PreferredBranchesHiLo",
                startValue: 6000L,
                incrementBy: 5);

            migrationBuilder.CreateSequence<int>(
                name: "RegionHiLo",
                startValue: 8000L,
                incrementBy: 5);

            migrationBuilder.CreateSequence<int>(
                name: "SuburbHiLo",
                startValue: 9000L,
                incrementBy: 5);

            migrationBuilder.CreateSequence<int>(
                name: "UserHiLo",
                startValue: 1000L,
                incrementBy: 5);

            migrationBuilder.CreateTable(
                name: "CustomerAccounts",
                columns: table => new
                {
                    CustomerId = table.Column<int>(nullable: false),
                    RowVersion = table.Column<byte[]>(rowVersion: true, nullable: true),
                    CurrentUserId = table.Column<string>(nullable: true),
                    CreateDate = table.Column<DateTime>(type: "datetime", nullable: false, defaultValueSql: "(getdate())"),
                    LastModified = table.Column<DateTime>(type: "datetime", nullable: true, defaultValueSql: "(getdate())"),
                    CustomerCode = table.Column<string>(maxLength: 50, nullable: true),
                    CustomerAccountName = table.Column<string>(maxLength: 50, nullable: true),
                    CustomerType = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CustomerAccounts", x => x.CustomerId);
                });

            migrationBuilder.CreateTable(
                name: "Regions",
                columns: table => new
                {
                    RegionId = table.Column<int>(nullable: false),
                    RowVersion = table.Column<byte[]>(rowVersion: true, nullable: true),
                    CurrentUserId = table.Column<string>(nullable: true),
                    CreateDate = table.Column<DateTime>(type: "datetime", nullable: false, defaultValueSql: "(getdate())"),
                    LastModified = table.Column<DateTime>(type: "datetime", nullable: true, defaultValueSql: "(getdate())"),
                    StateCode = table.Column<string>(maxLength: 50, nullable: true),
                    RegionName = table.Column<string>(maxLength: 50, nullable: true),
                    Active = table.Column<bool>(nullable: false),
                    RegionType = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Regions", x => x.RegionId)
                        .Annotation("SqlServer:Clustered", false);
                });

            migrationBuilder.CreateTable(
                name: "Requests",
                columns: table => new
                {
                    RequestId = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Time = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Requests", x => x.RequestId);
                });

            migrationBuilder.CreateTable(
                name: "Suburbs",
                columns: table => new
                {
                    SuburbId = table.Column<int>(nullable: false),
                    RowVersion = table.Column<byte[]>(rowVersion: true, nullable: true),
                    CurrentUserId = table.Column<string>(nullable: true),
                    CreateDate = table.Column<DateTime>(type: "datetime", nullable: false, defaultValueSql: "(getdate())"),
                    LastModified = table.Column<DateTime>(type: "datetime", nullable: true, defaultValueSql: "(getdate())"),
                    SuburbName = table.Column<string>(maxLength: 50, nullable: true),
                    PostCode = table.Column<string>(nullable: true),
                    StateCode = table.Column<string>(maxLength: 50, nullable: true),
                    SuburbType = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Suburbs", x => x.SuburbId)
                        .Annotation("SqlServer:Clustered", false);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    UserId = table.Column<int>(nullable: false),
                    RowVersion = table.Column<byte[]>(rowVersion: true, nullable: true),
                    CurrentUserId = table.Column<string>(nullable: true),
                    CreateDate = table.Column<DateTime>(type: "datetime", nullable: false, defaultValueSql: "(getdate())"),
                    LastModified = table.Column<DateTime>(type: "datetime", nullable: true, defaultValueSql: "(getdate())"),
                    DisplayName = table.Column<string>(nullable: true),
                    UserStatus = table.Column<string>(nullable: false),
                    Phone = table.Column<string>(type: "varchar(100)", nullable: true),
                    Email = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.UserId);
                });

            migrationBuilder.CreateTable(
                name: "Branches",
                columns: table => new
                {
                    BranchId = table.Column<int>(nullable: false),
                    RowVersion = table.Column<byte[]>(rowVersion: true, nullable: true),
                    CurrentUserId = table.Column<string>(nullable: true),
                    CreateDate = table.Column<DateTime>(type: "datetime", nullable: false, defaultValueSql: "(getdate())"),
                    LastModified = table.Column<DateTime>(type: "datetime", nullable: true, defaultValueSql: "(getdate())"),
                    BranchType = table.Column<string>(nullable: false),
                    PickupRules_MinimumPickupWindowDuration = table.Column<int>(nullable: true),
                    PickupRules_MaximumDaysPickupDateIsAllowedIntoTheFuture = table.Column<int>(nullable: true),
                    PickupRules_MaximumDaysPickupDateIsAllowedIntoThePast = table.Column<int>(nullable: true),
                    Branch = table.Column<string>(maxLength: 50, nullable: true),
                    Address_Address1 = table.Column<string>(nullable: true),
                    Address_Address2 = table.Column<string>(nullable: true),
                    Address_PostCode = table.Column<string>(nullable: true),
                    Address_State = table.Column<string>(nullable: true),
                    Address_Suburb = table.Column<string>(nullable: true),
                    DefaultRegionId = table.Column<int>(nullable: false),
                    PickupCutoff = table.Column<DateTime>(type: "datetime", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Branches", x => x.BranchId)
                        .Annotation("SqlServer:Clustered", false);
                    table.ForeignKey(
                        name: "FK_Branches_Regions_DefaultRegionId",
                        column: x => x.DefaultRegionId,
                        principalTable: "Regions",
                        principalColumn: "RegionId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Pickups",
                columns: table => new
                {
                    PickupId = table.Column<int>(nullable: false),
                    RowVersion = table.Column<byte[]>(rowVersion: true, nullable: true),
                    CurrentUserId = table.Column<string>(nullable: true),
                    CreateDate = table.Column<DateTime>(type: "datetime", nullable: false, defaultValueSql: "(getdate())"),
                    LastModified = table.Column<DateTime>(type: "datetime", nullable: true, defaultValueSql: "(getdate())"),
                    PickupType = table.Column<string>(nullable: false),
                    PickupNumber = table.Column<int>(type: "int", nullable: false),
                    ChargeAccountId = table.Column<int>(nullable: true),
                    SenderDetailsId = table.Column<int>(nullable: true),
                    SpecialInstructions = table.Column<string>(nullable: true),
                    HasPallets = table.Column<bool>(nullable: false),
                    PickupSuburbId = table.Column<int>(nullable: true),
                    ReceiverSuburbId = table.Column<int>(nullable: true),
                    FPBPays = table.Column<bool>(type: "bit", nullable: false),
                    SenderContactName = table.Column<string>(maxLength: 150, nullable: true),
                    SenderEmail = table.Column<string>(maxLength: 250, nullable: true),
                    SenderContactPhone = table.Column<string>(maxLength: 150, nullable: true),
                    HasDangerousGoodsInsurance = table.Column<bool>(type: "bit", nullable: false),
                    BookedByUserId = table.Column<int>(nullable: true),
                    PickupBranchId = table.Column<int>(nullable: true),
                    BookedLate = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Pickups", x => x.PickupId)
                        .Annotation("SqlServer:Clustered", false);
                    table.ForeignKey(
                        name: "FK_Pickups_Users_BookedByUserId",
                        column: x => x.BookedByUserId,
                        principalTable: "Users",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Pickups_CustomerAccounts_ChargeAccountId",
                        column: x => x.ChargeAccountId,
                        principalTable: "CustomerAccounts",
                        principalColumn: "CustomerId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Pickups_Branches_PickupBranchId",
                        column: x => x.PickupBranchId,
                        principalTable: "Branches",
                        principalColumn: "BranchId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Pickups_Suburbs_PickupSuburbId",
                        column: x => x.PickupSuburbId,
                        principalTable: "Suburbs",
                        principalColumn: "SuburbId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Pickups_Suburbs_ReceiverSuburbId",
                        column: x => x.ReceiverSuburbId,
                        principalTable: "Suburbs",
                        principalColumn: "SuburbId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Pickups_CustomerAccounts_SenderDetailsId",
                        column: x => x.SenderDetailsId,
                        principalTable: "CustomerAccounts",
                        principalColumn: "CustomerId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PreferredBranches",
                columns: table => new
                {
                    PreferredBranchId = table.Column<int>(nullable: false),
                    BranchId = table.Column<int>(nullable: false),
                    SuburbId = table.Column<int>(nullable: false),
                    IsOnForwardEnabled = table.Column<bool>(nullable: false),
                    Order = table.Column<int>(nullable: false),
                    PreferredBranchType = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PreferredBranches", x => x.PreferredBranchId);
                    table.ForeignKey(
                        name: "FK_PreferredBranches_Branches_BranchId",
                        column: x => x.BranchId,
                        principalTable: "Branches",
                        principalColumn: "BranchId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PreferredBranches_Suburbs_SuburbId",
                        column: x => x.SuburbId,
                        principalTable: "Suburbs",
                        principalColumn: "SuburbId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Pallets",
                columns: table => new
                {
                    PalletId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    HasPallets = table.Column<bool>(nullable: false),
                    PalletType = table.Column<int>(nullable: false),
                    ChepPalletTransferDocketNumber = table.Column<string>(nullable: true),
                    ExchangeChepPalletsOut = table.Column<int>(nullable: false),
                    TransferChepPalletsToBex = table.Column<int>(nullable: false),
                    TransferChepPalletsToReceiver = table.Column<int>(nullable: false),
                    LoscamPalletTransferDocketNumber = table.Column<string>(nullable: true),
                    ExchangeLoscamPalletsOut = table.Column<int>(nullable: false),
                    TransferLoscamPalletsToBex = table.Column<int>(nullable: false),
                    TransferLoscamPalletsToReceiver = table.Column<int>(nullable: false),
                    PickupId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Pallets", x => x.PalletId);
                    table.ForeignKey(
                        name: "FK_Pallets_Pickups_PickupId",
                        column: x => x.PickupId,
                        principalTable: "Pickups",
                        principalColumn: "PickupId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PickupAddress",
                columns: table => new
                {
                    PickupAddressId = table.Column<int>(nullable: false),
                    Address1 = table.Column<string>(nullable: true),
                    Address2 = table.Column<string>(nullable: true),
                    PostCode = table.Column<string>(nullable: true),
                    State = table.Column<string>(nullable: true),
                    Suburb = table.Column<string>(nullable: true),
                    PickupId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PickupAddress", x => x.PickupAddressId);
                    table.ForeignKey(
                        name: "FK_PickupAddress_Pickups_PickupId",
                        column: x => x.PickupId,
                        principalTable: "Pickups",
                        principalColumn: "PickupId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PickupConsignmentNumbers",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CustomerCode = table.Column<string>(nullable: true),
                    ConsignmentNum = table.Column<string>(nullable: true),
                    PickupId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PickupConsignmentNumbers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PickupConsignmentNumbers_Pickups_PickupId",
                        column: x => x.PickupId,
                        principalTable: "Pickups",
                        principalColumn: "PickupId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PickupItems",
                columns: table => new
                {
                    PickupItemId = table.Column<int>(nullable: false),
                    RowVersion = table.Column<byte[]>(rowVersion: true, nullable: true),
                    CurrentUserId = table.Column<string>(nullable: true),
                    CreateDate = table.Column<DateTime>(type: "datetime", nullable: false, defaultValueSql: "(getdate())"),
                    LastModified = table.Column<DateTime>(type: "datetime", nullable: true, defaultValueSql: "(getdate())"),
                    NumberOfUnits = table.Column<int>(nullable: false),
                    DescriptionOfGoods = table.Column<string>(maxLength: 50, nullable: true),
                    WeightOfGoods = table.Column<int>(nullable: false),
                    HasFoodstuffs = table.Column<bool>(nullable: false),
                    ReceivingRegionId = table.Column<int>(nullable: true),
                    PickupId = table.Column<int>(nullable: false),
                    PickupItemType = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PickupItems", x => x.PickupItemId)
                        .Annotation("SqlServer:Clustered", false);
                    table.ForeignKey(
                        name: "FK_PickupItems_Pickups_PickupId",
                        column: x => x.PickupId,
                        principalTable: "Pickups",
                        principalColumn: "PickupId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PickupItems_Regions_ReceivingRegionId",
                        column: x => x.ReceivingRegionId,
                        principalTable: "Regions",
                        principalColumn: "RegionId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PickupTimeInfo",
                columns: table => new
                {
                    TimeInfoId = table.Column<int>(nullable: false),
                    PickupDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    TimeReady = table.Column<DateTime>(type: "datetime", nullable: true),
                    TimeClose = table.Column<DateTime>(type: "datetime", nullable: true),
                    BookedDateTime = table.Column<DateTime>(type: "datetime", nullable: false, defaultValueSql: "(getdate())"),
                    BookedLate = table.Column<bool>(nullable: false),
                    PickupId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PickupTimeInfo", x => x.TimeInfoId);
                    table.ForeignKey(
                        name: "FK_PickupTimeInfo_Pickups_PickupId",
                        column: x => x.PickupId,
                        principalTable: "Pickups",
                        principalColumn: "PickupId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PickupItemDGoods",
                columns: table => new
                {
                    PickupItemDGoodId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UNNo = table.Column<int>(nullable: false),
                    Class = table.Column<double>(nullable: false),
                    PickupItemId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PickupItemDGoods", x => x.PickupItemDGoodId);
                    table.ForeignKey(
                        name: "FK_PickupItemDGoods_PickupItems_PickupItemId",
                        column: x => x.PickupItemId,
                        principalTable: "PickupItems",
                        principalColumn: "PickupItemId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Branches_DefaultRegionId",
                table: "Branches",
                column: "DefaultRegionId");

            migrationBuilder.CreateIndex(
                name: "IX_Pallets_PickupId",
                table: "Pallets",
                column: "PickupId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_PickupAddress_PickupId",
                table: "PickupAddress",
                column: "PickupId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_PickupConsignmentNumbers_PickupId",
                table: "PickupConsignmentNumbers",
                column: "PickupId");

            migrationBuilder.CreateIndex(
                name: "IX_PickupItemDGoods_PickupItemId",
                table: "PickupItemDGoods",
                column: "PickupItemId");

            migrationBuilder.CreateIndex(
                name: "IX_PickupItems_PickupId",
                table: "PickupItems",
                column: "PickupId");

            migrationBuilder.CreateIndex(
                name: "IX_PickupItems_ReceivingRegionId",
                table: "PickupItems",
                column: "ReceivingRegionId");

            migrationBuilder.CreateIndex(
                name: "IX_Pickups_BookedByUserId",
                table: "Pickups",
                column: "BookedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Pickups_ChargeAccountId",
                table: "Pickups",
                column: "ChargeAccountId");

            migrationBuilder.CreateIndex(
                name: "IX_Pickups_PickupBranchId",
                table: "Pickups",
                column: "PickupBranchId");

            migrationBuilder.CreateIndex(
                name: "IX_Pickups_PickupSuburbId",
                table: "Pickups",
                column: "PickupSuburbId");

            migrationBuilder.CreateIndex(
                name: "IX_Pickups_ReceiverSuburbId",
                table: "Pickups",
                column: "ReceiverSuburbId");

            migrationBuilder.CreateIndex(
                name: "IX_Pickups_SenderDetailsId",
                table: "Pickups",
                column: "SenderDetailsId");

            migrationBuilder.CreateIndex(
                name: "IX_PickupTimeInfo_PickupId",
                table: "PickupTimeInfo",
                column: "PickupId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_PreferredBranches_BranchId",
                table: "PreferredBranches",
                column: "BranchId");

            migrationBuilder.CreateIndex(
                name: "IX_PreferredBranches_SuburbId",
                table: "PreferredBranches",
                column: "SuburbId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Pallets");

            migrationBuilder.DropTable(
                name: "PickupAddress");

            migrationBuilder.DropTable(
                name: "PickupConsignmentNumbers");

            migrationBuilder.DropTable(
                name: "PickupItemDGoods");

            migrationBuilder.DropTable(
                name: "PickupTimeInfo");

            migrationBuilder.DropTable(
                name: "PreferredBranches");

            migrationBuilder.DropTable(
                name: "Requests");

            migrationBuilder.DropTable(
                name: "PickupItems");

            migrationBuilder.DropTable(
                name: "Pickups");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropTable(
                name: "CustomerAccounts");

            migrationBuilder.DropTable(
                name: "Branches");

            migrationBuilder.DropTable(
                name: "Suburbs");

            migrationBuilder.DropTable(
                name: "Regions");

            migrationBuilder.DropSequence(
                name: "BranchHiLo");

            migrationBuilder.DropSequence(
                name: "CustomerHiLo");

            migrationBuilder.DropSequence(
                name: "PickupAddressHiLo");

            migrationBuilder.DropSequence(
                name: "PickupHiLo");

            migrationBuilder.DropSequence(
                name: "PickupItemHiLo");

            migrationBuilder.DropSequence(
                name: "PickupTimeInfoHiLo");

            migrationBuilder.DropSequence(
                name: "PreferredBranchesHiLo");

            migrationBuilder.DropSequence(
                name: "RegionHiLo");

            migrationBuilder.DropSequence(
                name: "SuburbHiLo");

            migrationBuilder.DropSequence(
                name: "UserHiLo");
        }
    }
}
