﻿using System.Collections.Generic;

namespace PickupSvc.Infrastructure.IntegrationModels.Suburbs
{
    public class SuburbsIntegrationModel
    {
        public List<SuburbIntegrationModel> Suburbs { get;set; }
    }
}
