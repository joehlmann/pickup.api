﻿namespace PickupSvc.Infrastructure.IntegrationModels.Suburbs
{
    public class SuburbPreferredBranchIntegrationModel
    {
        public int BranchId { get; set; }
        public bool IsOnforwardEnabled { get; set; }
        public int Order { get;set; }
        public char Type { get; set; }
    }
}
