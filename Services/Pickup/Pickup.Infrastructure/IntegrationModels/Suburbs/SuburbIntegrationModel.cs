﻿using System.Collections.Generic;

namespace PickupSvc.Infrastructure.IntegrationModels.Suburbs
{
    public class SuburbIntegrationModel
    {
        public int Id { get;set; }
        public string Postcode { get; set; }
        public string Suburb { get; set; }
        public string State { get; set; }
        public List<SuburbPreferredBranchIntegrationModel> PreferredBranches { get;set; }
        
    }
}
