﻿using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PickupSvc.TheDomain.ValueObjects;

namespace PickupSvc.Infrastructure.EntityConfigurations
{
    public class DangerousGoodEntityTypeConfiguration : IEntityTypeConfiguration<DangerousGood>
    {
        public void Configure(EntityTypeBuilder<DangerousGood> builder)
        {
         

            builder.ToTable("DangerousGoods");

            builder.HasAlternateKey(e => new {e.UNNo,e.Class});
            


            builder.Property(e => e.Class)
                .HasColumnName("DgClass").HasColumnType("float");

            builder.Property(e => e.UNNo)
                .HasColumnName("UNNo").HasColumnType("int");





            builder.HasData(new List<DangerousGood>()
                {
                    new DangerousGood(333,2),
                    new DangerousGood(444,2),
                }
            );

        }
    }
}
