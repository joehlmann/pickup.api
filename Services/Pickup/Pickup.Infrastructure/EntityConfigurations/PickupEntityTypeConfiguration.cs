﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PickupSvc.TheDomain.Entities;
using PickupSvc.TheDomain.Enumerators;

namespace PickupSvc.Infrastructure.EntityConfigurations
{
   public  class PickupEntityTypeConfiguration : IEntityTypeConfiguration<Pickup>
    {
        public void Configure(EntityTypeBuilder<Pickup> builder)
        {

            builder.Ignore(e => e.Errors);
            builder.Ignore(e => e.DomainEvents);
            builder.Ignore(e => e.HasErrors);

            builder.Ignore(e => e.HasDangerousGoods);

            builder.ToTable("Pickups");

            builder.HasKey(e => e.Id)
                .IsClustered(false);


            builder.Property(e => e.Id).HasColumnName("PickupId").ValueGeneratedOnAdd().UseHiLo("PickupHiLo");


            builder.Property(e => e.PickupNumber)
                .HasColumnName("PickupNumber")
                .HasColumnType("int");


           

            

            builder.Property(e => e.SpecialInstructions)
                .HasColumnName("SpecialInstructions");

            builder.Property(e => e.HasPallets)
                .HasColumnName("HasPallets");


           

            // fk p-suburb
            builder.HasOne(p => p.PickupSuburb);

            // fk r-suburb
            builder.HasOne(p => p.ReceiverSuburb);


            builder.Property(e => e.FreightPayableByThirdParty)
                .HasColumnName("FreightPayableByThirdParty")
                .HasColumnType("bit");


           
            

            builder.Property(e => e.HasDangerousGoodsInsurance)
                .HasColumnName("HasDangerousGoodsInsurance")
                .HasColumnType("bit");


            //pallets

            builder.OwnsOne(p => p.Pallets, a =>
            {
                a.ToTable("Pallets");
                a.Property<int>("PalletId");
                a.HasKey("PalletId");
            });



            


            builder.HasOne(p => p.BookedByUser);

            builder.HasOne(p => p.PickupBranch);

            builder.Property(e => e.BookedLate)
                .HasColumnName("BookedLate")
                .HasColumnType("bit");



            //Value Objects

            //Contact 

            builder.OwnsOne(c => c.Contact)
                .Property(c=> c.ContactName)
                .HasColumnName("SenderContactName")
                .HasMaxLength(150);

            builder.OwnsOne(c => c.Contact)
                .Property(c => c.Phone)
                .HasColumnName("SenderContactPhone")
                .HasMaxLength(150);

            builder.OwnsOne(c => c.Contact)
                .Property(c => c.Email)
                .HasColumnName("SenderEmail")
                .HasMaxLength(250);

            //Address 

            builder.OwnsOne(p => p.Address, a =>
            {
                a.WithOwner().HasForeignKey("PickupId");

                
                a.Property<int>("PickupAddressId").ValueGeneratedOnAdd().UseHiLo("PickupAddressHiLo");
                a.HasKey("PickupAddressId");
                a.ToTable("PickupAddress");
            });


            // PickupTimeInfo

            builder.OwnsOne(p => p.TimeInfo, a =>
            {
                a.WithOwner().HasForeignKey("PickupId");

                a.Property(e => e.PickupDate)
                    .HasColumnName("PickupDate")
                    .HasColumnType("datetime");

                a.Property(e => e.BookedDateTime)
                    .HasColumnName("BookedDateTime")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())")
                    .IsRequired(true);

                a.Property(e => e.ReadyTime).HasColumnName("TimeReady").HasColumnType("datetime").IsRequired(false);
                a.Property(e => e.CloseTime).HasColumnName("TimeClose").HasColumnType("datetime").IsRequired(false);

                a.Property<int>("TimeInfoId").ValueGeneratedOnAdd().UseHiLo("PickupTimeInfoHiLo");
                a.HasKey("TimeInfoId");
                a.ToTable("PickupTimeInfo");
            });


            ////PickupItems 
            //// fk
            builder.HasMany(p => p.Items)
                .WithOne(p => p.Pickup)
                .HasForeignKey(p => p.PickupId);




            builder.Property(e => e.FreightPayableByThirdParty)
                .HasColumnName("FPBPays");


            //ConsignmentNumbers 



            builder.OwnsMany(p => p.ConsignmentNumbers, a =>
            {
                a.WithOwner(). HasForeignKey("PickupId");
                a.Property<int>("Id").ValueGeneratedOnAdd();
                a.HasKey("Id");
                a.ToTable("PickupConsignmentNumbers");
            });


            //Type
            builder.Property(c => c.PickupType)
                .HasConversion(
                    v => v.ToString(),
                    v => (PickupType)Enum.Parse(typeof(PickupType), v));


            // Audit 

            builder.Property(e => e.CreateDate).HasColumnType("datetime").ValueGeneratedOnAdd()
                .HasDefaultValueSql("(getdate())");

            builder.Property(e => e.LastModified).HasColumnType("datetime").ValueGeneratedOnUpdate()
                .HasDefaultValueSql("(getdate())");



        }
    }
}
