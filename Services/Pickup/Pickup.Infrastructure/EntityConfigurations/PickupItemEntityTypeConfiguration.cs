﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PickupSvc.TheDomain.Entities;
using PickupSvc.TheDomain.Enumerators;

namespace PickupSvc.Infrastructure.EntityConfigurations
{
    class PickupItemEntityTypeConfiguration : IEntityTypeConfiguration<PickupItem>
    {
        public void Configure(EntityTypeBuilder<PickupItem> builder)
        {
            builder.ToTable("PickupItems");
            builder.Ignore(e => e.Errors);
            builder.Ignore(e => e.DomainEvents);
            builder.Ignore(e => e.HasErrors);
            //itemConfiguration.Ignore(e => e.HasDangerousGoods);


            builder.HasKey(e => e.Id)
                .IsClustered(false);

            builder.Property(e => e.Id).HasColumnName("PickupItemId").ValueGeneratedOnAdd().UseHiLo("PickupItemHiLo");

            builder.Property(e => e.Quantity).HasColumnName("NumberOfUnits");

            builder.Property(e => e.Description)
                .HasColumnName("DescriptionOfGoods")
                .HasMaxLength(50);

            builder.Property(e => e.WeightKg).HasColumnName("WeightOfGoods");

            builder.Property(e => e.HasFoodStuff).HasColumnName("HasFoodstuffs");

            // fk region
            builder.HasOne(c => c.ReceivingRegion);


            //Type
            builder.Property(c => c.PickupItemType)
                .HasConversion(
                    v => v.ToString(),
                    v => (PickupItemType)Enum.Parse(typeof(PickupItemType), v));



            ////// fk pickup
            //builder.HasOne(c => c.Pickup)
            //    .WithMany(e => e.Items)
            //    .HasForeignKey(p => p.PickupId);


            //builder.Property(e => e.DangerousGoods).HasColumnName("HasDangerousGoods");

            //builder.Property(p => p.DangerousGoods)
            //    .HasField("_dangerousGoods")
            //    .UsePropertyAccessMode(PropertyAccessMode.Property);


            // DG collection
            builder.OwnsMany(p => p.DangerousGoods, a =>
            {
                a.WithOwner().HasForeignKey("PickupItemId");
                a.Property<int>("PickupItemDGoodId");
                a.HasKey("PickupItemDGoodId");
                a.ToTable("PickupItemDGoods");
            });




            // audit

            builder.Property(e => e.CreateDate).HasColumnType("datetime").ValueGeneratedOnAdd()
                .HasDefaultValueSql("(getdate())");

            builder.Property(e => e.LastModified).HasColumnType("datetime").ValueGeneratedOnUpdate()
                .HasDefaultValueSql("(getdate())");




        }
    }
}
