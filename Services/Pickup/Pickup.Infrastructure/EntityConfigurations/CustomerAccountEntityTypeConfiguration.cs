﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PickupSvc.TheDomain.Entities;
using PickupSvc.TheDomain.Enumerators;

namespace PickupSvc.Infrastructure.EntityConfigurations
{
    public  class CustomerAccountEntityTypeConfiguration : IEntityTypeConfiguration<CustomerAccount>
    {
        public void Configure(EntityTypeBuilder<CustomerAccount> builder)
        {
            builder.Ignore(e => e.DomainEvents);
            builder.Ignore(e => e.Errors);
            builder.Ignore(e => e.HasErrors);

            builder.ToTable("CustomerAccounts");

            builder.HasKey(e => e.Id);
                


            
            builder.Property(e => e.Id).HasColumnName("CustomerId").ValueGeneratedOnAdd().UseHiLo("CustomerHiLo");

            builder.Property(c => c.CustomerCode)
                .HasMaxLength(50);

            builder.Property(c => c.CustomerAccountName)
                .HasMaxLength(50);



            //Type
            builder.Property(c => c.CustomerType)
                .HasConversion(
                    v => v.ToString(),
                    v => (CustomerType)Enum.Parse(typeof(CustomerType), v));

            // audit

            builder.Property(e => e.CreateDate).HasColumnType("datetime").ValueGeneratedOnAdd()
                .HasDefaultValueSql("(getdate())");

            builder.Property(e => e.LastModified).HasColumnType("datetime").ValueGeneratedOnUpdate()
                .HasDefaultValueSql("(getdate())");


        }
    }
}
