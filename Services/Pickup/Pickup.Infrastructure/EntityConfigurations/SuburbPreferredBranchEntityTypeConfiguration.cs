﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PickupSvc.TheDomain.Entities;

namespace PickupSvc.Infrastructure.EntityConfigurations
{
     public class SuburbPreferredBranchEntityTypeConfiguration : IEntityTypeConfiguration<SuburbPreferredBranch>
    {
        public void Configure(EntityTypeBuilder<SuburbPreferredBranch> builder)
        {
            builder.ToTable("SuburbPreferredBranches");

            builder.HasKey(sb => new { sb.SuburbId, sb.BranchId });

            builder.HasOne(s => s.Suburb)
                .WithMany(s => s.PreferredBranches)
                .HasForeignKey(lu => lu.SuburbId);

            builder.HasOne(s => s.Branch)
                .WithMany()
                .HasForeignKey(lu => lu.BranchId);
        }
    }
}
