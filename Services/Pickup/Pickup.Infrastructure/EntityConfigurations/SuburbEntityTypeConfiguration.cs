﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PickupSvc.TheDomain.Entities;
using PickupSvc.TheDomain.Enumerators;

namespace PickupSvc.Infrastructure.EntityConfigurations
{
    public class SuburbEntityTypeConfiguration : IEntityTypeConfiguration<Suburb>
    {

        public void Configure(EntityTypeBuilder<Suburb> builder)
        {
            builder.Ignore(e => e.DomainEvents);
            builder.Ignore(e => e.Errors);
            builder.Ignore(e => e.HasErrors);


            builder.HasKey(e => e.Id)
                .IsClustered(false);


            builder.ToTable("Suburbs");
            builder.Property(e => e.Id).HasColumnName("SuburbId").ValueGeneratedOnAdd().UseHiLo("SuburbHiLo");

            builder.Property(e => e.Name)
                .HasColumnName("SuburbName")
                .HasMaxLength(50);
            builder.Property(e => e.State)
                .HasColumnName("StateCode")
                .HasMaxLength(50);
            builder.Property(e => e.Postcode)
                .HasColumnName("PostCode");


            // PreferredBranches
            builder.OwnsMany(p => p.PreferredBranches, a =>
            {
                //a.WithOwner().HasForeignKey("SuburbId");
                a.ToTable("PreferredBranches");
                a.Property<int>("PreferredBranchId").ValueGeneratedOnAdd().UseHiLo("PreferredBranchesHiLo"); ;
                a.HasKey("PreferredBranchId");
                

            });

            //Type
            builder.Property(c => c.SuburbType)
                .HasConversion(
                    v => v.ToString(),
                    v => (SuburbType)Enum.Parse(typeof(SuburbType), v));

            // audit

            builder.Property(e => e.CreateDate).HasColumnType("datetime").ValueGeneratedOnAdd()
                .HasDefaultValueSql("(getdate())");

            builder.Property(e => e.LastModified).HasColumnType("datetime").ValueGeneratedOnUpdate()
                .HasDefaultValueSql("(getdate())");

        }


    }
}
