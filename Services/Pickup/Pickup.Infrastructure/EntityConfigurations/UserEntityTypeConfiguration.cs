﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PickupSvc.TheDomain.Entities;
using PickupSvc.TheDomain.Enumerators;

namespace PickupSvc.Infrastructure.EntityConfigurations
{
    public class UserEntityTypeConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {

            builder.Ignore(e => e.DomainEvents);
            builder.Ignore(e => e.Errors);
            builder.Ignore(e => e.HasErrors);
            

            builder.ToTable("Users");

            builder.HasKey(u => u.Id);
            builder.Property(u => u.Id).HasColumnName("UserId").ValueGeneratedOnAdd().UseHiLo("UserHiLo");



            builder.Property(u => u.Phone).HasColumnType("varchar(100)");

            

            builder.Property(e => e.UserStatus)
                .HasConversion(
                    v => v.ToString(),
                    v => (UserStatus)Enum.Parse(typeof(UserStatus), v));



            //audit

            builder.Property(e => e.CreateDate).HasColumnType("datetime").ValueGeneratedOnAdd()
                .HasDefaultValueSql("(getdate())");

            builder.Property(e => e.LastModified).HasColumnType("datetime").ValueGeneratedOnUpdate()
                .HasDefaultValueSql("(getdate())");


            //builder.HasData(new List<User>
            //{
            //    User.Create(1, UserStatus.Registered, "John", "9999999", "1@1.com"),
            //    User.Create(2, UserStatus.Registered, "Mary", "9999999", "2@2.com")

            //});




        }

    }
}
