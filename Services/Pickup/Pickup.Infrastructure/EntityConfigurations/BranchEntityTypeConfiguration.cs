﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PickupSvc.TheDomain.Entities;
using PickupSvc.TheDomain.Enumerators;

namespace PickupSvc.Infrastructure.EntityConfigurations
{
    public class BranchEntityTypeConfiguration : IEntityTypeConfiguration<Branch>
    {
        /// <summary>
        ///     Configures the entity of type <typeparamref name="TEntity" />.
        /// </summary>
        /// <param name="builder"> The builder to be used to configure the entity type. </param>
        public void Configure(EntityTypeBuilder<Branch> builder)
        {
            
            builder.Ignore(e => e.DomainEvents);
            builder.Ignore(e => e.Errors);
            builder.Ignore(e => e.HasErrors);
            builder.Ignore(e => e.PickupBookingCutoffInMinutesVariance);

            builder.HasKey(e => e.Id)
                .IsClustered(false);
                
            
            builder.ToTable("Branches");
            builder.Property(e => e.Id).HasColumnName("BranchId").ValueGeneratedOnAdd().UseHiLo("BranchHiLo");

            builder.Property(e => e.PickupCutoffTime).HasColumnName("PickupCutoff").HasColumnType("datetime");
            builder.Property(e => e.Name)
                .HasColumnName("Branch")
                .HasMaxLength(50);

            //Type
            builder.Property(c => c.BranchType)
                .HasConversion(
                    v => v.ToString(),
                    v => (BranchType)Enum.Parse(typeof(BranchType), v));




            builder.OwnsOne(a => a.Address);

            builder.OwnsOne(a => a.PickupRules);


            // fk to Region 
            builder.HasOne(e => e.DefaultRegion);


            //audit 

            builder.Property(e => e.CreateDate).HasColumnType("datetime").ValueGeneratedOnAdd()
                .HasDefaultValueSql("(getdate())");

            builder.Property(e => e.LastModified).HasColumnType("datetime").ValueGeneratedOnUpdate()
                .HasDefaultValueSql("(getdate())");




        }
    }
}
