﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PickupSvc.TheDomain.Entities;
using PickupSvc.TheDomain.Enumerators;

namespace PickupSvc.Infrastructure.EntityConfigurations
{
    public class RegionEntityTypeConfiguration : IEntityTypeConfiguration<Region>
    {
        /// <summary>
        ///     Configures the entity of type <typeparamref name="TEntity" />.
        /// </summary>
        /// <param name="builder"> The builder to be used to configure the entity type. </param>
        public void Configure(EntityTypeBuilder<Region> builder)
        {
            builder.Ignore(e => e.DomainEvents);
            builder.Ignore(e => e.Errors);
            builder.Ignore(e => e.HasErrors);


            builder.HasKey(e => e.Id)
                .IsClustered(false);

            builder.ToTable("Regions");
            builder.Property(e => e.Id).HasColumnName("RegionId").ValueGeneratedOnAdd().UseHiLo("RegionHiLo");
            builder.Property(e => e.Name)
                .HasColumnName("RegionName")
                .HasMaxLength(50);
            builder.Property(e => e.State)
                .HasColumnName("StateCode")
                .HasMaxLength(50);
            builder.Property(e => e.Active)
                .HasColumnName("Active");

            //Type
            builder.Property(c => c.RegionType)
                .HasConversion(
                    v => v.ToString(),
                    v => (RegionType)Enum.Parse(typeof(RegionType), v));


            // audit

            builder.Property(e => e.CreateDate).HasColumnType("datetime").ValueGeneratedOnAdd()
                .HasDefaultValueSql("(getdate())");

            builder.Property(e => e.LastModified).HasColumnType("datetime").ValueGeneratedOnUpdate()
                .HasDefaultValueSql("(getdate())");
        }
    }
}

