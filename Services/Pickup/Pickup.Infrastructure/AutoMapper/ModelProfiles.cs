﻿using AutoMapper;

//using RabbitMQ.Client.MessagePatterns;

namespace PickupSvc.Infrastructure.AutoMapper
{
    public class ModelProfiles : Profile
    {
        /// <summary>
        /// Specifies the AutoMapper profiles when mapping from DbModels to Models (Entities)
        /// </summary>
        public ModelProfiles()
        {
            //CreateMap<BranchDbModel, Branch>()
            //    .ForMember(dest => dest.PickupCutoffTime, opt => opt.MapFrom(src => src.PickupCutoffTime.Value.ToString("hh:mm tt")));

            //CreateMap<Models.PickupDb, PickupRoot>()
            //    .ForMember(dest => dest.HasDangerousGoods,
            //        opt => opt.MapFrom(src => src.HasDangerousGoods != 2))
            //    .ForMember(dest => dest.HasDangerousGoodsInsurance,
            //        opt => opt.MapFrom(src => src.HasDangerousGoodsInsurance != 2))
            //    .ForMember(dest => dest.HasPallets,
            //        opt => opt.MapFrom(src => !src.PalletsNotApplicable))
            //    .ForMember(dest => dest.BookedDateTime,
            //        opt => opt.MapFrom(src => src.BookedTime))
            //    //.ForPath(dest => dest.Suburb,
            //    //    opt => opt.MapFrom(src => new Suburb(src.SuburbId,"",src.State,src.Postcode,null))) //TODO: Implement the suburb name as it's not stored
            //    //.ForPath(dest => dest.ReceiverSuburb,
            //    //    opt => opt.MapFrom(src => new Suburb(src.ReceiverSuburbId,"",src.ReceiverState,src.ReceiverPostcode,null))) //TODO: Implement the suburb name as it's not stored
                
            //    .ForPath(dest => dest.PickupBranch,
            //        opt => opt.MapFrom(src => new Branch(src.PickupBranch)))
            //    //.ForPath(dest => dest.ChargeAccount,
            //    //    opt => opt.MapFrom(src => new CustomerAccount(Int32.Parse(src.ChargeAccountForeignKey),src.ChargeAccountCode,""))) //TODO: Implement the customer name as it's not stored
            //    .ForPath(dest => dest.BookedByUser,
            //        opt => opt.MapFrom(src => new User(0,src.CreatedUser)))
            //    //.ForPath(dest => dest.SenderDetails,
            //    //    opt => opt.MapFrom(src => new CustomerAccount(Int32.Parse(src.SenderId),src.SenderCode,src.SenderName)))
            //    .ForCtorParam("pallets", opt => opt.MapFrom(src => new PickupPallets(
            //         src.ExchangeChepPalletsOut, src.TransferChepPalletsToBex, src.TransferChepPalletsToReceiver, src.ChepPalletTransferDocketNumber,
            //       src.ExchangeLoscamPalletsOut, src.TransferLoscamPalletsToBex, src.TransferLoscamPalletsToReceiver, src.LoscamPalletTransferDocketNumber)))
                
            //    .ForCtorParam("items", opt => opt.MapFrom(src => src.Items));
            
            //ShouldUseConstructor = ci => ci.IsPrivate;

            //CreateMap<PickupItemDbModel,PickupItem >()
            //    .ForMember(dest => dest.HasDangerousGoods,
            //        opt => opt.MapFrom(src =>  src.DangerousGoods == 1))
            //    .ForMember(dest => dest.HasFoodStuff,
            //        opt => opt.MapFrom(src =>  src.FoodStuff == 1))
            //    //.ForPath(dest => dest.ReceivingRegion, opt => opt.MapFrom(src => src.RegionId != null ? new Region(src.RegionId.Value,"") : null)) //TODO: Implement the region name as it's not stored
            //    .ForCtorParam("dangerousGoods", opt => opt.MapFrom((src, ctx) => new PickupItemDbDangerousGoodsResolver().Resolve(src,null,null,ctx)));
            //    ;
            
            
            
            // Base class profile
            //CreateMap<DbModel, Entity>().IncludeAllDerived().AfterMap((src, dest) =>
            //{
            //    dest.SetInternalGuid(src.InternalGuid);
            //});
        }
    }
}
