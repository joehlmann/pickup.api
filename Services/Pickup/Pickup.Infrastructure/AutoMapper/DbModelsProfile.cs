﻿using AutoMapper;

namespace PickupSvc.Infrastructure.AutoMapper
{
    public class DbModelsProfile : Profile
    {
        /// <summary>
        /// Specifies the AutoMapper profiles when mapping from Models (Entities) to DbModels
        /// </summary>
        public DbModelsProfile()
        {
            //ShouldMapField = info => info.IsPublic || info.IsPrivate;
            //CreateMap<Domain.Entities.Pickup, Models.PickupDb>()
            //    .ForMember(dest => dest.HasDangerousGoods,
            //        opt => opt.MapFrom(src => src.HasDangerousGoods ? 1 : 2))
            //    .ForMember(dest => dest.HasDangerousGoodsInsurance,
            //        opt => opt.MapFrom(src => src.HasDangerousGoodsInsurance ? 1 : 2))
            //    .ForMember(dest => dest.PalletsNotApplicable,
            //        opt => opt.MapFrom(src => !src.HasPallets))
            //    .ForPath(dest => dest.SuburbId,
            //        opt => opt.MapFrom(src => src.Suburb.Id))
            //    .ForPath(dest => dest.State,
            //        opt => opt.MapFrom(src => src.Suburb.State))
            //    .ForPath(dest => dest.Postcode,
            //        opt => opt.MapFrom(src => src.Suburb.Postcode))
            //    .ForPath(dest => dest.ReceiverState,
            //        opt => opt.MapFrom(src => src.ReceiverSuburb.State))
            //    .ForPath(dest => dest.ReceiverPostcode,
            //        opt => opt.MapFrom(src => src.ReceiverSuburb.Postcode))
            //    .ForPath(dest => dest.SenderName,
            //        opt => opt.MapFrom(src => src.SenderDetails.CustomerAccountName))
            //    .ForPath(dest => dest.SenderCode,
            //        opt => opt.MapFrom(src => src.SenderDetails.Code))
            //    //.ForPath(dest => dest.SenderId,
            //    //    opt => opt.MapFrom(src => src.SenderDetails.Id == 0 ? null : src.SenderDetails.Id.ToString()))
            //    //.ForPath(dest => dest.ChargeAccountCode,
            //    //    opt => opt.MapFrom(src => src.ChargeAccount.Code))
            //    //.ForPath(dest => dest.ChargeAccountForeignKey,
            //   //     opt => opt.MapFrom(src => src.ChargeAccount.Id == 0 ? null : src.ChargeAccount.Id.ToString()))
            //    .ForPath(dest => dest.BookedTime,
            //        opt => opt.MapFrom(src => src.BookedDateTime.Value.ToLocalTime()))
            //    .ForPath(dest => dest.CreatedUser,
            //        opt => opt.MapFrom(src => src.BookedByUser.DisplayName))
            //    .ForPath(dest => dest.ExchangeChepPalletsOut,
            //        opt => opt.MapFrom(src => src.Pallets.ExchangeChepPalletsOut))
            //    .ForPath(dest => dest.TransferChepPalletsToReceiver,
            //        opt => opt.MapFrom(src => src.Pallets.TransferChepPalletsToReceiver))
            //    .ForPath(dest => dest.TransferChepPalletsToBex,
            //        opt => opt.MapFrom(src => src.Pallets.TransferChepPalletsToBex))
            //    .ForPath(dest => dest.ChepPalletTransferDocketNumber,
            //        opt => opt.MapFrom(src => src.Pallets.ChepPalletTransferDocketNumber))
            //    .ForPath(dest => dest.PickupBranch,
            //        opt => opt.MapFrom(src => src.PickupBranch.Name))
            //    .ForPath(dest => dest.ExchangeLoscamPalletsOut,
            //        opt => opt.MapFrom(src => src.Pallets.ExchangeLoscamPalletsOut))
            //    .ForPath(dest => dest.TransferLoscamPalletsToReceiver,
            //        opt => opt.MapFrom(src => src.Pallets.TransferLoscamPalletsToReceiver))
            //    .ForPath(dest => dest.TransferLoscamPalletsToBex,
            //        opt => opt.MapFrom(src => src.Pallets.TransferLoscamPalletsToBex))
            //    .ForPath(dest => dest.LoscamPalletTransferDocketNumber,
            //        opt => opt.MapFrom(src => src.Pallets.LoscamPalletTransferDocketNumber))
            //    .ForPath(dest => dest.PalletType,
            //        opt => opt.MapFrom(src => src.Pallets.Type))
            //    .ForPath(dest => dest.HasChep,
            //        opt => opt.MapFrom(src => src.Pallets.HasChepPallets))
            //    .ForPath(dest => dest.HasLoscam,
            //        opt => opt.MapFrom(src => src.Pallets.HasLoscamPallets))
            //    .ForPath(dest => dest.PickupBranch,
            //        opt => opt.MapFrom(src => src.PickupBranch.Name))
            //    .ForMember(dest => dest.DangerousGoodClasses, opt => opt.MapFrom<PickupDbDangerousGoodClassesResolver>())
            //    .ForMember(dest => dest.DangerousGoodUNNumbers, opt => opt.MapFrom<PickupDbDangerousGoodUnNumberResolver>())
            //    ;

            //CreateMap<PickupItem, PickupItemDbModel>()
            //    .ForMember(dest => dest.DangerousGoods,
            //        opt => opt.MapFrom(src => src.HasDangerousGoods ? 1 : 0))
            //    .ForMember(dest => dest.FoodStuff,
            //        opt => opt.MapFrom(src => src.HasFoodStuff ? 1 : 0))
            //    .ForMember(dest => dest.DangerousGoodClasses,opt => opt.MapFrom(src => src.DangerousGoods.Any() ? string.Join(", ",src.DangerousGoods.Select(x=> x.Class.ToString())) : null)) 
            //    .ForMember(dest => dest.DangerousGoodUNNumbers,opt => opt.MapFrom(src => src.DangerousGoods.Any() ? string.Join(", ",src.DangerousGoods.Select(x=> x.UNNo.ToString())) : null)) 
            //    .ForMember(dest => dest.RegionId, opt => opt.MapFrom(src => src.ReceivingRegion.Id))
            //    ;

            //CreateMap<Entity, DbModel>().ForMember(dest => dest.InternalGuid,
            //    opt => opt.MapFrom(src => src.Id()));

            

        }
    }
}
