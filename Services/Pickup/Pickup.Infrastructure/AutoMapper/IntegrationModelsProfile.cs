﻿using AutoMapper;

namespace PickupSvc.Infrastructure.AutoMapper
{
    public class IntegrationModelsProfile : Profile
    {
        public IntegrationModelsProfile()
        {
            //CreateMap<SuburbIntegrationModel,Suburb>()
            //    .ForCtorParam("id", opt => opt.MapFrom(src => src.Id))
            //    .ForCtorParam("name", opt => opt.MapFrom(src => src.Suburb))
            //    .ForCtorParam("state", opt => opt.MapFrom(src => src.State))
            //    .ForCtorParam("postcode", opt => opt.MapFrom(src => src.Postcode))
            //    .ForCtorParam("preferredBranches", opt => opt.MapFrom(src => src.PreferredBranches))
            //    .ForAllMembers(opt => opt.Ignore());

            //CreateMap<SuburbPreferredBranchIntegrationModel, SuburbPreferredBranch>()
            //    .ForCtorParam("branchId", opt => opt.MapFrom(src => src.BranchId))
            //    .ForCtorParam("isOnforwardEnabled", opt => opt.MapFrom(src => src.IsOnforwardEnabled))
            //    .ForCtorParam("order", opt => opt.MapFrom(src => src.Order))
            //    .ForCtorParam("type", opt => opt.MapFrom(src => src.Type == 'S' ? PreferredBranchType.Receiver : src.Type == 'R' ? PreferredBranchType.Sender : PreferredBranchType.Unknown))
            //    .ForAllMembers(opt => opt.Ignore());

        }
    }
}
