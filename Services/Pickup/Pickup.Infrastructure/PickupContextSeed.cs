﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Bex.Utilities.Helper;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using PickupSvc.TheDomain.Entities;
using PickupSvc.TheDomain.Enumerators;
using PickupSvc.TheDomain.ValueObjects;

namespace PickupSvc.Infrastructure
{
    public class PickupContextSeed
    {

        public static async Task SeedAsync(PickupContext context)
        {
            using (context)
            {

                //context.Database.EnsureDeleted();
                //context.SaveChanges();

                //context.Database.Migrate();
                //context.Database.EnsureCreated();

                if (!context.Users.Any())

                {

                    context.Users.AddRange(GetPreconfiguredUser());

                    await context.SaveChangesAsync();


                }

                if (!context.Regions.Any())

                {

                    context.Regions.AddRange(GetPreconfiguredRegions());

                    await context.SaveChangesAsync();


                }


                if (!context.CustomerAccounts.Any())
                {
                    context.CustomerAccounts.AddRange(GetPreconfiguredCustomers());

                    await context.SaveChangesAsync();


                }

                if (!context.Branches.Any())
                {
                    var region = await context.Regions.FirstOrDefaultAsync();

                    context.Branches.AddRange(GetPreconfiguredBranches(region));

                    await context.SaveChangesAsync();

                }

                if (!context.Suburbs.Any())
                {

                    var suburbMelb = Suburb.Create("Melbourne", "3000", "Vic");

                    var suburbSyd = Suburb.Create("Sydney", "2000", "Nsw");

                    context.Suburbs.AddRange(new List<Suburb>()
                    {
                        suburbMelb,
                        suburbSyd
                        
                    });

                    await context.SaveChangesAsync();

                    var branchMelb = await context.Branches.FirstOrDefaultAsync();
                    var branchSyd =  await context.Branches.FirstOrDefaultAsync(b => b.Id == EntityTypeRange.Branch.ToInt(1));

                    suburbMelb.AddBranchToSuburb(branchMelb.Id,PreferredBranchType.Sender,true,1);
                    suburbSyd.AddBranchToSuburb(branchSyd.Id, PreferredBranchType.Receiver, true, 1);

                    await context.SaveChangesAsync();
                }

                if (!context.Pickups.Any())
                {
                    var senderSuburb = await context.Suburbs.FirstOrDefaultAsync(s=> s.Postcode == "3000");
                    var recieverSuburb = await context.Suburbs.FirstAsync(s => s.Postcode == "2000");
                    var chargeAccount = await  context.CustomerAccounts.FirstOrDefaultAsync(c=> c.CustomerCode=="MAK");
                    var user = await context.Users.FirstOrDefaultAsync();
                    var branch = await context.Branches.FirstOrDefaultAsync(b => b.Id == EntityTypeRange.Branch.ToInt(0));
                    var pickup = Pickup.Seed(0);
                    var sender = await context.CustomerAccounts.FirstOrDefaultAsync(c => c.CustomerCode == "BUN");

                    var region = await context.Regions.FirstOrDefaultAsync();
                    var pickupItem = PickupItem.Create(1, "food", 10, true,region);

                    pickup.SetPayingAccount(chargeAccount);
                    pickup.SetSender(sender);
                    pickup.SetNumber(EntityTypeRange.Pickup.ToInt());
                    pickup.SetAddress("Pickup Address Line1","Pickup Address Line2",senderSuburb);
                    pickup.SetReceiverAddress(recieverSuburb);
                    pickup.SetPayingAccount(chargeAccount);
                    pickup.SetPickupDate((DateTime.Now).AddDays(3));
                    pickup.SetContact("ContactName","1234567","1@1.com");
                    pickup.SetBookerDetails(user,DateTime.Now);
                    pickup.SetPickupBranch(branch);

                    pickup.AddItem(pickupItem);
                    pickup.AddPalletDetails(PickupPallets.Default());

                    context.Pickups.Add(pickup);

                    await context.SaveChangesAsync();

                }

                

            }
        }

        

        private static IEnumerable<Branch> GetPreconfiguredBranches(Region region)
        {
            var result = new List<Branch>
            {
                Branch.Seed(1, "MelbourneBranch", Address.Create(  "VIC", "Melbourne","101 street","","3000"),DateTime.MaxValue,120,region),
                Branch.Seed(2,"SydneyBranch",Address.Create(  "NSW", "Sydney","10 Other street","","2000"),DateTime.MaxValue,120,region),


            };

            return result;
        }

        private static IEnumerable<Region> GetPreconfiguredRegions()
        {
            var result = new List<Region>
            {
                Region.Seed(   "Vic","Melbourne",true),
                Region.Seed("Nsw","Sydney",true),

            };

            return result;
        }


        private static IEnumerable<User> GetPreconfiguredUser()
        {


            var result = new List<User>
            {
                User.Create(UserStatus.Registered, "John","9999999","1@1.com"),
                    User.Create(UserStatus.Registered, "Mary","9999999","2@2.com"),

            };

            return result;
        }
        private static IEnumerable<CustomerAccount> GetPreconfiguredCustomers()
        {


            var result = new List<CustomerAccount>
            {
                new CustomerAccount("BUN","Bunnings" ),
                new CustomerAccount("MAK","Makita Tools" )
            

            };

            return result;
        }
    }
}
