﻿using System;
using System.Collections.Generic;
using MediatR;

namespace PickupSvc.Infrastructure.Models
{
    public abstract class DbModel
    {
        private Guid _internalGuid;
        private List<INotification> _domainEvents;
        public Guid InternalGuid
        {
            get
            {
                if (_internalGuid == Guid.Empty)
                {
                    _internalGuid = Guid.NewGuid();
                }

                return _internalGuid;
            }
            set => _internalGuid = value;
        }

        public List<INotification> DomainEvents
        {
            get => _domainEvents;
            set => _domainEvents = value;
        }
    }
}
