﻿using System;
using System.Text;

namespace PickupSvc.Infrastructure.Extensions
{
    public static class StringHelper
    {
        public static string RemoveSpecialCharacters(this string str)
        {
            StringBuilder sb = new StringBuilder();
            foreach (char c in str)
            {
                if ((c >= '0' && c <= '9') || (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') || c == '.' || c == '_')
                {
                    sb.Append(c);

                }
            }
            return sb.ToString();
        }

        public static byte[] ToByte(this string str)
        {
            var result = Convert.FromBase64String(str);
            return result;
        }

        /// <summary>
        /// Converts int String To Guid String
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string IsGuid(this string str)
        {
            Guid id;

            string result;
            if (!Guid.TryParse(str, out id))
            {
                int idInt;
                if (!int.TryParse(str, out idInt))
                {
                    return str;
                }

                return (Guid.Empty).SeedId(idInt).ToString();
            }

            return str;
        }


        public static bool IsByteArray(this string str)
        {
            byte byteValue;
            return Byte.TryParse(str, out byteValue);
        }

        public static Guid ToGuid(this string str)
        {
            Guid id;

            string result;
            if (!Guid.TryParse(str, out id))
            {
                int idInt;
                if (!int.TryParse(str, out idInt))
                {
                    return default;
                }

                return (Guid.Empty).SeedId(idInt);
            }

            return Guid.Parse(str);
        }

        public static T ToEnum<T>(this string value)
        {
            return (T)Enum.Parse(typeof(T), value, true);
        }
    }
}
