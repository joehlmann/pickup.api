﻿using System;
using System.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using EFSecondLevelCache.Core;
using EFSecondLevelCache.Core.Contracts;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage;
using PickupSvc.Infrastructure.EntityConfigurations;
using PickupSvc.Infrastructure.Providers.Interfaces;
using PickupSvc.TheDomain.Entities;
using PickupSvc.TheDomain.ValueObjects;

//using System.Data.SqlClient;

namespace PickupSvc.Infrastructure
{
    public class PickupContext : DbContext, IUnitOfWork
    {
        private readonly IMediator _mediator;
        private readonly int _pickUpNumberSequenceIncrement=5;
        private readonly int _retryDatabase;
        private IDbContextTransaction _currentTransaction;
        private IEFCacheServiceProvider _cacheServiceProvider;


        public PickupContext(DbContextOptions<PickupContext> options)
            : base(options)
        {
           
        }
        public PickupContext(DbContextOptions<PickupContext> options, IMediator mediator, IEFCacheServiceProvider cacheServiceProvider)
            : base(options)
        {
            _cacheServiceProvider = cacheServiceProvider;
            _retryDatabase = 5;
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
        }

        

        public virtual DbSet<Pickup> Pickups { get; set; }
        public virtual DbSet<PickupItem> PickupItems { get; set; }
        public virtual DbSet<Branch> Branches { get; set; }
        public virtual DbSet<Region> Regions { get; set; }

        public virtual DbSet<CustomerAccount> CustomerAccounts { get; set; }

        public virtual DbSet<DangerousGood> DangerousGoods { get; set; }

        public virtual DbSet<SuburbPreferredBranch> SuburbPreferredBranches { get; set; }

        public virtual DbSet<User> Users { get; set; }

        public virtual DbSet<Suburb> Suburbs { get; set; }


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            
        }



        //public int GetPickupNumberSequence()
        //{
        //    SqlParameter result = new SqlParameter("@result", System.Data.SqlDbType.Int)
        //    {
        //        Direction = System.Data.ParameterDirection.Output
        //    };

        //    Database.ExecuteSqlCommand(
        //        "SELECT @result = (NEXT VALUE FOR PickupNumberSequenceHiLo)", result);

        //    return (int)result.Value;
        //}


        public IDbContextTransaction GetCurrentTransaction() => _currentTransaction;

        public bool HasActiveTransaction => _currentTransaction != null;

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.HasSequence<int>("UserHiLo")
                .StartsAt(1000).IncrementsBy(_pickUpNumberSequenceIncrement);

            builder.HasSequence<int>("CustomerHiLo")
                .StartsAt(2000).IncrementsBy(_pickUpNumberSequenceIncrement);

            builder.HasSequence<int>("BranchHiLo")
                .StartsAt(3000).IncrementsBy(_pickUpNumberSequenceIncrement);

            builder.HasSequence<int>("PickupItemHiLo")
                .StartsAt(4000).IncrementsBy(_pickUpNumberSequenceIncrement);

            builder.HasSequence<int>("PickupHiLo")
                .StartsAt(5005).IncrementsBy(_pickUpNumberSequenceIncrement);

            builder.HasSequence<int>("PreferredBranchesHiLo")
                .StartsAt(6000).IncrementsBy(_pickUpNumberSequenceIncrement);


            builder.HasSequence<int>("PickupTimeInfoHiLo")
                .StartsAt(7000).IncrementsBy(_pickUpNumberSequenceIncrement);


            builder.HasSequence<int>("RegionHiLo")
                .StartsAt(8000).IncrementsBy(_pickUpNumberSequenceIncrement);

            builder.HasSequence<int>("SuburbHiLo")
                .StartsAt(9000).IncrementsBy(_pickUpNumberSequenceIncrement);

            builder.ApplyConfiguration(new ClientRequestEntityTypeConfiguration());
            builder.ApplyConfiguration(new BranchEntityTypeConfiguration());
            builder.ApplyConfiguration(new RegionEntityTypeConfiguration());
            builder.ApplyConfiguration(new PickupEntityTypeConfiguration());
            builder.ApplyConfiguration(new PickupItemEntityTypeConfiguration());
            builder.ApplyConfiguration(new UserEntityTypeConfiguration());
            builder.ApplyConfiguration(new CustomerAccountEntityTypeConfiguration());
            //builder.ApplyConfiguration(new DangerousGoodEntityTypeConfiguration());
            builder.ApplyConfiguration(new SuburbEntityTypeConfiguration());
            //modelBuilder.ApplyConfiguration(new SuburbPreferredBranchEntityTypeConfiguration());


            


        }
        public async Task<bool> SaveEntitiesAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            // TODO: Check the errors collection and throw as we never want to save an invalid Entity to the database


            // Dispatch Domain Events collection. 
            // Choices:
            // A) Right BEFORE committing data (EF SaveChanges) into the DB will make a single transaction including  
            // side effects from the domain event handlers which are using the same DbContext with "InstancePerLifetimeScope" or "scoped" lifetime
            await _mediator.DispatchDomainEventsAsync(this);

            // After executing this line all the changes (from the Command Handler and Domain Event Handlers) 
            // performed through the DbContext will be committed
            var result = await SaveChangesAsync(cancellationToken);

            // B) Right AFTER committing data (EF SaveChanges) into the DB will make multiple transactions. 
            // You will need to handle eventual consistency and compensatory actions in case of failures in any of the Handlers. 
            //await _mediator.DispatchDomainEventsAsync(this);

            return true;
        }


        public override async Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess,  CancellationToken cancellationToken = default(CancellationToken))
        {
            var entities = (from entry in ChangeTracker.Entries()
                where entry.State == EntityState.Modified || entry.State == EntityState.Added
                select entry.Entity);

            

            //cache
            var changedEntityNames = this.GetChangedEntityNames();

            try
            {
                ChangeTracker.AutoDetectChangesEnabled = false;

                var result = await base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
                ChangeTracker.AutoDetectChangesEnabled = true;
                _cacheServiceProvider.InvalidateCacheDependencies(changedEntityNames);

                return Int32.Parse(result.ToString());
            }
            catch (DBConcurrencyException ex)
            {
                throw ex;
            }


            //var validationResults = new List<ValidationResult>();

            //foreach (var entity in entities)
            //{
            //    if (!Validator.TryValidateObject(entity, new ValidationContext(entity), validationResults))
            //    {
            //        throw new ValidationException();
            //    }
            //}
        }



        public async Task<IDbContextTransaction> BeginTransactionAsync()
        {
            if (_currentTransaction != null) return null;

            _currentTransaction = await Database.BeginTransactionAsync(IsolationLevel.ReadCommitted);

            return _currentTransaction;
        }

        public async Task CommitTransactionAsync(IDbContextTransaction transaction)
        {
            if (transaction == null) throw new ArgumentNullException(nameof(transaction));
            if (transaction != _currentTransaction) throw new InvalidOperationException($"Transaction {transaction.TransactionId} is not current");

            try
            {
                await SaveChangesAsync();
                transaction.Commit();
            }
            catch
            {
                RollbackTransaction();
                throw;
            }
            finally
            {
                if (_currentTransaction != null)
                {
                    _currentTransaction.Dispose();
                    _currentTransaction = null;
                }
            }
        }

        public void RollbackTransaction()
        {
            try
            {
                _currentTransaction?.Rollback();
            }
            finally
            {
                if (_currentTransaction != null)
                {
                    _currentTransaction.Dispose();
                    _currentTransaction = null;
                }
            }
        }
    }
}
