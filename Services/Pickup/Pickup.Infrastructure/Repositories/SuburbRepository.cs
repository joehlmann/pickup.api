﻿using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using PickupSvc.Infrastructure.Providers.Interfaces;
using PickupSvc.Infrastructure.Repositories.Interfaces;
using PickupSvc.TheDomain.Entities;

namespace PickupSvc.Infrastructure.Repositories
{
    public class SuburbRepository : ISuburbRepository
    {

        private readonly PickupContext _context;
        private readonly IMapper _mapper;


        public IUnitOfWork UnitOfWork => _context;


        public SuburbRepository(PickupContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<Suburb> FindByIdAsync(int id)
        {
            return await _context.Suburbs.FirstOrDefaultAsync(b => b.Id == id);
        }

        public async Task<Suburb> FindByNameAndPostCodeAsync(string name, string postcode)
        {
            return await _context.Suburbs
                .Include(s=> s.PreferredBranches)
                    .ThenInclude(pb => pb.Branch)
                .FirstOrDefaultAsync(b => b.Name == name && b.Postcode == postcode);
        }
    }
}
