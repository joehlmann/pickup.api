﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using PickupSvc.Infrastructure.Providers.Interfaces;
using PickupSvc.Infrastructure.Repositories.Interfaces;
using PickupSvc.TheDomain.Entities;

namespace PickupSvc.Infrastructure.Repositories
{
    public  class BranchRepository : IBranchRepository
    {
        private readonly PickupContext _context;
        private readonly IMapper _mapper;

        public BranchRepository(PickupContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<bool> FindById(int id)
        {
           return await _context.Branches.AnyAsync(b => b.Id == id);
        }

        public IUnitOfWork UnitOfWork => _context;


       

        public async Task<Branch> FindByIdAsync(int id)
        {
            Branch result;

            try
            {
                result = await _context.Branches.FirstOrDefaultAsync(b => b.Id == id);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                throw;
            }

            

            return result;
        }

        //ToDo Clean up pickup to string
        public async Task<Branch> FindByIdAndPickupNumberAsync(int branchId,int number)
        {

            Branch result;
            try
            {
                result = await _context.Pickups
                    .Where(p => p.Id == number)
                    .Join(_context.Branches,
                        pick => pick.PickupBranch.Id,
                        bran => bran.Id,
                        ((pickup, branch) => branch))
                    .Where(b => b.Id == branchId)
                    .FirstOrDefaultAsync();

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            

            return result ?? Branch.Empty();
        }
    }
}