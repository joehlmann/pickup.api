﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using PickupSvc.Infrastructure.Providers.Interfaces;
using PickupSvc.Infrastructure.Repositories.Interfaces;
using PickupSvc.TheDomain.Entities;

namespace PickupSvc.Infrastructure.Repositories
{
    public class CustomerRepository : ICustomerRepository
    {
        private readonly PickupContext _context;
        private readonly IMapper _mapper;

        public CustomerRepository(PickupContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }


        public IUnitOfWork UnitOfWork => _context;

        public Task<CustomerAccount> FindByIdAsync(int id)
        {
            throw new NotImplementedException();
        }

        public Task<CustomerAccount> FindByCodeAsync(string code)
        {
            throw new NotImplementedException();
        }

        public Task<CustomerAccount> FindByNameAsync(string name)
        {
            throw new NotImplementedException();
        }
    }
}
