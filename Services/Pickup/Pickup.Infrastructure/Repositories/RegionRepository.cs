﻿using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using PickupSvc.Infrastructure.Providers.Interfaces;
using PickupSvc.Infrastructure.Repositories.Interfaces;
using PickupSvc.TheDomain.Entities;

namespace PickupSvc.Infrastructure.Repositories
{
    public class RegionRepository : IRegionRepository
    {
        public IUnitOfWork UnitOfWork => _context;

        private readonly PickupContext _context;

        public RegionRepository(PickupContext context)
        {
            _context = context;
        }

        public Task<bool> FindByIdExistAsync(int id)
        {
            throw new System.NotImplementedException();
        }

        public async Task<Region> FindByIdAsync(int id)
        {
            return await _context.Regions.FirstOrDefaultAsync(b => b.Id == id);
        }
    }
}