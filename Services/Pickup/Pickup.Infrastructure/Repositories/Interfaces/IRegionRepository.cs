﻿using System.Threading.Tasks;
using PickupSvc.TheDomain.Entities;

namespace PickupSvc.Infrastructure.Repositories.Interfaces
{
    public interface IRegionRepository : IRepository<Region>
    {
        Task<bool> FindByIdExistAsync(int userId);

        Task<Region> FindByIdAsync(int userId);
    }
}