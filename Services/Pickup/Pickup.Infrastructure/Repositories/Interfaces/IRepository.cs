﻿using PickupSvc.Infrastructure.Providers.Interfaces;

namespace PickupSvc.Infrastructure.Repositories.Interfaces
{
    public interface IRepository<T>
    {
        IUnitOfWork UnitOfWork { get; }
    }
}