﻿namespace PickupSvc.Infrastructure.Repositories.Interfaces
{
    public interface IApplicationConfig
    {
        string PickupNumberGenerationBatchSize { get; }
        string MaximumNumberOfPickupRenumberAttempts { get;  }
    }
}
