﻿using System.Threading.Tasks;
using PickupSvc.TheDomain.Entities;

namespace PickupSvc.Infrastructure.Repositories.Interfaces
{
    public interface ISuburbRepository : IRepository<Suburb>
    {
        Task<Suburb> FindByIdAsync(int id);
        Task<Suburb> FindByNameAndPostCodeAsync(string name, string postcode);
    }
}
