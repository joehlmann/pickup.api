﻿using System.Threading;
using System.Threading.Tasks;
using PickupSvc.TheDomain.ValueObjects;

namespace PickupSvc.Infrastructure.Repositories.Interfaces
{
    public interface IDangerousGoodRepository
    {
        Task<DangerousGood> GetByUNNumberAsync(int UNNumber, CancellationToken cancellationToken = default(CancellationToken));
    }
}