﻿using System.Threading.Tasks;
using PickupSvc.TheDomain.Entities;

namespace PickupSvc.Infrastructure.Repositories.Interfaces
{
    public interface IUserRepository : IRepository<User>
    {
        Task<bool> FindByIdExistAsync(int userId);

        Task<User> FindByIdAsync(int userId);
    }
}