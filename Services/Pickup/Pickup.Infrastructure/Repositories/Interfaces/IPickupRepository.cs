﻿using System.Threading;
using System.Threading.Tasks;
using PickupSvc.TheDomain.Entities;

namespace PickupSvc.Infrastructure.Repositories.Interfaces
{
    public interface IPickupRepository : IRepository<Pickup>
    {

        Task<Pickup> FindByIdAsync(int id);

        Pickup Add(Pickup pickup);
        void Update(Pickup pickup);
        
        Task<Pickup> GetAsync(int pickupId, CancellationToken cancellationToken = default(CancellationToken));
        Task<Pickup> GetByNumberAsync(int pickupNumber, CancellationToken cancellationToken = default(CancellationToken));

        Task<Pickup> GetByItemIdAsync(int pickupItemId,
            CancellationToken cancellationToken = default(CancellationToken));
        //PickupRoot GetCacheOnly(Guid pickupGuid);
        //Task<PickupNumberRangeModel> GetAvailablePickupNumberRangeAsync(int rangeSize);

        

        //Task<Branch> GetBranchAsync(int branchId, CancellationToken cancellationToken = default(CancellationToken));
        //Task<Region> GetRegionAsync(int regionId, CancellationToken cancellationToken = default(CancellationToken));
    }
}