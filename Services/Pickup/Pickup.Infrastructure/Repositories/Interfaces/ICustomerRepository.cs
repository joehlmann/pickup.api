﻿using System.Threading.Tasks;
using PickupSvc.TheDomain.Entities;

namespace PickupSvc.Infrastructure.Repositories.Interfaces
{
    public interface ICustomerRepository : IRepository<CustomerAccount>
    {
        Task<CustomerAccount> FindByIdAsync(int id);
        Task<CustomerAccount> FindByCodeAsync(string code);
        Task<CustomerAccount> FindByNameAsync(string name);
    }
}