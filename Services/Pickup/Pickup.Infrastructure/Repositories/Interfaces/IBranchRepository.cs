﻿using System.Threading.Tasks;
using PickupSvc.TheDomain.Entities;

namespace PickupSvc.Infrastructure.Repositories.Interfaces
{
   public  interface IBranchRepository : IRepository<Branch>
    {
        Task<Branch> FindByIdAsync(int id);

        Task<Branch> FindByIdAndPickupNumberAsync(int id, int number);

    }
}
