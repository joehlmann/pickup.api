﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using PickupSvc.Infrastructure.Providers.Interfaces;
using PickupSvc.Infrastructure.Repositories.Interfaces;
using PickupSvc.TheDomain.Entities;

//using System.Data.SqlClient;

namespace PickupSvc.Infrastructure.Repositories
{
    public class PickupRepository : IPickupRepository
    {
        private readonly PickupContext _context;
        private readonly IMapper _mapper;
        

        public PickupRepository(IMapper mapper, PickupContext context)
        {
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }
        public IUnitOfWork UnitOfWork => _context;

        public async Task<Pickup> FindByIdAsync(int id)
        {
            return await _context.Pickups.FirstOrDefaultAsync(b => b.Id == id);
        }

        public Pickup Add(Pickup pickup)
        {
          
      
            
            // Add the object to EF context to allow change tracking
            _context.Pickups.Add(pickup);
            
            // Return the passed in object
            return pickup;
            
        }

        public void Update(Pickup pickup)
        {
            _context.Entry(pickup).State = EntityState.Modified;

            

            //var existingPickup = _context.Pickups.SingleOrDefault(cPickup => cPickup.Id == pickup.Id);


            //if (existingPickup == null)
            //{
            //    throw new ArgumentNullException(nameof(existingPickup));
            //}

            //// Apply the changes to the tracked entity
            //_context.Entry(existingPickup).CurrentValues.SetValues(pickup);

            //// Delete any items that have been removed
            //foreach (var pickupItemDb in existingPickup.Items)
            //{
            //    if (pickup.Items.All(x => x.Id != pickupItemDb.Id))
            //    {
            //        existingPickup.RemoveItem(pickupItemDb);
            //    }
            //}

            //// Update or Add the new items
            //var itemDetails = from newItem in pickup.Items
            //    join dbItem in existingPickup.Items
            //        on newItem.Id equals dbItem.Id into grp
            //    from dbItem in grp.DefaultIfEmpty()
            //    select new {newItem, dbItem};

            //foreach (var itemDetail in itemDetails)
            //{

            //    if (itemDetail.dbItem != null)
            //    {
            //        // Update the existing item
            //        _context.Entry(itemDetail.dbItem).CurrentValues.SetValues(itemDetail.newItem);
            //    }
            //    else
            //    {
            //        // Add the new item
            //        existingPickup.AddItem(itemDetail.newItem);
            //    }
            //}

            //// Copy the domain events
            ////existingPickup.DomainEvents = pickup.DomainEvents;
        }

        public void UpdateCache(Pickup pickup)
        {
            // Retrieve the EF Context object (this should already be in memory and not require a DB round trip)
            var existingValues = _context.Pickups.Local.SingleOrDefault(cPickup => cPickup.Id == pickup.Id);
            
            
            if (existingValues != null)
            {
                // Apply the changes to the DB Model
                _context.Entry(existingValues).CurrentValues.SetValues(pickup);
            }
            
        }

        public async Task<Pickup> GetAsync(int pickupId, CancellationToken cancellationToken = default(CancellationToken))
        {
            // Find the pickup by Id
            var pickup = await _context.Pickups
                .Where(p => p.Id == pickupId)
                .Include(pu => pu.Items)
                .FirstOrDefaultAsync(cancellationToken);

            // Convert the object to the domain entity
            return pickup;
        }

        public async Task<Pickup> GetByItemIdAsync(int pickupItemId, CancellationToken cancellationToken = default(CancellationToken))
        {
            // Find the pickup by Id
            var pickup = await _context.PickupItems
                .Where(p => p.Id == pickupItemId)
                .Include(p => p.Pickup)
                .FirstOrDefaultAsync(cancellationToken);
            if (pickup != null)
            {
                // Convert the object to the domain entity
                return pickup.Pickup;
            }

            return null;
        }

       
       

        public async Task<Pickup> GetByNumberAsync(int pickupNumber, CancellationToken cancellationToken = default(CancellationToken))
        {
            // Find the pickup by Number
            var pickup = await _context.Pickups
                .Where(p => p.PickupNumber == pickupNumber)
                .Include(pu => pu.Items)
                .FirstOrDefaultAsync(cancellationToken);

            
            return pickup;
        }




    }
}
