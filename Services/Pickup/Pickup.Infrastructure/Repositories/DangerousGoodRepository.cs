﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using PickupSvc.Infrastructure.Repositories.Interfaces;
using PickupSvc.TheDomain.ValueObjects;

namespace PickupSvc.Infrastructure.Repositories
{



    public class DangerousGoodRepository : IDangerousGoodRepository
    {
        private readonly PickupContext _context;
        private readonly IMapper _mapper;

        public DangerousGoodRepository(PickupContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }



        public async Task<DangerousGood> GetByUNNumberAsync(int UNNumber, CancellationToken cancellationToken = default(CancellationToken))
        {
            return await _context.DangerousGoods
                .DefaultIfEmpty(DangerousGood.Empty())
                .FirstOrDefaultAsync(b => b.UNNo == UNNumber).ConfigureAwait(false);
        }
    }
}
