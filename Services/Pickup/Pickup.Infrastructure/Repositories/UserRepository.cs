﻿using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using PickupSvc.Infrastructure.Providers.Interfaces;
using PickupSvc.Infrastructure.Repositories.Interfaces;
using PickupSvc.TheDomain.Entities;

namespace PickupSvc.Infrastructure.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly PickupContext _context;
        private readonly IMapper _mapper;

        public UserRepository(PickupContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }



        public IUnitOfWork UnitOfWork => _context;

        public async Task<bool> FindByIdExistAsync(int id)
        {
            return await _context.Users.AnyAsync(b => b.Id == id);
        }

        public async Task<User> FindByIdAsync(int id)
        {
            return await _context.Users.FirstOrDefaultAsync(b => b.Id == id);
        }

       
    }
}
