﻿using System.Threading;
using System.Threading.Tasks;
using PickupSvc.Infrastructure.Repositories.Interfaces;
using PickupSvc.TheDomain.ValueObjects;

namespace PickupSvc.Infrastructure.Repositories.InMemory
{
    public class InMemoryDangerousGoodRepository : IDangerousGoodRepository
    {
        public async Task<DangerousGood> GetByUNNumberAsync(int UNNumber, CancellationToken cancellationToken = default(CancellationToken))
        {
            if (UNNumber == 1234)
            {
                return new DangerousGood(UNNumber,2.3);
            }
            return new DangerousGood(UNNumber,0);
        }
    }
}
