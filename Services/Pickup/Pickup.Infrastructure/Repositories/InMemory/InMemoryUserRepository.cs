﻿using System.Collections.Generic;
using System.Threading.Tasks;
using PickupSvc.Infrastructure.Providers.Interfaces;
using PickupSvc.Infrastructure.Repositories.Interfaces;
using PickupSvc.TheDomain.Entities;

namespace PickupSvc.Infrastructure.Repositories.InMemory
{
    public class InMemoryUserRepository : IUserRepository
    {
        private List<User> _users;

        public InMemoryUserRepository()
        {
            _users = new List<User>();
            //{new User(726,"Chris Wright")};
        }

        public Task<bool> FindByIdExist(int userId)
        {
            throw new System.NotImplementedException();
        }

        public async Task<User> FindById(int userId)
        {
            return  await Task.FromResult(User.EmptyUser());
        }

        public IUnitOfWork UnitOfWork { get; }
        public Task<bool> FindByIdExistAsync(int userId)
        {
            throw new System.NotImplementedException();
        }

        public Task<User> FindByIdAsync(int userId)
        {
            throw new System.NotImplementedException();
        }
    }
}
