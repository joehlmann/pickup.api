﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using PickupSvc.Infrastructure.Providers.Interfaces;
using PickupSvc.Infrastructure.Repositories.Interfaces;
using PickupSvc.TheDomain.Entities;

namespace PickupSvc.Infrastructure.Repositories.InMemory
{
    public class InMemoryCustomerRepository : ICustomerRepository
    {
        private List<CustomerAccount> _customers;

        public InMemoryCustomerRepository()
        {
            _customers = new List<CustomerAccount> {
                new CustomerAccount("002B","Norske Skog Albury"),
                new CustomerAccount ("002A","Test2")
            };
        }
        public async Task<CustomerAccount> FindByIdAsync(int id)
        {
            return _customers.Find(x => x.Id == id);
        }

        public async Task<CustomerAccount> FindByCodeAsync(string code)
        {
            return _customers.Find(x => x.CustomerCode.Equals(code,StringComparison.OrdinalIgnoreCase));
        }

        public async Task<CustomerAccount> FindByNameAsync(string name)
        {
            return _customers.Find(x => x.CustomerAccountName.Equals(name,StringComparison.OrdinalIgnoreCase));
        }

        public IUnitOfWork UnitOfWork { get; }
    }
}
