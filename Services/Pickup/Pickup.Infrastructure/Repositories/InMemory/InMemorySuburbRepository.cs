﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using PickupSvc.Infrastructure.Providers.Interfaces;
using PickupSvc.Infrastructure.Repositories.Interfaces;
using PickupSvc.TheDomain.Entities;

namespace PickupSvc.Infrastructure.Repositories.InMemory
{
    public class InMemorySuburbRepository : ISuburbRepository
    {
        private List<Suburb> _suburbs;

        public InMemorySuburbRepository()
        {
            //var pre = new List<SuburbPreferredBranch>();
            //pre.Add(new SuburbPreferredBranch(1, 1,true,1,PreferredBranchType.Sender));
            //_suburbs = new List<Suburb>
            //{
            //    new Suburb(2,"Albury","NSW","2640",pre),
            //    new Suburb( 14352,"Wodonga","VIC","3690",pre),
            //    new Suburb(3,"Melbourne","3000","VIC",pre)
            //};
        }
        public async Task<Suburb> FindByIdAsync(int id)
        {
            return _suburbs.Find(x => x.Id == id);
        }

        public async Task<Suburb> FindByNameAndPostCodeAsync(string name, string postcode)
        {
            return _suburbs.Find(x => x.Postcode.Equals(postcode,StringComparison.OrdinalIgnoreCase) && x.Name.Equals(name,StringComparison.OrdinalIgnoreCase));
        }

        public IUnitOfWork UnitOfWork { get; }
    }
}
