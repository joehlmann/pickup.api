﻿using System;

namespace PickupSvc.TheDomain.Exceptions
{
    public class PickupDomainException : Exception
    {
        public PickupDomainException()
        { }

        public PickupDomainException(string message)
            : base(message)
        { }

        public PickupDomainException(string message, Exception innerException)
            : base(message, innerException)
        { }
    }
}
