﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace PickupSvc.TheDomain.SeedWork
{
    public class Guard
    {
        public static void ForLessEqualZero(int value, string parameterName)
        {
            if (value <= 0)
            {
                throw new ArgumentOutOfRangeException(parameterName);
            }
        }

        public static void ForLessEqualZero(long value, string parameterName)
        {
            if (value <= 0)
            {
                throw new ArgumentOutOfRangeException(parameterName);
            }
        }

        public static void ForLessEqualZero(decimal value, string parameterName)
        {
            if (value <= 0)
            {
                throw new ArgumentOutOfRangeException(parameterName);
            }
        }

        public static void ForLessEqualZero(int value, string parameterName, string message)
        {
            if (value <= 0)
            {
                throw new ArgumentOutOfRangeException(parameterName, value, message);
            }
        }

        public static void ForLessEqualZero(decimal value, string parameterName, string message)
        {
            if (value <= 0)
            {
                throw new ArgumentOutOfRangeException(parameterName, value, message);
            }
        }

        public static void ForLessEqualZero(long value, string parameterName, string message)
        {
            if (value <= 0)
            {
                throw new ArgumentOutOfRangeException(parameterName, value, message);
            }
        }

        public static void ForPrecedesDate(DateTime value, DateTime dateToPrecede, string parameterName)
        {
            if (value >= dateToPrecede)
            {
                throw new ArgumentOutOfRangeException(parameterName);
            }
        }

        public static void ForDate(DateTime value, string msg, string parameterName)
        {
            if (value <= DateTime.MinValue || value >= DateTime.MaxValue)
            {
                throw new ArgumentOutOfRangeException(parameterName, value, msg);
            }
        }

        public static void ForNullOrEmpty(string value, string parameterName)
        {
            if (string.IsNullOrEmpty(value))
            {
                throw new ArgumentOutOfRangeException(parameterName);
            }
        }

        public static void ForNullOrEmpty(string value, string parameterName, string message)
        {
            if (string.IsNullOrEmpty(value))
            {
                throw new ArgumentOutOfRangeException(parameterName, value, message);
            }
        }

        public static void ForEmptyList<TList>(TList list, string parameterName)
        {
            var value = list as IEnumerable<TList>;
            if (!value.Any())
            {
                throw new ArgumentOutOfRangeException(parameterName);
            }
        }

        public static void ForGuidValidateing(string value, string parameterName)
        {
            Guid outGuid;
            bool isValid = Guid.TryParse(value, out outGuid);
            if (!isValid)
            {
                throw new ArgumentOutOfRangeException(parameterName);
            }
        }
    }
}
