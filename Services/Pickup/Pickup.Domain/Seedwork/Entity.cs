﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using MediatR;
using PickupSvc.TheDomain.Infrastructure;

namespace PickupSvc.TheDomain.SeedWork
{
    

    public abstract class Entity<TId> : IEquatable<Entity<TId>>
    {
        
        public TId Id { get; set; }

        //for db and user track
        [Timestamp]
        public byte[] RowVersion { get; set; }

        public string CurrentUserId { get; set; }


        public DateTime CreateDate { get; set; }

        public DateTime? LastModified { get; set; }


        protected Entity(TId id, string currentUserId)
        {
            CurrentUserId = currentUserId;
            Id = id;
        }

        protected Entity()
        {
        }

        private List<INotification> _domainEvents;
        public IReadOnlyCollection<INotification> DomainEvents => _domainEvents?.AsReadOnly();


        public void AddDomainEvent(INotification eventItem)
        {
            _domainEvents = _domainEvents ?? new List<INotification>();
            _domainEvents.Add(eventItem);
        }

        public void RemoveDomainEvent(INotification eventItem)
        {
            _domainEvents?.Remove(eventItem);
        }

        public void ClearDomainEvents()
        {
            _domainEvents?.Clear();
        }

        private List<Error> _errors;
        public IReadOnlyCollection<Error> Errors => _errors?.AsReadOnly();
        public bool HasErrors => 0 != (_errors?.Count ?? 0);

        public void AddError(Error error)
        {
            _errors = _errors ?? new List<Error>();
            _errors.Add(error);
        }

        public void RemoveError(Error error)
        {
            _errors?.Remove(error);
        }
        public void ClearErrors()
        {
            _errors?.Clear();
        }

        // Comparable 
        public override bool Equals(object otherObject)
        {
            var entity = otherObject as Entity<TId>;
            if (entity != null)
            {
                return Equals(entity);
            }
            return base.Equals(otherObject);
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }

        public bool Equals(Entity<TId> other)
        {
            if (other == null)
            {
                return false;
            }
            return Id.Equals(other.Id);
        }
    }
}
