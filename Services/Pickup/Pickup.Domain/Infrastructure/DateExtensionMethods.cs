﻿using System;

namespace PickupSvc.TheDomain.Infrastructure
{
    public static class DateExtensionMethods
    {
        public static DateTime? Parse( string text){
            return DateTime.TryParse(text, out var date) ? date : (DateTime?) null;
        }
        
    }
}
