﻿namespace PickupSvc.TheDomain.Infrastructure
{
    public static class ValidationMessages
    {
        
        public static readonly Error PickupSenderNameMissing;
        public static readonly Error PickupContactNameMissing;
        public static readonly Error PickupContactPhoneMissing;

        public static readonly Error PickupAddressMissing;
        public static readonly Error PickupSuburbMissing;
        public static readonly Error PickupPostcodeMissing;
        public static readonly Error PickupInvalidSuburbDetails;
        public static readonly Error PickupMissingSuburbDetails;

        public static readonly Error PickupCustomerProvidedDuplicatePickupNumber;


        public static readonly Error PickupReadyTimeIsMissing;
        public static readonly Error PickupReadyTimeIsInvalid;
        public static readonly Error PickupCloseTimeIsMissing;
        public static readonly Error PickupWindowNotLongEnough;
        public static readonly Error PickupDateIsToFarIntoTheFuture;
        public static readonly Error PickupDateIsToFarIntoThePast;
        public static readonly Error PickupDateIsNotDate;
        public static readonly Error PickupDateIsMissing;


        public static readonly Error PickupBookingUserIsMissing;
        public static readonly Error PickupBookingUserIsInvalid;
        public static readonly Error PickupBookingDateIsMissing;
        public static readonly Error PickupBranchIsMissing;
        public static readonly Error PickupBranchCutoffTimeIsMissing;
        public static readonly Error PickupBranchCutoffTimeIsInvalid;
        public static readonly Error PickupUnknownPickup;
        public static readonly Error PickupBranchIsUnknown;

        public static readonly Error BranchPreferredSendingBranchIsMissing;
        public static readonly Error BranchPreferredSendingBranchIsUnknown;
        public static readonly Error BranchPreferredReceivingBranchIsUnknown;
        public static readonly Error BranchPickupBranchSuburbIsUnknown;

        public static readonly Error PickupItemDGUnknown;
        public static readonly Error PickupItemUnknown;
        public static readonly Error PickupItemDescriptionMissing;
        public static readonly Error PickupItemWeightMissing;
        public static readonly Error PickupItemQuantityMissing;


        public static readonly Error RegionReceivingRegionIsUnknown;

        static ValidationMessages()
        {
            PickupReadyTimeIsMissing = new Error{Code = "1.1.1", Message = "Ready time must be provided"};
            PickupCloseTimeIsMissing = new Error{Code = "1.1.2", Message = "Close time must be provided"};
            PickupWindowNotLongEnough = new Error{Code = "1.1.3", Message = "A pickup windows of greater than {MinimumPickupWindowDuration} must be provided"};
            PickupSenderNameMissing = new Error{Code ="1.1.4", Message = "A sender name must be provided"};
            PickupContactNameMissing = new Error{Code ="1.1.5", Message = "A contact name must be provided"};
            PickupContactPhoneMissing = new Error{Code ="1.1.6", Message = "A contact phone number must be provided"};
            PickupAddressMissing = new Error{Code ="1.1.7", Message = "Address line details must be provided"};
            PickupSuburbMissing = new Error{Code ="1.1.8", Message = "Suburb details must be provided"};
            PickupPostcodeMissing = new Error{Code ="1.1.9", Message = "A postcode must be provided"};
            PickupInvalidSuburbDetails = new Error{Code ="1.1.10", Message = "The provided suburb details are invalid"};
            PickupMissingSuburbDetails = new Error{Code ="1.1.11", Message = "The provided suburb details are missing"};
            PickupCustomerProvidedDuplicatePickupNumber = new Error{Code = "1.1.12",Message = "The pickup number provided is already in use"};
            PickupDateIsToFarIntoTheFuture = new Error{Code = "1.1.13",Message = "You can not forward date a Pickup by more than {MaximumDaysPickupDateIsAllowedIntoTheFuture} days"};
            PickupDateIsToFarIntoThePast = new Error{Code = "1.1.14",Message = "You can not date a Pickup more than {MaximumDaysPickupDateIsAllowedIntoThePast} days into the past"};
            PickupDateIsNotDate = new Error { Code = "1.2.15", Message = "A Pickup Date must be provided" };
            PickupDateIsMissing = new Error { Code = "1.2.16", Message = "A Pickup Date is missing " };

            PickupBookingDateIsMissing = new Error{Code = "1.1.15", Message = "A booking date/time must be provided"};
            
            PickupBookingUserIsInvalid = new Error{Code = "1.1.16", Message = "The provided booked by user is invalid"};
            PickupBookingUserIsMissing = new Error{Code="1.1.17", Message = "A booking user must be provided"};
            PickupReadyTimeIsInvalid = new Error{Code = "1.1.18", Message = "The Ready time must be a valid time"};
            PickupBranchIsMissing = new Error{Code = "1.1.19", Message = "A pickup branch must be provided"};
            PickupBranchCutoffTimeIsMissing = new Error{Code = "1.1.20", Message = "The pickup branch must have a cut off time"};
            PickupBranchCutoffTimeIsInvalid = new Error{Code = "1.1.21", Message = "The pickup branch cut off time must be a valid time"};
            PickupUnknownPickup = new Error{Code = "1.1.22", Message = "The pickup is unknown"};
            PickupBranchIsUnknown = new Error{Code = "1.1.23", Message = "The pickup branch is unknown"};
            
            BranchPreferredSendingBranchIsMissing = new Error{Code = "1.2.1", Message = "The preferred sending branch has not been set"};
            BranchPreferredSendingBranchIsUnknown = new Error{Code = "1.2.2", Message = "The preferred sending branch is unknown"};
            BranchPickupBranchSuburbIsUnknown = new Error{Code = "1.2.3", Message = "The pickup branch suburb is unknown"};
            BranchPreferredReceivingBranchIsUnknown = new Error{Code = "1.2.4", Message = "The preferred receiving branch is unknown"};

            PickupItemDGUnknown = new Error {Code = "1.3.1", Message = "The provided dangerous good details are invalid"};
            PickupItemUnknown = new Error {Code = "1.3.2", Message = "The provided item is unknown/invalid :("};
            PickupItemDescriptionMissing = new Error { Code = "1.3.3", Message = "PickupItemDescriptionMissing" };
            PickupItemWeightMissing = new Error { Code = "1.3.4", Message = "PickupItemWeightMissing" };
            PickupItemQuantityMissing = new Error { Code = "1.3.5", Message = "PickupItemQuantityMissing" };
            

            RegionReceivingRegionIsUnknown = new Error{Code = "1.4.1", Message = "The receiving region is unknown"};
        }

        
    }
}
