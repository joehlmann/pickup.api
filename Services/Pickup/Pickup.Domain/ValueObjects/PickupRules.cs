﻿using System.Collections.Generic;
using PickupSvc.TheDomain.SeedWork;

namespace PickupSvc.TheDomain.ValueObjects
{
    public class PickupRules : ValueObject
    {

        public int MinimumPickupWindowDuration { get; private set; } 

        public int MaximumDaysPickupDateIsAllowedIntoTheFuture { get; private set; } 


        public int MaximumDaysPickupDateIsAllowedIntoThePast { get; private set ;}


        protected PickupRules() { }

        

        private PickupRules(int minimumPickupWindowDuration, int maximumDaysPickupDateIsAllowedIntoTheFuture, int maximumDaysPickupDateIsAllowedIntoThePast)
        {
            MinimumPickupWindowDuration = minimumPickupWindowDuration;
            MaximumDaysPickupDateIsAllowedIntoTheFuture = maximumDaysPickupDateIsAllowedIntoTheFuture;
            MaximumDaysPickupDateIsAllowedIntoThePast = maximumDaysPickupDateIsAllowedIntoThePast;
        }



        // Constructors & Factories


        public static PickupRules Create(int minimumPickupWindowDuration,
            int maximumDaysPickupDateIsAllowedIntoTheFuture, int maximumDaysPickupDateIsAllowedIntoThePast)
        {
            return new PickupRules(minimumPickupWindowDuration,maximumDaysPickupDateIsAllowedIntoTheFuture,maximumDaysPickupDateIsAllowedIntoThePast);
        }

        public static PickupRules Default()
        {
            return new PickupRules(2,10,10);
        }


        protected override IEnumerable<object> GetEqualityComponents()
        {
            yield return MinimumPickupWindowDuration;
            yield return MaximumDaysPickupDateIsAllowedIntoTheFuture;
            yield return MaximumDaysPickupDateIsAllowedIntoThePast;
        }
    }
}
