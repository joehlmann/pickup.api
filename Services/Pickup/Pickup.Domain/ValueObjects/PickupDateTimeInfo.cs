﻿using System;
using System.Collections.Generic;
using PickupSvc.TheDomain.SeedWork;

namespace PickupSvc.TheDomain.ValueObjects
{
    public class PickupDateTimeInfo : ValueObject
    {

       


        public DateTime PickupDate { get; private set; }
        public DateTime? ReadyTime { get; private set; }
        public DateTime? CloseTime { get; private set; }

        public DateTime BookedDateTime { get; private set; }

        public bool BookedLate { get; private set; }



      


    // constructors & factories


    private PickupDateTimeInfo()
        {
            PickupDate = DateTime.Today;
        }

        public PickupDateTimeInfo(DateTime pickupDate)
        {
            PickupDate = pickupDate;
            
        }

        public PickupDateTimeInfo(DateTime pickupDate,DateTime bookDateTime) 
            :this(pickupDate)
        {
            BookedDateTime = bookDateTime;

        }

        public PickupDateTimeInfo(DateTime pickupDate, DateTime? readyTime, DateTime? closeTime) 
            :this (pickupDate)
        {
            
            ReadyTime = readyTime;
            CloseTime = closeTime;
            
        }

        public PickupDateTimeInfo(DateTime pickupDate, DateTime? readyTime, DateTime? closeTime, DateTime bookedDateTime)
            : this(pickupDate,readyTime,closeTime)
        {

            
            BookedDateTime = bookedDateTime;

        }
        public PickupDateTimeInfo(DateTime pickupDate, DateTime? readyTime, DateTime? closeTime, DateTime bookedDateTime, bool bookedLate)
            : this(pickupDate, readyTime, closeTime,bookedDateTime)
        {
            BookedLate = bookedLate;
        }

        public static PickupDateTimeInfo Empty()
        {
            return new PickupDateTimeInfo();
        }



      


        protected override IEnumerable<object> GetEqualityComponents()
        {
            yield return PickupDate;
            
            yield return ReadyTime;
            yield return CloseTime;
            yield return BookedDateTime;
            
        }
    }

    public static class PickupDatetimeInfoExtension
    {
        public static PickupDateTimeInfo SetReadyCloseTime(this PickupDateTimeInfo dateTimeInfo, DateTime readyTime,
            DateTime closeTime)
        {
            return new PickupDateTimeInfo(dateTimeInfo.PickupDate, readyTime, closeTime);
        }


        public static PickupDateTimeInfo SetBookedTime(this PickupDateTimeInfo dateTimeInfo, DateTime bookDateTime)
        {
            return new PickupDateTimeInfo(dateTimeInfo.PickupDate, dateTimeInfo.ReadyTime, dateTimeInfo.CloseTime, bookDateTime);
        }

        public static PickupDateTimeInfo SetPickupDate(this PickupDateTimeInfo dateTimeInfo, DateTime pickupDateTime)
        {
            return new PickupDateTimeInfo(dateTimeInfo.PickupDate, dateTimeInfo.ReadyTime, dateTimeInfo.CloseTime, dateTimeInfo.BookedDateTime);
        }

        public static PickupDateTimeInfo SetBookedLate(this PickupDateTimeInfo dateTimeInfo,bool bookedLate )
        {
            return new PickupDateTimeInfo(dateTimeInfo.PickupDate, dateTimeInfo.ReadyTime, dateTimeInfo.CloseTime, dateTimeInfo.BookedDateTime,bookedLate);
        }
    }

}
