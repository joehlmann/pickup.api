﻿using System.Collections.Generic;
using PickupSvc.TheDomain.SeedWork;

namespace PickupSvc.TheDomain.ValueObjects
{
    public class DangerousGood : ValueObject
    {

        public int UNNo { get; private set; }
        public double Class { get; private set; }

        public DangerousGood(int unNo, double dgClass=0)
        {
        
            UNNo = unNo;
            Class = dgClass;
        }

        public DangerousGood()
        {
        
            UNNo = 0;
            Class = 0;
        }


        public static DangerousGood Empty()
        {
            return new DangerousGood(0);
        }


        protected override IEnumerable<object> GetEqualityComponents()
        {
            yield return UNNo;
            yield return Class;
        }
    }
}
