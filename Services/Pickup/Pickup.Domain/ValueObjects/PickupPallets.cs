﻿using System.Collections.Generic;
using PickupSvc.TheDomain.Enumerators;
using PickupSvc.TheDomain.SeedWork;

namespace PickupSvc.TheDomain.ValueObjects
{
    public class PickupPallets : ValueObject
    {
        public bool HasPallets { get; private set; }
        public PalletType PalletType { get; private set; }
        public string ChepPalletTransferDocketNumber { get;private set; }
        
        public int ExchangeChepPalletsOut { get; private set; }
        
        public int TransferChepPalletsToBex { get; private set; }
        public int TransferChepPalletsToReceiver { get; private set; }

        public string LoscamPalletTransferDocketNumber { get;private set; }
        
        public int ExchangeLoscamPalletsOut { get; private set; }
        
        public int TransferLoscamPalletsToBex { get; private set; }
        public int TransferLoscamPalletsToReceiver { get; private set; }

        private PickupPallets()
        {
            
        }
        public PickupPallets(
            int exchangeChepPalletsOut, int transferChepPalletsToBex, int transferChepPalletsToReceiver, string chepPalletTransferDocketNumber,
            int exchangeLoscamPalletsOut, int transferLoscamPalletsToBex, int transferLoscamPalletsToReceiver, string loscamPalletTransferDocketNumber)
        {
           // Chep Details
            ExchangeChepPalletsOut = exchangeChepPalletsOut;
            TransferChepPalletsToBex = transferChepPalletsToBex;
            TransferChepPalletsToReceiver = transferChepPalletsToReceiver;
            ChepPalletTransferDocketNumber = chepPalletTransferDocketNumber;

            // Loscam details
            ExchangeLoscamPalletsOut = exchangeLoscamPalletsOut;
            TransferLoscamPalletsToBex = transferLoscamPalletsToBex;
            TransferLoscamPalletsToReceiver = transferLoscamPalletsToReceiver;
            LoscamPalletTransferDocketNumber = loscamPalletTransferDocketNumber;

            // Check if any pallet information has been provided
            HasPallets = HasLoscamPallets || HasChepPallets;

            // Set the pallet type
            SetPalletType();

        }


        public static PickupPallets Default()
        {
            return new PickupPallets(0,0,0,"",0,0,0,"");
        }


        /// <summary>
        /// Determine if the pallets object contains any Loscam pallets
        /// </summary>
        /// <returns></returns>
        public bool HasLoscamPallets => ExchangeLoscamPalletsOut > 0 ||
                   TransferLoscamPalletsToBex > 0 ||
                   TransferLoscamPalletsToReceiver > 0;

        /// <summary>
        /// Determine if the pallets object contains any Chep pallets
        /// </summary>
        /// <returns></returns>
        public bool HasChepPallets => ExchangeChepPalletsOut > 0 ||
                   TransferChepPalletsToBex > 0 ||
                   TransferChepPalletsToReceiver > 0;

        /// <summary>
        /// Determine the type of pallet details contained in the object
        /// </summary>
        private void SetPalletType()
        {
            if (!HasPallets)
            {
                PalletType = PalletType.NotApplicable;
            }
            else
            {
                if (HasChepPallets && HasLoscamPallets)
                {
                    PalletType = PalletType.Both;
                }
                else
                {
                    if (HasChepPallets)
                    {
                        PalletType = PalletType.Chep;
                    }

                    if (HasLoscamPallets)
                    {
                        PalletType = PalletType.Loscam;
                    }
                }

            }
        }

        protected override IEnumerable<object> GetEqualityComponents()
        {
            yield return HasPallets;
            yield return PalletType;
            yield return ChepPalletTransferDocketNumber;
            yield return ExchangeChepPalletsOut;
            yield return TransferChepPalletsToBex;
            yield return TransferChepPalletsToReceiver;
            yield return LoscamPalletTransferDocketNumber;
            yield return ExchangeLoscamPalletsOut;
            yield return TransferLoscamPalletsToBex;
            yield return TransferLoscamPalletsToReceiver;

        }
    }
}
