﻿using System.Collections.Generic;
using PickupSvc.TheDomain.SeedWork;

namespace PickupSvc.TheDomain.ValueObjects
{

    
    public class Contact : ValueObject
    {
        public Contact() { }

        private Contact(string contactName, string phone, string email)
        {
            ContactName = contactName;
            Phone = phone;
            Email = email;
        }

        public string ContactName { get; private set; }

        public string Email { get; private set; }

        public string Phone { get; private set; }

        public override string ToString()
        {
            return ContactName + ":" + Phone + ":" + Email;
        }

        public static Contact Create(string contactName, string phone, string email)
        {
            Guard.ForNullOrEmpty(contactName, "contactName");
            Guard.ForNullOrEmpty(phone, "phone");
            Guard.ForNullOrEmpty(email, "email");
            return new Contact(contactName, phone, email);
        }

        public static Contact Empty()
        {
            
            return new Contact("contactName", "phone", "email");
        }

        protected override IEnumerable<object> GetEqualityComponents()
        {
            yield return ContactName;
            yield return Email;
            yield return Phone;
        }
    }
}
