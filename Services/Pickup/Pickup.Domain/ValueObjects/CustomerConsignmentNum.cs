﻿using System.Collections.Generic;
using PickupSvc.TheDomain.SeedWork;

namespace PickupSvc.TheDomain.ValueObjects
{
    public class CustomerConsignmentNum : ValueObject
    {
        public string CustomerCode { get; private set; }

        public string ConsignmentNum { get; private set; }


        public CustomerConsignmentNum(string customerCode, string consignmentNum)
        {
            CustomerCode = customerCode;
            ConsignmentNum = consignmentNum;
        }

        protected override IEnumerable<object> GetEqualityComponents()
        {
            yield return CustomerCode;
            yield return ConsignmentNum;
        }
    }
}
