﻿using System.Collections.Generic;
using PickupSvc.TheDomain.SeedWork;

namespace PickupSvc.TheDomain.ValueObjects
{
    
    public class Address : ValueObject
    {
        public Address()
        {
        }

        private Address(string state, string suburb, string address1, string address2, string postCode)
        {
            State = state;
            Suburb = suburb;
            Address1 = address1;
            PostCode = postCode;
            Address2 = address2;
        }
        public string Address1 { get; private set; }
        public string Address2 { get; private set; }
        public string PostCode { get; private set; }
        public string State { get; private set; }
        public string Suburb { get; private set; }

        public override string ToString()
        {
            return Address1 + ":" + Address2 + ":" + Suburb + ":" + State + ":" + PostCode;
        }


        public static Address Create(string state, string suburb, string address1, string address2, string postCode)
        {
            Guard.ForNullOrEmpty(address1, "address1");
            Guard.ForNullOrEmpty(address1, "address2");
            Guard.ForNullOrEmpty(postCode, "postcode");
            Guard.ForNullOrEmpty(state, "state");
            Guard.ForNullOrEmpty(suburb, "suburb");
            return new Address(state, suburb, address1,address2,postCode);
        }

        public static Address Empty()
        {
            
            return new Address("state", "suburb", "address1", "address2", "postcode");
        }

        protected override IEnumerable<object> GetEqualityComponents()
        {
            yield return Address1;
            yield return Address2;
            yield return PostCode;
            yield return State;
            yield return Suburb;
        }
    }
}
