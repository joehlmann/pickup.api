﻿using MediatR;
using PickupSvc.TheDomain.Entities;
using PickupSvc.TheDomain.ValueObjects;

namespace PickupSvc.TheDomain.Events
{
    public class DangerousGoodAddedToPickupItemEvent : INotification
    {
        public PickupItem Item { get; private set; }
        public DangerousGood DangerousGood { get; private set; }
        public DangerousGoodAddedToPickupItemEvent(PickupItem item, DangerousGood dg)
        {
            Item = item;
            DangerousGood = dg;
        }
    }
}
