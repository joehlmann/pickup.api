﻿using MediatR;

namespace PickupSvc.TheDomain.Events
{
    public class BookedPickupEvent : INotification
    {
        public Entities.Pickup Pickup { get; private set; }

        public BookedPickupEvent(Entities.Pickup pickup)
        {
            Pickup = pickup;
        }
    }
}
