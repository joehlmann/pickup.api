﻿using MediatR;
using PickupSvc.TheDomain.Entities;

namespace PickupSvc.TheDomain.Events
{
    public class ItemAddedToPickupEvent : INotification
    {
        public Pickup Pickup { get; private set; }
        public PickupItem Item { get; private set; }

        public ItemAddedToPickupEvent(Pickup pickup, PickupItem item)
        {
            Pickup = pickup;
            Item = item;
        }
    }
}
