﻿using MediatR;

namespace PickupSvc.TheDomain.Events
{
    public class NewPickupBranchSetEvent : INotification
    {
        public Entities.Pickup Pickup { get; private set; }

        public NewPickupBranchSetEvent(Entities.Pickup pickup)
        {
            Pickup = pickup;
        }
    }
}
