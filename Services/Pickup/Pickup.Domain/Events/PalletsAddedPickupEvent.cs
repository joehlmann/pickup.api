﻿using MediatR;
using PickupSvc.TheDomain.ValueObjects;

namespace PickupSvc.TheDomain.Events
{
    public class PalletsAddedPickupEvent : INotification
    {
        public Entities.Pickup PickupRoot { get; private set; }

        public PickupPallets Pallets { get; private set; }



        public PalletsAddedPickupEvent(Entities.Pickup pickupRoot, PickupPallets pallets)
        {
            PickupRoot = pickupRoot;
            Pallets = pallets;
        }

    }
}
