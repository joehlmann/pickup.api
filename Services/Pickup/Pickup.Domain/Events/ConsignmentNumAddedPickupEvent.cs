﻿using MediatR;
using PickupSvc.TheDomain.ValueObjects;

namespace PickupSvc.TheDomain.Events
{
    public class ConsignmentNumAddedPickupEvent : INotification
    {

        public Entities.Pickup Pickup { get; private set; }

        public CustomerConsignmentNum CustomerConsignmentNum { get; private set; }


        public ConsignmentNumAddedPickupEvent(CustomerConsignmentNum customerConsignmentNum, Entities.Pickup pickup)
        {
            CustomerConsignmentNum = customerConsignmentNum;
            Pickup = pickup;
        }


    }
}
