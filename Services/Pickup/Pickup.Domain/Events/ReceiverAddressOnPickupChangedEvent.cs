﻿using MediatR;

namespace PickupSvc.TheDomain.Events
{
    public class ReceiverAddressOnPickupChangedEvent : INotification
    {
        public Entities.Pickup Pickup { get; }

        public ReceiverAddressOnPickupChangedEvent(Entities.Pickup pickup)
        {
            Pickup = pickup;
        }
    }
}
