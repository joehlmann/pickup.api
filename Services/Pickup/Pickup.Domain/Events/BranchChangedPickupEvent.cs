﻿using MediatR;

namespace PickupSvc.TheDomain.Events
{
    public class BranchChangedPickupEvent : INotification
    {
        public Entities.Pickup Pickup { get; private set; }

        public BranchChangedPickupEvent(Entities.Pickup pickup)
        {
            Pickup = pickup;
        }
    }
}
