﻿using Bex.Utilities.Helper;
using PickupSvc.TheDomain.Enumerators;
using PickupSvc.TheDomain.SeedWork;

namespace PickupSvc.TheDomain.Entities
{
    public class Region : Entity<int>
    {
        
        public string State { get;  set; }
        public string Name { get;  set; }
        public bool Active { get;  set; }

        public RegionType RegionType { get; private set; }

        
        #region Constructors & factories

        private Region()
        {
            RegionType = RegionType.Empty;
        }

        public  Region(string state, string name, bool active)
        {
            State = state;
            Name = name;
            Active = active;
            RegionType = RegionType.Default;
        }

        private Region(int id,  string state, string name, bool active)
            :this(state, name, active)
        {
            Id = id;
            
        }

        

        public static Region Create(string state, string name, bool active)
        {
            Guard.ForNullOrEmpty(name, "name");
            Guard.ForNullOrEmpty(state, "state");
            return new Region(state, name,active);
        }

        public static Region Seed( string state, string name, bool active)
        {
            
            return new Region( state, name, active);
        }
        public static Region Empty()
        {
            return new Region();
        }

        public static Region Default()
        {
            return new Region(EntityTypeRange.Region.ToInt(),"VIC","DefaultRegion",true);
        }
#endregion
    }
}
