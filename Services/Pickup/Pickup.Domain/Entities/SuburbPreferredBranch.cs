﻿using PickupSvc.TheDomain.Enumerators;
using PickupSvc.TheDomain.SeedWork;

namespace PickupSvc.TheDomain.Entities
{
    public class SuburbPreferredBranch
    {
        public int BranchId { get; set; }
        public Branch Branch { get; set; }


        public int SuburbId { get; set; }
        public Suburb Suburb { get; set; }

        public bool IsOnForwardEnabled { get; set; }
        public int Order { get;set; }
        public PreferredBranchType PreferredBranchType { get; set; }

        public SuburbPreferredBranch(int branchId, int suburbId, bool isOnForwardEnabled, int order, PreferredBranchType preferredBranchType)
        {
            BranchId = branchId;
            SuburbId = suburbId;
            IsOnForwardEnabled = isOnForwardEnabled;
            Order = order;
            PreferredBranchType = preferredBranchType;
        }

        public static SuburbPreferredBranch Create(int branchId, int suburbId, bool isOnForwardEnabled, int order,
            PreferredBranchType preferredBranchType)
        {
            Guard.ForLessEqualZero(branchId, "branchId");
            Guard.ForLessEqualZero(suburbId, "suburbId");
            Guard.ForLessEqualZero(order, "order");
            Guard.ForLessEqualZero((int)preferredBranchType, "preferredBranchType");

            return new SuburbPreferredBranch(branchId, suburbId, isOnForwardEnabled, order, preferredBranchType);

        }

        

    }
}
