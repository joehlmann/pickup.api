﻿using System;
using System.Collections.Generic;
using Bex.Utilities.Helper;
using PickupSvc.TheDomain.Enumerators;
using PickupSvc.TheDomain.Events;
using PickupSvc.TheDomain.Infrastructure;
using PickupSvc.TheDomain.SeedWork;
using PickupSvc.TheDomain.ValueObjects;

namespace PickupSvc.TheDomain.Entities
{
    public class Pickup : Entity<int>
    {

        public PickupType PickupType { get; private set; }

        public int PickupNumber { get; private set; }


        public PickupDateTimeInfo TimeInfo { get; private set; }


        public CustomerAccount ChargeAccount { get; private set; }
        public CustomerAccount SenderDetails { get; private set; }
        public string SpecialInstructions { get; set; }
        public bool HasPallets { get;private set; }
        public Address Address { get; private set; }
        
        public Suburb PickupSuburb { get; private set; }
        
        
        public Suburb ReceiverSuburb { get; set; }
        
        public bool FreightPayableByThirdParty { get; private set; }
        public Contact Contact { get; private set; }
        
        public bool HasDangerousGoods => _items != null && _items.Exists(item => item.HasDangerousGoods);

        private bool _hasDangerousGoodsInsurance;
        public bool HasDangerousGoodsInsurance => _hasDangerousGoodsInsurance; // DG Insurance is directly linked to the DG flag
        

        public PickupPallets Pallets {get; private set; }
        
        public User BookedByUser { get; private set; }
        public Branch PickupBranch { get; private set; }
        public bool BookedLate { get; private set; }
        private readonly List<PickupItem> _items = new List<PickupItem>();

        public IReadOnlyList<PickupItem> Items => _items;

        private readonly List<CustomerConsignmentNum> _consignmentNumbers = new List<CustomerConsignmentNum>();

        public IReadOnlyList<CustomerConsignmentNum> ConsignmentNumbers => _consignmentNumbers;


       public void AddCustomerConsignmentNumbers(IEnumerable<CustomerConsignmentNum> consignmentNumbers)
        {
            if (consignmentNumbers != null)
                foreach (var consignmentNum in consignmentNumbers)
                {
                    _consignmentNumbers.Add(consignmentNum);
                    AddDomainEvent(new ConsignmentNumAddedPickupEvent(consignmentNum, this));
                }
        }

        public void SetPayingAccount(CustomerAccount payee)
        {
            ChargeAccount = payee;
            FreightPayableByThirdParty = true;
        }

        public void SetAddress(string addressLine1, string addressLine2, Suburb pickupSuburb)
        {
            Address = Address.Create(pickupSuburb.State,pickupSuburb.Name,addressLine1,addressLine2,pickupSuburb.Postcode);
            PickupSuburb = pickupSuburb;
        }

        public void SetReceiverAddress(Suburb receiverSuburb)
        {
            AddDomainEvent(new ReceiverAddressOnPickupChangedEvent(this));
            ReceiverSuburb = receiverSuburb;
        }
        public void SetPickupWindow(DateTime readyTime, DateTime closeTime)
        {

            var timeInfo = TimeInfo.SetReadyCloseTime(readyTime, closeTime);

            if (PickupBranch.IsPickupWindowValid(timeInfo))
            {
                TimeInfo = timeInfo;
            }
            
        }
        
        /// <summary>
        /// Add a new item to the pickup
        /// </summary>
        /// <param name="item">The item to be added</param>
        public void AddItem(PickupItem item)
        {
            AddDomainEvent(new ItemAddedToPickupEvent(this,item));
            
            _items.Add(item);
        }

        public void AddItems(List<PickupItem> items)
        {
            foreach (var item in items)
            {
                AddItem(item);
            }
        }

        public void RemoveItems(List<PickupItem> items)
        {
            foreach (var item in items)
            {
                RemoveItem(item);
            }
        }
        public void RemoveItem(PickupItem item)
        {
            
            AddDomainEvent(new ItemRemovedFromPickupEvent(this,item));

            _items.Remove(item);
        }

        public void SetReceivingRegionOnItems(Region region)
        {
            foreach (var item in Items)
            {
                 AddDomainEvent(new ItemAddedToPickupEvent(this,item));
                 item.SetReceivingRegion(region);

            }
            
        }
        public void SetReceivingRegionOnItem(PickupItem item, Region region)
        {
            //TODO: Dispatch domain event that the receiving region is set

            if (item != null && region !=null)
            {
                item.SetReceivingRegion(region);
                AddDomainEvent(new ItemAddedToPickupEvent(this, item));
            }
        }

        public void SetContact(string contactName, string contactPhone,string email)
        {
            Contact = Contact.Create(contactName,contactPhone,email);
        }

        public void SetPickupDate(DateTime pickupDate)
        {
            // Make sure it's always midnight of the provided date
          TimeInfo = TimeInfo.SetPickupDate(pickupDate.Date);
        }
        
        public void SetNumber(int pickupNumber){
            PickupNumber = pickupNumber;
        }

        public void SetInstructions(string specialInstructions)
        {
            SpecialInstructions = specialInstructions;
        }
        public void SetSender(CustomerAccount senderDetails)
        {
            SenderDetails = senderDetails;
        }

        public void SetPickupBranch(Branch branch)
        {
            if (string.IsNullOrEmpty(PickupBranch?.Name) && !string.IsNullOrEmpty(branch?.Name))
            {
                AddDomainEvent(new NewPickupBranchSetEvent(this));
            }
            else
            {
                if (PickupBranch != branch)
                {
                    AddDomainEvent(new BranchChangedPickupEvent(this));
                }
            }

            PickupBranch = branch;
            TimeInfo = branch.DetermineIfLateBooking(TimeInfo);
        }
        
        public void AddPalletDetails(PickupPallets pallets)
        {
            Pallets = pallets;
            HasPallets = Pallets.HasPallets;

            
            // Pallets have been added to the pickup
            AddDomainEvent(new PalletsAddedPickupEvent(this,pallets));
        }

        public void SetBookerDetails(User bookedByUser, DateTime bookedDateTime)
        {
            TimeInfo = TimeInfo.SetBookedTime( bookedDateTime);
            BookedByUser = bookedByUser;
        }

       

        #region Pickup Status Actions

        public void Book()
        {
            
            // Validate
            if (
                IsContactCompleteForBooking() &
                IsSenderDetailsCompleteForBooking() &
                IsPayingAccountValidForBooking() &
                IsSenderAccountValidForBooking() &
                PickupBranch.IsPickupDateValidForBooking(TimeInfo) &
                IsBookerDetailsValidForBooking()
            )
            {
                
                //TODO: Add status field
                AddDomainEvent(new BookedPickupEvent(this));
            }
        }

        public void Cancel()
        {

        }

        public void AllocateToDriver()
        {

        }

        #endregion

        // Constructors & Factories
        #region Constructors & Factorys
        private Pickup()
        {
            _items = new List<PickupItem>();

            _consignmentNumbers = new List<CustomerConsignmentNum>();

            TimeInfo = PickupDateTimeInfo.Empty();
            PickupType = PickupType.Empty;

        }
        private Pickup(PickupItem item)
        {
            _items = new List<PickupItem>
            {
                item
            };
            _consignmentNumbers = new List<CustomerConsignmentNum>();
            Id = default;
            PickupType = PickupType.Default;
        }

        public Pickup(List<PickupItem> items, PickupPallets pallets)
        {
            _items = items ?? new List<PickupItem>();

            _items = items;

            Pallets = pallets ?? PickupPallets.Default();

        }


        private Pickup(int num)
        {
            Id = EntityTypeRange.Pickup.ToInt(num);
            _items = new List<PickupItem>();

            _consignmentNumbers = new List<CustomerConsignmentNum>();

            TimeInfo = PickupDateTimeInfo.Empty();
            PickupType = PickupType.Default;

        }

        public static Pickup Seed(int num =0)
        {
            return new Pickup(num);
        }

        public static Pickup Empty()
        {
            return new Pickup();
        }


        public static Pickup Default()
        {
            return new Pickup(PickupItem.Empty());
        }
        public static Pickup Create()
        {
            return new Pickup();
        }
        #endregion

        #region Validation

       

        private bool IsSenderDetailsCompleteForBooking()
        {
            var isValid = true;

            if (string.IsNullOrEmpty(SenderDetails?.CustomerAccountName))
            {
                isValid = false;
                AddError(ValidationMessages.PickupSenderNameMissing);
            }

            if (string.IsNullOrEmpty(Address.Address1) && string.IsNullOrEmpty(Address.Address2))
            {
                isValid = false;
                AddError(ValidationMessages.PickupAddressMissing);
            }
            
            if (PickupSuburb == null)
            {
                isValid = false;
                AddError(ValidationMessages.PickupSuburbMissing);
            }

            if (string.IsNullOrEmpty(PickupSuburb?.Postcode))
            {
                isValid = false;
                AddError(ValidationMessages.PickupPostcodeMissing);
            }
            
            return isValid;
        }

        private bool IsContactCompleteForBooking()
        {
            var isValid = true;

            if (string.IsNullOrEmpty(Contact.ContactName))
            {
                isValid = false;
                AddError(ValidationMessages.PickupContactNameMissing);
            }

            if (string.IsNullOrEmpty(Contact.Phone))
            {
                isValid = false;
                AddError(ValidationMessages.PickupContactPhoneMissing);
            }
            return isValid;
        }

        private bool IsPayingAccountValidForBooking()
        {
            //TODO: Validate account conditions e.g OnHold etc
            return true;
        }

        private bool IsSenderAccountValidForBooking()
        {
            //TODO: Validate account conditions e.g OnHold etc
            return true;
        }

       

        private bool IsBookerDetailsValidForBooking()
        {
            var isValid = true;

            if (BookedByUser == null || BookedByUser.Id == 0)
            {
                AddError(ValidationMessages.PickupBookingUserIsMissing);
                isValid = false;
            }
          
            return isValid;
        }

        

        
        #endregion
    }
}
