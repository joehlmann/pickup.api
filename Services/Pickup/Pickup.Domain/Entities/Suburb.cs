﻿using System.Collections.Generic;
using PickupSvc.TheDomain.Enumerators;
using PickupSvc.TheDomain.SeedWork;

namespace PickupSvc.TheDomain.Entities
{
    public class Suburb : Entity<int>
    {
        public string Name { get;private set; }
        public string Postcode { get; private set; }
        public string State { get; private set; }


        public SuburbType SuburbType { get; private set; }

        public List<SuburbPreferredBranch> PreferredBranches { get; private set; } = new List<SuburbPreferredBranch>();

       

        public void AddBranchToSuburb(int branchId, PreferredBranchType preferredBranchType, bool onForwardEnabled, int order)
        {
            var result = new SuburbPreferredBranch(branchId, Id, onForwardEnabled,order,preferredBranchType);

            PreferredBranches.Add(result);
        }



        // Constructors & Factories


        private Suburb()
        {
            SuburbType = SuburbType.Empty;
        }

        private Suburb(string name, string postcode, string state, List<SuburbPreferredBranch> preferredBranches = null)
        {
            
            Name = name;
            Postcode = postcode;
            State = state;
            SuburbType = SuburbType.Default;
            PreferredBranches = preferredBranches ?? new List<SuburbPreferredBranch>();
        }

        private Suburb(int id, string name, string postcode, string state, Branch branch)
        {
            Id = id;
            Name = name;
            Postcode = postcode;
            State = state;
            SuburbType = SuburbType.Default;
            PreferredBranches.Add(new SuburbPreferredBranch(branch.Id,id,true,1,PreferredBranchType.Unknown));

            
        }



        public static Suburb Create(string name, string postCode, string state)
        {
            return new Suburb(name, postCode, state);
        }

        public static Suburb Empty()
        {
            return new Suburb();
        }


    }
}
