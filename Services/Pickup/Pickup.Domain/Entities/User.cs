﻿using PickupSvc.TheDomain.Enumerators;
using PickupSvc.TheDomain.SeedWork;

namespace PickupSvc.TheDomain.Entities
{
    public class User : Entity<int> , IAggregateRoot
    {
        
        public string DisplayName { get; private set; }

        public UserStatus UserStatus { get; private set; }

        public string Phone { get; private set; }

        public string Email { get; private set; }

        private User( UserStatus userStatus ,string displayName,string phone,string email)
        {
            //Id = id;
            DisplayName = displayName;
            UserStatus = userStatus;
        }

        private User()
        {
            Id = default;
            DisplayName = "DisplayName";
            UserStatus = UserStatus.Empty;
        }

        public static User EmptyUser()
        {
            return new User();
        }


        public static User Create(  UserStatus userStatus, string displayName, string phone,string email)
        {
            return new User(userStatus,displayName,phone,email);
        }
    }
}
