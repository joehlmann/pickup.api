﻿using System;
using PickupSvc.TheDomain.Enumerators;
using PickupSvc.TheDomain.Infrastructure;
using PickupSvc.TheDomain.SeedWork;
using PickupSvc.TheDomain.ValueObjects;

namespace PickupSvc.TheDomain.Entities
{
    public class Branch : Entity<int>
    {


        //public int MinimumPickupWindowDuration { get; } = 2;

        //public int MaximumDaysPickupDateIsAllowedIntoTheFuture { get; } = 10;


        //public int MaximumDaysPickupDateIsAllowedIntoThePast { get; } = 10;


        public BranchType BranchType { get; private set; }

        public PickupRules PickupRules { get; private set; } = PickupRules.Default();


        public string Name { get; private set; }

        public Address Address { get; private set; }

        public int DefaultRegionId { get; set; }

        public Region DefaultRegion { get; set; }


        // The latest time a branch will accept a pickup 
        public DateTime PickupCutoffTime { get; private set; }

        // How many minutes prior to the cut off time is allowed before a pickup booking is considered late
        public int PickupBookingCutoffInMinutesVariance { get; private set; }



        public PickupDateTimeInfo DetermineIfLateBooking(PickupDateTimeInfo dateTimeInfo)
        {
            if (PickupCutoffTime == DateTime.MinValue)
            {
                AddError(ValidationMessages.PickupBranchCutoffTimeIsMissing);
                
            }

            // If the pickup is being booked for the past, flag it as late
            if (dateTimeInfo.PickupDate < dateTimeInfo.BookedDateTime)
            {
                    return dateTimeInfo.SetBookedLate(true);
            }

            var pickupBranchCutoffTime = PickupCutoffTime;

            // If the pickup is being booked after the pickup branches cut off time, it's late
            if (dateTimeInfo.PickupDate.Date == dateTimeInfo.BookedDateTime.Date && DateTime.Now > pickupBranchCutoffTime)
            {
                return dateTimeInfo.SetBookedLate(true);
            }

            var pickupTimeReady = dateTimeInfo.ReadyTime;
            var cutoffAllowedTime = pickupBranchCutoffTime.AddMinutes(-PickupBookingCutoffInMinutesVariance);

            // Check if the pickup ready time is within the allowed booking time of the cut off
            if (pickupTimeReady > cutoffAllowedTime)
            {
                return dateTimeInfo.SetBookedLate(true);

            }

            return dateTimeInfo.SetBookedLate(false);
        }



        public  bool IsPickupWindowValid(PickupDateTimeInfo dateTimeInfo)
        {
            
            var isValid = true;

            if (dateTimeInfo.ReadyTime is null)
            {
                isValid = false;
                AddError(ValidationMessages.PickupReadyTimeIsMissing);
            }

            if (dateTimeInfo.CloseTime is null)
            {
                isValid = false;
                AddError(ValidationMessages.PickupCloseTimeIsMissing);
            }

            if (isValid && (dateTimeInfo.CloseTime.Value - dateTimeInfo.ReadyTime.Value).TotalHours <= PickupRules.MinimumPickupWindowDuration)
            {
                isValid = false;
                var error = ValidationMessages.PickupWindowNotLongEnough;
                error.Message = error.Message.Replace("{MinimumPickupWindowDuration}", PickupRules.MinimumPickupWindowDuration.ToString());
                AddError(error);
            }

            return isValid;
        }
        public bool IsPickupDateValidForBooking(PickupDateTimeInfo dateTimeInfo)
        {
            var isValid = true;

            var backDate = DateTime.Now.AddDays(- PickupRules.MaximumDaysPickupDateIsAllowedIntoThePast);

            // Check if the pickup date is greater than or equal too today (can't be backdated)
            if (dateTimeInfo.PickupDate.Date < backDate.Date)
            {
                isValid = false;
                var error = ValidationMessages.PickupDateIsToFarIntoThePast;

                error.Message = error.Message.Replace("{MaximumDaysPickupDateIsAllowedIntoThePast}", PickupRules.MaximumDaysPickupDateIsAllowedIntoThePast.ToString());
                AddError(error);
            }

            // Check that the pickup isn't x days into the future
            var forwardDate = DateTime.Now.AddDays(PickupRules.MaximumDaysPickupDateIsAllowedIntoTheFuture);
            if (dateTimeInfo.PickupDate.Date > forwardDate.Date)
            {
                isValid = false;

                var error = ValidationMessages.PickupDateIsToFarIntoTheFuture;
                error.Message = error.Message.Replace("{MaximumDaysPickupDateIsAllowedIntoTheFuture}",PickupRules.MaximumDaysPickupDateIsAllowedIntoTheFuture.ToString());
                AddError(error);
            }
            return isValid;
        }


        //Constructors & Factories 
        private Branch()
        {
            BranchType = BranchType.Empty;
            // Used for automapper
        }
        public Branch( string name, Address address, DateTime pickupCutoffTime, int pickupBookingCutoffInMinutesVariance)
        {
            
            Name = name;
            Address = address;
            PickupCutoffTime = pickupCutoffTime;
            PickupBookingCutoffInMinutesVariance = pickupBookingCutoffInMinutesVariance;
            BranchType = BranchType.Default;
        }

        private Branch(string name, Address address, DateTime pickupCutoffTime, int pickupBookingCutoffInMinutesVariance, Region region)
         : this( name, address, pickupCutoffTime, pickupBookingCutoffInMinutesVariance)
        {
            DefaultRegionId = region.Id;
            DefaultRegion = region;
        }

        public Branch(string name)
        {
            Name = name;
        }

        public static Branch Empty()
        {
            return new Branch();
        }

        public static  Branch Create( string name, Address address, DateTime pickupCutoffTime, int pickupBookingCutoffInMinutesVariance)
        {
            Guard.ForNullOrEmpty(name, "name");
            Guard.ForNullOrEmpty(name, "name");
            

            return new Branch(name,address,pickupCutoffTime,pickupBookingCutoffInMinutesVariance);
        }

        public static Branch Seed(int id, string name, Address address, DateTime pickupCutoffTime, int pickupBookingCutoffInMinutesVariance,Region region){
            

            return new Branch(name, address, pickupCutoffTime, pickupBookingCutoffInMinutesVariance,region);
        }
    }
}
