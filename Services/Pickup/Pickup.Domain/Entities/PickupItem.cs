﻿using System.Collections.Generic;
using System.Linq;
using PickupSvc.TheDomain.Enumerators;
using PickupSvc.TheDomain.Events;
using PickupSvc.TheDomain.Infrastructure;
using PickupSvc.TheDomain.SeedWork;
using PickupSvc.TheDomain.ValueObjects;

namespace PickupSvc.TheDomain.Entities
{
    public class PickupItem : Entity<int>
    {
       
        public int Quantity { get; private set; }
        public string Description { get; private set; }
        public int WeightKg { get; private set; }
        public bool HasFoodStuff { get; private set; }
        public Region ReceivingRegion { get; private set; }

        public Pickup Pickup { get;  set; }
        public int PickupId { get; set; }

        public bool HasDangerousGoods => DangerousGoods != null && DangerousGoods.Any();
      
        private readonly List<DangerousGood> _dangerousGoods;        

        public IEnumerable<DangerousGood> DangerousGoods => _dangerousGoods.AsReadOnly();
        public PickupItemType PickupItemType { get; private set; }


        public void SetReceivingRegion(Region receivingRegion)
        {
            ReceivingRegion = receivingRegion;
        }

        public void SetWeight(int weightKg)
        {
            WeightKg = weightKg;
        }

        public void SetDescription(string description)
        {
            Description = description;
        }

        public void AddDangerousGoods(List<DangerousGood> dangerousGoods)
        {
            if (dangerousGoods != null)
            {
                foreach (var dangerousGood in dangerousGoods)
                {
                    AddDangerousGood(dangerousGood);
                }
            }
            
        }

        public void AddDangerousGood(DangerousGood dangerousGood)
        {
            if (dangerousGood != null)
            {
                // Validate the DG
                IsValidDangerousGoodItem(dangerousGood);
           
                // The item we are adding has DG's, raise an event
                AddDomainEvent(new DangerousGoodAddedToPickupItemEvent(this,dangerousGood));

                _dangerousGoods.Add(dangerousGood);
            }
            
        }
        private bool IsValidDangerousGoodItem(DangerousGood dangerousGood)
        {
            var isValid = true;

            // Check if all of the DG info has been set
            if (dangerousGood.Class < 1 || dangerousGood.UNNo < 1)
            {
                isValid = false;
                AddError(ValidationMessages.PickupItemDGUnknown);
            }

            return isValid;
        }

        #region Constructors & Factory Methods

        public PickupItem()
        {
            Id = default;
            _dangerousGoods = new List<DangerousGood>();
            PickupItemType = PickupItemType.Empty;
        }

        private PickupItem(List<DangerousGood> dangerousGoods)
        {
            _dangerousGoods = dangerousGoods;
        }

        public PickupItem(int quantity, string description, int weightInKg, bool hasFoodstuff, List<DangerousGood> dangerousGoods)
            : this(quantity, description, weightInKg, hasFoodstuff, dangerousGoods, null)
        {
            // Calls overall constructor
        }
        public PickupItem(int quantity, string description, int weightInKg, bool hasFoodstuff, List<DangerousGood> dangerousGoods = null, Region receivingRegion = null)
        {
            _dangerousGoods = dangerousGoods ??  new List<DangerousGood>();

            ReceivingRegion = receivingRegion ?? Region.Default();

            Guard.ForNullOrEmpty(description, "description");
            Guard.ForLessEqualZero(quantity, "quantity");

            Description = description;
            HasFoodStuff = hasFoodstuff;
            Quantity = quantity;
            WeightKg = weightInKg;
            PickupItemType = PickupItemType.Default;
            AddDangerousGoods(dangerousGoods);

        }

        public static PickupItem Empty()
        {
            return new PickupItem();
        }

        public static PickupItem Create(int quantity, string description, int weightInKg, bool hasFoodstuff)
        {
            return new PickupItem(quantity, description, weightInKg, hasFoodstuff);
        }

        public static PickupItem Create(int quantity, string description, int weightInKg, bool hasFoodstuff,Region receivingRegion)
        {
            return new PickupItem(quantity, description, weightInKg, hasFoodstuff,null, receivingRegion);
        }

#endregion

       
    }
}
