﻿using PickupSvc.TheDomain.Enumerators;
using PickupSvc.TheDomain.SeedWork;

namespace PickupSvc.TheDomain.Entities
{
    public class CustomerAccount : Entity<int>
    {
        
        public string CustomerCode { get; private set; }
        public string CustomerAccountName { get; private set; }

        public CustomerType CustomerType { get; private set; }

        public CustomerAccount(int id, string customerCode, string customerAccountName)
        {
            Id = id;
            CustomerCode = customerCode;
            CustomerAccountName = customerAccountName;
            CustomerType = CustomerType.Default;
        }

        public CustomerAccount(string customerCode,string name)
        {
            
            CustomerCode = customerCode;
            CustomerAccountName = name;
            CustomerType = CustomerType.Default;
        }


        private CustomerAccount()
        {
            CustomerType = CustomerType.Empty;
        }

        public static CustomerAccount Empty()
        {
            return new CustomerAccount();
        }
    }
}
