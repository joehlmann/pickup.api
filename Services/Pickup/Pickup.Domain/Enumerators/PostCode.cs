﻿namespace PickupSvc.TheDomain.Enumerators
{
    public enum PostCode
    {
        Vic = 3000,
        Nsw = 2000,
        Qld = 4000,
        Sa = 5000,
        Wa= 6000,
        Tas = 7000,
        Nt = 0800
    }
}
