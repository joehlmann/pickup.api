﻿namespace PickupSvc.TheDomain.Enumerators
{
    public enum UserStatus
    {
        Empty,
        New,
        Registered,
        Admin
    }
}