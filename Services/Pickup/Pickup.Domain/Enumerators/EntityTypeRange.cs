﻿namespace PickupSvc.TheDomain.Enumerators
{
    public enum EntityTypeRange
    {
        User =1000,
        Customer = 2000,
        Branch = 3000,
        PickupItem = 4000,
        Pickup = 5000,
        PreferredBranches=6000,
        PickupTimeInfo = 7000,
        Region = 8000,
        Suburb = 9000
    }
}