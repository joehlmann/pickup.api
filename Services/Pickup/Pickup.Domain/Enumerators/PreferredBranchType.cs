﻿namespace PickupSvc.TheDomain.Enumerators
{
    public enum PreferredBranchType
    {
        Empty = 'E',
        Unknown = 'U',
        Sender = 'S',
        Receiver = 'R'
    }
}
