﻿namespace PickupSvc.TheDomain.Enumerators
{
    public enum PalletType
    {
        Empty = 0, 
        Chep = 1,
        Loscam = 2,
        Both = 3,
        NotApplicable = 4
    }
}