﻿namespace PickupSvc.TheDomain.Enumerators
{
    public enum BranchType
    {
        Empty,
        Default,
        Main,
        Satellite
    }
}
