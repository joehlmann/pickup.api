﻿using Autofac;
using EventBusAbstract;
using EventBusAbstract.Abstractions;
using IntegrationEventLog.Services;
using Microsoft.Data.SqlClient;
using PickupSvc.API.Application.DomainServices;
using PickupSvc.API.Application.DomainServices.Interfaces;
using PickupSvc.API.Infrastructure.Authorization;
using PickupSvc.Infrastructure.TokenContext;
using Telerik.JustMock;

namespace Pickup.Domain.UnitTesting.ArrangeModules
{
    public class ServicesModuleUnitTests : Module
    {
        protected string _connectionStringEventLog;
        protected IEventBus _eventBus;
        



        public ServicesModuleUnitTests(string connectionStringEventLog)
        {
            _connectionStringEventLog = connectionStringEventLog;
            
        }

        private void LoadMocks(ContainerBuilder builder)
        {
            
           

            builder.RegisterInstance(Mock.Create<IEventBus>())
                .As<IEventBus>()
                .SingleInstance();

           



        }


        protected override void Load(ContainerBuilder builder)
        {
            LoadMocks(builder);

            builder.RegisterType<TokenContextService>()
                .As<ITokenContextService>()
                .InstancePerLifetimeScope();




            

            builder.RegisterType<EntityIdValidService>()
                .As<IEntityIdValidService>()
                .InstancePerLifetimeScope();



            builder.RegisterType<IntegrationEventLogService>()
                .WithParameter("dbConnection", new SqlConnection(_connectionStringEventLog))
                .As<IIntegrationEventLogService>()
                .InstancePerLifetimeScope();


           
            



            //Singleton
            builder.RegisterType<InMemoryEventBusSubscriptionsManager>()
                .As<IEventBusSubscriptionsManager>()
                .SingleInstance();






            // Register the Command's Validators (Validators based on FluentValidation library)
            //builder
            //    .RegisterAssemblyTypes(typeof(Startup).GetTypeInfo().Assembly)
            //    .Where(t => t.IsClosedTypeOf(typeof(IValidator<>)))
            //    .AsImplementedInterfaces();



        }
    }
}
