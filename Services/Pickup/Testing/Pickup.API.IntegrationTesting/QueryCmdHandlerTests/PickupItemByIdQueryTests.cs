﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Autofac;
using Bex.Utilities.Helper;
using FluentAssertions;
using FluentValidation;
using MediatR;
using PickupSvc.API.Application.CommandsQueries.PickupItems;
using PickupSvc.API.Application.DTOs;
using PickupSvc.TheDomain.Enumerators;
using PickupSvc.TheDomain.Exceptions;
using Telerik.JustMock;
using Xunit;

namespace PickupSvc.API.IntegrationTesting.QueryCmdHandlerTests
{
    
    public class PickupItemByIdQueryTests : BaseIntegrationTest
    {

        [InlineData((int)EntityTypeRange.PickupItem, 0, PickupItemType.Default)]
        [InlineData((int)EntityTypeRange.PickupItem - 200, 1, PickupItemType.Empty)]
        [Theory]
        public void Number_PickupQueryCmd_ByNumber(int value, int numErrors, PickupItemType expecPickupType)
        {
            //Arrange 


            var errors = new List<string>();
            var _mediator = _containerScope.Resolve<IMediator>();

            var command = new PickupItemByIdQuery(value);

            var response = PickupItemDTO.Empty();


            //Act

            try
            {
                var result = _mediator.Send(command, Arg.IsAny<CancellationToken>()).Result;
                response = result;
            }
            catch (AggregateException e) when (e.InnerException.InnerException.GetType() == typeof(ValidationException))
            {
                errors = ((ValidationException)e.InnerException.InnerException)?.Errors
                    .Select(v => v.ErrorMessage).ToList();
            }
            catch (AggregateException e) when (e.InnerException.GetType() == typeof(PickupDomainException))
            {
                errors.Add(((PickupDomainException)e.InnerException)?.Message);
            }
            catch (Exception e)
            {
                errors.Clear();
                errors.Add(e.Message);
            }


            //Assert

            errors.Count.Should().Be(numErrors);
            response.PickupItemType.Should().Be(expecPickupType.GetEnumName());
        }



    }
}
