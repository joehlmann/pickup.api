﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Autofac;
using Bex.Utilities.Helper;
using FluentAssertions;
using FluentValidation;
using MediatR;
using PickupSvc.API.Application.CommandsQueries.Pickups;
using PickupSvc.API.Application.DTOs;
using PickupSvc.TheDomain.Enumerators;
using PickupSvc.TheDomain.Exceptions;
using Telerik.JustMock;
using Xunit;

namespace PickupSvc.API.IntegrationTesting.QueryCmdHandlerTests
{
    
    public class PickupQueryTests : BaseIntegrationTest
    {
        [InlineData((int)EntityTypeRange.Pickup, 0, PickupType.Default)]
        [InlineData((int)EntityTypeRange.Pickup - 200, 1, PickupType.Empty)]
        [Theory]
        public void Number_PickupQueryCmd_ByNumber(int value, int numErrors, PickupType expecPickupType)
        {
            //Arrange 


            var errors = new List<string>();
            var _mediator = _containerScope.Resolve<IMediator>();

            var command = new PickupByNumberQuery(value);

            var response = PickupDTO.Empty();


            //Act

            try
            {
                var result = _mediator.Send(command, Arg.IsAny<CancellationToken>()).Result;
                response = result;
            }
            catch (AggregateException e) when (e.InnerException.InnerException.GetType() == typeof(ValidationException))
            {
                errors = ((ValidationException)e.InnerException.InnerException)?.Errors
                    .Select(v => v.ErrorMessage).ToList();
            }
            catch (AggregateException e) when (e.InnerException.GetType() == typeof(PickupDomainException))
            {
                errors.Add(((PickupDomainException)e.InnerException)?.Message);
            }
            catch (Exception e)
            {
                errors.Clear();
                errors.Add(e.Message);
            }


            //Assert

            errors.Count.Should().Be(numErrors);
            response.PickupType.Should().Be(expecPickupType.GetEnumName());
        }

    }

}

