﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Autofac;
using FluentAssertions;
using FluentValidation;
using MediatR;
using PickupSvc.API.Application.CommandsQueries.Pickups;
using PickupSvc.API.Application.DTOs;
using PickupSvc.TheDomain.Enumerators;
using PickupSvc.TheDomain.Exceptions;
using Telerik.JustMock;
using Xunit;

namespace PickupSvc.API.IntegrationTesting.QueryCmdHandlerTests
{

    public class PickupsForUserTests : BaseIntegrationTest
    {
        [InlineData((int)EntityTypeRange.User, 0, 0)]
        [InlineData((int)EntityTypeRange.User-200, 1, -1)]
        [Theory]
        public void UserId_PickupForUserQueryCmd_ById_Results(int value, int numErrors, int numResult)
        {
            //Arrange 


            var errors = new List<string>();
            var _mediator = _containerScope.Resolve<IMediator>();

            var command = new PickupsForUserQuery(value);

            var response = new List<PickupDTO>();


            //Act

            try
            {
                var result = _mediator.Send(command, Arg.IsAny<CancellationToken>()).Result;
                response = result.ToList();
            }
            catch (AggregateException e) when (e.InnerException.InnerException.GetType() == typeof(ValidationException))
            {
                errors = ((ValidationException) e.InnerException.InnerException)?.Errors
                    .Select(v => v.ErrorMessage).ToList();
            }
            catch (AggregateException e) when (e.InnerException.GetType() == typeof(PickupDomainException))
            {
                errors.Add(((PickupDomainException) e.InnerException)?.Message);
            }
            catch (Exception e)
            {
                errors.Clear();
                errors.Add(e.Message);
            }


            //Assert

            errors.Count.Should().Be(numErrors);
            response.Count.Should().BeGreaterThan(numResult);
        }

    }
}

