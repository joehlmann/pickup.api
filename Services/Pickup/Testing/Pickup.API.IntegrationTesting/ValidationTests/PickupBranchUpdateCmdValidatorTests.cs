﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using Autofac;
using Bex.Utilities.Helper;
using FluentAssertions;
using FluentValidation;
using MediatR;
using Microsoft.Data.SqlClient;
using PickupSvc.API.Application.Commands.Pickups;
using PickupSvc.API.Application.Validations;
using PickupSvc.API.IntegrationTesting.ReferenceData;
using PickupSvc.TheDomain.Enumerators;
using PickupSvc.TheDomain.Exceptions;
using Telerik.JustMock;
using Xunit;

namespace PickupSvc.API.IntegrationTesting.ValidationTests
{
    public class PickupBranchUpdateCmdValidatorTests : BaseIntegrationTest
    {

        private IMediator _mediator;


        //ToDo Clean 


        [MemberData(nameof(HappyData))]
        [Theory]
        public void Pickup_PickupBranchUpdateCmd_validator(PickupBranchUpdateCommand command, int numErrors, string errorMsg)
        {

            //Arrange 
            _mediator = _containerScope.Resolve<IMediator>();

           // ClearDatabase();

            var errors = new List<string>();
            errors.Add(ErrorMsg.NoErrors);

            //Act

            try
            {
                var response = _mediator.Send(command, Arg.IsAny<CancellationToken>()).Result;
            }
            catch (AggregateException e) when (e.InnerException.InnerException.GetType() == typeof(ValidationException))
            {
                errors = ((ValidationException)e.InnerException.InnerException)?.Errors
                    .Select(v => v.ErrorMessage).ToList();

            }
            catch (AggregateException e) when (e.InnerException.GetType() == typeof(PickupDomainException))
            {

                errors.Add(((PickupDomainException)e.InnerException)?.Message);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                errors.Clear();
                errors.Add(e.Message);
            }



            //Assert

            errors.Count.Should().Be(numErrors);

            errors[0].Should().Be(errorMsg);


        }



        public static List<object[]> HappyData()
        {
            return new List<object[]>
            {
                new object[] {
                    FakeRequest( new Dictionary<string, object>()
                    {
                        [CmdParams.PickupItem.number]=EntityTypeRange.Pickup.ToInt(),

                    } ) , 1 , ErrorMsg.NoErrors }


            };
        }

        public static List<object[]> InvalidPickupNumberData()
        {
            return new List<object[]>
            {
                new object[] {
                    FakeRequest( new Dictionary<string, object>()
                    {
                        [CmdParams.PickupItem.number]=EntityTypeRange.Pickup.ToInt(-100),

                    } ) , 1 , $"Pickup With Id:{EntityTypeRange.Pickup.ToInt(-100)} does not Exist" }


            };
        }

        private static PickupBranchUpdateCommand FakeRequest(Dictionary<string, object> args = null)
        {


            return new PickupBranchUpdateCommand(
                id: args != null && args.ContainsKey("id") ? (int)args["id"] : EntityTypeRange.Pickup.ToInt(),
                number: args != null && args.ContainsKey("number") ? (int)args["number"] : EntityTypeRange.Pickup.ToInt(),
                suburb: args != null && args.ContainsKey("suburb") ? (string)args["suburb"] : "Melbourne",
                postcode: args != null && args.ContainsKey("postcode") ? (string)args["postcode"] : "3000"
                
            );

        }

        private void ClearDatabase()
        {
            string query =
                $"DELETE FROM dbo.[PickupItems] where PickupItemId >={(EntityTypeRange.PickupItem.ToInt() + 2).ToString()};";

            using (var connection = new SqlConnection(_connectionString))
            {
                var command = new SqlCommand(query, connection)
                {
                    CommandType = CommandType.Text
                };

                connection.Open();
                command.ExecuteNonQuery();
            }
        }
    }
}
