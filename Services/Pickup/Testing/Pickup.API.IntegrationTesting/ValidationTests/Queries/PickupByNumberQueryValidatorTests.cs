﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using Autofac;
using Bex.Utilities.Helper;
using FluentAssertions;
using FluentValidation;
using MediatR;
using PickupSvc.API.Application.CommandsQueries.Pickups;
using PickupSvc.API.Application.DTOs;
using PickupSvc.API.Application.Validations;
using PickupSvc.TheDomain.Enumerators;
using PickupSvc.TheDomain.Exceptions;
using Telerik.JustMock;
using Xunit;

namespace PickupSvc.API.IntegrationTesting.ValidationTests.Queries
{
    public class PickupByNumberQueryValidatorTests : BaseIntegrationTest
    {
        [InlineData((int)EntityTypeRange.Pickup, 0, PickupType.Default)]
        [InlineData((int)EntityTypeRange.Pickup-200, 1, PickupType.Empty)]
        [Theory]
        public void QueryPickupByIdCustomerCmd_validator_Id_invariant_rules(int id, int numErrors, PickupType expectedType)
        {

            //Arrange 
            var errors = new List<string>();

            errors.Add(ErrorMsg.NoErrors);
            var _mediator = _containerScope.Resolve<IMediator>();


            var command = new PickupByNumberQuery(id);

            var defaultResponse = PickupDTO.Empty();


            //Act

            try
            {
                var response = _mediator.Send(command, Arg.IsAny<CancellationToken>()).Result;
                defaultResponse = response;
            }
            catch (AggregateException e) when (e.InnerException.InnerException.GetType() == typeof(ValidationException))
            {
                errors = ((ValidationException)e.InnerException.InnerException)?.Errors
                    .Select(v => v.ErrorMessage).ToList();

            }
            catch (AggregateException e) when (e.InnerException.GetType() == typeof(PickupDomainException))
            {

                errors.Add(((PickupDomainException)e.InnerException)?.Message);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                errors.Clear();
                errors.Add(e.Message);
            }



            //Assert

            errors.Count.Should().Be(numErrors);
            defaultResponse.PickupType.Should().Be(expectedType.GetEnumName());



        }
    }
}
