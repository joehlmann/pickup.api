﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using Autofac;
using FluentAssertions;
using FluentValidation;
using MediatR;
using PickupSvc.API.Application.Commands.PickupItems;
using PickupSvc.API.Application.Validations;
using PickupSvc.TheDomain.Enumerators;
using PickupSvc.TheDomain.Exceptions;
using Telerik.JustMock;
using Xunit;

namespace PickupSvc.API.IntegrationTesting.ValidationTests
{
    public class PickupItemRemoveCmdValidatorTests : BaseIntegrationTest
    {
        [InlineData((int)EntityTypeRange.PickupItem, (int)EntityTypeRange.User, 0, "NoErrors" )]
        [InlineData(0, (int)EntityTypeRange.User, 0,"PickupItem 0 Doesnt Exist")]
        [Theory]
        public void PickupItemRemove_validator_Id(int id,int userid ,int numErrors,string errorMsg)
        {
            //ToDo Clean Up Test

            //Arrange 
            var errors = new List<string>();

            errors.Add(ErrorMsg.NoErrors);
            var _mediator = _containerScope.Resolve<IMediator>();


            var command = new PickupItemRemoveCommand(id,userid);

            var defaultResponse = new PickupItemRemoveResponse();

            //Act

            try
            {
                var response = _mediator.Send(command, Arg.IsAny<CancellationToken>()).Result;
                defaultResponse = response;
            }
            catch (AggregateException e) when (e.InnerException.InnerException.GetType() == typeof(ValidationException))
            {
                errors = ((ValidationException)e.InnerException.InnerException)?.Errors
                    .Select(v => v.ErrorMessage).ToList();

            }
            catch (AggregateException e) when (e.InnerException.GetType() == typeof(PickupDomainException))
            {

                errors.Add(((PickupDomainException)e.InnerException)?.Message);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                errors.Clear();
                errors.Add(e.Message);
            }



            //Assert

            errors.Count.Should().Be(numErrors);
            errors[0].Should().Be(errorMsg);



        }
    }
}
