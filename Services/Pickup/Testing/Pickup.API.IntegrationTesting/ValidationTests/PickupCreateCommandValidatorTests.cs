﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using Autofac;
using Bex.Utilities.Helper;
using FluentAssertions;
using FluentValidation;
using MediatR;
using Microsoft.Data.SqlClient;
using PickupSvc.API.Application.Commands.Pickups;
using PickupSvc.API.Application.DTOs;
using PickupSvc.API.Application.Validations;
using PickupSvc.API.IntegrationTesting.ReferenceData;
using PickupSvc.TheDomain.Enumerators;
using PickupSvc.TheDomain.Exceptions;
using Telerik.JustMock;
using Xunit;

namespace PickupSvc.API.IntegrationTesting.ValidationTests
{
    
    public class PickupCreateCommandValidatorTests : BaseIntegrationTest
    {
        private IMediator _mediator;


        [MemberData(nameof(HappyData))]
        [Theory]
        public void ValidPickup_createPickupCmd_validator_invariant_rule(PickupCreateCommand command, int numErrors, string errorMsg)
        {

            //Arrange 
            _mediator = _containerScope.Resolve<IMediator>();

            ClearDatabase();

            var errors = new List<string>();
            errors.Add(ErrorMsg.NoErrors);

            //Act

            try
            {
                var response = _mediator.Send(command, Arg.IsAny<CancellationToken>()).Result;
            }
            catch (AggregateException e) when (e.InnerException.InnerException.GetType() == typeof(ValidationException))
            {
                errors = ((ValidationException)e.InnerException.InnerException)?.Errors
                    .Select(v => v.ErrorMessage).ToList();

            }
            catch (AggregateException e) when (e.InnerException.GetType() == typeof(PickupDomainException))
            {

                errors.Add(((PickupDomainException)e.InnerException)?.Message);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                errors.Clear();
                errors.Add(e.Message);
            }



            //Assert

            errors.Count.Should().Be(numErrors);

            errors[0].Should().Be(errorMsg);


        }

        [MemberData(nameof(Charge_invalid_Data))]
        [Theory]
        public void ChargeAccountCodeInvalid_createPickupCmd_validator(PickupCreateCommand command, int numErrors, string errorMsg)
        {

            //Arrange 
            _mediator = _containerScope.Resolve<IMediator>();

            ClearDatabase();

            var errors = new List<string>();
            errors.Add(ErrorMsg.NoErrors);
                        
            //Act

            try
            {
                var response = _mediator.Send(command, Arg.IsAny<CancellationToken>()).Result;
            }
            catch (AggregateException e) when (e.InnerException.InnerException.GetType() == typeof(ValidationException))
            {
                errors = ((ValidationException)e.InnerException.InnerException)?.Errors
                    .Select(v => v.ErrorMessage).ToList();

            }
            catch (AggregateException e) when (e.InnerException.GetType() == typeof(PickupDomainException))
            {

                errors.Add(((PickupDomainException)e.InnerException)?.Message);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                errors.Clear();
                errors.Add(e.Message);
            }



            //Assert

            errors.Count.Should().Be(numErrors);

            errors[0].Should().Be(errorMsg);
            

        }

        
        




        public static List<object[]> HappyData()
        {
            return new List<object[]>
            {
                new object[] {
                    FakePickupRequest( new Dictionary<string, object>()
                    {
                        [CmdParams.Pickup.number]=EntityTypeRange.Pickup.ToIntStr(3),
                        [CmdParams.Pickup.bookedByUserId]=EntityTypeRange.User.ToIntStr(),
                        [CmdParams.Pickup.userId]=EntityTypeRange.User.ToIntStr()

                    } ) , 1 , ErrorMsg.NoErrors }


            };
        }

        

        public static List<object[]> Charge_invalid_Data()
        {
            return new List<object[]>
            {
                new object[] {
                    FakePickupRequest( new Dictionary<string, object>()
                    {
                        [CmdParams.Pickup.number]=EntityTypeRange.Pickup.ToIntStr(3),
                        [CmdParams.Pickup.bookedByUserId]=EntityTypeRange.User.ToIntStr(),
                        [CmdParams.Pickup.userId]=EntityTypeRange.User.ToIntStr(),
                        [CmdParams.Pickup.chargeAccountCode]=CustomerCodes.Bunnings,
                        [CmdParams.Pickup.senderCode]=CustomerCodes.Bunnings,
                        [CmdParams.Pickup.senderName]=CustomerCodes.Bunnings,
                        

                    } ) , 1 , CmdParams.Customer.suburb }


            };
        }

        private static TimeInfoDTO GoodTimeInfoDTO()
        {
            return new TimeInfoDTO()
            {
                BookedDateTime = DateTime.Now.AddDays(0).ToShortDateString(),
                CloseTime = new TimeSpan(17, 30, 0).ToString(),
                PickupDate = DateTime.Now.AddDays(2).ToShortDateString(),
                ReadyTime = new TimeSpan(14, 30, 0).ToString(),
            };
        }
        private static AddressDTO GoodAddressDTO()
        {
            return new AddressDTO()
            {
                Address1 = "unit 1 ",
                Address2 = "456 happy street",
                PostCode = "3162",
                State = "VIC",
                Suburb = "Caulfield"
            };
        }
        private static ContactDTO GoodContactDTO()
        {
            return new ContactDTO()
            {
                ContactName = "fred",
                Email = "1@1.com",
                Phone = "12345678"
            };
        }

        private static List< PickupItemDTO> GoodPickupItemDTOs()
        {
            return new List<PickupItemDTO>()
            {
                new PickupItemDTO()
                {
                    Description = "N95 Masks",
                    WeightKg = 1,
                    Quantity = 1,
                    DangerousGoods = new List<DangerousGoodItemDTO>(),
                    FoodStuff = false
                }
            };

        }


        private static PickupCreateCommand FakePickupRequest(Dictionary<string, object> args = null)
        {
            
            return new PickupCreateCommand(
                number: args != null && args.ContainsKey("number") ? (string)args["number"] : EntityTypeRange.Pickup.ToIntStr(),
                bookedByUserId: args != null && args.ContainsKey("bookedByUserId") ? int.Parse((string)args["bookedByUserId"]) : EntityTypeRange.User.ToInt(),
                userId: args != null && args.ContainsKey("userId") ? int.Parse((string)args["userId"]) : EntityTypeRange.User.ToInt(),
                chargeAccountCode: args != null && args.ContainsKey("chargeAccountCode") ? (string)args["chargeAccountCode"] : CustomerCodes.Bunnings,
                senderCode: args != null && args.ContainsKey("senderCode") ? (string)args["senderCode"] : CustomerCodes.Bunnings,
                senderName: args != null && args.ContainsKey("senderName") ? (string)args["senderName"] : CustomerCodes.Bunnings,
                specialInstructions: args != null && args.ContainsKey("specialInstructions") ? (string)args["specialInstructions"] : "specialInstructions",
                timeInfo: args != null && args.ContainsKey("timeInfo") ? (TimeInfoDTO)args["timeInfo"] : GoodTimeInfoDTO(),
                address: args != null && args.ContainsKey("address") ? (AddressDTO)args["address"] : GoodAddressDTO(),
                contact: args != null && args.ContainsKey("contact") ? (ContactDTO)args["contact"] : GoodContactDTO(),
                pallets: args != null && args.ContainsKey("pallets") ? (PalletDto)args["pallets"] : PalletDto.Empty(),
                items: args != null && args.ContainsKey("items") ? (List<PickupItemDTO>)args["items"] : GoodPickupItemDTOs()

            );

        }

        private void ClearDatabase()
        {
            string query =
                $"DELETE FROM dbo.[Pickups] where PickupId={(EntityTypeRange.Pickup.ToInt()+2).ToString()};";

            using (var connection = new SqlConnection(_connectionString))
            {
                var command = new SqlCommand(query, connection)
                {
                    CommandType = CommandType.Text
                };

                connection.Open();
                command.ExecuteNonQuery();
            }
        }

    }
}
