﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using Autofac;
using FluentAssertions;
using FluentValidation;
using MediatR;
using PickupSvc.API.Application.CommandsQueries.PickupItems;
using PickupSvc.API.Application.DTOs;
using PickupSvc.API.Application.Validations;
using PickupSvc.TheDomain.Enumerators;
using PickupSvc.TheDomain.Exceptions;
using Telerik.JustMock;
using Xunit;

namespace PickupSvc.API.IntegrationTesting.ValidationTests
{
    public class PickupItemsByNumberQueryValidatorTests : BaseIntegrationTest
    {
        [InlineData((int)EntityTypeRange.Pickup, 0, 1)]
        [InlineData(0, 0, 0)]
        [Theory]
        public void PickupItemsbyNumber_validator_Number(int id, int numErrors, int numResult)
        {

            //Arrange 
            var errors = new List<string>();

            errors.Add(ErrorMsg.NoErrors);
            var _mediator = _containerScope.Resolve<IMediator>();


            var command = new PickupItemsByNumberQuery(id);

            var defaultResponse = new List<PickupItemDTO>();


            //Act

            try
            {
                var response = _mediator.Send(command, Arg.IsAny<CancellationToken>()).Result;
                defaultResponse = response.ToList();
            }
            catch (AggregateException e) when (e.InnerException.InnerException.GetType() == typeof(ValidationException))
            {
                errors = ((ValidationException)e.InnerException.InnerException)?.Errors
                    .Select(v => v.ErrorMessage).ToList();

            }
            catch (AggregateException e) when (e.InnerException.GetType() == typeof(PickupDomainException))
            {

                errors.Add(((PickupDomainException)e.InnerException)?.Message);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                errors.Clear();
                errors.Add(e.Message);
            }



            //Assert

            errors.Count.Should().Be(numErrors);
            defaultResponse.Count.Should().BeGreaterOrEqualTo(numResult);



        }
    }
}
