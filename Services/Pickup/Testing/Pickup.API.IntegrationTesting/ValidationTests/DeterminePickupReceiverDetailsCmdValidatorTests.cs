﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using Autofac;
using Bex.Utilities.Helper;
using FluentAssertions;
using FluentValidation;
using MediatR;
using PickupSvc.API.Application.Commands.Pickups;
using PickupSvc.API.Application.Validations;
using PickupSvc.API.IntegrationTesting.ReferenceData;
using PickupSvc.TheDomain.Enumerators;
using PickupSvc.TheDomain.Exceptions;
using Telerik.JustMock;
using Xunit;

namespace PickupSvc.API.IntegrationTesting.ValidationTests
{
    public class DeterminePickupReceiverDetailsCmdValidatorTests : BaseIntegrationTest
    {
        //TODO Clean


        private IMediator _mediator;

        [MemberData(nameof(HappyData))]
        [Theory]
        public void DeterminePickupReceiverDetails_PickupId_validator(DeterminePickupReceiverDetailsCommand command, int numErrors, string errorMsg)
        {

            //Arrange 
            _mediator = _containerScope.Resolve<IMediator>();

            
            var errors = new List<string>();
            errors.Add(ErrorMsg.NoErrors);

            //Act

            try
            {
                var response = _mediator.Send(command, Arg.IsAny<CancellationToken>()).Result;
            }
            catch (AggregateException e) when (e.InnerException.InnerException.GetType() == typeof(ValidationException))
            {
                errors = ((ValidationException)e.InnerException.InnerException)?.Errors
                    .Select(v => v.ErrorMessage).ToList();

            }
            catch (AggregateException e) when (e.InnerException.GetType() == typeof(PickupDomainException))
            {

                errors.Add(((PickupDomainException)e.InnerException)?.Message);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                errors.Clear();
                errors.Add(e.Message);
            }



            //Assert

            errors.Count.Should().Be(numErrors);

            errors[0].Should().Be(errorMsg);


        }

        






        public static List<object[]> HappyData()
        {
            return new List<object[]>
            {
                new object[] {
                    FakeRequest( new Dictionary<string, object>()
                    {
                        [CmdParams.PickupItem.number]=EntityTypeRange.Pickup.ToInt(),

                    } ) , 0 , ErrorMsg.NoErrors }


            };
        }

        public static List<object[]> InvalidPickupNumberData()
        {
            return new List<object[]>
            {
                new object[] {
                    FakeRequest( new Dictionary<string, object>()
                    {
                        [CmdParams.PickupItem.number]=EntityTypeRange.Pickup.ToInt(-100),

                    } ) , 1 , $"Pickup With Id:{EntityTypeRange.Pickup.ToInt(-100)} does not Exist" }


            };
        }




        private static DeterminePickupReceiverDetailsCommand FakeRequest(Dictionary<string, object> args = null)
        {


            return new DeterminePickupReceiverDetailsCommand(
            pickupId: args != null && args.ContainsKey("pickupId") ? (int)args["pickupId"] : EntityTypeRange.Pickup.ToInt(),
            pickupBranchId: args != null && args.ContainsKey("pickupBranchId") ? (int)args["pickupBranchId"] : EntityTypeRange.Branch.ToInt()
            
            );

        }

        

    }
}

