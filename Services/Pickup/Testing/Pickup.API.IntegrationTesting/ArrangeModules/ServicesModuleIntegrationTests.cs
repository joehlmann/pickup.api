﻿using Autofac;
using EFSecondLevelCache.Core.Contracts;
using EventBusAbstract;
using EventBusAbstract.Abstractions;
using IntegrationEventLog.Services;
using Microsoft.Data.SqlClient;
using PickupSvc.API.Application.DomainServices;
using PickupSvc.API.Application.DomainServices.Interfaces;
using PickupSvc.API.Application.IntegrationEvents;
using PickupSvc.API.Infrastructure.Authorization;
using PickupSvc.API.Infrastructure.DelegateHandlers;
using PickupSvc.Infrastructure.Idempotency;
using PickupSvc.Infrastructure.TokenContext;
using Telerik.JustMock;

namespace PickupSvc.API.IntegrationTesting.ArrangeModules
{
    public class ServicesModuleIntegrationTests : Module
    {
        protected string _connectionString;

        public ServicesModuleIntegrationTests(string connectionString)
        {
            _connectionString = connectionString;
        }


        private void LoadMocks(ContainerBuilder builder)
        {

            //EventBus
            builder.RegisterInstance(Mock.Create<IEventBus>())
                .As<IEventBus>()
                .SingleInstance();

            //Services
            
            builder.RegisterInstance(Mock.Create<IEFCacheServiceProvider>())
                .As<IEFCacheServiceProvider>()
                .SingleInstance();

        }


        protected override void Load(ContainerBuilder builder)
        {

            LoadMocks(builder);

            // Authentication 
            builder.RegisterType<TokenContextService>()
                .As<ITokenContextService>()
                .InstancePerLifetimeScope();


            builder.RegisterType<BorderExpressAssertions>()
                .As<IAssertions>()
                .InstancePerLifetimeScope();



            //Domain Services

            builder.RegisterType<ChargeAccountValidService>()
                .As<IChargeAccountValidService>()
                .SingleInstance();

            builder.RegisterType<EntityIdValidService>()
                .As<IEntityIdValidService>()
                .InstancePerLifetimeScope();

            builder.RegisterType<AddressValidService>()
                .As<IAddressValidService>()
                .InstancePerLifetimeScope();

            builder.RegisterType<BranchValidService>()
                .As<IBranchValidService>()
                .InstancePerLifetimeScope();

            builder.RegisterType<ContactValidService>()
                .As<IContactValidService>()
                .InstancePerLifetimeScope();

            builder.RegisterType<DGoodsValidService>()
                .As<IDGoodsValidService>()
                .InstancePerLifetimeScope();

            builder.RegisterType<PalletValidService>()
                .As<IPalletValidService>()
                .InstancePerLifetimeScope();

            builder.RegisterType<PickupItemValidService>()
                .As<IPickupItemValidService>()
                .InstancePerLifetimeScope();

            builder.RegisterType<TimeInfoValidService>()
                .As<ITimeInfoValidService>()
                .InstancePerLifetimeScope();

            builder.RegisterType<UserValidService>()
                .As<IUserValidService>()
                .InstancePerLifetimeScope();

            builder.RegisterType<PickupValidService>()
                .As<IPickupValidService>()
                .InstancePerLifetimeScope();


            builder.RegisterType<PostCodeSuburbValidService>()
                .As<IPostCodeSuburbValidService>()
                .InstancePerLifetimeScope();


            // Infra Services


            builder.RegisterType<PickupIntegrationEventSvc>()
                .As<IPickupIntegrationEventSvc>()
                .InstancePerLifetimeScope();


            builder.RegisterType<RequestManager>()
                .As<IRequestManager>()
                .InstancePerLifetimeScope();

            builder.RegisterType<PickupNumberService>()
                .WithParameter("connectionString", _connectionString)
                .As<IPickupNumberService>()
                .InstancePerLifetimeScope();

            builder.RegisterType<IntegrationEventLogService>()
                .WithParameter("dbConnection", new SqlConnection(_connectionString))
                .As<IIntegrationEventLogService>()
                .InstancePerLifetimeScope();



            builder.RegisterType<HttpClientCorrelationIdDelegatingHandler>()
                .As<HttpClientCorrelationIdDelegatingHandler>()
                .InstancePerLifetimeScope();  // Authentication 
            builder.RegisterType<TokenContextService>()
                .As<ITokenContextService>()
                .InstancePerLifetimeScope();


            builder.RegisterType<BorderExpressAssertions>()
                .As<IAssertions>()
                .InstancePerLifetimeScope();



            //Domain Services

            builder.RegisterType<ChargeAccountValidService>()
                .As<IChargeAccountValidService>()
                .SingleInstance();

            builder.RegisterType<EntityIdValidService>()
                .As<IEntityIdValidService>()
                .InstancePerLifetimeScope();

            builder.RegisterType<AddressValidService>()
                .As<IAddressValidService>()
                .InstancePerLifetimeScope();

            builder.RegisterType<BranchValidService>()
                .As<IBranchValidService>()
                .InstancePerLifetimeScope();

            builder.RegisterType<ContactValidService>()
                .As<IContactValidService>()
                .InstancePerLifetimeScope();

            builder.RegisterType<DGoodsValidService>()
                .As<IDGoodsValidService>()
                .InstancePerLifetimeScope();

            builder.RegisterType<PalletValidService>()
                .As<IPalletValidService>()
                .InstancePerLifetimeScope();

            builder.RegisterType<PickupItemValidService>()
                .As<IPickupItemValidService>()
                .InstancePerLifetimeScope();

            builder.RegisterType<TimeInfoValidService>()
                .As<ITimeInfoValidService>()
                .InstancePerLifetimeScope();

            builder.RegisterType<UserValidService>()
                .As<IUserValidService>()
                .InstancePerLifetimeScope();



            // Infra Services


            builder.RegisterType<PickupIntegrationEventSvc>()
                .As<IPickupIntegrationEventSvc>()
                .InstancePerLifetimeScope();

            builder.RegisterType<PickupNumberService>()
                .WithParameter("connectionString", _connectionString)
                .As<IPickupNumberService>()
                .InstancePerLifetimeScope();


            builder.RegisterType<RequestManager>()
                .As<IRequestManager>()
                .InstancePerLifetimeScope();


            builder.RegisterType<IntegrationEventLogService>()
                .WithParameter("dbConnection", new SqlConnection(_connectionString))
                .As<IIntegrationEventLogService>()
                .InstancePerLifetimeScope();



            builder.RegisterType<HttpClientCorrelationIdDelegatingHandler>()
                .As<HttpClientCorrelationIdDelegatingHandler>()
                .InstancePerLifetimeScope();





            //Singleton
            builder.RegisterType<InMemoryEventBusSubscriptionsManager>()
                .As<IEventBusSubscriptionsManager>()
                .SingleInstance();

            builder.RegisterType<RequestManager>()
                .As<IRequestManager>()
                .SingleInstance();


            //Singleton
            builder.RegisterType<InMemoryEventBusSubscriptionsManager>()
                .As<IEventBusSubscriptionsManager>()
                .SingleInstance();

            builder.RegisterType<RequestManager>()
                .As<IRequestManager>()
                .SingleInstance();



        }
    }
}
