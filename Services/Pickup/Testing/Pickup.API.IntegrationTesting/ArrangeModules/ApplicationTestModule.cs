﻿using Autofac;
using PickupSvc.API.Application.CommandsQueries.CQRSQueries;
using PickupSvc.Infrastructure.Repositories;
using PickupSvc.Infrastructure.Repositories.Interfaces;

namespace PickupSvc.API.IntegrationTesting.ArrangeModules
{

    public class ApplicationTestModule
        :Module
    {

        public string ConnectionString { get; }

        

        public ApplicationTestModule(string connectionString)
        {
            ConnectionString = connectionString;
        
        }

        protected override void Load(ContainerBuilder builder)
        {
            

            builder.Register(c => new PickupQueries(ConnectionString))
                .As<IPickupQueries>()
                .InstancePerLifetimeScope();

          

            builder.RegisterType<CustomerRepository>()
                .As<ICustomerRepository>()
                .InstancePerLifetimeScope();

            builder.RegisterType<UserRepository>()
                .As<IUserRepository>()
                .InstancePerLifetimeScope();

            builder.RegisterType<PickupRepository>()
                .As<IPickupRepository>()
                .InstancePerLifetimeScope();

            builder.RegisterType<SuburbRepository>()
                .As<ISuburbRepository>()
                .InstancePerLifetimeScope();
            

            builder.RegisterType<DangerousGoodRepository>()
                .As<IDangerousGoodRepository>()
                .InstancePerLifetimeScope();

            builder.RegisterType<BranchRepository>()
                .As<IBranchRepository>()
                .InstancePerLifetimeScope();

          
            

        }
    }
}
