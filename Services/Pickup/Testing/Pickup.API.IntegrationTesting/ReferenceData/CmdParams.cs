﻿namespace PickupSvc.API.IntegrationTesting.ReferenceData
{
   public static class CmdParams
    {

        public static class Customer
        {
            public static string clientNumber = "clientNumber";
            public static string companyName = "companyName";
            public static string address1 = "address1";
            public static string address2 = "address2";
            public static string state = "state";
            public static string suburb = "suburb";
            public static string postCode = "postCode";
            public static string contactName = "contactName";
            public static string contactPhone = "contactPhone";
            public static string email = "email";

        }

        public static class Pickup
        {
            public static string number = "number";
            public static string bookedByUserId = "bookedByUserId";
            public static string userId = "userId";
            public static string chargeAccountCode = "chargeAccountCode";
            public static string senderCode = "senderCode";
            public static string senderName = "senderName";
            public static string specialInstructions = "specialInstructions";
            public static string timeInfo = "timeInfo";
            public static string address = "address";
            public static string contact = "contact";
            public static string pallets = "pallets";
            public static string items = "items";

        }

        public static class PickupItem
        {
            public static string number = "number";
            public static string bookedByUserId = "bookedByUserId";
            public static string userId = "userId";
            public static string chargeAccountCode = "chargeAccountCode";
            public static string senderCode = "senderCode";
            public static string senderName = "senderName";
            public static string specialInstructions = "specialInstructions";
            public static string timeInfo = "timeInfo";
            public static string address = "address";
            public static string contact = "contact";
            public static string pallets = "pallets";
            public static string items = "items";

        }



        public static class User
        {
            public static string userId = "userId";
            public static string aliasName = "aliasName";
            public static string phone = "phone";
            public static string userImages = "userImages";

        }

       

        
       

       

        public static class Identified
        {
            public static string command = "command";
            public static string id = "id";

        }
    }
}
