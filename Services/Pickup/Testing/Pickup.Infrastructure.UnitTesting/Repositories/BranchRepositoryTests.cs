﻿using Autofac;
using AutoMapper.Contrib.Autofac.DependencyInjection;
using MediatR;
using PickupSvc.Infrastructure.Repositories.Interfaces;
using PickupSvc.Infrastructure.UnitTesting.ArrangeModules;
using Telerik.JustMock;
using Xunit;

namespace PickupSvc.Infrastructure.UnitTesting.Repositories
{
    
    public class BranchRepositoryTests
    {
        private readonly IMediator _mediator;
        private readonly ILifetimeScope _container;
        private readonly PickupContext _context;
        private readonly IPickupRepository _repository;

        public BranchRepositoryTests()
        {
            _context = Mock.Create<PickupContext>();
            var builder = new ContainerBuilder();
            builder.RegisterInstance(_context).As<PickupContext>();
            builder.AddAutoMapper(typeof(Startup).Assembly);
            builder.RegisterModule(new MediatorModuleForTesting());
            _container = builder.Build();
            _mediator = _container.Resolve<IMediator>();
        }



        [Fact]
        public void BranchRepositoryGetById_ValidId_ReturnedDto()
        {


        }

    }
}
