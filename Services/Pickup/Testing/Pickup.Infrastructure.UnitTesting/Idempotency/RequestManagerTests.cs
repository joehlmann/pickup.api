﻿using Autofac;
using AutoMapper.Contrib.Autofac.DependencyInjection;
using MediatR;
using PickupSvc.Infrastructure.Repositories.Interfaces;
using PickupSvc.Infrastructure.UnitTesting.ArrangeModules;
using Telerik.JustMock;
using Xunit;

namespace PickupSvc.Infrastructure.UnitTesting.Idempotency
{
    
    public class RequestManagerTests
    {
        private readonly IMediator _mediator;
        private readonly ILifetimeScope _container;
        private readonly PickupContext _context;
        private readonly IPickupRepository _repository;

        public RequestManagerTests()
        {
            _context = Mock.Create<PickupContext>();
            var builder = new ContainerBuilder();
            builder.RegisterInstance(_context).As<PickupContext>();
            builder.AddAutoMapper(typeof(Startup).Assembly);
            builder.RegisterModule(new MediatorModuleForTesting());
            _container = builder.Build();
            _mediator = _container.Resolve<IMediator>();
        }



        [Fact]
        public void RequestManagerGetById_NewId_Success()
        {


        }

        [Fact]
        public void RequestManagerGetById_RepeatedId_Fail()
        {


        }

    }
}
