﻿using Autofac;
using FluentAssertions;
using PickupSvc.API.Application.DomainServices.Interfaces;
using PickupSvc.API.Application.DTOs;
using Xunit;

namespace PickupSvc.UnitTesting.DomainServices
{
    public class ContactValidServiceTests : BaseUnitTest
    {
        private IContactValidService _contactValidService;



        [Theory]
        //[InlineData("", false)]
        //[InlineData("abcdefghijklmnopqrstwewqdwqaxaqwdadfewawdawdawdawdawd", false)]
        [InlineData("John Smith", true)]
        public void ContactName_For_dto_ValidContact(string value, bool expectedResult)
        {
            //Arrange 

            _contactValidService = _containerScope.Resolve<IContactValidService>();
            var dto = GoodContactDTO();
            dto.ContactName = value;

            //Act

            var result = _contactValidService.ValidContactAsync(dto).Result;

            //Assert

            result.IsSuccess.Should().Be(expectedResult);
        }


        [Theory]
        [InlineData("", false)]
        [InlineData("abcdefghijklmnopqrstwewqdwqaxaqwdadfewawdawdawdawdawdabcdefghijklmnopqrstwewqdwqaxaqwdadfewawdawdawdawdawd", false)]
        [InlineData("John@componay.com", true)]
        public void Email_For_dto_ValidContact(string value, bool expectedResult)
        {
            //Arrange 

            _contactValidService = _containerScope.Resolve<IContactValidService>();
            var dto = GoodContactDTO();
            dto.Email = value;

            //Act

            var result = _contactValidService.ValidContactAsync(dto).Result;

            //Assert

            result.IsSuccess.Should().Be(expectedResult);
        }
        [Theory]
        [InlineData("", false)]
        [InlineData("123456", false)]
        [InlineData("12345678910", false)]
        [InlineData("1234567", true)]
        public void Phone_For_dto_ValidContact(string value, bool expectedResult)
        {
            //Arrange 

            _contactValidService = _containerScope.Resolve<IContactValidService>();
            var dto = GoodContactDTO();
            dto.Phone = value;

            //Act

            var result = _contactValidService.ValidContactAsync(dto).Result;

            //Assert

            result.IsSuccess.Should().Be(expectedResult);
        }

        private static ContactDTO GoodContactDTO()
        {
            return new ContactDTO()
            {
                ContactName = "fred",
                Email = "1@1.com",
                Phone = "1234567"
            };
        }
    }
}
