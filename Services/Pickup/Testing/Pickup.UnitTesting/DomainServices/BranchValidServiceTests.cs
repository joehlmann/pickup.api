﻿using Autofac;
using FluentAssertions;
using PickupSvc.API.Application.DomainServices.Interfaces;
using PickupSvc.TheDomain.Enumerators;
using Xunit;

namespace PickupSvc.UnitTesting.DomainServices
{
    public class BranchValidServiceTests : BaseUnitTest
    {
        private IBranchValidService _branchValidService;



        [Theory]
        [InlineData(0, false)]
        [InlineData((int)EntityTypeRange.Branch, true)]
        public void BranchId_For_ValidBranch(int value, bool expectedResult)
        {
            //Arrange 

            _branchValidService = _containerScope.Resolve<IBranchValidService>();
            

            //Act

            var result = _branchValidService.ValidBranchAsync(value).Result;

            //Assert

            result.IsSuccess.Should().Be(expectedResult);
        }

        [Theory]
        [InlineData((int)EntityTypeRange.Branch, 0, false)]
        [InlineData((int)EntityTypeRange.Branch,(int)EntityTypeRange.Pickup ,true)]
        public void PickupId_For_ValidBranchForPickup(int value,int pickupNumber  ,bool expectedResult)
        {
            //Arrange 

            _branchValidService = _containerScope.Resolve<IBranchValidService>();


            //Act

            var result = _branchValidService.ValidBranchForPickupAsync(value,pickupNumber.ToString()).Result;

            //Assert

            result.IsSuccess.Should().Be(expectedResult);
        }
    }
}
