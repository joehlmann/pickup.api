﻿using Autofac;
using FluentAssertions;
using PickupSvc.API.Application.DomainServices.Interfaces;
using PickupSvc.TheDomain.Enumerators;
using Xunit;

namespace PickupSvc.UnitTesting.DomainServices
{
    
    public class PickupValidServiceTests : BaseUnitTest
    {

        private IPickupValidService _pickupValidService;

        public PickupValidServiceTests()
        {
            
        }



        [Theory]
        [InlineData(1, false)]
        [InlineData((int)EntityTypeRange.Pickup, true)]
        public void PickupId_for_PickupValidService(int id, bool expectedResult)
        {
            //Arrange 

            _pickupValidService = _containerScope.Resolve<IPickupValidService>();

            //Act

            var result = _pickupValidService.ValidPickupIdAsync(id).Result;

            //Assert

            result.IsSuccess.Should().Be(expectedResult);
        }

        [Theory]
        [InlineData(1, false)]
        [InlineData((int)EntityTypeRange.Pickup, true)]
        public void PickupNumber_for_PickupValidService(int id, bool expectedResult)
        {
            //Arrange 

            _pickupValidService = _containerScope.Resolve<IPickupValidService>();

            //Act

            var result = _pickupValidService.ValidPickupIdAsync(id).Result;

            //Assert

            result.IsSuccess.Should().Be(expectedResult);
        }
        [Theory]
        [InlineData("1", false)]
        [InlineData("5000", true)]
        public void PickupNumberString_for_PickupValidService(string number, bool expectedResult)
        {
            //Arrange 

            _pickupValidService = _containerScope.Resolve<IPickupValidService>();

            //Act

            var result = _pickupValidService.ValidPickupNumberAsync(number).Result;

            //Assert

            result.IsSuccess.Should().Be(expectedResult);
        }

    }
}
