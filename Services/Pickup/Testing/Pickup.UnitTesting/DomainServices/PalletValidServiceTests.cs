﻿using Autofac;
using FluentAssertions;
using PickupSvc.API.Application.DomainServices.Interfaces;
using PickupSvc.API.Application.DTOs;
using PickupSvc.TheDomain.Enumerators;
using Xunit;

namespace PickupSvc.UnitTesting.DomainServices
{
    public class PalletValidServiceTests : BaseUnitTest
    {
        private IPalletValidService _palletValidService;



        [Theory]
        [InlineData("", false)]
        [InlineData("3000", true)]
        public void ChepDocNum_For_dto_ValidPallet(string value, bool expectedResult)
        {
            //Arrange 

            _palletValidService = _containerScope.Resolve<IPalletValidService>();
            var dto = GoodDTO();
            dto.ChepPalletTransferDocketNumber = value;

            //Act

            var result = _palletValidService.ValidPalletAsync(dto).Result;

            //Assert

            result.IsSuccess.Should().Be(expectedResult);
        }

        [Theory]
        [InlineData("", false)]
        [InlineData("3000", true)]
        public void LosCamDocNum_For_dto_ValidPallet(string value, bool expectedResult)
        {
            //Arrange 

            _palletValidService = _containerScope.Resolve<IPalletValidService>();
            var dto = GoodDTO();
            dto.LoscamPalletTransferDocketNumber = value;

            //Act

            var result = _palletValidService.ValidPalletAsync(dto).Result;

            //Assert

            result.IsSuccess.Should().Be(expectedResult);
        }


        private static PalletDto GoodDTO()
        {
            return new PalletDto()
            {
                ChepPalletTransferDocketNumber = "1000",
                LoscamPalletTransferDocketNumber = "1000",
                ExchangeChepPalletsIn = 1,
                ExchangeChepPalletsOut = 1,
                ExchangeLoscamPalletsIn = 1,
                ExchangeLoscamPalletsOut = 1,
                TransferChepPalletsToBex = 1,
                TransferChepPalletsToReceiver = 1,
                PalletType = PalletType.Both
            };
        }
    }
}
