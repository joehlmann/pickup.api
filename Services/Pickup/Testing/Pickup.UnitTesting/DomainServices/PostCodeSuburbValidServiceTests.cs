﻿using Autofac;
using FluentAssertions;
using PickupSvc.API.Application.DomainServices.Interfaces;
using PickupSvc.TheDomain.Enumerators;
using Xunit;

namespace PickupSvc.UnitTesting.DomainServices
{
    public class PostCodeSuburbValidServiceTests : BaseUnitTest
    {
        private IPostCodeSuburbValidService _postCodeSuburbValidService;

        public PostCodeSuburbValidServiceTests()
        {

        }


        [Theory]
        [InlineData(200, false)]
        [InlineData((int)PostCode.Vic, true)]
        public void ValidPostCode_For_PostCode_ValidService(int postcode, bool expectedResult)
        {
            //Arrange 

            _postCodeSuburbValidService = _containerScope.Resolve<IPostCodeSuburbValidService>();

            //Act

            var result = _postCodeSuburbValidService.ValidPostCodeAsync(postcode).Result;

            //Assert

            result.IsSuccess.Should().Be(expectedResult);
        }

        [Theory]
        [InlineData("Unknown", false)]
        [InlineData("Caulfield", true)]
        public void ValidSuburb_For_suburb_ValidService(string suburb, bool expectedResult)
        {
            //Arrange 

            _postCodeSuburbValidService = _containerScope.Resolve<IPostCodeSuburbValidService>();

            //Act

            var result = _postCodeSuburbValidService.ValidSuburbAsync(suburb).Result;

            //Assert

            result.IsSuccess.Should().Be(expectedResult);
        }
    }
}
