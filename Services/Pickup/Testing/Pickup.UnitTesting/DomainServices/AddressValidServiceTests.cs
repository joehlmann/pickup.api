﻿using Autofac;
using FluentAssertions;
using PickupSvc.API.Application.DomainServices.Interfaces;
using PickupSvc.API.Application.DTOs;
using Xunit;

namespace PickupSvc.UnitTesting.DomainServices
{
    public class AddressValidServiceTests : BaseUnitTest
    {
        private IAddressValidService _addressValidService;

        

        [Theory]
        [InlineData("", false)]
        [InlineData("abc", false)]
        [InlineData("1", false)]
        [InlineData("3000", true)]
        public void PostCode_For_dto_ValidService(string value, bool expectedResult)
        {
            //Arrange 

            _addressValidService = _containerScope.Resolve<IAddressValidService>();
            var dto = GoodAddressDTO();
            dto.PostCode = value;

            //Act

            var result = _addressValidService.ValidAddressAsync(dto).Result;

            //Assert

            result.IsSuccess.Should().Be(expectedResult);
        }

        [Theory]
        [InlineData("", false)]
        [InlineData("A address that is here", true)]
        public void Address1_For_dto_ValidService(string value, bool expectedResult)
        {
            //Arrange 

            _addressValidService = _containerScope.Resolve<IAddressValidService>();
            var dto = GoodAddressDTO();
            dto.Address1 = value;

            //Act

            var result = _addressValidService.ValidAddressAsync(dto).Result;

            //Assert

            result.IsSuccess.Should().Be(expectedResult);
        }

        [Theory]
        [InlineData("", false)]
        [InlineData("aaa", false)]
        [InlineData("Vic", true)]
        public void State_For_dto_ValidService(string value, bool expectedResult)
        {
            //Arrange 

            _addressValidService = _containerScope.Resolve<IAddressValidService>();
            var dto = GoodAddressDTO();
            dto.State = value;

            //Act

            var result = _addressValidService.ValidAddressAsync(dto).Result;

            //Assert

            result.IsSuccess.Should().Be(expectedResult);
        }


        [Theory]
        //[InlineData("", false)]
        //[InlineData("aa", false)]
        [InlineData("Caulfield", true)]
        public void Suburb_For_dto_ValidService(string value, bool expectedResult)
        {
            //Arrange 

            _addressValidService = _containerScope.Resolve<IAddressValidService>();
            var dto = GoodAddressDTO();
            dto.Suburb = value;

            //Act

            var result = _addressValidService.ValidAddressAsync(dto).Result;

            //Assert

            result.IsSuccess.Should().Be(expectedResult);
        }

        private static AddressDTO GoodAddressDTO()
        {
            return new AddressDTO()
            {
                Address1 = "unit 1 ",
                Address2 = "456 happy street",
                PostCode = "3162",
                State = "VIC",
                Suburb = "Caulfield"
            };
        }
    }
}
