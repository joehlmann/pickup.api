﻿using Autofac;
using FluentAssertions;
using PickupSvc.API.Application.DomainServices.Interfaces;
using PickupSvc.TheDomain.Enumerators;
using Xunit;

namespace PickupSvc.UnitTesting.DomainServices
{
    
    public class UserServiceTests : BaseUnitTest
    {
        private IUserValidService _userValidService;

        public UserServiceTests()
        {

        }


        [Theory]
        [InlineData(((int) EntityTypeRange.User - 2000), false)]
        [InlineData((int) EntityTypeRange.User, true)]
        public void ValidUser_For_UserId_UserValidService(int userId, bool expectedResult)
        {
            //Arrange 

            _userValidService = _containerScope.Resolve<IUserValidService>();

            //Act

            var result = _userValidService.ValidUserIdAsync(userId).Result;

            //Assert

            result.IsSuccess.Should().Be(expectedResult);
        }

    }
}
