﻿using System.Collections.Generic;
using Autofac;
using Bex.Utilities.Helper;
using FluentAssertions;
using PickupSvc.API.Application.DomainServices.Interfaces;
using PickupSvc.API.Application.DTOs;
using PickupSvc.TheDomain.Enumerators;
using Xunit;

namespace PickupSvc.UnitTesting.DomainServices
{
    public class PickupItemValidServiceTests : BaseUnitTest
    {
        private IPickupItemValidService _pickupItemValidService;

        public PickupItemValidServiceTests()
        {

        }


        [Theory]
        [InlineData(-1, false)]
        [InlineData(1, true)]
        public void Weight_For_dto_ValidService(int weight, bool expectedResult)
        {
            //Arrange 

            _pickupItemValidService = _containerScope.Resolve<IPickupItemValidService>();
            var dto = GoodPickupItemDTO();
            dto.WeightKg = weight;

            //Act

            var result = _pickupItemValidService.ValidPickupItemAsync(dto).Result;

            //Assert

            result.IsSuccess.Should().Be(expectedResult);
        }

        [Theory]
        [InlineData(-1, false)]
        [InlineData((int)EntityTypeRange.PickupItem, true)]
        public void Id_For_dto_ValidService(int id, bool expectedResult)
        {
            //Arrange 

            _pickupItemValidService = _containerScope.Resolve<IPickupItemValidService>();
            

            //Act

            var result = _pickupItemValidService.ValidPickupItemIdAsync(id).Result;

            //Assert

            result.IsSuccess.Should().Be(expectedResult);
        }

        [Theory]
        [InlineData("", false)]
        [InlineData("A description", true)]
        public void Description_For_dto_ValidService(string desc, bool expectedResult)
        {
            //Arrange 

            _pickupItemValidService = _containerScope.Resolve<IPickupItemValidService>();
            var dto = GoodPickupItemDTO();
            dto.Description = desc;

            //Act

            var result = _pickupItemValidService.ValidPickupItemAsync(dto).Result;

            //Assert

            result.IsSuccess.Should().Be(expectedResult);
        }

        [Theory]
        [InlineData(-1, false)]
        [InlineData(1, true)]
        public void Quantity_For_dto_ValidService(int quantity, bool expectedResult)
        {
            //Arrange 

            _pickupItemValidService = _containerScope.Resolve<IPickupItemValidService>();
            var dto = GoodPickupItemDTO();
            dto.Quantity = quantity;

            //Act

            var result = _pickupItemValidService.ValidPickupItemAsync(dto).Result;

            //Assert

            result.IsSuccess.Should().Be(expectedResult);
        }


        private static PickupItemDTO GoodPickupItemDTO()
        {
            return
                new PickupItemDTO()
                {
                    Id = EntityTypeRange.Pickup.ToInt(),
                    Description = "N95 Masks",
                    WeightKg = 1,
                    Quantity = 1,
                    DangerousGoods = new List<DangerousGoodItemDTO>(),
                    FoodStuff = false
                };


        }
    }
}
