﻿using System;
using Autofac;
using FluentAssertions;
using PickupSvc.API.Application.DomainServices.Interfaces;
using PickupSvc.API.Application.DTOs;
using Xunit;

namespace PickupSvc.UnitTesting.DomainServices
{
    public class TimeInfoValidServiceTests : BaseUnitTest
    {
        private ITimeInfoValidService _timeInfoValidService;


        [Theory]
        [InlineData("", false)]
        [InlineData("99", false)]
        [InlineData("12", true)]
        public void ReadyTime_For_dto_ValidTimeInfo(string value, bool expectedResult)
        {
            //Arrange 

            _timeInfoValidService = _containerScope.Resolve<ITimeInfoValidService>();
            var dto = GoodDTO();

            var adate = $"12/{value}/2020";
            dto.ReadyTime = adate;

            //Act

            var result = _timeInfoValidService.ValidTimeInfoAsync(dto).Result;

            //Assert

            result.IsSuccess.Should().Be(expectedResult);
        }

        [Theory]
        [InlineData("", false)]
        [InlineData("99", false)]
        [InlineData("12", true)]
        public void BookedTime_For_dto_ValidTimeInfo(string value, bool expectedResult)
        {
            //Arrange 

            _timeInfoValidService = _containerScope.Resolve<ITimeInfoValidService>();
            var dto = GoodDTO();

            var adate = $"12/{value}/2020";
            dto.BookedDateTime = adate;

            //Act

            var result = _timeInfoValidService.ValidTimeInfoAsync(dto).Result;

            //Assert

            result.IsSuccess.Should().Be(expectedResult);
        }
        [Theory]
        [InlineData("", false)]
        [InlineData("99", false)]
        [InlineData("12", true)]
        public void CloseTime_For_dto_ValidTimeInfo(string value, bool expectedResult)
        {
            //Arrange 

            _timeInfoValidService = _containerScope.Resolve<ITimeInfoValidService>();
            var dto = GoodDTO();

            var adate = $"12/{value}/2020";
            dto.CloseTime = adate;

            //Act

            var result = _timeInfoValidService.ValidTimeInfoAsync(dto).Result;

            //Assert

            result.IsSuccess.Should().Be(expectedResult);
        }

        private static TimeInfoDTO GoodDTO()
        {
            return new TimeInfoDTO()
            {
                ReadyTime = DateTime.Now.AddDays(0).ToShortDateString(),
                BookedDateTime = DateTime.Now.AddDays(1).ToShortDateString(),
                CloseTime = DateTime.Now.AddDays(0).ToShortTimeString(),


            };
        }
    }
}
