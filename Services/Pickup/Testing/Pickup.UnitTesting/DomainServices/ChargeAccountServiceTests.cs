﻿using Autofac;
using FluentAssertions;
using PickupSvc.API.Application.DomainServices.Interfaces;
using PickupSvc.TheDomain.Enumerators;
using Xunit;

namespace PickupSvc.UnitTesting.DomainServices
{
    
    public class ChargeAccountServiceTests : BaseUnitTest
    {
        private IChargeAccountValidService _chargeAccountValidService;


        public ChargeAccountServiceTests()
        {

        }



        [Theory]
        [InlineData("INVALIDCODE", false)]
        [InlineData("BUN", true)]
        public void Validates_ValidChargeAccount_for_ChargeAccountValidService(string account , bool expectedResult)
        {
            //Arrange 

            _chargeAccountValidService = _containerScope.Resolve<IChargeAccountValidService>();

            //Act

            var result = _chargeAccountValidService.ValidChargeAccount(account).Result;

            //Assert

            result.IsSuccess.Should().Be(expectedResult);
        }

        [Theory]
        [InlineData("INVALIDCODE",(int)EntityTypeRange.User , false)]
        [InlineData("BUN",(int)EntityTypeRange.User, true)]
        public void ValidUserForChargeAccount_For_UserId_ChargeAccountValidService(string code,int userId, bool expectedResult)
        {
            //Arrange 

            _chargeAccountValidService = _containerScope.Resolve<IChargeAccountValidService>();

            //Act

            var result = _chargeAccountValidService.ValidUserForChargeAccount(code,userId).Result;

            //Assert

            result.IsSuccess.Should().Be(expectedResult);
        }
        

    }
}
