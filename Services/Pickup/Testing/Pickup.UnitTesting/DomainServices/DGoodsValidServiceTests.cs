﻿using System.Collections.Generic;
using Autofac;
using FluentAssertions;
using PickupSvc.API.Application.DomainServices.Interfaces;
using PickupSvc.API.Application.DTOs;
using Xunit;

namespace PickupSvc.UnitTesting.DomainServices
{
    public class DGoodsValidServiceTests : BaseUnitTest
    {
        private IDGoodsValidService _dGoodsValidService;



        [Theory]
        [InlineData(0, false)]
        [InlineData(333, true)]
        public void UNNo_For_dto_ValidDgood(int value, bool expectedResult)
        {
            //Arrange 

            _dGoodsValidService = _containerScope.Resolve<IDGoodsValidService>();

            var dto = new DangerousGoodItemDTO();
            dto.UNNo = value;

            //Act

            var result = _dGoodsValidService.ValidDGoodAsync(dto).Result;

            //Assert

            result.IsSuccess.Should().Be(expectedResult);
        }

        [Theory]
        [InlineData(0,0 ,false)]
        [InlineData(333,444 ,true)]
        public void UNNos_For_dto_ValidDgoods(int value, int aValue, bool expectedResult)
        {
            //Arrange 

            _dGoodsValidService = _containerScope.Resolve<IDGoodsValidService>();

            var dtos = new List<DangerousGoodItemDTO>()
            {
                new DangerousGoodItemDTO()
                {
                    UNNo = value
                },
                new DangerousGoodItemDTO()
                {
                    UNNo = aValue
                }
            };
            //Act

            var result = _dGoodsValidService.ValidDGoodsAsync(dtos).Result;

            //Assert

            result.IsSuccess.Should().Be(expectedResult);
        }
    }
}
