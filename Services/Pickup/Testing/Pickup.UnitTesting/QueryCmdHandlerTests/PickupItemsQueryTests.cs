﻿using System.Linq;
using System.Threading;
using Autofac;
using AutoMapper.Contrib.Autofac.DependencyInjection;
using Bex.Utilities.Helper;
using FluentAssertions;
using MediatR;
using PickupSvc.API.Application.CommandsQueries.PickupItems;
using PickupSvc.Infrastructure;
using PickupSvc.Infrastructure.Repositories.Interfaces;
using PickupSvc.TheDomain.Entities;
using PickupSvc.TheDomain.Enumerators;
using PickupSvc.UnitTesting.ArrangeModules;
using Telerik.JustMock;
using Xunit;
using Startup = PickupSvc.API.Startup;

namespace PickupSvc.UnitTesting.QueryCmdHandlerTests
{
    
    public class PickupItemsQueryTests
    {

        private readonly IMediator _mediator;
        private readonly ILifetimeScope _container;
        private readonly PickupContext _context;
        private readonly IPickupRepository _repository;
        public PickupItemsQueryTests()
        {
            _context = Mock.Create<PickupContext>();
            var builder = new ContainerBuilder();
            builder.RegisterInstance(_context).As<PickupContext>();
            builder.AddAutoMapper(typeof(Startup).Assembly);
            builder.RegisterModule(new MediatorModuleForUnitTesting());
            _container = builder.Build();
            _mediator = _container.Resolve<IMediator>();
        }

        [Fact]
        public void GetPickupItems_ByPickupNumber_Count1()
        {
            //Arrange 
            var request = new PickupItemsByNumberQuery(EntityTypeRange.PickupItem.ToInt());
            var pickup = Pickup.Default();

            pickup.AddItem(PickupItem.Empty());

            Mock.Arrange(() => _context.Pickups.Add(pickup));

            //Act

            var response = _mediator.Send(request, Arg.IsAny<CancellationToken>()).Result;

            //Assert

            response.Count().Should().Be(1);
        }



    }
}
