﻿using System.Threading;
using Autofac;
using AutoMapper.Contrib.Autofac.DependencyInjection;
using MediatR;
using PickupSvc.API.Application.CommandsQueries.PickupItems;
using PickupSvc.Infrastructure;
using PickupSvc.Infrastructure.Repositories.Interfaces;
using PickupSvc.TheDomain.Entities;
using PickupSvc.UnitTesting.ArrangeModules;
using Telerik.JustMock;
using Xunit;
using Startup = PickupSvc.API.Startup;

namespace PickupSvc.UnitTesting.QueryCmdHandlerTests
{
    
    public class PickupItemQueryTests : BaseUnitTest
    {

        

        private readonly IMediator _mediator;
        private readonly ILifetimeScope _container;
        private readonly PickupContext _context;
        private readonly IPickupRepository _repository;
        public PickupItemQueryTests()
        {
            _context = Mock.Create<PickupContext>();
            var builder = new ContainerBuilder();
            builder.RegisterInstance(_context).As<PickupContext>();
            builder.AddAutoMapper(typeof(Startup).Assembly);
            builder.RegisterModule(new MediatorModuleForUnitTesting());
            _container = builder.Build();
            _mediator = _container.Resolve<IMediator>();
        }




        [Fact]
        public void GetPickupItem_ByGuid_Success()
        {
            //Arrange 

            

            var request = new PickupItemByIdQuery(0);
            var pickupItem = PickupItem.Empty();


            Mock.Arrange(() => _context.PickupItems.Add(pickupItem));

            //Act

            var response = _mediator.Send(request, Arg.IsAny<CancellationToken>()).Result;

            //Assert

            Assert.Equal(response.Id,1);
        }
        


    }
}
