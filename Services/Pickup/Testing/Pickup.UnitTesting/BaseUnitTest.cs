﻿using System;
using System.Reflection;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using AutoMapper.Contrib.Autofac.DependencyInjection;
using IntegrationEventLog;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using PickupSvc.API.Infrastructure.Modules;
using PickupSvc.Infrastructure;
using PickupSvc.UnitTesting.ArrangeModules;
using Startup = PickupSvc.API.Startup;

namespace PickupSvc.UnitTesting
{
    public class BaseUnitTest
    {
        protected IConfiguration _configuration;
        protected IServiceProvider _provider;
        protected ILifetimeScope _containerScope;
        protected IContainer _container;
        protected string _connectionString;

        public BaseUnitTest()
        {
            HibernatingRhinos.Profiler.Appender.EntityFramework.EntityFrameworkProfiler.Initialize();
            _configuration = new  ConfigurationBuilder()
                .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
                .AddJsonFile("appsettings.json")
                .AddEnvironmentVariables()
                .Build();

            _connectionString = _configuration.GetConnectionString("PickupDb");

            _provider = Configure();

            _containerScope = _container.BeginLifetimeScope();
        }

        public  IServiceProvider Configure()
        {
            
            var containerBuilder = new ContainerBuilder();


            containerBuilder.RegisterInstance(new LoggerFactory())
                .As<ILoggerFactory>();

            containerBuilder.RegisterGeneric(typeof(Logger<>))
                .As(typeof(ILogger<>));

            containerBuilder.RegisterType<PickupContext>()
                .WithParameter("options", DbContextConfig.Configure(_connectionString))
                .InstancePerLifetimeScope();


            containerBuilder.RegisterType<IntegrationEventLogContext>()
                .WithParameter("options", DbContextConfig.ConfigureEventLog(_connectionString))
                .InstancePerLifetimeScope();

            containerBuilder.RegisterModule(new ApplicationModule(containerBuilder,_connectionString));
            containerBuilder.RegisterModule(new ServicesModuleUnitTests(_connectionString));

            containerBuilder.RegisterModule(new MediatorModuleForUnitTesting());


            containerBuilder.AddAutoMapper(typeof(Startup).Assembly);
            
            _container = containerBuilder.Build();

            return new AutofacServiceProvider(_container);
        }

        public static class CustomerCodes
        {
            public static string Bunnings = "BUN";
            public static string Makita = "MAK";
        }
        public class DbContextConfig
        {
            public static DbContextOptions<PickupContext> Configure(string connectionString)
            {
                var builder = new DbContextOptionsBuilder<PickupContext>();
                builder.UseSqlServer(connectionString,
                    sqlServerOptionsAction: sqlOptions =>
                    {
                        sqlOptions.MigrationsAssembly(typeof(Startup).GetTypeInfo().Assembly.GetName().Name);
                        sqlOptions.EnableRetryOnFailure(maxRetryCount: 10, maxRetryDelay: TimeSpan.FromSeconds(30), errorNumbersToAdd: null);

                    });
                //builder.UseLazyLoadingProxies();
                return builder.Options;
            }


            public static DbContextOptions<IntegrationEventLogContext> ConfigureEventLog(string connectionString)
            {
                var builder = new DbContextOptionsBuilder<IntegrationEventLogContext>();
                builder.UseSqlServer(connectionString,
                    sqlServerOptionsAction: sqlOptions =>
                    {
                        sqlOptions.MigrationsAssembly(typeof(Startup).GetTypeInfo().Assembly.GetName().Name);
                        sqlOptions.EnableRetryOnFailure(maxRetryCount: 10, maxRetryDelay: TimeSpan.FromSeconds(30), errorNumbersToAdd: null);

                    });
                //builder.UseLazyLoadingProxies();
                return builder.Options;
            }
        }
    }
}
