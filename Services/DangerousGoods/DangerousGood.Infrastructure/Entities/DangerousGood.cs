﻿namespace DangerousGoodSvc.Infrastructure.Entities
{
    public class DangerousGood
    {
        public int Id { get; set; }
        public string UNNumber { get; set; }
        public string Class { get; set; }
    }
}
