﻿// <auto-generated />

using DangerousGoodSvc.Infrastructure.Providers;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DangerousGoodSvc.Infrastructure.Migrations
{
    [DbContext(typeof(DGoodsContext))]
    [Migration("20191206002306_init")]
    partial class init
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.2.6-servicing-10079")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("DangerousGood.Infrastructure.Entities.DangerousGood", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("DangerousGoodId")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Class")
                        .HasColumnName("Class")
                        .HasMaxLength(100);

                    b.Property<string>("UNNumber")
                        .HasColumnName("Unno")
                        .HasMaxLength(100);

                    b.HasKey("Id")
                        .HasAnnotation("SqlServer:Clustered", false);

                    b.ToTable("DangerousGoods");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            Class = "Flammable",
                            UNNumber = "CC1022709"
                        });
                });
#pragma warning restore 612, 618
        }
    }
}
