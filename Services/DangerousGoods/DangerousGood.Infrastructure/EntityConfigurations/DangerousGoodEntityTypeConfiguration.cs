﻿using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DangerousGoodSvc.Infrastructure.EntityConfigurations
{
    public class DangerousGoodEntityTypeConfiguration : IEntityTypeConfiguration<Entities.DangerousGood>
    {
        public void Configure(EntityTypeBuilder<Entities.DangerousGood> dgConfiguration)
        {
            dgConfiguration.HasKey(e => e.Id)
                .ForSqlServerIsClustered(false);

            dgConfiguration.ToTable("DangerousGoods");

            dgConfiguration.Property(e => e.Id)
                .HasColumnName("DangerousGoodId")
                .ValueGeneratedOnAdd();

            dgConfiguration.Property(e => e.Class)
                .HasColumnName("Class")
                .HasMaxLength(100);

            dgConfiguration.Property(e => e.UNNumber)
                .HasColumnName("Unno")
                .HasMaxLength(100);

            dgConfiguration.HasData(new List<Entities.DangerousGood>()
            {
                new Entities.DangerousGood()
                {
                    Id = 1,
                    Class = "Flammable",
                    UNNumber = "CC1022709"
                }
            });
        }
    }                   
}
