﻿using DangerousGoodSvc.Infrastructure.EntityConfigurations;
using DangerousGoodSvc.Infrastructure.Providers.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace DangerousGoodSvc.Infrastructure.Providers
{
    public class DGoodsContext : DbContext, IUnitOfWork
    {
        public DGoodsContext(DbContextOptions<DGoodsContext> options) : base(options)
        {

        }
        public virtual DbSet<Entities.DangerousGood> DangerousGoods { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            
            modelBuilder.ApplyConfiguration(new DangerousGoodEntityTypeConfiguration());
        }
    }
}
