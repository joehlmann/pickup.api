﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace DangerousGoodSvc.Infrastructure.Providers.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default(CancellationToken));
    }
}