﻿using System;
using System.Reflection;
using AutoMapper;
using CorrelationId;
using DangerousGoodSvc.API.Application.Queries.DangerousGoods;
using DangerousGoodSvc.API.Application.ViewModels;
using DangerousGoodSvc.API.Infrastructure.Authorization;
using DangerousGoodSvc.API.Infrastructure.Logging;
using DangerousGoodSvc.Infrastructure.Entities;
using DangerousGoodSvc.Infrastructure.Providers;
using IdentityServer4.AccessTokenValidation;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;

namespace DangerousGoodSvc.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IHostingEnvironment hostingEnvironment, IAssertions assertions)
        {
            _configuration = configuration;
            _assertions = assertions;
            _hostingEnvironment = hostingEnvironment;
        }

        public IConfiguration _configuration { get; }
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IAssertions _assertions;

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services
                .AddAutoMapper(typeof(DangerousGoodViewModel).GetTypeInfo().Assembly, typeof(DangerousGood).GetTypeInfo().Assembly)
                .AddCustomDbContext(_configuration, _hostingEnvironment)
                .AddMediatR(typeof(DangerousGoodsQuery).GetTypeInfo().Assembly)
                .AddCorrelationId();


            // Configure swagger
            services.AddSwaggerGen(c => { c.SwaggerDoc("v1", new Info { Title = "DGoods API", Version = "v1" }); });

            services.AddAuthentication(IdentityServerAuthenticationDefaults.AuthenticationScheme)
                .AddIdentityServerAuthentication(
                    options =>
                    {
                        var config = _configuration.GetSection("Authentication");
                        var host = _hostingEnvironment;

                        options.Authority = config["Authority"];
                        options.ApiName = config["APIName"];
                        options.SupportedTokens = SupportedTokens.Jwt;
                        options.RoleClaimType = "role";
                        //if (host.EnvironmentName == "Debug")
                        //{
                        options.RequireHttpsMetadata = false;
                        //}

                    });
            services.AddBExAuthorization(_assertions);
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseCorrelationId("CorrelationId");
            app.UseMiddleware<SerilogMiddleware>();
            app.UseAuthentication();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseSwagger();
            app.UseSwaggerUI(c => { c.SwaggerEndpoint("/swagger/v1/swagger.json", "DGoods API V1"); });
            //app.UseHttpsRedirection();
            app.UseMvc();
        }

    }
    static class CustomExtensionsMethods
    {
        /// <summary>
        /// Configure Entity frameworks db contexts
        /// </summary>
        /// <param name="services"></param>
        /// <param name="configuration"></param>
        /// <param name="hostingEnvironment"></param>
        /// <returns></returns>
        public static IServiceCollection AddCustomDbContext(this IServiceCollection services,
            IConfiguration configuration, IHostingEnvironment hostingEnvironment)
        {

            var connection = configuration.GetConnectionString("DangerousGoodDb") ?? throw new ArgumentNullException("DangerousGoodDb");

            services.AddDbContext<DGoodsContext>(
                options => options.UseSqlServer(connection)
                    .ConfigureWarnings(x => x.Throw(RelationalEventId.QueryClientEvaluationWarning))
                    .EnableSensitiveDataLogging(hostingEnvironment.IsDevelopment()));

            //TODO: Investigate adding Sql Retry 
            //Configuring Connection Resiliency: https://docs.microsoft.com/en-us/ef/core/miscellaneous/connection-resiliency 
            //sqlOptions.EnableRetryOnFailure(maxRetryCount: 10, maxRetryDelay: TimeSpan.FromSeconds(30), errorNumbersToAdd: null);
            return services;
        }

        /// <summary>
        /// Configure authorization for a border express service
        /// </summary>
        /// <param name="services"></param>
        /// <param name="assertions"></param>
        /// <returns></returns>
        public static IServiceCollection AddBExAuthorization(this IServiceCollection services,
            IAssertions assertions)
        {
            services.AddAuthorization(options =>
            {
                options.AddPolicy("ReadDangerousGoods", builder => builder.RequireAssertion(context => assertions.CanReadDangerousGoods(context)));
                options.AddPolicy("WriteDangerousGoods", builder => builder.RequireAssertion(context => assertions.CanWriteDangerousGoods(context)));
                options.AddPolicy("DeleteDangerousGoods", builder => builder.RequireAssertion(context => assertions.CanDeleteDangerousGoods(context)));
            });

            return services;
        }
    }
}
