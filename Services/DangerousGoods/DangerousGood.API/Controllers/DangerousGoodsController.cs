﻿using System;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DangerousGoodSvc.API.Application.Dtos;
using DangerousGoodSvc.API.Application.Queries.DangerousGoods;
using DangerousGoodSvc.API.Application.ViewModels;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace DangerousGoodSvc.API.Controllers
{
    [Route("[controller]")]
    [ApiController]
    [Authorize]
    public class DangerousGoodsController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IMediator _mediator;

        public DangerousGoodsController(IMapper mapper, IMediator mediator)
        {
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
        }


        [HttpGet]
        [Authorize(Policy = "ReadDangerousGoods")]
        public async Task<ActionResult<DangerousGoodsViewModel>> Get(DangerousGoodFilterViewModel filter = null, int pageSize = 0, int pageNumber = 0)
        {
            var search = _mapper.Map<DangerousGoodSearchDtoModel>(filter);

            var result = await _mediator.Send(new DangerousGoodsQuery
            {
                Filter = search,
                PageNumber = pageNumber,
                PageSize = pageSize
            });

            //TODO: Add links and total row counts header

            if (result?.DangerousGoods != null && result.DangerousGoods.Any())
            {
                return Ok(_mapper.Map<DangerousGoodsViewModel>(result));
            }

            return NotFound();
        }

        
        [HttpGet("{id}")]
        [Authorize(Policy = "ReadDangerousGoods")]
        public async Task<ActionResult<DangerousGoodViewModel>> Get(int id)
        {
            var result = await _mediator.Send(new DangerousGoodQuery { Id = id });

            if (result != null && result.Id > 0)
            {
                return Ok(_mapper.Map<DangerousGoodViewModel>(result));
            }

            return NotFound();
        }
        [HttpPost]
        [Authorize(Policy = "WriteDangerousGoods")]
        public ActionResult Post([FromBody] string value)
        {
            return StatusCode(501);
        }


        [HttpPut("{id}")]
        [Authorize(Policy = "WriteDangerousGoods")]
        public ActionResult Put(int id, [FromBody] string value)
        {
            return StatusCode(501);
        }


        [HttpDelete("{id}")]
        [Authorize(Policy = "DeleteDangerousGoods")]
        public ActionResult Delete(int id)
        {
            return StatusCode(501);
        }
    }
}
