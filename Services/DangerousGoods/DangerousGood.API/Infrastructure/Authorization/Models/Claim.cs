﻿namespace DangerousGoodSvc.API.Infrastructure.Authorization.Models
{
    public class Claim
    {
        public string Type { get; set; }
        public string Issuer { get; set; }
        public string Value { get; set; }
    }
}
