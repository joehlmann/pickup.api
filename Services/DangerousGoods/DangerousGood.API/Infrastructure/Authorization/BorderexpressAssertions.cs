﻿using System.Linq;
using Microsoft.AspNetCore.Authorization;

namespace DangerousGoodSvc.API.Infrastructure.Authorization
{
    public class BorderExpressAssertions : IAssertions
    {
        public bool CanDeleteDangerousGoods(AuthorizationHandlerContext context)
        {
            // Split the scopes string into an array
            var scopes = context.User.Claims.Where(c => c.Type == BorderExpressIdentity.Claims.Scope.Type && c.Issuer == BorderExpressIdentity.Claims.Scope.Issuer).ToList();

            // Succeed if the scope array contains the required scope
            var hasRequiredScopes = scopes.Any(s => (
                // Check if the access token has the delete scope
                s.Value == BorderExpressIdentity.Scopes.DeleteDangerousGoods.Name && s.Issuer == BorderExpressIdentity.Scopes.DeleteDangerousGoods.Issuer
            ));

            return hasRequiredScopes;
        }

        public bool CanReadDangerousGoods(AuthorizationHandlerContext context)
        {
            // Split the scopes string into an array
            var scopes = context.User.Claims.Where(c => c.Type == BorderExpressIdentity.Claims.Scope.Type && c.Issuer == BorderExpressIdentity.Claims.Scope.Issuer).ToList();

            // Succeed if the scope array contains the required scope
            var hasRequiredScopes = scopes.Any(s => (
                // Check if the access token has the read scope
                s.Value == BorderExpressIdentity.Scopes.ReadDangerousGoods.Name && s.Issuer == BorderExpressIdentity.Scopes.ReadDangerousGoods.Issuer
            ));

            return hasRequiredScopes;
        }

        public bool CanWriteDangerousGoods(AuthorizationHandlerContext context)
        {
            // Split the scopes string into an array
            var scopes = context.User.Claims.Where(c => c.Type == BorderExpressIdentity.Claims.Scope.Type && c.Issuer == BorderExpressIdentity.Claims.Scope.Issuer).ToList();

            // Succeed if the scope array contains the required scope
            var hasRequiredScopes = scopes.Any(s => (
                // Check if the access token has the write scope
                s.Value == BorderExpressIdentity.Scopes.WriteDangerousGoods.Name && s.Issuer == BorderExpressIdentity.Scopes.WriteDangerousGoods.Issuer
            ));

            return hasRequiredScopes;
        }
    }
}
