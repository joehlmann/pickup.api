﻿using Microsoft.AspNetCore.Authorization;

namespace DangerousGoodSvc.API.Infrastructure.Authorization
{
    public interface IAssertions
    {
        bool CanReadDangerousGoods(AuthorizationHandlerContext context);
        bool CanWriteDangerousGoods(AuthorizationHandlerContext context);
        bool CanDeleteDangerousGoods(AuthorizationHandlerContext context);
    }
}