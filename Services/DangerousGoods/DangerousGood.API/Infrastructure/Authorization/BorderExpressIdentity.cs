﻿using DangerousGoodSvc.API.Infrastructure.Authorization.Models;

namespace DangerousGoodSvc.API.Infrastructure.Authorization
{
    public class BorderExpressIdentity
    {
        //TODO: This should come from a config as it will need to change with environments
        //private static string _issuer = "https://borderexpress.idp";
        private static string _issuer = "http://borderexpressdevidp:6004";
        public static class Scopes
        {
            public static Scope ReadDangerousGoods = new Scope { Name = "dangerousgood|read", Issuer = _issuer };
            public static Scope DeleteDangerousGoods = new Scope { Name = "dangerousgood|delete", Issuer = _issuer };
            public static Scope WriteDangerousGoods = new Scope { Name = "dangerousgood|write", Issuer = _issuer };
        }

        public static class Claims
        {
            public static Claim Scope = new Claim { Issuer = _issuer, Type = "scope", Value = "" };
        }
    }
}
