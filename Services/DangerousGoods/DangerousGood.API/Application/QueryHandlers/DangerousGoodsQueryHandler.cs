﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DangerousGoodSvc.API.Application.Dtos;
using DangerousGoodSvc.API.Application.Queries.DangerousGoods;
using DangerousGoodSvc.Infrastructure.Providers;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace DangerousGoodSvc.API.Application.QueryHandlers
{
    public class DangerousGoodsQueryHandler : IRequestHandler<DangerousGoodsQuery, DangerousGoodsDtoModel>
    {
        private readonly DGoodsContext _context;
        public DangerousGoodsQueryHandler(DGoodsContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }
        public async Task<DangerousGoodsDtoModel> Handle(DangerousGoodsQuery request, CancellationToken cancellationToken)
        {

            var query = _context.DangerousGoods
                .AsQueryable()
                .AsNoTracking();

            if (request.Filter != null)
            {
                if (request.Filter.UNNumbers != null && request.Filter.UNNumbers.Any())
                {
                    query = query.Where(dbDg => request.Filter.UNNumbers.Contains(dbDg.UNNumber));
                }
               

            }


            //TODO: Throw error if page size is too high
            query = query.OrderBy(f => f.Id); // Required to be before Take/Skip otherwise children aren't included
            query = query.Take(request.PageSize > 0 ? request.PageSize : 100);
            query = query.Skip(request.PageNumber * request.PageSize);

            var results = await query.Select(dg => new DangerousGoodDtoModel()
            {
                Id = dg.Id,
                Class = dg.Class,
                UNNumber = dg.UNNumber

            }).ToListAsync(cancellationToken);

            //TODO: Add total rowcount so it can be returned in a header
            return new DangerousGoodsDtoModel() { DangerousGoods = results };
        }
    }
}
