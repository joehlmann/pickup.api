﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DangerousGoodSvc.API.Application.Dtos;
using DangerousGoodSvc.API.Application.Queries.DangerousGoods;
using DangerousGoodSvc.Infrastructure.Providers;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace DangerousGoodSvc.API.Application.QueryHandlers
{
    public class DangerousGoodQueryHandler : IRequestHandler<DangerousGoodQuery, DangerousGoodDtoModel>
    {
        private readonly DGoodsContext _context;
        public DangerousGoodQueryHandler(DGoodsContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }
        public async Task<DangerousGoodDtoModel> Handle(DangerousGoodQuery request, CancellationToken cancellationToken)
        {
            return await _context.DangerousGoods.AsNoTracking().Where(dg => dg.Id == request.Id)
                .Select(d => new DangerousGoodDtoModel
                {
                    Id = d.Id,
                    Class = d.Class,
                    UNNumber = d.UNNumber
                })
                .FirstOrDefaultAsync(cancellationToken);
        }
    }
}
