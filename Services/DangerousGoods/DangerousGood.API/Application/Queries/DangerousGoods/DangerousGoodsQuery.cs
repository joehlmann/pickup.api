﻿using DangerousGoodSvc.API.Application.Dtos;
using MediatR;

namespace DangerousGoodSvc.API.Application.Queries.DangerousGoods
{
    public class DangerousGoodsQuery : PaginatedQuery, IRequest<DangerousGoodsDtoModel>
    {
        public DangerousGoodSearchDtoModel Filter { get; set; }
    }
}
