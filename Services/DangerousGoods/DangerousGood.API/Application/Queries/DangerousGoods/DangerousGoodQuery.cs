﻿using DangerousGoodSvc.API.Application.Dtos;
using MediatR;

namespace DangerousGoodSvc.API.Application.Queries.DangerousGoods
{
    public class DangerousGoodQuery : IRequest<DangerousGoodDtoModel>
    {
        public int Id { get; set; }
    }
}
