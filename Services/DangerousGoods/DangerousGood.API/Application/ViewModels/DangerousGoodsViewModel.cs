﻿using System.Collections.Generic;

namespace DangerousGoodSvc.API.Application.ViewModels
{
    public class DangerousGoodsViewModel
    {
        public List<DangerousGoodViewModel> DangerousGoods { get; set; }
    }
}
