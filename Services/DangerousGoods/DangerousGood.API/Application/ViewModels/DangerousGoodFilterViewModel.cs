﻿using System.Collections.Generic;
using System.ComponentModel;
using DangerousGoodSvc.API.Infrastructure.JsonConverters;
using DangerousGoodSvc.API.Infrastructure.TypeConverters;
using Newtonsoft.Json;

namespace DangerousGoodSvc.API.Application.ViewModels
{
    [TypeConverter(typeof(DangerousGoodFilterTypeConverter))]
    [JsonConverter(typeof(NoTypeConverterJsonConverter<DangerousGoodFilterViewModel>))]
    public class DangerousGoodFilterViewModel
    {
        public List<string> UNNumbers { get; set; }
    }
}
