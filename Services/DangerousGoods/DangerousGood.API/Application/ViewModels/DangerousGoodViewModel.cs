﻿namespace DangerousGoodSvc.API.Application.ViewModels
{
    public class DangerousGoodViewModel
    {
        public int Id { get; set; }
        public string UNNumber { get; set; }
        public string Class { get; set; }
    }
}
