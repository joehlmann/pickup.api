﻿using System.Collections.Generic;

namespace DangerousGoodSvc.API.Application.Dtos
{
    public class DangerousGoodSearchDtoModel
    {
        public List<string> UNNumbers { get; set; }
        
    }
}
