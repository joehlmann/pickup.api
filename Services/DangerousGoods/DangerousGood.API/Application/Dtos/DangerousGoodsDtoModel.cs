﻿using System.Collections.Generic;

namespace DangerousGoodSvc.API.Application.Dtos
{
    public class DangerousGoodsDtoModel
    {
        public List<DangerousGoodDtoModel> DangerousGoods { get; set; }
    }
}
