﻿namespace DangerousGoodSvc.API.Application.Dtos
{
    public class DangerousGoodDtoModel
    {
        public int Id { get; set; }
        public string UNNumber { get; set; }
        public string Class { get; set; }
    }
}
