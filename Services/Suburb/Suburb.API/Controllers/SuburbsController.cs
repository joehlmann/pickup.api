﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SuburbSvc.API.Application.Queries.Suburbs;
using SuburbSvc.API.ViewModels;
using SuburbSvc.Infrastructure.Models;

namespace SuburbSvc.API.Controllers
{
    [Route("[controller]")]
    [Authorize]
    [ApiController]
    public class SuburbsController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IMediator _mediator;

        public SuburbsController(IMapper mapper, IMediator mediator)
        {
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
        }
        
        [HttpGet]
        [Authorize(Policy = "ReadSuburbs")]
        public async Task<ActionResult<IEnumerable<SuburbViewModel>>> Get(SuburbFilterViewModel filter = null, int pageSize = 0, int pageNumber = 0)
        {
            var search = _mapper.Map<SuburbSearchModel>(filter);

            var result = await _mediator.Send(new SuburbsQuery
            {
                Filter = search,
                PageNumber = pageNumber,
                PageSize = pageSize
            });

            //TODO: Add links and total row counts header

            if (result?.Suburbs != null && result.Suburbs.Any())
            {
                return Ok(result);
            }

            return NotFound();
        }
        // GET suburbs/5
        [HttpGet("{id}")]
        [Authorize(Policy = "ReadSuburbs")]
        public async Task<ActionResult<SuburbViewModel>> Get(int id)
        {
            var result = await _mediator.Send(new SuburbQuery{Id = id});
            
            if (result != null && result.Id > 0)
            {
                return Ok(result);
            }

            return NotFound();
        }

        // POST api/values
        [HttpPost]
        [Authorize(Policy = "WriteSuburbs")]
        public ActionResult Post([FromBody] string value)
        {
            return StatusCode(501);
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        [Authorize(Policy = "WriteSuburbs")]
        public ActionResult Put(int id, [FromBody] string value)
        {
            return StatusCode(501);
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        [Authorize(Policy = "DeleteSuburbs")]
        public ActionResult Delete(int id)
        {
            return StatusCode(501);
        }
        
    }
}
