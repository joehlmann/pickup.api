﻿using System;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Serilog;
using Serilog.Core;
using Serilog.Events;
using SuburbSvc.API.Authorization;

namespace SuburbSvc.API
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateWebHostBuilder(args).Build().Run();
        }
        /// <summary>
        /// Setup the static Log instance
        /// </summary>
        /// <param name="config"></param>
        public static void ConfigureLogger(IConfiguration config)
        {
            var loggingConfig = config.GetSection("Logging");
            var levelSwitch = new LoggingLevelSwitch();
            var seqConnection = loggingConfig["SeqServerUrl"];
            var seqAPIKey = loggingConfig["SeqAPIKey"];
            var idOut = 0;
            Int32.TryParse(loggingConfig["SeqEventBodyLimitBytes"], out idOut);

            //Default event body size
            var eventBodyLimit = idOut > 0 ? idOut : 262144;

            //TODO: Handle nulls
            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Verbose()
                .MinimumLevel.Override("Microsoft", LogEventLevel.Verbose)
                .MinimumLevel.Override("System", LogEventLevel.Information)
                .MinimumLevel.Override("Ocelot", LogEventLevel.Information)
                .Enrich.FromLogContext()
                .WriteTo.Seq(seqConnection, apiKey: seqAPIKey, controlLevelSwitch: levelSwitch, eventBodyLimitBytes: eventBodyLimit)
                .CreateLogger();

        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .ConfigureLogging((ctx, logging) => { ConfigureLogger(ctx.Configuration); })
                .ConfigureServices(serviceCollection =>
                    serviceCollection.AddSingleton<IAssertions, BorderExpressAssertions>())
                .UseStartup<Startup>()
            .UseIIS()
                .UseSerilog();
    }
}
