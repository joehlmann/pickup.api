﻿using Microsoft.AspNetCore.Authorization;

namespace SuburbSvc.API.Authorization
{
    public interface IAssertions
    {
        bool CanReadSuburbs(AuthorizationHandlerContext context);
        bool CanWriteSuburbs(AuthorizationHandlerContext context);
        bool CanDeleteSuburbs(AuthorizationHandlerContext context);
    }
}