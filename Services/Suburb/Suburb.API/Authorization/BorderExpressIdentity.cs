﻿using SuburbSvc.API.Authorization.Models;

namespace SuburbSvc.API.Authorization
{
    //TODO: Load these values from DB as they are reused across applications
    public static class BorderExpressIdentity
    {
        private static string _issuer = "https://borderexpress.idp";
        public static class Scopes
        {
            public static Scope ReadSuburbs = new Scope{Name = "suburb|read", Issuer = _issuer};
            public static Scope DeleteSuburbs = new Scope{Name = "suburb|delete", Issuer = _issuer};
            public static Scope WriteSuburbs = new Scope{Name = "suburb|write", Issuer = _issuer};
        }

        public static class Claims
        {
            public static Claim Scope = new Claim {Issuer = _issuer, Type = "scope", Value = ""};
        }
    }
}
