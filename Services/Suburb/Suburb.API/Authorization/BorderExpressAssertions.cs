﻿using System.Linq;
using Microsoft.AspNetCore.Authorization;

namespace SuburbSvc.API.Authorization
{
    public class BorderExpressAssertions : IAssertions
    {
        public bool CanReadSuburbs(AuthorizationHandlerContext context)
        {
           // Split the scopes string into an array
            var scopes = context.User.Claims.Where(c => c.Type == BorderExpressIdentity.Claims.Scope.Type && c.Issuer == BorderExpressIdentity.Claims.Scope.Issuer).ToList();

            // Succeed if the scope array contains the required scope
            var hasRequiredScopes = scopes.Any(s => (
                // Check if the access token has the read only scope
                s.Value == BorderExpressIdentity.Scopes.ReadSuburbs.Name && s.Issuer == BorderExpressIdentity.Scopes.ReadSuburbs.Issuer
                ));

            return hasRequiredScopes;
        }

        public bool CanWriteSuburbs(AuthorizationHandlerContext context)
        {
            // Split the scopes string into an array
            var scopes = context.User.Claims.Where(c => c.Type == BorderExpressIdentity.Claims.Scope.Type && c.Issuer == BorderExpressIdentity.Claims.Scope.Issuer).ToList();

            // Succeed if the scope array contains the required scope
            var hasRequiredScopes = scopes.Any(s => (
                // Check if the access token has the read only scope
                s.Value == BorderExpressIdentity.Scopes.WriteSuburbs.Name && s.Issuer == BorderExpressIdentity.Scopes.ReadSuburbs.Issuer
            ));

            return hasRequiredScopes;
        }

        public bool CanDeleteSuburbs(AuthorizationHandlerContext context)
        {
            // Split the scopes string into an array
            var scopes = context.User.Claims.Where(c => c.Type == BorderExpressIdentity.Claims.Scope.Type && c.Issuer == BorderExpressIdentity.Claims.Scope.Issuer).ToList();

            // Succeed if the scope array contains the required scope
            var hasRequiredScopes = scopes.Any(s => (
                // Check if the access token has the read only scope
                s.Value == BorderExpressIdentity.Scopes.DeleteSuburbs.Name && s.Issuer == BorderExpressIdentity.Scopes.ReadSuburbs.Issuer
            ));

            return hasRequiredScopes;
        }

        
    }
}
