﻿using MediatR;
using SuburbSvc.API.ViewModels;
using SuburbSvc.Infrastructure.Models;

namespace SuburbSvc.API.Application.Queries.Suburbs
{
    public class SuburbsQuery : PaginatedQuery,IRequest<SuburbsViewModel>
    {
        public SuburbSearchModel Filter { get;set; }
    }
}
