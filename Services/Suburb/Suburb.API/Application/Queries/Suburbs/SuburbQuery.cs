﻿using MediatR;
using SuburbSvc.API.ViewModels;

namespace SuburbSvc.API.Application.Queries.Suburbs
{
    public class SuburbQuery : IRequest<SuburbViewModel>
    {
        public int Id { get; set; }
    }
}
