﻿namespace SuburbSvc.API.Application.Queries
{
    public class PaginatedQuery
    {
        public int PageSize { get;set; }
        public int PageNumber { get; set; }
    }
}
