﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.EntityFrameworkCore;
using SuburbSvc.API.Application.Queries.Suburbs;
using SuburbSvc.API.ViewModels;
using SuburbSvc.Infrastructure.Providers;

namespace SuburbSvc.API.Application.Query_Handler
{
    public class SuburbsQueryHandler : IRequestHandler<SuburbsQuery,SuburbsViewModel>
    {
        private readonly SuburbContext _context;

        public SuburbsQueryHandler(SuburbContext context)
        {
            _context = context;
        }

        public async Task<SuburbsViewModel> Handle(SuburbsQuery request, CancellationToken cancellationToken)
        {
            //TODO: Performance considerations: This makes two round trips to the DB, would like to reduce it to one if possible
            var query =  _context.Suburbs
                .AsQueryable()
                .AsNoTracking();

            if (request.Filter != null)
            {
                if (request.Filter.Postcodes != null && request.Filter.Suburbs.Any())
                {
                    query = query.Where(dbSub => request.Filter.Postcodes.Contains(dbSub.Postcode));
                }

                if (request.Filter.Suburbs != null && request.Filter.Suburbs.Any())
                {
                    query = query.Where(dbSub => request.Filter.Suburbs.Contains(dbSub.SuburbName));
                }
            }

            query = query.Include(pref => pref.PreferredBranches);
           
            //TODO: Throw error if page size is too high
            query = query.OrderBy(f => f.Id); // Required to be before Take/Skip otherwise children aren't included
            query = query.Take(request.PageSize > 0 ? request.PageSize : 100);
            query = query.Skip(request.PageNumber * request.PageSize);
            
            var results = await query.Select(s => new SuburbViewModel
            {
                Id = s.Id,
                Postcode = s.Postcode,
                State = s.State,
                Suburb = s.SuburbName,
                PreferredBranches = s.PreferredBranches.Select(pb => new PreferredBranchViewModel
                {
                    BranchId = pb.BranchId, IsOnforwardEnabled = pb.IsOnforwardEnabled, Order = pb.Order,
                    Type = pb.SuburbBranchType
                }).ToList()
            }).ToListAsync(cancellationToken);

            //TODO: Add total rowcount so it can be returned in a header
            return new SuburbsViewModel{Suburbs = results};
        }
    }
}
