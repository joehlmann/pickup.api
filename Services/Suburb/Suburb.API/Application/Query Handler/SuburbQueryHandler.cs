﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.EntityFrameworkCore;
using SuburbSvc.API.Application.Queries.Suburbs;
using SuburbSvc.API.ViewModels;
using SuburbSvc.Infrastructure.Providers;

namespace SuburbSvc.API.Application.Query_Handler
{
    public class SuburbQueryHandler : IRequestHandler<SuburbQuery, SuburbViewModel>
    {
        private readonly SuburbContext _context;

        public SuburbQueryHandler(SuburbContext context)
        {
            _context = context;
        }
        public async Task<SuburbViewModel> Handle(SuburbQuery request, CancellationToken cancellationToken)
        {
            return await _context.Suburbs.AsNoTracking().Where(sub => sub.Id == request.Id)
                .Select(s => new SuburbViewModel
                {
                    Id = s.Id,
                    Postcode = s.Postcode,
                    State = s.State,
                    Suburb = s.SuburbName,
                    PreferredBranches = s.PreferredBranches.Select(pb => new PreferredBranchViewModel
                    {
                        BranchId = pb.BranchId, IsOnforwardEnabled = pb.IsOnforwardEnabled, Order = pb.Order,
                        Type = pb.SuburbBranchType
                    }).ToList()
                })
                .FirstOrDefaultAsync(cancellationToken);
        }
    }
}