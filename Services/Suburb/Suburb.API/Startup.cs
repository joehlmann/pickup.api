﻿using System;
using System.Reflection;
using AutoMapper;
using CorrelationId;
using IdentityServer4.AccessTokenValidation;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using SuburbSvc.API.Application.Queries.Suburbs;
using SuburbSvc.API.Authorization;
using SuburbSvc.API.Infrastructure.Middleware;
using SuburbSvc.Infrastructure.Providers;
using SuburbSvc.Infrastructure.Repositories;
using SuburbSvc.Infrastructure.Repositories.Interfaces;
using Swashbuckle.AspNetCore.Swagger;

namespace SuburbSvc.API
{
    public class Startup
    {
        public IConfiguration _configuration { get; }
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly ILogger _logger;
        private readonly IAssertions _assertions;
        public Startup(IConfiguration configuration, IHostingEnvironment hostingEnvironment, ILogger<Startup> logger, IAssertions assertions)
        {
            _configuration = configuration;
            _hostingEnvironment = hostingEnvironment;
            _logger = logger;
            _assertions = assertions;
        }

        

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services
                .AddAutoMapper()
                .AddCustomDbContext(_configuration, _hostingEnvironment)
                .AddMediatR(typeof(SuburbsQuery).GetTypeInfo().Assembly)
                .AddCorrelationId();

            // Configure swagger
            services.AddSwaggerGen(c => { c.SwaggerDoc("v1", new Info { Title = "Suburb API", Version = "v1" }); });

            services.AddAuthentication(IdentityServerAuthenticationDefaults.AuthenticationScheme)
                .AddIdentityServerAuthentication(
                    options =>
                    {
                        var config = _configuration.GetSection("Authentication");

                        options.Authority = config["Authority"];
                        options.ApiName = config["APIName"];
                        options.SupportedTokens = SupportedTokens.Jwt;
                        options.RoleClaimType = "role";

                    });



            services.AddBExAuthorization(_assertions);
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.AddScoped<ISuburbRepository, SuburbRepository>();
            
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseMiddleware<StackifyMiddleware.RequestTracerMiddleware>();
            app.UseCorrelationId("CorrelationId");
            app.UseMiddleware<SerilogMiddleware>();
            app.UseAuthentication();

            if (env.IsDevelopment())
            {
                HibernatingRhinos.Profiler.Appender.EntityFramework.EntityFrameworkProfiler.Initialize();
                app.UseDeveloperExceptionPage();
            }
            else
            {
                //app.UseHsts();
            }

            //app.UseHttpsRedirection();
            app.UseMvc();

            app.UseSwagger();
            app.UseSwaggerUI(c => { c.SwaggerEndpoint("/swagger/v1/swagger.json", "Suburb API V1"); });
        }

   
    }

    static class CustomExtensionsMethods
    {
        /// <summary>
        /// Configure Entity frameworks db contexts
        /// </summary>
        /// <param name="services"></param>
        /// <param name="configuration"></param>
        /// <param name="hostingEnvironment"></param>
        /// <returns></returns>
        public static IServiceCollection AddCustomDbContext(this IServiceCollection services,
            IConfiguration configuration,IHostingEnvironment hostingEnvironment)
        {
            
            var connection = configuration.GetConnectionString("SuburbDb") ?? throw new ArgumentNullException("SuburbDb");
            
            services.AddDbContext<SuburbContext>(
                options => options.UseSqlServer(connection)
                    .ConfigureWarnings(x => x.Throw(RelationalEventId.QueryClientEvaluationWarning))
                    .EnableSensitiveDataLogging(hostingEnvironment.IsDevelopment()));

            //TODO: Investigate adding Sql Retry 
            //Configuring Connection Resiliency: https://docs.microsoft.com/en-us/ef/core/miscellaneous/connection-resiliency 
            //sqlOptions.EnableRetryOnFailure(maxRetryCount: 10, maxRetryDelay: TimeSpan.FromSeconds(30), errorNumbersToAdd: null);
            return services;
        }

        /// <summary>
        /// Configure authorization for a border express service
        /// </summary>
        /// <param name="services"></param>
        /// <param name="assertions"></param>
        /// <returns></returns>
        public static IServiceCollection AddBExAuthorization(this IServiceCollection services,
            IAssertions assertions)
        {
            services.AddAuthorization(options =>
            {
                options.AddPolicy("ReadSuburbs",builder => builder.RequireAssertion(context => assertions.CanReadSuburbs(context)));
                options.AddPolicy("WriteSuburbs",builder => builder.RequireAssertion(context => assertions.CanWriteSuburbs(context)));
                options.AddPolicy("DeleteSuburbs",builder => builder.RequireAssertion(context => assertions.CanDeleteSuburbs(context)));
            });

            return services;
        }
    }
}
