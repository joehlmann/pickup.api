﻿using System;
using System.ComponentModel;
using System.Globalization;
using Newtonsoft.Json;
using SuburbSvc.API.ViewModels;

namespace SuburbSvc.API.Infrastructure.TypeConverters
{
    public class SuburbFilterTypeConverter : TypeConverter
    {
       
       
        public override bool CanConvertFrom(
            ITypeDescriptorContext context, 
            Type sourceType)
        {
            return sourceType == typeof(string);
        }

        public override object ConvertFrom(ITypeDescriptorContext context, 
            CultureInfo culture, object value)
        {
            if (value != null)
            {
                //try
                //{
                   
                    return JsonConvert.DeserializeObject<SuburbFilterViewModel>(value.ToString());
                //}
                //catch (Exception e)
                //{
                //    // TODO: Work out how to inject _logger instance here, TypeConverters constructor is called as parameterless, not sure where to intercept so DI can do it's job
                //    _logger.LogWarning(e.ToString());
                //}
               
                
            }

            return null;
        }

        public override bool CanConvertTo(ITypeDescriptorContext context, 
            Type destinationType)
        {
            return destinationType == typeof(string);
        }

        public override object ConvertTo(ITypeDescriptorContext context, 
            CultureInfo culture, object value, Type destinationType)
        {
            return JsonConvert.SerializeObject(value);
        }
    }
}
