﻿using AutoMapper;
using SuburbSvc.API.ViewModels;
using SuburbSvc.Infrastructure.Entities;

namespace SuburbSvc.API.Infrastructure.AutoMapper
{
    public class ViewModelsProfile : Profile
    {
        public ViewModelsProfile()
        {
            CreateMap<SuburbViewModel, Suburb>();
        }
    }
}
