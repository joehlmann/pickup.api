﻿using AutoMapper;
using SuburbSvc.API.ViewModels;
using SuburbSvc.Infrastructure.Entities;

namespace SuburbSvc.API.Infrastructure.AutoMapper
{
    public class DbModelsProfile : Profile
    {
        public DbModelsProfile()
        {
            CreateMap<Suburb, SuburbViewModel>();
            CreateMap<SuburbBranch, PreferredBranchViewModel>();

        }
    }
}
