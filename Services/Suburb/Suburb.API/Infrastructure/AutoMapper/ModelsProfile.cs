﻿using AutoMapper;
using SuburbSvc.API.ViewModels;
using SuburbSvc.Infrastructure.Models;

namespace SuburbSvc.API.Infrastructure.AutoMapper
{
    public class ModelsProfile : Profile
    {
        public ModelsProfile()
        {
            CreateMap<SuburbSearchModel, SuburbFilterViewModel>();
        }
    }
}
