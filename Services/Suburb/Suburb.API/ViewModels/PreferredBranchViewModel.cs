﻿namespace SuburbSvc.API.ViewModels
{
    public class PreferredBranchViewModel
    {
        public int BranchId { get; set; }
        public int Order { get; set; }
        public bool IsOnforwardEnabled { get; set; }
        public string Type { get; set; }
    }
}
