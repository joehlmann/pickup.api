﻿using System.Collections.Generic;
using System.ComponentModel;
using Newtonsoft.Json;
using SuburbSvc.API.Infrastructure.JsonConverters;
using SuburbSvc.API.Infrastructure.TypeConverters;

namespace SuburbSvc.API.ViewModels
{
    [TypeConverter(typeof(SuburbFilterTypeConverter))]
    [JsonConverter(typeof(NoTypeConverterJsonConverter<SuburbFilterViewModel>))]
    public class SuburbFilterViewModel
    {
        public List<string> Suburbs { get; set; }
        public List<string> Postcodes { get; set; }

    }
}
