﻿using System.Collections.Generic;

namespace SuburbSvc.API.ViewModels
{
    public class SuburbViewModel
    {
        public int Id { get; set; }
        public string Suburb { get; set; }
        public string State { get;set; }
        public string Postcode { get;set; }

        public List<PreferredBranchViewModel> PreferredBranches { get; set; }
    }
}
