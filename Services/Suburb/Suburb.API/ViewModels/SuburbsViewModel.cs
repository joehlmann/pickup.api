﻿using System.Collections.Generic;

namespace SuburbSvc.API.ViewModels
{
    public class SuburbsViewModel
    {
        public List<SuburbViewModel> Suburbs { get;set; }
    }
}
