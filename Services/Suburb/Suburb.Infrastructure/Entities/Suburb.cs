﻿using System.Collections.Generic;

namespace SuburbSvc.Infrastructure.Entities
{
    public class Suburb
    {
        public int Id { get; set; }
        public string SuburbName { get; set; }
        public string State { get;set; }
        public string Postcode { get;set; }

        public List<SuburbBranch> PreferredBranches { get; set; }
    }
}
