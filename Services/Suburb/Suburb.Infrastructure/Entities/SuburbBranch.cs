﻿namespace SuburbSvc.Infrastructure.Entities
{
    public class SuburbBranch
    {
        public int Id { get; set; }
        
        public string SuburbBranchType { get; set; }
        public bool IsOnforwardEnabled { get; set; }
        public byte Order { get; set; }
        public int BranchId { get;set; }

        public int SuburbId { get; set; }
        public Suburb Suburb { get;set; }
    }
}
