﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SuburbSvc.Infrastructure.Migrations
{
    public partial class data : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Suburbs",
                columns: new[] { "SuburbId", "Postcode", "State", "SuburbName" },
                values: new object[] { 1, "3162", "Victoria", "Caulfield" });

            migrationBuilder.InsertData(
                table: "Suburbs",
                columns: new[] { "SuburbId", "Postcode", "State", "SuburbName" },
                values: new object[] { 2, "3141", "Victoria", "South Yarra" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Suburbs",
                keyColumn: "SuburbId",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Suburbs",
                keyColumn: "SuburbId",
                keyValue: 2);
        }
    }
}
