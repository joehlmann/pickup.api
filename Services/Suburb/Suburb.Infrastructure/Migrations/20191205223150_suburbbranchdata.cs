﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SuburbSvc.Infrastructure.Migrations
{
    public partial class suburbbranchdata : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "SuburbBranchType",
                table: "SuburbBranch",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.InsertData(
                table: "SuburbBranch",
                columns: new[] { "SuburbBranchId", "BranchId", "Onforward", "Order", "SuburbBranchType", "SuburbId" },
                values: new object[] { 1, 1, true, (byte)0, "D", 1 });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "SuburbBranch",
                keyColumn: "SuburbBranchId",
                keyValue: 1);

            migrationBuilder.AlterColumn<string>(
                name: "SuburbBranchType",
                table: "SuburbBranch",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);
        }
    }
}
