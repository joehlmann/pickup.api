﻿using System.Collections.Generic;

namespace SuburbSvc.Infrastructure.Models
{
    public class SuburbSearchModel
    {
        public List<string> Suburbs { get; set; }
        public List<string> Postcodes { get;set; }
    }
}
