﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using SuburbSvc.Infrastructure.Providers;
using SuburbSvc.Infrastructure.Repositories.Interfaces;

namespace SuburbSvc.Infrastructure.Repositories
{
    public class SuburbRepository : ISuburbRepository
    {
        private readonly SuburbContext _context;
        private readonly IMapper _mapper;

        public SuburbRepository(IMapper mapper, SuburbContext context)
        {
            _context = context;
            _mapper = mapper;
        }
       
        public async Task<Entities.Suburb> GetAsync(int id, CancellationToken cancellationToken = default(CancellationToken))
        {
            return await _context.Suburbs.Include(pref => pref.PreferredBranches).Where(sub => sub.Id == id)
                .FirstOrDefaultAsync(cancellationToken);
        }
    }
}
