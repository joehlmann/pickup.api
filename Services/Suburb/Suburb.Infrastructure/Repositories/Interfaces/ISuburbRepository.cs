﻿using System.Threading;
using System.Threading.Tasks;

namespace SuburbSvc.Infrastructure.Repositories.Interfaces
{
    public interface ISuburbRepository
    {
        Task<Entities.Suburb> GetAsync(int id, CancellationToken cancellationToken = default(CancellationToken));
    }
}
