﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace SuburbSvc.Infrastructure.Providers.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default(CancellationToken));
    }
}