﻿using Microsoft.EntityFrameworkCore;
using SuburbSvc.Infrastructure.EntityConfigurations;
using SuburbSvc.Infrastructure.Providers.Interfaces;

namespace SuburbSvc.Infrastructure.Providers
{
    public class SuburbContext : DbContext, IUnitOfWork
    {
        
        public SuburbContext(DbContextOptions<SuburbContext> options)
            : base(options)
        {
           
        }
        public virtual DbSet<Entities.Suburb> Suburbs { get; set; }

        public virtual DbSet<Entities.SuburbBranch> SuburbBranches { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //modelBuilder.HasDefaultSchema("suburbs");
            modelBuilder.ApplyConfiguration(new SuburbEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new SuburbBranchEntityTypeConfiguration());
        }
    }
}
