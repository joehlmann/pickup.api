﻿using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SuburbSvc.Infrastructure.Entities;

namespace SuburbSvc.Infrastructure.EntityConfigurations
{
    public class SuburbBranchEntityTypeConfiguration : IEntityTypeConfiguration<SuburbBranch>
    {
        public void Configure(EntityTypeBuilder<SuburbBranch> suburbConfiguration)
        {
            suburbConfiguration.HasKey(e => e.Id)
                    .ForSqlServerIsClustered(false);
            suburbConfiguration.HasOne(c => c.Suburb)
                .WithMany(e => e.PreferredBranches)
                .HasForeignKey(c => c.SuburbId);    

            suburbConfiguration.ToTable("SuburbBranch");
            suburbConfiguration.Property(e => e.Id).HasColumnName("SuburbBranchId").ValueGeneratedOnAdd();
            suburbConfiguration.Property(e => e.SuburbId).HasColumnName("SuburbId");
            suburbConfiguration.Property(e => e.BranchId)
                .HasColumnName("BranchId");


            suburbConfiguration.Property(e => e.SuburbBranchType)
                .HasColumnName("SuburbBranchType");

            suburbConfiguration.Property(e => e.IsOnforwardEnabled)
                    .HasColumnName("Onforward");
            suburbConfiguration.Property(e => e.Order)
                .HasColumnName("Order");


            suburbConfiguration.HasData(new List<SuburbBranch>()
            {
                new SuburbBranch()
                {
                    Id = 1,
                    SuburbBranchType = "D",
                    BranchId = 1,
                    IsOnforwardEnabled = true,
                    SuburbId = 1
                }
            });

        }
    }
}
