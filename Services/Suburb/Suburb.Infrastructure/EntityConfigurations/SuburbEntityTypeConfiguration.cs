﻿using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace SuburbSvc.Infrastructure.EntityConfigurations
{
    public class SuburbEntityTypeConfiguration : IEntityTypeConfiguration<Entities.Suburb>
    {
        public void Configure(EntityTypeBuilder<Entities.Suburb> suburbConfiguration)
        {
            suburbConfiguration.HasKey(e => e.Id)
                    .ForSqlServerIsClustered(false);
                

            suburbConfiguration.ToTable("Suburbs");
            suburbConfiguration.Property(e => e.Id).HasColumnName("SuburbId").ValueGeneratedOnAdd();
                
            suburbConfiguration.Property(e => e.SuburbName)
                    .HasColumnName("SuburbName")
                    .HasMaxLength(50);

            suburbConfiguration.Property(e => e.Postcode)
                .HasColumnName("Postcode")
                .HasMaxLength(50);
                   

            suburbConfiguration.Property(e => e.State)
                    .HasColumnName("State")
                .HasMaxLength(50);


            suburbConfiguration.HasData(new List<Entities.Suburb>()
                                        {
                                            new Entities.Suburb()
                                            {
                                                Id = 1,
                                                Postcode = "3162",
                                                State = "Victoria",
                                                SuburbName = "Caulfield"
                                            },
                                            new Entities.Suburb()
                                            {
                                                Id = 2,
                                                Postcode = "3141",
                                                State = "Victoria",
                                                SuburbName = "South Yarra"
                                            }

                                        }
            
                                    );

        }
    }
}
