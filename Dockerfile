FROM microsoft/dotnet:2.2-aspnetcore-runtime AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM microsoft/dotnet:2.2-sdk AS build
WORKDIR /app
COPY . ./
RUN dotnet restore Pickup.API/*.csproj
RUN dotnet publish Pickup.API/*.csproj -c Release -o out

FROM base AS final
WORKDIR /app
COPY --from=build app/Pickup.API/out ./
ENTRYPOINT ["dotnet", "Pickup.API.dll"]